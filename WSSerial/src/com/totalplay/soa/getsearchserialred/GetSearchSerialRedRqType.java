
package com.totalplay.soa.getsearchserialred;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetSearchSerialRedRqType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSearchSerialRedRqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LiginArg" type="{http://soa.totalplay.com/GetSearchSerialRed}Logintype"/>
 *         &lt;element name="Cuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSearchSerialRedRqType", propOrder = {
    "liginArg",
    "cuenta"
})
public class GetSearchSerialRedRqType {

    @XmlElement(name = "LiginArg", required = true)
    protected Logintype liginArg;
    @XmlElement(name = "Cuenta", required = true)
    protected String cuenta;

    /**
     * Gets the value of the liginArg property.
     * 
     * @return
     *     possible object is
     *     {@link Logintype }
     *     
     */
    public Logintype getLiginArg() {
        return liginArg;
    }

    /**
     * Sets the value of the liginArg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Logintype }
     *     
     */
    public void setLiginArg(Logintype value) {
        this.liginArg = value;
    }

    /**
     * Gets the value of the cuenta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Sets the value of the cuenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

}
