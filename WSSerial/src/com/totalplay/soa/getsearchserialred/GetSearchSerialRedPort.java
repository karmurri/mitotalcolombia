package com.totalplay.soa.getsearchserialred;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (11.1.1.0.0, build 130224.1947.04102)

@WebService(wsdlLocation="http://10.213.4.184:80/soa-infra/services/Wifi/GetSearchSerialRed/GetSearchSerialRed?WSDL",
  targetNamespace="http://soa.totalplay.com/GetSearchSerialRed", name="GetSearchSerialRedPort")
@XmlSeeAlso(
  { com.totalplay.soa.getsearchserialred.ObjectFactory.class })
@SOAPBinding(style=Style.DOCUMENT, parameterStyle=ParameterStyle.BARE)
public interface GetSearchSerialRedPort
{
  @WebMethod(operationName="GetSearchSerialRed", action="GetSearchSerialRed")
  @SOAPBinding(parameterStyle=ParameterStyle.BARE)
  @Action(input="GetSearchSerialRed", output="http://soa.totalplay.com/GetSearchSerialRed/GetSearchSerialRedPort/GetSearchSerialRedResponse")
  @WebResult(targetNamespace="http://soa.totalplay.com/GetSearchSerialRed",
    partName="GetSearchSerialRedRsPart", name="GetSearchSerialRedRs")
  public com.totalplay.soa.getsearchserialred.GetSearchSerialRedRsType getSearchSerialRed(@WebParam(targetNamespace="http://soa.totalplay.com/GetSearchSerialRed",
      partName="GetSearchSerialRedRqPart", name="GetSearchSerialRedRq")
    com.totalplay.soa.getsearchserialred.GetSearchSerialRedRqType GetSearchSerialRedRqPart);
}
