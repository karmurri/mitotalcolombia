
package com.totalplay.soa.getsearchserialred;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.totalplay.soa.getsearchserialred package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetSearchSerialRedRs_QNAME = new QName("http://soa.totalplay.com/GetSearchSerialRed", "GetSearchSerialRedRs");
    private final static QName _GetSearchSerialRedRq_QNAME = new QName("http://soa.totalplay.com/GetSearchSerialRed", "GetSearchSerialRedRq");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.totalplay.soa.getsearchserialred
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSearchSerialRedRqType }
     * 
     */
    public GetSearchSerialRedRqType createGetSearchSerialRedRqType() {
        return new GetSearchSerialRedRqType();
    }

    /**
     * Create an instance of {@link GetSearchSerialRedRsType }
     * 
     */
    public GetSearchSerialRedRsType createGetSearchSerialRedRsType() {
        return new GetSearchSerialRedRsType();
    }

    /**
     * Create an instance of {@link Logintype }
     * 
     */
    public Logintype createLogintype() {
        return new Logintype();
    }

    /**
     * Create an instance of {@link ResultType }
     * 
     */
    public ResultType createResultType() {
        return new ResultType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSearchSerialRedRsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.totalplay.com/GetSearchSerialRed", name = "GetSearchSerialRedRs")
    public JAXBElement<GetSearchSerialRedRsType> createGetSearchSerialRedRs(GetSearchSerialRedRsType value) {
        return new JAXBElement<GetSearchSerialRedRsType>(_GetSearchSerialRedRs_QNAME, GetSearchSerialRedRsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSearchSerialRedRqType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.totalplay.com/GetSearchSerialRed", name = "GetSearchSerialRedRq")
    public JAXBElement<GetSearchSerialRedRqType> createGetSearchSerialRedRq(GetSearchSerialRedRqType value) {
        return new JAXBElement<GetSearchSerialRedRqType>(_GetSearchSerialRedRq_QNAME, GetSearchSerialRedRqType.class, null, value);
    }

}
