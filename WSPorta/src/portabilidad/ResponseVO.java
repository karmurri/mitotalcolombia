
package portabilidad;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for responseVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="responseVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="porta" type="{http://project1/}portabilidadVO" minOccurs="0"/>
 *         &lt;element name="portaList" type="{http://project1/}portabilidadVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responseVO", propOrder = {
    "code",
    "descripcion",
    "porta",
    "portaList"
})
public class ResponseVO {

    protected String code;
    protected String descripcion;
    protected PortabilidadVO porta;
    @XmlElement(nillable = true)
    protected List<PortabilidadVO> portaList;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the descripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Gets the value of the porta property.
     * 
     * @return
     *     possible object is
     *     {@link PortabilidadVO }
     *     
     */
    public PortabilidadVO getPorta() {
        return porta;
    }

    /**
     * Sets the value of the porta property.
     * 
     * @param value
     *     allowed object is
     *     {@link PortabilidadVO }
     *     
     */
    public void setPorta(PortabilidadVO value) {
        this.porta = value;
    }

    /**
     * Gets the value of the portaList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the portaList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPortaList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortabilidadVO }
     * 
     * 
     */
    public List<PortabilidadVO> getPortaList() {
        if (portaList == null) {
            portaList = new ArrayList<PortabilidadVO>();
        }
        return this.portaList;
    }

}
