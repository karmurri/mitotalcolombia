
package portabilidad;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for portabilidadVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="portabilidadVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="COMENTARIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ESTATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ESTATUS_BRM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ESTATUS_IMS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ESTATUS_IPTV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ESTATUS_RED" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FECHA_APLICACION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FECHA_PROGRAMACION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FECHA_SOLICITUD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FOLIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ID_PORTABILIDAD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NO_CUENTA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NO_SERIE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PORTADO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PROVISIONAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RUTA_ACTA_CONSTITUTIVA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RUTA_CARTA_CANCELACION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RUTA_IDENTIFICACION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RUTA_SOLICITUD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TIPO_PERSONA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portabilidadVO", propOrder = {
    "comentario",
    "email",
    "estatus",
    "estatusbrm",
    "estatusims",
    "estatusiptv",
    "estatusred",
    "fechaaplicacion",
    "fechaprogramacion",
    "fechasolicitud",
    "folio",
    "idportabilidad",
    "nip",
    "nocuenta",
    "noserie",
    "portado",
    "provisional",
    "rutaactaconstitutiva",
    "rutacartacancelacion",
    "rutaidentificacion",
    "rutasolicitud",
    "tipopersona"
})
public class PortabilidadVO {

    @XmlElement(name = "COMENTARIO")
    protected String comentario;
    @XmlElement(name = "EMAIL")
    protected String email;
    @XmlElement(name = "ESTATUS")
    protected String estatus;
    @XmlElement(name = "ESTATUS_BRM")
    protected String estatusbrm;
    @XmlElement(name = "ESTATUS_IMS")
    protected String estatusims;
    @XmlElement(name = "ESTATUS_IPTV")
    protected String estatusiptv;
    @XmlElement(name = "ESTATUS_RED")
    protected String estatusred;
    @XmlElement(name = "FECHA_APLICACION")
    protected String fechaaplicacion;
    @XmlElement(name = "FECHA_PROGRAMACION")
    protected String fechaprogramacion;
    @XmlElement(name = "FECHA_SOLICITUD")
    protected String fechasolicitud;
    @XmlElement(name = "FOLIO")
    protected String folio;
    @XmlElement(name = "ID_PORTABILIDAD")
    protected String idportabilidad;
    @XmlElement(name = "NIP")
    protected String nip;
    @XmlElement(name = "NO_CUENTA")
    protected String nocuenta;
    @XmlElement(name = "NO_SERIE")
    protected String noserie;
    @XmlElement(name = "PORTADO")
    protected String portado;
    @XmlElement(name = "PROVISIONAL")
    protected String provisional;
    @XmlElement(name = "RUTA_ACTA_CONSTITUTIVA")
    protected String rutaactaconstitutiva;
    @XmlElement(name = "RUTA_CARTA_CANCELACION")
    protected String rutacartacancelacion;
    @XmlElement(name = "RUTA_IDENTIFICACION")
    protected String rutaidentificacion;
    @XmlElement(name = "RUTA_SOLICITUD")
    protected String rutasolicitud;
    @XmlElement(name = "TIPO_PERSONA")
    protected String tipopersona;

    /**
     * Gets the value of the comentario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMENTARIO() {
        return comentario;
    }

    /**
     * Sets the value of the comentario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMENTARIO(String value) {
        this.comentario = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMAIL() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMAIL(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the estatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESTATUS() {
        return estatus;
    }

    /**
     * Sets the value of the estatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESTATUS(String value) {
        this.estatus = value;
    }

    /**
     * Gets the value of the estatusbrm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESTATUSBRM() {
        return estatusbrm;
    }

    /**
     * Sets the value of the estatusbrm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESTATUSBRM(String value) {
        this.estatusbrm = value;
    }

    /**
     * Gets the value of the estatusims property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESTATUSIMS() {
        return estatusims;
    }

    /**
     * Sets the value of the estatusims property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESTATUSIMS(String value) {
        this.estatusims = value;
    }

    /**
     * Gets the value of the estatusiptv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESTATUSIPTV() {
        return estatusiptv;
    }

    /**
     * Sets the value of the estatusiptv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESTATUSIPTV(String value) {
        this.estatusiptv = value;
    }

    /**
     * Gets the value of the estatusred property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESTATUSRED() {
        return estatusred;
    }

    /**
     * Sets the value of the estatusred property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESTATUSRED(String value) {
        this.estatusred = value;
    }

    /**
     * Gets the value of the fechaaplicacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFECHAAPLICACION() {
        return fechaaplicacion;
    }

    /**
     * Sets the value of the fechaaplicacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFECHAAPLICACION(String value) {
        this.fechaaplicacion = value;
    }

    /**
     * Gets the value of the fechaprogramacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFECHAPROGRAMACION() {
        return fechaprogramacion;
    }

    /**
     * Sets the value of the fechaprogramacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFECHAPROGRAMACION(String value) {
        this.fechaprogramacion = value;
    }

    /**
     * Gets the value of the fechasolicitud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFECHASOLICITUD() {
        return fechasolicitud;
    }

    /**
     * Sets the value of the fechasolicitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFECHASOLICITUD(String value) {
        this.fechasolicitud = value;
    }

    /**
     * Gets the value of the folio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFOLIO() {
        return folio;
    }

    /**
     * Sets the value of the folio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFOLIO(String value) {
        this.folio = value;
    }

    /**
     * Gets the value of the idportabilidad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDPORTABILIDAD() {
        return idportabilidad;
    }

    /**
     * Sets the value of the idportabilidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDPORTABILIDAD(String value) {
        this.idportabilidad = value;
    }

    /**
     * Gets the value of the nip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIP() {
        return nip;
    }

    /**
     * Sets the value of the nip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIP(String value) {
        this.nip = value;
    }

    /**
     * Gets the value of the nocuenta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOCUENTA() {
        return nocuenta;
    }

    /**
     * Sets the value of the nocuenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOCUENTA(String value) {
        this.nocuenta = value;
    }

    /**
     * Gets the value of the noserie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOSERIE() {
        return noserie;
    }

    /**
     * Sets the value of the noserie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOSERIE(String value) {
        this.noserie = value;
    }

    /**
     * Gets the value of the portado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPORTADO() {
        return portado;
    }

    /**
     * Sets the value of the portado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPORTADO(String value) {
        this.portado = value;
    }

    /**
     * Gets the value of the provisional property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROVISIONAL() {
        return provisional;
    }

    /**
     * Sets the value of the provisional property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROVISIONAL(String value) {
        this.provisional = value;
    }

    /**
     * Gets the value of the rutaactaconstitutiva property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRUTAACTACONSTITUTIVA() {
        return rutaactaconstitutiva;
    }

    /**
     * Sets the value of the rutaactaconstitutiva property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRUTAACTACONSTITUTIVA(String value) {
        this.rutaactaconstitutiva = value;
    }

    /**
     * Gets the value of the rutacartacancelacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRUTACARTACANCELACION() {
        return rutacartacancelacion;
    }

    /**
     * Sets the value of the rutacartacancelacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRUTACARTACANCELACION(String value) {
        this.rutacartacancelacion = value;
    }

    /**
     * Gets the value of the rutaidentificacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRUTAIDENTIFICACION() {
        return rutaidentificacion;
    }

    /**
     * Sets the value of the rutaidentificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRUTAIDENTIFICACION(String value) {
        this.rutaidentificacion = value;
    }

    /**
     * Gets the value of the rutasolicitud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRUTASOLICITUD() {
        return rutasolicitud;
    }

    /**
     * Sets the value of the rutasolicitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRUTASOLICITUD(String value) {
        this.rutasolicitud = value;
    }

    /**
     * Gets the value of the tipopersona property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOPERSONA() {
        return tipopersona;
    }

    /**
     * Sets the value of the tipopersona property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOPERSONA(String value) {
        this.tipopersona = value;
    }

}
