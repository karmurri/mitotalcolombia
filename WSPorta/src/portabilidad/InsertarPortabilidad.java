
package portabilidad;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for insertarPortabilidad complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="insertarPortabilidad">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NO_CUENTA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PORTADO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PROVISIONAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FECHA_PROGRAMACION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ESTATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TIPO_PERSONA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FOLIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARCHIVO_IDENTIFICACION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARCHIVO_SOLICITUD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARCHIVO_CARTA_CAN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARCHIVO_ACTA_CONS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NOMBRES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="APELLIDOS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="COMENTARIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "insertarPortabilidad", propOrder = {
    "nocuenta",
    "portado",
    "provisional",
    "fechaprogramacion",
    "estatus",
    "nip",
    "email",
    "tipopersona",
    "folio",
    "archivoidentificacion",
    "archivosolicitud",
    "archivocartacan",
    "archivoactacons",
    "nombres",
    "apellidos",
    "comentario"
})
public class InsertarPortabilidad {

    @XmlElement(name = "NO_CUENTA")
    protected String nocuenta;
    @XmlElement(name = "PORTADO")
    protected String portado;
    @XmlElement(name = "PROVISIONAL")
    protected String provisional;
    @XmlElement(name = "FECHA_PROGRAMACION")
    protected String fechaprogramacion;
    @XmlElement(name = "ESTATUS")
    protected String estatus;
    @XmlElement(name = "NIP")
    protected String nip;
    @XmlElement(name = "EMAIL")
    protected String email;
    @XmlElement(name = "TIPO_PERSONA")
    protected String tipopersona;
    @XmlElement(name = "FOLIO")
    protected String folio;
    @XmlElement(name = "ARCHIVO_IDENTIFICACION")
    protected String archivoidentificacion;
    @XmlElement(name = "ARCHIVO_SOLICITUD")
    protected String archivosolicitud;
    @XmlElement(name = "ARCHIVO_CARTA_CAN")
    protected String archivocartacan;
    @XmlElement(name = "ARCHIVO_ACTA_CONS")
    protected String archivoactacons;
    @XmlElement(name = "NOMBRES")
    protected String nombres;
    @XmlElement(name = "APELLIDOS")
    protected String apellidos;
    @XmlElement(name = "COMENTARIO")
    protected String comentario;

    /**
     * Gets the value of the nocuenta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOCUENTA() {
        return nocuenta;
    }

    /**
     * Sets the value of the nocuenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOCUENTA(String value) {
        this.nocuenta = value;
    }

    /**
     * Gets the value of the portado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPORTADO() {
        return portado;
    }

    /**
     * Sets the value of the portado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPORTADO(String value) {
        this.portado = value;
    }

    /**
     * Gets the value of the provisional property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROVISIONAL() {
        return provisional;
    }

    /**
     * Sets the value of the provisional property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROVISIONAL(String value) {
        this.provisional = value;
    }

    /**
     * Gets the value of the fechaprogramacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFECHAPROGRAMACION() {
        return fechaprogramacion;
    }

    /**
     * Sets the value of the fechaprogramacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFECHAPROGRAMACION(String value) {
        this.fechaprogramacion = value;
    }

    /**
     * Gets the value of the estatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESTATUS() {
        return estatus;
    }

    /**
     * Sets the value of the estatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESTATUS(String value) {
        this.estatus = value;
    }

    /**
     * Gets the value of the nip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIP() {
        return nip;
    }

    /**
     * Sets the value of the nip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIP(String value) {
        this.nip = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMAIL() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMAIL(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the tipopersona property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOPERSONA() {
        return tipopersona;
    }

    /**
     * Sets the value of the tipopersona property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOPERSONA(String value) {
        this.tipopersona = value;
    }

    /**
     * Gets the value of the folio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFOLIO() {
        return folio;
    }

    /**
     * Sets the value of the folio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFOLIO(String value) {
        this.folio = value;
    }

    /**
     * Gets the value of the archivoidentificacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARCHIVOIDENTIFICACION() {
        return archivoidentificacion;
    }

    /**
     * Sets the value of the archivoidentificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARCHIVOIDENTIFICACION(String value) {
        this.archivoidentificacion = value;
    }

    /**
     * Gets the value of the archivosolicitud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARCHIVOSOLICITUD() {
        return archivosolicitud;
    }

    /**
     * Sets the value of the archivosolicitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARCHIVOSOLICITUD(String value) {
        this.archivosolicitud = value;
    }

    /**
     * Gets the value of the archivocartacan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARCHIVOCARTACAN() {
        return archivocartacan;
    }

    /**
     * Sets the value of the archivocartacan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARCHIVOCARTACAN(String value) {
        this.archivocartacan = value;
    }

    /**
     * Gets the value of the archivoactacons property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARCHIVOACTACONS() {
        return archivoactacons;
    }

    /**
     * Sets the value of the archivoactacons property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARCHIVOACTACONS(String value) {
        this.archivoactacons = value;
    }

    /**
     * Gets the value of the nombres property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOMBRES() {
        return nombres;
    }

    /**
     * Sets the value of the nombres property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOMBRES(String value) {
        this.nombres = value;
    }

    /**
     * Gets the value of the apellidos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPELLIDOS() {
        return apellidos;
    }

    /**
     * Sets the value of the apellidos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPELLIDOS(String value) {
        this.apellidos = value;
    }

    /**
     * Gets the value of the comentario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMENTARIO() {
        return comentario;
    }

    /**
     * Sets the value of the comentario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMENTARIO(String value) {
        this.comentario = value;
    }

}
