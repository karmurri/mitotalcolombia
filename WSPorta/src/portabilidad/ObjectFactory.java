
package portabilidad;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the portabilidad package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InsertarPortabilidadResponse_QNAME = new QName("http://project1/", "insertarPortabilidadResponse");
    private final static QName _ConsultaPortabilidadResponse_QNAME = new QName("http://project1/", "consultaPortabilidadResponse");
    private final static QName _ConsultaPortabilidad_QNAME = new QName("http://project1/", "consultaPortabilidad");
    private final static QName _InsertarPortabilidad_QNAME = new QName("http://project1/", "insertarPortabilidad");
    private final static QName _ConsultaFolioResponse_QNAME = new QName("http://project1/", "consultaFolioResponse");
    private final static QName _ConsultaFolio_QNAME = new QName("http://project1/", "consultaFolio");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: portabilidad
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultaFolio }
     * 
     */
    public ConsultaFolio createConsultaFolio() {
        return new ConsultaFolio();
    }

    /**
     * Create an instance of {@link ConsultaFolioResponse }
     * 
     */
    public ConsultaFolioResponse createConsultaFolioResponse() {
        return new ConsultaFolioResponse();
    }

    /**
     * Create an instance of {@link InsertarPortabilidad }
     * 
     */
    public InsertarPortabilidad createInsertarPortabilidad() {
        return new InsertarPortabilidad();
    }

    /**
     * Create an instance of {@link ConsultaPortabilidad }
     * 
     */
    public ConsultaPortabilidad createConsultaPortabilidad() {
        return new ConsultaPortabilidad();
    }

    /**
     * Create an instance of {@link InsertarPortabilidadResponse }
     * 
     */
    public InsertarPortabilidadResponse createInsertarPortabilidadResponse() {
        return new InsertarPortabilidadResponse();
    }

    /**
     * Create an instance of {@link ConsultaPortabilidadResponse }
     * 
     */
    public ConsultaPortabilidadResponse createConsultaPortabilidadResponse() {
        return new ConsultaPortabilidadResponse();
    }

    /**
     * Create an instance of {@link PortabilidadVO }
     * 
     */
    public PortabilidadVO createPortabilidadVO() {
        return new PortabilidadVO();
    }

    /**
     * Create an instance of {@link ResponseVO }
     * 
     */
    public ResponseVO createResponseVO() {
        return new ResponseVO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertarPortabilidadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://project1/", name = "insertarPortabilidadResponse")
    public JAXBElement<InsertarPortabilidadResponse> createInsertarPortabilidadResponse(InsertarPortabilidadResponse value) {
        return new JAXBElement<InsertarPortabilidadResponse>(_InsertarPortabilidadResponse_QNAME, InsertarPortabilidadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaPortabilidadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://project1/", name = "consultaPortabilidadResponse")
    public JAXBElement<ConsultaPortabilidadResponse> createConsultaPortabilidadResponse(ConsultaPortabilidadResponse value) {
        return new JAXBElement<ConsultaPortabilidadResponse>(_ConsultaPortabilidadResponse_QNAME, ConsultaPortabilidadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaPortabilidad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://project1/", name = "consultaPortabilidad")
    public JAXBElement<ConsultaPortabilidad> createConsultaPortabilidad(ConsultaPortabilidad value) {
        return new JAXBElement<ConsultaPortabilidad>(_ConsultaPortabilidad_QNAME, ConsultaPortabilidad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertarPortabilidad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://project1/", name = "insertarPortabilidad")
    public JAXBElement<InsertarPortabilidad> createInsertarPortabilidad(InsertarPortabilidad value) {
        return new JAXBElement<InsertarPortabilidad>(_InsertarPortabilidad_QNAME, InsertarPortabilidad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaFolioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://project1/", name = "consultaFolioResponse")
    public JAXBElement<ConsultaFolioResponse> createConsultaFolioResponse(ConsultaFolioResponse value) {
        return new JAXBElement<ConsultaFolioResponse>(_ConsultaFolioResponse_QNAME, ConsultaFolioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaFolio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://project1/", name = "consultaFolio")
    public JAXBElement<ConsultaFolio> createConsultaFolio(ConsultaFolio value) {
        return new JAXBElement<ConsultaFolio>(_ConsultaFolio_QNAME, ConsultaFolio.class, null, value);
    }

}
