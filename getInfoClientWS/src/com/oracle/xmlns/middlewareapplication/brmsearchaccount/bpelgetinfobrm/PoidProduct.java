
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tcgId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tcgParameter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tcgValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tcgId",
    "tcgParameter",
    "tcgValue"
})
@XmlRootElement(name = "PoidProduct")
public class PoidProduct {

    @XmlElement(required = true)
    protected String tcgId;
    @XmlElement(required = true)
    protected String tcgParameter;
    @XmlElement(required = true)
    protected String tcgValue;

    /**
     * Gets the value of the tcgId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTcgId() {
        return tcgId;
    }

    /**
     * Sets the value of the tcgId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTcgId(String value) {
        this.tcgId = value;
    }

    /**
     * Gets the value of the tcgParameter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTcgParameter() {
        return tcgParameter;
    }

    /**
     * Sets the value of the tcgParameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTcgParameter(String value) {
        this.tcgParameter = value;
    }

    /**
     * Gets the value of the tcgValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTcgValue() {
        return tcgValue;
    }

    /**
     * Sets the value of the tcgValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTcgValue(String value) {
        this.tcgValue = value;
    }

}
