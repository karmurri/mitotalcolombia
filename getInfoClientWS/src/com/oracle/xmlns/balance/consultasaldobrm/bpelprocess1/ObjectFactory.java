
package com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OutRes }
     * 
     */
    public OutRes createOutRes() {
        return new OutRes();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrResult }
     * 
     */
    public ProcessResponse.ArrResult createProcessResponseArrResult() {
        return new ProcessResponse.ArrResult();
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link ARGS }
     * 
     */
    public ARGS createARGS() {
        return new ARGS();
    }

    /**
     * Create an instance of {@link Facturas }
     * 
     */
    public Facturas createFacturas() {
        return new Facturas();
    }

    /**
     * Create an instance of {@link OutRes.ArrDatosFactura }
     * 
     */
    public OutRes.ArrDatosFactura createOutResArrDatosFactura() {
        return new OutRes.ArrDatosFactura();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrResult.ArrDatosFactura }
     * 
     */
    public ProcessResponse.ArrResult.ArrDatosFactura createProcessResponseArrResultArrDatosFactura() {
        return new ProcessResponse.ArrResult.ArrDatosFactura();
    }

}
