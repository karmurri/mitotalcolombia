
package com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaFacturacion" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="FechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="saldoVencido" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="totalPagar" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="usoNoFacturdo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accountNo",
    "fechaFacturacion",
    "fechaVencimiento",
    "saldoVencido",
    "totalPagar",
    "usoNoFacturdo"
})
@XmlRootElement(name = "Facturas")
public class Facturas {

    @XmlElement(required = true)
    protected String accountNo;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaFacturacion;
    @XmlElement(name = "FechaVencimiento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaVencimiento;
    @XmlElement(required = true)
    protected BigInteger saldoVencido;
    @XmlElement(required = true)
    protected String totalPagar;
    @XmlElement(required = true)
    protected BigInteger usoNoFacturdo;

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNo(String value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the fechaFacturacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaFacturacion() {
        return fechaFacturacion;
    }

    /**
     * Sets the value of the fechaFacturacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaFacturacion(XMLGregorianCalendar value) {
        this.fechaFacturacion = value;
    }

    /**
     * Gets the value of the fechaVencimiento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Sets the value of the fechaVencimiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaVencimiento(XMLGregorianCalendar value) {
        this.fechaVencimiento = value;
    }

    /**
     * Gets the value of the saldoVencido property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSaldoVencido() {
        return saldoVencido;
    }

    /**
     * Sets the value of the saldoVencido property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSaldoVencido(BigInteger value) {
        this.saldoVencido = value;
    }

    /**
     * Gets the value of the totalPagar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPagar() {
        return totalPagar;
    }

    /**
     * Sets the value of the totalPagar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPagar(String value) {
        this.totalPagar = value;
    }

    /**
     * Gets the value of the usoNoFacturdo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUsoNoFacturdo() {
        return usoNoFacturdo;
    }

    /**
     * Sets the value of the usoNoFacturdo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUsoNoFacturdo(BigInteger value) {
        this.usoNoFacturdo = value;
    }

}
