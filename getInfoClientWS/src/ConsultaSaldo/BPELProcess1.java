package ConsultaSaldo;

import com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1.Process;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (11.1.1.0.0, build 130224.1947.04102)

@WebService(wsdlLocation="http://soabalanceada.totalplay.com.mx/soa-infra/services/TotalPlayProcessBPEL/ConsultaSaldoBRM/bpelprocess1_client_ep?wsdl",
  targetNamespace="http://xmlns.oracle.com/Balance/ConsultaSaldoBRM/BPELProcess1",
  name="BPELProcess1")
@XmlSeeAlso(
  { com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1.ObjectFactory.class })
@SOAPBinding(style=Style.DOCUMENT, parameterStyle=ParameterStyle.BARE)
public interface BPELProcess1
{
  @WebMethod(action="process")
  @SOAPBinding(parameterStyle=ParameterStyle.BARE)
  @Action(input="process", output="http://xmlns.oracle.com/Balance/ConsultaSaldoBRM/BPELProcess1/BPELProcess1/processResponse")
  @WebResult(targetNamespace="http://xmlns.oracle.com/Balance/ConsultaSaldoBRM/BPELProcess1",
    partName="payload", name="processResponse")
  public com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1.ProcessResponse process(@WebParam(targetNamespace="http://xmlns.oracle.com/Balance/ConsultaSaldoBRM/BPELProcess1",
      partName="payload", name="process")
    Process payload);
}
