package ConsultaSaldo;

import javax.xml.ws.WebServiceRef;
import com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1.Process;
import com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1.ProcessResponse;
import com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1.ProcessResponse.ArrResult.ArrDatosFactura;
// !THE CHANGES MADE TO THIS FILE WILL BE DESTROYED IF REGENERATED!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (11.1.1.0.0, build 130224.1947.04102)

public class BPELProcess1_ptClient
{
  @WebServiceRef
  private static Bpelprocess1_client_ep bpelprocess1_client_ep;

  public static void main(String [] args)
  {
    bpelprocess1_client_ep = new Bpelprocess1_client_ep();
    BPELProcess1 bPELProcess1 = bpelprocess1_client_ep.getBPELProcess1_pt();
    
    
    // Add your code to call the desired methods.
    Process payload= new Process();
    
    payload.setACCOUNTNO("1.5127525");
    
    ProcessResponse respuestaWS= bPELProcess1.process(payload);
    System.out.println("Saldo total a pagar: "+respuestaWS.getArrResult().getArrDatosFactura().getTotalPagar());
  }
  public static String obtieneSaldoActualReal(String numCuenta){
      bpelprocess1_client_ep = new Bpelprocess1_client_ep();
      BPELProcess1 bPELProcess1 = bpelprocess1_client_ep.getBPELProcess1_pt();
      
      // Add your code to call the desired methods.
      Process payload= new Process();
      payload.setACCOUNTNO(numCuenta);
      
      ProcessResponse respuestaWS= bPELProcess1.process(payload);
     String saldoActual= respuestaWS.getArrResult().getArrDatosFactura().getTotalPagar();
      return saldoActual;
      
  }
}


