
package com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Response">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="datosCliente" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="dn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ciudad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="idPackage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="packageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="statusCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "response",
    "datosCliente"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "Response", required = true)
    protected ProcessResponse.Response response;
    protected List<ProcessResponse.DatosCliente> datosCliente;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.Response }
     *     
     */
    public ProcessResponse.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Response }
     *     
     */
    public void setResponse(ProcessResponse.Response value) {
        this.response = value;
    }

    /**
     * Gets the value of the datosCliente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the datosCliente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDatosCliente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessResponse.DatosCliente }
     * 
     * 
     */
    public List<ProcessResponse.DatosCliente> getDatosCliente() {
        if (datosCliente == null) {
            datosCliente = new ArrayList<ProcessResponse.DatosCliente>();
        }
        return this.datosCliente;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="dn" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ciudad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idPackage" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="packageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="statusCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountNo",
        "name",
        "lastName",
        "dn",
        "ciudad",
        "idPackage",
        "packageDescription",
        "statusCuenta"
    })
    public static class DatosCliente {

        @XmlElement(required = true)
        protected String accountNo;
        @XmlElement(required = true)
        protected String name;
        @XmlElement(required = true)
        protected String lastName;
        @XmlElement(required = true)
        protected String dn;
        @XmlElement(required = true)
        protected String ciudad;
        protected int idPackage;
        @XmlElement(required = true)
        protected String packageDescription;
        @XmlElement(required = true)
        protected String statusCuenta;

        /**
         * Gets the value of the accountNo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountNo() {
            return accountNo;
        }

        /**
         * Sets the value of the accountNo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountNo(String value) {
            this.accountNo = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the lastName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * Sets the value of the lastName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLastName(String value) {
            this.lastName = value;
        }

        /**
         * Gets the value of the dn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDn() {
            return dn;
        }

        /**
         * Sets the value of the dn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDn(String value) {
            this.dn = value;
        }

        /**
         * Gets the value of the ciudad property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCiudad() {
            return ciudad;
        }

        /**
         * Sets the value of the ciudad property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCiudad(String value) {
            this.ciudad = value;
        }

        /**
         * Gets the value of the idPackage property.
         * 
         */
        public int getIdPackage() {
            return idPackage;
        }

        /**
         * Sets the value of the idPackage property.
         * 
         */
        public void setIdPackage(int value) {
            this.idPackage = value;
        }

        /**
         * Gets the value of the packageDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPackageDescription() {
            return packageDescription;
        }

        /**
         * Sets the value of the packageDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPackageDescription(String value) {
            this.packageDescription = value;
        }

        /**
         * Gets the value of the statusCuenta property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatusCuenta() {
            return statusCuenta;
        }

        /**
         * Sets the value of the statusCuenta property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatusCuenta(String value) {
            this.statusCuenta = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "description"
    })
    public static class Response {

        @XmlElement(required = true)
        protected String code;
        @XmlElement(required = true)
        protected String description;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }

}
