
package com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link ArgService }
     * 
     */
    public ArgService createArgService() {
        return new ArgService();
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.DatosCliente }
     * 
     */
    public com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.DatosCliente createDatosCliente() {
        return new com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.DatosCliente();
    }

    /**
     * Create an instance of {@link ProcessResponse.Response }
     * 
     */
    public ProcessResponse.Response createProcessResponseResponse() {
        return new ProcessResponse.Response();
    }

    /**
     * Create an instance of {@link ProcessResponse.DatosCliente }
     * 
     */
    public ProcessResponse.DatosCliente createProcessResponseDatosCliente() {
        return new ProcessResponse.DatosCliente();
    }

    /**
     * Create an instance of {@link ArgService.ARGS }
     * 
     */
    public ArgService.ARGS createArgServiceARGS() {
        return new ArgService.ARGS();
    }

    /**
     * Create an instance of {@link ArgService.RESULTS }
     * 
     */
    public ArgService.RESULTS createArgServiceRESULTS() {
        return new ArgService.RESULTS();
    }

}
