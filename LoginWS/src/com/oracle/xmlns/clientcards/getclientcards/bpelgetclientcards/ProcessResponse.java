
package com.oracle.xmlns.clientcards.getclientcards.bpelgetclientcards;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Response">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClientCards">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ClientCard" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="endingCardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="idCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="expirationYear" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="expirationMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="idUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="current" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "response",
    "accountNo",
    "clientCards"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "Response", required = true)
    protected ProcessResponse.Response response;
    @XmlElement(required = true)
    protected String accountNo;
    @XmlElement(name = "ClientCards", required = true)
    protected ProcessResponse.ClientCards clientCards;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.Response }
     *     
     */
    public ProcessResponse.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Response }
     *     
     */
    public void setResponse(ProcessResponse.Response value) {
        this.response = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNo(String value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the clientCards property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.ClientCards }
     *     
     */
    public ProcessResponse.ClientCards getClientCards() {
        return clientCards;
    }

    /**
     * Sets the value of the clientCards property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.ClientCards }
     *     
     */
    public void setClientCards(ProcessResponse.ClientCards value) {
        this.clientCards = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ClientCard" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="endingCardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="idCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="expirationYear" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="expirationMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="idUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="current" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "clientCard"
    })
    public static class ClientCards {

        @XmlElement(name = "ClientCard", required = true)
        protected List<ProcessResponse.ClientCards.ClientCard> clientCard;

        /**
         * Gets the value of the clientCard property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the clientCard property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getClientCard().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProcessResponse.ClientCards.ClientCard }
         * 
         * 
         */
        public List<ProcessResponse.ClientCards.ClientCard> getClientCard() {
            if (clientCard == null) {
                clientCard = new ArrayList<ProcessResponse.ClientCards.ClientCard>();
            }
            return this.clientCard;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="endingCardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="idCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="expirationYear" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="expirationMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="idUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="current" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "bank",
            "bankId",
            "endingCardNumber",
            "idCard",
            "cardNumber",
            "expirationYear",
            "expirationMonth",
            "idUser",
            "firstName",
            "lastName",
            "status",
            "provider",
            "current"
        })
        public static class ClientCard {

            @XmlElement(required = true)
            protected String bank;
            @XmlElement(required = true)
            protected String bankId;
            @XmlElement(required = true)
            protected String endingCardNumber;
            @XmlElement(required = true)
            protected String idCard;
            @XmlElement(required = true)
            protected String cardNumber;
            @XmlElement(required = true)
            protected String expirationYear;
            @XmlElement(required = true)
            protected String expirationMonth;
            @XmlElement(required = true)
            protected String idUser;
            @XmlElement(required = true)
            protected String firstName;
            @XmlElement(required = true)
            protected String lastName;
            @XmlElement(required = true)
            protected String status;
            @XmlElement(required = true)
            protected String provider;
            @XmlElement(required = true)
            protected String current;

            /**
             * Gets the value of the bank property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBank() {
                return bank;
            }

            /**
             * Sets the value of the bank property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBank(String value) {
                this.bank = value;
            }

            /**
             * Gets the value of the bankId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBankId() {
                return bankId;
            }

            /**
             * Sets the value of the bankId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBankId(String value) {
                this.bankId = value;
            }

            /**
             * Gets the value of the endingCardNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEndingCardNumber() {
                return endingCardNumber;
            }

            /**
             * Sets the value of the endingCardNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEndingCardNumber(String value) {
                this.endingCardNumber = value;
            }

            /**
             * Gets the value of the idCard property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdCard() {
                return idCard;
            }

            /**
             * Sets the value of the idCard property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdCard(String value) {
                this.idCard = value;
            }

            /**
             * Gets the value of the cardNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCardNumber() {
                return cardNumber;
            }

            /**
             * Sets the value of the cardNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCardNumber(String value) {
                this.cardNumber = value;
            }

            /**
             * Gets the value of the expirationYear property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationYear() {
                return expirationYear;
            }

            /**
             * Sets the value of the expirationYear property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationYear(String value) {
                this.expirationYear = value;
            }

            /**
             * Gets the value of the expirationMonth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationMonth() {
                return expirationMonth;
            }

            /**
             * Sets the value of the expirationMonth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationMonth(String value) {
                this.expirationMonth = value;
            }

            /**
             * Gets the value of the idUser property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdUser() {
                return idUser;
            }

            /**
             * Sets the value of the idUser property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdUser(String value) {
                this.idUser = value;
            }

            /**
             * Gets the value of the firstName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstName() {
                return firstName;
            }

            /**
             * Sets the value of the firstName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstName(String value) {
                this.firstName = value;
            }

            /**
             * Gets the value of the lastName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastName() {
                return lastName;
            }

            /**
             * Sets the value of the lastName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastName(String value) {
                this.lastName = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

            /**
             * Gets the value of the provider property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProvider() {
                return provider;
            }

            /**
             * Sets the value of the provider property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProvider(String value) {
                this.provider = value;
            }

            /**
             * Gets the value of the current property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrent() {
                return current;
            }

            /**
             * Sets the value of the current property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrent(String value) {
                this.current = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "description"
    })
    public static class Response {

        @XmlElement(required = true)
        protected String code;
        @XmlElement(required = true)
        protected String description;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }

}
