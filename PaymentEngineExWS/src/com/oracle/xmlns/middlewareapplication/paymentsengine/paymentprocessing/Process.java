
package com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserPassIp">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ChargeModel">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bussinesId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="clientIp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ClientCard">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="idCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="monthsNoInterest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cvv2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userPassIp",
    "chargeModel"
})
@XmlRootElement(name = "process")
public class Process {

    @XmlElement(name = "UserPassIp", required = true)
    protected Process.UserPassIp userPassIp;
    @XmlElement(name = "ChargeModel", required = true)
    protected Process.ChargeModel chargeModel;

    /**
     * Gets the value of the userPassIp property.
     * 
     * @return
     *     possible object is
     *     {@link Process.UserPassIp }
     *     
     */
    public Process.UserPassIp getUserPassIp() {
        return userPassIp;
    }

    /**
     * Sets the value of the userPassIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.UserPassIp }
     *     
     */
    public void setUserPassIp(Process.UserPassIp value) {
        this.userPassIp = value;
    }

    /**
     * Gets the value of the chargeModel property.
     * 
     * @return
     *     possible object is
     *     {@link Process.ChargeModel }
     *     
     */
    public Process.ChargeModel getChargeModel() {
        return chargeModel;
    }

    /**
     * Sets the value of the chargeModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.ChargeModel }
     *     
     */
    public void setChargeModel(Process.ChargeModel value) {
        this.chargeModel = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bussinesId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="clientIp" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ClientCard">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="idCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="monthsNoInterest" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cvv2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountNo",
        "amount",
        "bussinesId",
        "clientIp",
        "paymentType",
        "clientCard"
    })
    public static class ChargeModel {

        @XmlElement(required = true)
        protected String accountNo;
        @XmlElement(required = true)
        protected String amount;
        @XmlElement(required = true)
        protected String bussinesId;
        @XmlElement(required = true)
        protected String clientIp;
        @XmlElement(required = true)
        protected String paymentType;
        @XmlElement(name = "ClientCard", required = true)
        protected Process.ChargeModel.ClientCard clientCard;

        /**
         * Gets the value of the accountNo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountNo() {
            return accountNo;
        }

        /**
         * Sets the value of the accountNo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountNo(String value) {
            this.accountNo = value;
        }

        /**
         * Gets the value of the amount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAmount(String value) {
            this.amount = value;
        }

        /**
         * Gets the value of the bussinesId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBussinesId() {
            return bussinesId;
        }

        /**
         * Sets the value of the bussinesId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBussinesId(String value) {
            this.bussinesId = value;
        }

        /**
         * Gets the value of the clientIp property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClientIp() {
            return clientIp;
        }

        /**
         * Sets the value of the clientIp property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClientIp(String value) {
            this.clientIp = value;
        }

        /**
         * Gets the value of the paymentType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaymentType() {
            return paymentType;
        }

        /**
         * Sets the value of the paymentType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaymentType(String value) {
            this.paymentType = value;
        }

        /**
         * Gets the value of the clientCard property.
         * 
         * @return
         *     possible object is
         *     {@link Process.ChargeModel.ClientCard }
         *     
         */
        public Process.ChargeModel.ClientCard getClientCard() {
            return clientCard;
        }

        /**
         * Sets the value of the clientCard property.
         * 
         * @param value
         *     allowed object is
         *     {@link Process.ChargeModel.ClientCard }
         *     
         */
        public void setClientCard(Process.ChargeModel.ClientCard value) {
            this.clientCard = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="idCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="monthsNoInterest" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cvv2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idCard",
            "monthsNoInterest",
            "cvv2"
        })
        public static class ClientCard {

            @XmlElement(required = true)
            protected String idCard;
            @XmlElement(required = true)
            protected String monthsNoInterest;
            @XmlElement(required = true)
            protected String cvv2;

            /**
             * Gets the value of the idCard property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdCard() {
                return idCard;
            }

            /**
             * Sets the value of the idCard property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdCard(String value) {
                this.idCard = value;
            }

            /**
             * Gets the value of the monthsNoInterest property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMonthsNoInterest() {
                return monthsNoInterest;
            }

            /**
             * Sets the value of the monthsNoInterest property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMonthsNoInterest(String value) {
                this.monthsNoInterest = value;
            }

            /**
             * Gets the value of the cvv2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCvv2() {
                return cvv2;
            }

            /**
             * Sets the value of the cvv2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCvv2(String value) {
                this.cvv2 = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userId",
        "password",
        "ip"
    })
    public static class UserPassIp {

        @XmlElement(required = true)
        protected String userId;
        @XmlElement(required = true)
        protected String password;
        @XmlElement(required = true)
        protected String ip;

        /**
         * Gets the value of the userId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserId() {
            return userId;
        }

        /**
         * Sets the value of the userId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserId(String value) {
            this.userId = value;
        }

        /**
         * Gets the value of the password property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassword() {
            return password;
        }

        /**
         * Sets the value of the password property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassword(String value) {
            this.password = value;
        }

        /**
         * Gets the value of the ip property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIp() {
            return ip;
        }

        /**
         * Sets the value of the ip property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIp(String value) {
            this.ip = value;
        }

    }

}
