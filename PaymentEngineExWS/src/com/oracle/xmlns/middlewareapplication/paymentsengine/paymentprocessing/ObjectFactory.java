
package com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Text_QNAME = new QName("http://xmlns.oracle.com/MiddlewareApplication/PaymentsEngine/PaymentProcessing", "text");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.BankResponse }
     * 
     */
    public com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.BankResponse createBankResponse() {
        return new com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.BankResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult }
     * 
     */
    public ProcessResponse.BrmResult createProcessResponseBrmResult() {
        return new ProcessResponse.BrmResult();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.BrmResponse }
     * 
     */
    public ProcessResponse.BrmResult.BrmResponse createProcessResponseBrmResultBrmResponse() {
        return new ProcessResponse.BrmResult.BrmResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse.BankResponse }
     * 
     */
    public ProcessResponse.BankResponse createProcessResponseBankResponse() {
        return new ProcessResponse.BankResponse();
    }

    /**
     * Create an instance of {@link Process.ChargeModel }
     * 
     */
    public Process.ChargeModel createProcessChargeModel() {
        return new Process.ChargeModel();
    }

    /**
     * Create an instance of {@link Process.UserPassIp }
     * 
     */
    public Process.UserPassIp createProcessUserPassIp() {
        return new Process.UserPassIp();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.BankResponse.Response }
     * 
     */
    public com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.BankResponse.Response createBankResponseResponse() {
        return new com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.BankResponse.Response();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.BankResponse.BankingAuthorizerResponse }
     * 
     */
    public com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.BankResponse.BankingAuthorizerResponse createBankResponseBankingAuthorizerResponse() {
        return new com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.BankResponse.BankingAuthorizerResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.Response }
     * 
     */
    public ProcessResponse.BrmResult.Response createProcessResponseBrmResultResponse() {
        return new ProcessResponse.BrmResult.Response();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.BrmResponse.Event }
     * 
     */
    public ProcessResponse.BrmResult.BrmResponse.Event createProcessResponseBrmResultBrmResponseEvent() {
        return new ProcessResponse.BrmResult.BrmResponse.Event();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.BrmResponse.Payment }
     * 
     */
    public ProcessResponse.BrmResult.BrmResponse.Payment createProcessResponseBrmResultBrmResponsePayment() {
        return new ProcessResponse.BrmResult.BrmResponse.Payment();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo }
     * 
     */
    public ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo createProcessResponseBrmResultBrmResponsePaymentFaildeInfo() {
        return new ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo();
    }

    /**
     * Create an instance of {@link ProcessResponse.BankResponse.Response }
     * 
     */
    public ProcessResponse.BankResponse.Response createProcessResponseBankResponseResponse() {
        return new ProcessResponse.BankResponse.Response();
    }

    /**
     * Create an instance of {@link ProcessResponse.BankResponse.BankingAuthorizerResponse }
     * 
     */
    public ProcessResponse.BankResponse.BankingAuthorizerResponse createProcessResponseBankResponseBankingAuthorizerResponse() {
        return new ProcessResponse.BankResponse.BankingAuthorizerResponse();
    }

    /**
     * Create an instance of {@link Process.ChargeModel.ClientCard }
     * 
     */
    public Process.ChargeModel.ClientCard createProcessChargeModelClientCard() {
        return new Process.ChargeModel.ClientCard();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/MiddlewareApplication/PaymentsEngine/PaymentProcessing", name = "text")
    public JAXBElement<String> createText(String value) {
        return new JAXBElement<String>(_Text_QNAME, String.class, null, value);
    }

}
