
package com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Text_QNAME = new QName("http://xmlns.oracle.com/FlujosMiddleware/BankingAuthorizer/ExecuteGeneralCharge", "text");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link ArrParameterValuePair }
     * 
     */
    public ArrParameterValuePair createArrParameterValuePair() {
        return new ArrParameterValuePair();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link ArrParameterValuePairReturn }
     * 
     */
    public ArrParameterValuePairReturn createArrParameterValuePairReturn() {
        return new ArrParameterValuePairReturn();
    }

    /**
     * Create an instance of {@link Process.UserPassIp }
     * 
     */
    public Process.UserPassIp createProcessUserPassIp() {
        return new Process.UserPassIp();
    }

    /**
     * Create an instance of {@link Process.Charge }
     * 
     */
    public Process.Charge createProcessCharge() {
        return new Process.Charge();
    }

    /**
     * Create an instance of {@link Process.Card }
     * 
     */
    public Process.Card createProcessCard() {
        return new Process.Card();
    }

    /**
     * Create an instance of {@link ArrParameterValuePair.ParameterValuePair }
     * 
     */
    public ArrParameterValuePair.ParameterValuePair createArrParameterValuePairParameterValuePair() {
        return new ArrParameterValuePair.ParameterValuePair();
    }

    /**
     * Create an instance of {@link ProcessResponse.Response }
     * 
     */
    public ProcessResponse.Response createProcessResponseResponse() {
        return new ProcessResponse.Response();
    }

    /**
     * Create an instance of {@link ProcessResponse.BankResponse }
     * 
     */
    public ProcessResponse.BankResponse createProcessResponseBankResponse() {
        return new ProcessResponse.BankResponse();
    }

    /**
     * Create an instance of {@link ArrParameterValuePairReturn.DicParameterValuePair }
     * 
     */
    public ArrParameterValuePairReturn.DicParameterValuePair createArrParameterValuePairReturnDicParameterValuePair() {
        return new ArrParameterValuePairReturn.DicParameterValuePair();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge.DicParameterValuePair }
     * 
     */
    public com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge.DicParameterValuePair createDicParameterValuePair() {
        return new com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge.DicParameterValuePair();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge.ParameterValuePair }
     * 
     */
    public com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge.ParameterValuePair createParameterValuePair() {
        return new com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge.ParameterValuePair();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/FlujosMiddleware/BankingAuthorizer/ExecuteGeneralCharge", name = "text")
    public JAXBElement<String> createText(String value) {
        return new JAXBElement<String>(_Text_QNAME, String.class, null, value);
    }

}
