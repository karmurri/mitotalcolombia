
package com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Response">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BankResponse">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bankResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="descriptionResponse" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="transactionControlNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="standardResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="affiliation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="affiliationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="transactionDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "response",
    "bankResponse"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "Response", required = true)
    protected ProcessResponse.Response response;
    @XmlElement(name = "BankResponse", required = true)
    protected ProcessResponse.BankResponse bankResponse;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.Response }
     *     
     */
    public ProcessResponse.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Response }
     *     
     */
    public void setResponse(ProcessResponse.Response value) {
        this.response = value;
    }

    /**
     * Gets the value of the bankResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.BankResponse }
     *     
     */
    public ProcessResponse.BankResponse getBankResponse() {
        return bankResponse;
    }

    /**
     * Sets the value of the bankResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.BankResponse }
     *     
     */
    public void setBankResponse(ProcessResponse.BankResponse value) {
        this.bankResponse = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bankResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="descriptionResponse" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="transactionControlNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="standardResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="affiliation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="affiliationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="transactionDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idResult",
        "authorizationCode",
        "bankResponseCode",
        "descriptionResponse",
        "transactionControlNumber",
        "standardResponseCode",
        "affiliation",
        "affiliationName",
        "transactionDate"
    })
    public static class BankResponse {

        @XmlElement(name = "IdResult", required = true)
        protected String idResult;
        @XmlElement(required = true)
        protected String authorizationCode;
        @XmlElement(required = true)
        protected String bankResponseCode;
        @XmlElement(required = true)
        protected String descriptionResponse;
        @XmlElement(required = true)
        protected String transactionControlNumber;
        @XmlElement(required = true)
        protected String standardResponseCode;
        @XmlElement(required = true)
        protected String affiliation;
        @XmlElement(required = true)
        protected String affiliationName;
        @XmlElement(required = true)
        protected String transactionDate;

        /**
         * Gets the value of the idResult property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdResult() {
            return idResult;
        }

        /**
         * Sets the value of the idResult property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdResult(String value) {
            this.idResult = value;
        }

        /**
         * Gets the value of the authorizationCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAuthorizationCode() {
            return authorizationCode;
        }

        /**
         * Sets the value of the authorizationCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAuthorizationCode(String value) {
            this.authorizationCode = value;
        }

        /**
         * Gets the value of the bankResponseCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBankResponseCode() {
            return bankResponseCode;
        }

        /**
         * Sets the value of the bankResponseCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBankResponseCode(String value) {
            this.bankResponseCode = value;
        }

        /**
         * Gets the value of the descriptionResponse property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescriptionResponse() {
            return descriptionResponse;
        }

        /**
         * Sets the value of the descriptionResponse property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescriptionResponse(String value) {
            this.descriptionResponse = value;
        }

        /**
         * Gets the value of the transactionControlNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionControlNumber() {
            return transactionControlNumber;
        }

        /**
         * Sets the value of the transactionControlNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionControlNumber(String value) {
            this.transactionControlNumber = value;
        }

        /**
         * Gets the value of the standardResponseCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStandardResponseCode() {
            return standardResponseCode;
        }

        /**
         * Sets the value of the standardResponseCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStandardResponseCode(String value) {
            this.standardResponseCode = value;
        }

        /**
         * Gets the value of the affiliation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAffiliation() {
            return affiliation;
        }

        /**
         * Sets the value of the affiliation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAffiliation(String value) {
            this.affiliation = value;
        }

        /**
         * Gets the value of the affiliationName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAffiliationName() {
            return affiliationName;
        }

        /**
         * Sets the value of the affiliationName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAffiliationName(String value) {
            this.affiliationName = value;
        }

        /**
         * Gets the value of the transactionDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionDate() {
            return transactionDate;
        }

        /**
         * Sets the value of the transactionDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionDate(String value) {
            this.transactionDate = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idResult",
        "code",
        "description"
    })
    public static class Response {

        @XmlElement(name = "IdResult", required = true)
        protected String idResult;
        @XmlElement(name = "Code", required = true)
        protected String code;
        @XmlElement(name = "Description", required = true)
        protected String description;

        /**
         * Gets the value of the idResult property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdResult() {
            return idResult;
        }

        /**
         * Sets the value of the idResult property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdResult(String value) {
            this.idResult = value;
        }

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }

}
