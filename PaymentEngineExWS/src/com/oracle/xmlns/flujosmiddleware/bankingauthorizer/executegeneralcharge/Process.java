
package com.oracle.xmlns.flujosmiddleware.bankingauthorizer.executegeneralcharge;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserPassIp">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Charge">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Account_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ammount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="mesesSinIntereses" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bussinesId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="usoTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="idSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="clientIp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Card">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bankIdProvider" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="colony" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cvv2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="expirationMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="expirationYear" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="municipality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="rfc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="telephone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="trackCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="trackCardDos" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="trackCardUno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userPassIp",
    "charge",
    "card"
})
@XmlRootElement(name = "process")
public class Process {

    @XmlElement(name = "UserPassIp", required = true)
    protected Process.UserPassIp userPassIp;
    @XmlElement(name = "Charge", required = true)
    protected Process.Charge charge;
    @XmlElement(name = "Card", required = true)
    protected Process.Card card;

    /**
     * Gets the value of the userPassIp property.
     * 
     * @return
     *     possible object is
     *     {@link Process.UserPassIp }
     *     
     */
    public Process.UserPassIp getUserPassIp() {
        return userPassIp;
    }

    /**
     * Sets the value of the userPassIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.UserPassIp }
     *     
     */
    public void setUserPassIp(Process.UserPassIp value) {
        this.userPassIp = value;
    }

    /**
     * Gets the value of the charge property.
     * 
     * @return
     *     possible object is
     *     {@link Process.Charge }
     *     
     */
    public Process.Charge getCharge() {
        return charge;
    }

    /**
     * Sets the value of the charge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.Charge }
     *     
     */
    public void setCharge(Process.Charge value) {
        this.charge = value;
    }

    /**
     * Gets the value of the card property.
     * 
     * @return
     *     possible object is
     *     {@link Process.Card }
     *     
     */
    public Process.Card getCard() {
        return card;
    }

    /**
     * Sets the value of the card property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.Card }
     *     
     */
    public void setCard(Process.Card value) {
        this.card = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bankIdProvider" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="colony" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cvv2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="expirationMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="expirationYear" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="municipality" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="rfc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="telephone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="trackCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="trackCardDos" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="trackCardUno" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "address",
        "bank",
        "bankIdProvider",
        "cardNumber",
        "city",
        "colony",
        "country",
        "cvv2",
        "expirationMonth",
        "expirationYear",
        "firstName",
        "lastName",
        "municipality",
        "provider",
        "rfc",
        "state",
        "status",
        "telephone",
        "trackCard",
        "trackCardDos",
        "zip",
        "trackCardUno"
    })
    public static class Card {

        @XmlElement(required = true)
        protected String address;
        @XmlElement(required = true)
        protected String bank;
        @XmlElement(required = true)
        protected String bankIdProvider;
        @XmlElement(required = true)
        protected String cardNumber;
        @XmlElement(required = true)
        protected String city;
        @XmlElement(required = true)
        protected String colony;
        @XmlElement(required = true)
        protected String country;
        @XmlElement(required = true)
        protected String cvv2;
        @XmlElement(required = true)
        protected String expirationMonth;
        @XmlElement(required = true)
        protected String expirationYear;
        @XmlElement(required = true)
        protected String firstName;
        @XmlElement(required = true)
        protected String lastName;
        @XmlElement(required = true)
        protected String municipality;
        @XmlElement(required = true)
        protected String provider;
        @XmlElement(required = true)
        protected String rfc;
        @XmlElement(required = true)
        protected String state;
        @XmlElement(required = true)
        protected String status;
        @XmlElement(required = true)
        protected String telephone;
        @XmlElement(required = true)
        protected String trackCard;
        @XmlElement(required = true)
        protected String trackCardDos;
        @XmlElement(required = true)
        protected String zip;
        @XmlElement(required = true)
        protected String trackCardUno;

        /**
         * Gets the value of the address property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAddress() {
            return address;
        }

        /**
         * Sets the value of the address property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAddress(String value) {
            this.address = value;
        }

        /**
         * Gets the value of the bank property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBank() {
            return bank;
        }

        /**
         * Sets the value of the bank property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBank(String value) {
            this.bank = value;
        }

        /**
         * Gets the value of the bankIdProvider property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBankIdProvider() {
            return bankIdProvider;
        }

        /**
         * Sets the value of the bankIdProvider property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBankIdProvider(String value) {
            this.bankIdProvider = value;
        }

        /**
         * Gets the value of the cardNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCardNumber() {
            return cardNumber;
        }

        /**
         * Sets the value of the cardNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCardNumber(String value) {
            this.cardNumber = value;
        }

        /**
         * Gets the value of the city property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Sets the value of the city property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Gets the value of the colony property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getColony() {
            return colony;
        }

        /**
         * Sets the value of the colony property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setColony(String value) {
            this.colony = value;
        }

        /**
         * Gets the value of the country property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountry() {
            return country;
        }

        /**
         * Sets the value of the country property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountry(String value) {
            this.country = value;
        }

        /**
         * Gets the value of the cvv2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCvv2() {
            return cvv2;
        }

        /**
         * Sets the value of the cvv2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCvv2(String value) {
            this.cvv2 = value;
        }

        /**
         * Gets the value of the expirationMonth property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationMonth() {
            return expirationMonth;
        }

        /**
         * Sets the value of the expirationMonth property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationMonth(String value) {
            this.expirationMonth = value;
        }

        /**
         * Gets the value of the expirationYear property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationYear() {
            return expirationYear;
        }

        /**
         * Sets the value of the expirationYear property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationYear(String value) {
            this.expirationYear = value;
        }

        /**
         * Gets the value of the firstName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * Sets the value of the firstName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFirstName(String value) {
            this.firstName = value;
        }

        /**
         * Gets the value of the lastName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * Sets the value of the lastName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLastName(String value) {
            this.lastName = value;
        }

        /**
         * Gets the value of the municipality property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMunicipality() {
            return municipality;
        }

        /**
         * Sets the value of the municipality property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMunicipality(String value) {
            this.municipality = value;
        }

        /**
         * Gets the value of the provider property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProvider() {
            return provider;
        }

        /**
         * Sets the value of the provider property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProvider(String value) {
            this.provider = value;
        }

        /**
         * Gets the value of the rfc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRfc() {
            return rfc;
        }

        /**
         * Sets the value of the rfc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRfc(String value) {
            this.rfc = value;
        }

        /**
         * Gets the value of the state property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getState() {
            return state;
        }

        /**
         * Sets the value of the state property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setState(String value) {
            this.state = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the telephone property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTelephone() {
            return telephone;
        }

        /**
         * Sets the value of the telephone property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTelephone(String value) {
            this.telephone = value;
        }

        /**
         * Gets the value of the trackCard property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrackCard() {
            return trackCard;
        }

        /**
         * Sets the value of the trackCard property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrackCard(String value) {
            this.trackCard = value;
        }

        /**
         * Gets the value of the trackCardDos property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrackCardDos() {
            return trackCardDos;
        }

        /**
         * Sets the value of the trackCardDos property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrackCardDos(String value) {
            this.trackCardDos = value;
        }

        /**
         * Gets the value of the zip property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZip() {
            return zip;
        }

        /**
         * Sets the value of the zip property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZip(String value) {
            this.zip = value;
        }

        /**
         * Gets the value of the trackCardUno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrackCardUno() {
            return trackCardUno;
        }

        /**
         * Sets the value of the trackCardUno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrackCardUno(String value) {
            this.trackCardUno = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Account_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ammount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="mesesSinIntereses" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bussinesId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="usoTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="clientIp" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountNo",
        "ammount",
        "mesesSinIntereses",
        "bussinesId",
        "usoTarjeta",
        "idSystem",
        "clientIp"
    })
    public static class Charge {

        @XmlElement(name = "Account_no", required = true)
        protected String accountNo;
        @XmlElement(required = true)
        protected String ammount;
        @XmlElement(required = true)
        protected String mesesSinIntereses;
        @XmlElement(required = true)
        protected String bussinesId;
        @XmlElement(required = true)
        protected String usoTarjeta;
        @XmlElement(required = true)
        protected String idSystem;
        @XmlElement(required = true)
        protected String clientIp;

        /**
         * Gets the value of the accountNo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountNo() {
            return accountNo;
        }

        /**
         * Sets the value of the accountNo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountNo(String value) {
            this.accountNo = value;
        }

        /**
         * Gets the value of the ammount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAmmount() {
            return ammount;
        }

        /**
         * Sets the value of the ammount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAmmount(String value) {
            this.ammount = value;
        }

        /**
         * Gets the value of the mesesSinIntereses property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMesesSinIntereses() {
            return mesesSinIntereses;
        }

        /**
         * Sets the value of the mesesSinIntereses property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMesesSinIntereses(String value) {
            this.mesesSinIntereses = value;
        }

        /**
         * Gets the value of the bussinesId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBussinesId() {
            return bussinesId;
        }

        /**
         * Sets the value of the bussinesId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBussinesId(String value) {
            this.bussinesId = value;
        }

        /**
         * Gets the value of the usoTarjeta property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUsoTarjeta() {
            return usoTarjeta;
        }

        /**
         * Sets the value of the usoTarjeta property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUsoTarjeta(String value) {
            this.usoTarjeta = value;
        }

        /**
         * Gets the value of the idSystem property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdSystem() {
            return idSystem;
        }

        /**
         * Sets the value of the idSystem property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdSystem(String value) {
            this.idSystem = value;
        }

        /**
         * Gets the value of the clientIp property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClientIp() {
            return clientIp;
        }

        /**
         * Sets the value of the clientIp property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClientIp(String value) {
            this.clientIp = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userId",
        "password",
        "ip",
        "transactionId"
    })
    public static class UserPassIp {

        @XmlElement(required = true)
        protected String userId;
        @XmlElement(required = true)
        protected String password;
        @XmlElement(required = true)
        protected String ip;
        @XmlElement(required = true)
        protected String transactionId;

        /**
         * Gets the value of the userId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserId() {
            return userId;
        }

        /**
         * Sets the value of the userId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserId(String value) {
            this.userId = value;
        }

        /**
         * Gets the value of the password property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassword() {
            return password;
        }

        /**
         * Sets the value of the password property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassword(String value) {
            this.password = value;
        }

        /**
         * Gets the value of the ip property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIp() {
            return ip;
        }

        /**
         * Sets the value of the ip property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIp(String value) {
            this.ip = value;
        }

        /**
         * Gets the value of the transactionId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionId() {
            return transactionId;
        }

        /**
         * Sets the value of the transactionId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionId(String value) {
            this.transactionId = value;
        }

    }

}
