package com.oracle.xmlns.custos.mailingservice.mailingservice;

import com.oracle.xmlns.custos.mailingservice.mailingservice.Process;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (11.1.1.0.0, build 130224.1947.04102)

@WebService(wsdlLocation="http://10.213.4.49:8001/soa-infra/services/default/MailingService/mailingservice_client_ep?WSDL",
  targetNamespace="http://xmlns.oracle.com/Custos/MailingService/MailingService",
  name="MailingService")
@XmlSeeAlso(
  { com.oracle.xmlns.custos.mailingservice.mailingservice.ObjectFactory.class })
@SOAPBinding(style=Style.DOCUMENT, parameterStyle=ParameterStyle.BARE)
public interface MailingService
{
  @WebMethod(action="process")
  @SOAPBinding(parameterStyle=ParameterStyle.BARE)
  @Action(input="process", output="http://xmlns.oracle.com/Custos/MailingService/MailingService/MailingService/processResponse")
  @WebResult(targetNamespace="http://xmlns.oracle.com/Custos/MailingService/MailingService",
    partName="payload", name="processResponse")
  public com.oracle.xmlns.custos.mailingservice.mailingservice.ProcessResponse process(@WebParam(targetNamespace="http://xmlns.oracle.com/Custos/MailingService/MailingService",
      partName="payload", name="process")
    Process payload);
}
