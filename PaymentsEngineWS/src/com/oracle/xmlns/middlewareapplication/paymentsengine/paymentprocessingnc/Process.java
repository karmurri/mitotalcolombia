
package com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserPassIp">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="NewCard">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="blSaveCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bankIdProvider" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="colony" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cvv2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="expirationMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="expirationYear" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="municipality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="rfc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="telephone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="trackCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="trackCardDos" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ChargeModel">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="monthsNoInterest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bussinesId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="clientIp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userPassIp",
    "newCard",
    "chargeModel"
})
@XmlRootElement(name = "process")
public class Process {

    @XmlElement(name = "UserPassIp", required = true)
    protected Process.UserPassIp userPassIp;
    @XmlElement(name = "NewCard", required = true)
    protected Process.NewCard newCard;
    @XmlElement(name = "ChargeModel", required = true)
    protected Process.ChargeModel chargeModel;

    /**
     * Gets the value of the userPassIp property.
     * 
     * @return
     *     possible object is
     *     {@link Process.UserPassIp }
     *     
     */
    public Process.UserPassIp getUserPassIp() {
        return userPassIp;
    }

    /**
     * Sets the value of the userPassIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.UserPassIp }
     *     
     */
    public void setUserPassIp(Process.UserPassIp value) {
        this.userPassIp = value;
    }

    /**
     * Gets the value of the newCard property.
     * 
     * @return
     *     possible object is
     *     {@link Process.NewCard }
     *     
     */
    public Process.NewCard getNewCard() {
        return newCard;
    }

    /**
     * Sets the value of the newCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.NewCard }
     *     
     */
    public void setNewCard(Process.NewCard value) {
        this.newCard = value;
    }

    /**
     * Gets the value of the chargeModel property.
     * 
     * @return
     *     possible object is
     *     {@link Process.ChargeModel }
     *     
     */
    public Process.ChargeModel getChargeModel() {
        return chargeModel;
    }

    /**
     * Sets the value of the chargeModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.ChargeModel }
     *     
     */
    public void setChargeModel(Process.ChargeModel value) {
        this.chargeModel = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="monthsNoInterest" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bussinesId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="clientIp" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountNo",
        "amount",
        "monthsNoInterest",
        "bussinesId",
        "clientIp",
        "paymentType"
    })
    public static class ChargeModel {

        @XmlElement(required = true)
        protected String accountNo;
        @XmlElement(required = true)
        protected String amount;
        @XmlElement(required = true)
        protected String monthsNoInterest;
        @XmlElement(required = true)
        protected String bussinesId;
        @XmlElement(required = true)
        protected String clientIp;
        @XmlElement(required = true)
        protected String paymentType;

        /**
         * Gets the value of the accountNo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountNo() {
            return accountNo;
        }

        /**
         * Sets the value of the accountNo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountNo(String value) {
            this.accountNo = value;
        }

        /**
         * Gets the value of the amount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAmount(String value) {
            this.amount = value;
        }

        /**
         * Gets the value of the monthsNoInterest property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMonthsNoInterest() {
            return monthsNoInterest;
        }

        /**
         * Sets the value of the monthsNoInterest property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMonthsNoInterest(String value) {
            this.monthsNoInterest = value;
        }

        /**
         * Gets the value of the bussinesId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBussinesId() {
            return bussinesId;
        }

        /**
         * Sets the value of the bussinesId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBussinesId(String value) {
            this.bussinesId = value;
        }

        /**
         * Gets the value of the clientIp property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClientIp() {
            return clientIp;
        }

        /**
         * Sets the value of the clientIp property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClientIp(String value) {
            this.clientIp = value;
        }

        /**
         * Gets the value of the paymentType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaymentType() {
            return paymentType;
        }

        /**
         * Sets the value of the paymentType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaymentType(String value) {
            this.paymentType = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="blSaveCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bankIdProvider" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="colony" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cvv2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="expirationMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="expirationYear" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="municipality" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="rfc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="telephone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="trackCard" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="trackCardDos" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "blSaveCard",
        "address",
        "bank",
        "bankIdProvider",
        "cardNumber",
        "city",
        "colony",
        "country",
        "cvv2",
        "expirationMonth",
        "expirationYear",
        "firstName",
        "lastName",
        "municipality",
        "provider",
        "rfc",
        "state",
        "status",
        "telephone",
        "trackCard",
        "trackCardDos",
        "zip"
    })
    public static class NewCard {

        @XmlElement(required = true)
        protected String blSaveCard;
        @XmlElement(required = true)
        protected String address;
        @XmlElement(required = true)
        protected String bank;
        @XmlElement(required = true)
        protected String bankIdProvider;
        @XmlElement(required = true)
        protected String cardNumber;
        @XmlElement(required = true)
        protected String city;
        @XmlElement(required = true)
        protected String colony;
        @XmlElement(required = true)
        protected String country;
        @XmlElement(required = true)
        protected String cvv2;
        @XmlElement(required = true)
        protected String expirationMonth;
        @XmlElement(required = true)
        protected String expirationYear;
        @XmlElement(required = true)
        protected String firstName;
        @XmlElement(required = true)
        protected String lastName;
        @XmlElement(required = true)
        protected String municipality;
        @XmlElement(required = true)
        protected String provider;
        @XmlElement(required = true)
        protected String rfc;
        @XmlElement(required = true)
        protected String state;
        @XmlElement(required = true)
        protected String status;
        @XmlElement(required = true)
        protected String telephone;
        @XmlElement(required = true)
        protected String trackCard;
        @XmlElement(required = true)
        protected String trackCardDos;
        @XmlElement(required = true)
        protected String zip;

        /**
         * Gets the value of the blSaveCard property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBlSaveCard() {
            return blSaveCard;
        }

        /**
         * Sets the value of the blSaveCard property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBlSaveCard(String value) {
            this.blSaveCard = value;
        }

        /**
         * Gets the value of the address property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAddress() {
            return address;
        }

        /**
         * Sets the value of the address property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAddress(String value) {
            this.address = value;
        }

        /**
         * Gets the value of the bank property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBank() {
            return bank;
        }

        /**
         * Sets the value of the bank property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBank(String value) {
            this.bank = value;
        }

        /**
         * Gets the value of the bankIdProvider property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBankIdProvider() {
            return bankIdProvider;
        }

        /**
         * Sets the value of the bankIdProvider property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBankIdProvider(String value) {
            this.bankIdProvider = value;
        }

        /**
         * Gets the value of the cardNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCardNumber() {
            return cardNumber;
        }

        /**
         * Sets the value of the cardNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCardNumber(String value) {
            this.cardNumber = value;
        }

        /**
         * Gets the value of the city property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Sets the value of the city property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Gets the value of the colony property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getColony() {
            return colony;
        }

        /**
         * Sets the value of the colony property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setColony(String value) {
            this.colony = value;
        }

        /**
         * Gets the value of the country property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountry() {
            return country;
        }

        /**
         * Sets the value of the country property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountry(String value) {
            this.country = value;
        }

        /**
         * Gets the value of the cvv2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCvv2() {
            return cvv2;
        }

        /**
         * Sets the value of the cvv2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCvv2(String value) {
            this.cvv2 = value;
        }

        /**
         * Gets the value of the expirationMonth property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationMonth() {
            return expirationMonth;
        }

        /**
         * Sets the value of the expirationMonth property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationMonth(String value) {
            this.expirationMonth = value;
        }

        /**
         * Gets the value of the expirationYear property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationYear() {
            return expirationYear;
        }

        /**
         * Sets the value of the expirationYear property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationYear(String value) {
            this.expirationYear = value;
        }

        /**
         * Gets the value of the firstName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * Sets the value of the firstName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFirstName(String value) {
            this.firstName = value;
        }

        /**
         * Gets the value of the lastName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * Sets the value of the lastName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLastName(String value) {
            this.lastName = value;
        }

        /**
         * Gets the value of the municipality property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMunicipality() {
            return municipality;
        }

        /**
         * Sets the value of the municipality property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMunicipality(String value) {
            this.municipality = value;
        }

        /**
         * Gets the value of the provider property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProvider() {
            return provider;
        }

        /**
         * Sets the value of the provider property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProvider(String value) {
            this.provider = value;
        }

        /**
         * Gets the value of the rfc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRfc() {
            return rfc;
        }

        /**
         * Sets the value of the rfc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRfc(String value) {
            this.rfc = value;
        }

        /**
         * Gets the value of the state property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getState() {
            return state;
        }

        /**
         * Sets the value of the state property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setState(String value) {
            this.state = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the telephone property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTelephone() {
            return telephone;
        }

        /**
         * Sets the value of the telephone property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTelephone(String value) {
            this.telephone = value;
        }

        /**
         * Gets the value of the trackCard property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrackCard() {
            return trackCard;
        }

        /**
         * Sets the value of the trackCard property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrackCard(String value) {
            this.trackCard = value;
        }

        /**
         * Gets the value of the trackCardDos property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrackCardDos() {
            return trackCardDos;
        }

        /**
         * Sets the value of the trackCardDos property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrackCardDos(String value) {
            this.trackCardDos = value;
        }

        /**
         * Gets the value of the zip property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZip() {
            return zip;
        }

        /**
         * Sets the value of the zip property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZip(String value) {
            this.zip = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userId",
        "password",
        "ip"
    })
    public static class UserPassIp {

        @XmlElement(required = true)
        protected String userId;
        @XmlElement(required = true)
        protected String password;
        @XmlElement(required = true)
        protected String ip;

        /**
         * Gets the value of the userId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserId() {
            return userId;
        }

        /**
         * Sets the value of the userId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserId(String value) {
            this.userId = value;
        }

        /**
         * Gets the value of the password property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassword() {
            return password;
        }

        /**
         * Sets the value of the password property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassword(String value) {
            this.password = value;
        }

        /**
         * Gets the value of the ip property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIp() {
            return ip;
        }

        /**
         * Sets the value of the ip property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIp(String value) {
            this.ip = value;
        }

    }

}
