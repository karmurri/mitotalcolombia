
package com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Text_QNAME = new QName("http://xmlns.oracle.com/MiddlewareApplication/PaymentsEngine/PaymentProcessingNC", "text");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.BankResponse }
     * 
     */
    public com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.BankResponse createBankResponse() {
        return new com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.BankResponse();
    }

    /**
     * Create an instance of {@link ArrParameterValuePair }
     * 
     */
    public ArrParameterValuePair createArrParameterValuePair() {
        return new ArrParameterValuePair();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link ArrParameterValuePairReturn }
     * 
     */
    public ArrParameterValuePairReturn createArrParameterValuePairReturn() {
        return new ArrParameterValuePairReturn();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult }
     * 
     */
    public ProcessResponse.BrmResult createProcessResponseBrmResult() {
        return new ProcessResponse.BrmResult();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.BrmResponse }
     * 
     */
    public ProcessResponse.BrmResult.BrmResponse createProcessResponseBrmResultBrmResponse() {
        return new ProcessResponse.BrmResult.BrmResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse.SaveCardResponse }
     * 
     */
    public ProcessResponse.SaveCardResponse createProcessResponseSaveCardResponse() {
        return new ProcessResponse.SaveCardResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse.BankResponse }
     * 
     */
    public ProcessResponse.BankResponse createProcessResponseBankResponse() {
        return new ProcessResponse.BankResponse();
    }

    /**
     * Create an instance of {@link Process.UserPassIp }
     * 
     */
    public Process.UserPassIp createProcessUserPassIp() {
        return new Process.UserPassIp();
    }

    /**
     * Create an instance of {@link Process.NewCard }
     * 
     */
    public Process.NewCard createProcessNewCard() {
        return new Process.NewCard();
    }

    /**
     * Create an instance of {@link Process.ChargeModel }
     * 
     */
    public Process.ChargeModel createProcessChargeModel() {
        return new Process.ChargeModel();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.BankResponse.Response }
     * 
     */
    public com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.BankResponse.Response createBankResponseResponse() {
        return new com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.BankResponse.Response();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.BankResponse.BankingAuthorizerResponse }
     * 
     */
    public com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.BankResponse.BankingAuthorizerResponse createBankResponseBankingAuthorizerResponse() {
        return new com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.BankResponse.BankingAuthorizerResponse();
    }

    /**
     * Create an instance of {@link ArrParameterValuePair.ParameterValuePair }
     * 
     */
    public ArrParameterValuePair.ParameterValuePair createArrParameterValuePairParameterValuePair() {
        return new ArrParameterValuePair.ParameterValuePair();
    }

    /**
     * Create an instance of {@link ArrParameterValuePairReturn.DicParameterValuePair }
     * 
     */
    public ArrParameterValuePairReturn.DicParameterValuePair createArrParameterValuePairReturnDicParameterValuePair() {
        return new ArrParameterValuePairReturn.DicParameterValuePair();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.DicParameterValuePair }
     * 
     */
    public com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.DicParameterValuePair createDicParameterValuePair() {
        return new com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.DicParameterValuePair();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ParameterValuePair }
     * 
     */
    public com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ParameterValuePair createParameterValuePair() {
        return new com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ParameterValuePair();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.Response }
     * 
     */
    public ProcessResponse.BrmResult.Response createProcessResponseBrmResultResponse() {
        return new ProcessResponse.BrmResult.Response();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.BrmResponse.Event }
     * 
     */
    public ProcessResponse.BrmResult.BrmResponse.Event createProcessResponseBrmResultBrmResponseEvent() {
        return new ProcessResponse.BrmResult.BrmResponse.Event();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.BrmResponse.Payment }
     * 
     */
    public ProcessResponse.BrmResult.BrmResponse.Payment createProcessResponseBrmResultBrmResponsePayment() {
        return new ProcessResponse.BrmResult.BrmResponse.Payment();
    }

    /**
     * Create an instance of {@link ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo }
     * 
     */
    public ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo createProcessResponseBrmResultBrmResponsePaymentFaildeInfo() {
        return new ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo();
    }

    /**
     * Create an instance of {@link ProcessResponse.SaveCardResponse.Response }
     * 
     */
    public ProcessResponse.SaveCardResponse.Response createProcessResponseSaveCardResponseResponse() {
        return new ProcessResponse.SaveCardResponse.Response();
    }

    /**
     * Create an instance of {@link ProcessResponse.SaveCardResponse.CardsResponsitoryResponse }
     * 
     */
    public ProcessResponse.SaveCardResponse.CardsResponsitoryResponse createProcessResponseSaveCardResponseCardsResponsitoryResponse() {
        return new ProcessResponse.SaveCardResponse.CardsResponsitoryResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse.BankResponse.Response }
     * 
     */
    public ProcessResponse.BankResponse.Response createProcessResponseBankResponseResponse() {
        return new ProcessResponse.BankResponse.Response();
    }

    /**
     * Create an instance of {@link ProcessResponse.BankResponse.BankingAuthorizerResponse }
     * 
     */
    public ProcessResponse.BankResponse.BankingAuthorizerResponse createProcessResponseBankResponseBankingAuthorizerResponse() {
        return new ProcessResponse.BankResponse.BankingAuthorizerResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/MiddlewareApplication/PaymentsEngine/PaymentProcessingNC", name = "text")
    public JAXBElement<String> createText(String value) {
        return new JAXBElement<String>(_Text_QNAME, String.class, null, value);
    }

}
