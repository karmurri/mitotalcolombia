
package com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankResponse">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Response">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BankingAuthorizerResponse">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="affiliation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="affiliationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bankResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="descriptionResponse" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="standardResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="transactionControlNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="transactionDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SaveCardResponse">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Response">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CardsResponsitoryResponse">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="airTimeSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="crmCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="crmDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Ticket" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BrmResult">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Response">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BrmResponse">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Poid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Account_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Event">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Event_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Created_T" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Effective_T" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Event_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Item_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Item_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Payment">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Account_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Ach" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Bill_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Sub_trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Payment_failde_info">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Payment_trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bankResponse",
    "saveCardResponse",
    "brmResult"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "BankResponse", required = true)
    protected ProcessResponse.BankResponse bankResponse;
    @XmlElement(name = "SaveCardResponse", required = true)
    protected ProcessResponse.SaveCardResponse saveCardResponse;
    @XmlElement(name = "BrmResult", required = true)
    protected ProcessResponse.BrmResult brmResult;

    /**
     * Gets the value of the bankResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.BankResponse }
     *     
     */
    public ProcessResponse.BankResponse getBankResponse() {
        return bankResponse;
    }

    /**
     * Sets the value of the bankResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.BankResponse }
     *     
     */
    public void setBankResponse(ProcessResponse.BankResponse value) {
        this.bankResponse = value;
    }

    /**
     * Gets the value of the saveCardResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.SaveCardResponse }
     *     
     */
    public ProcessResponse.SaveCardResponse getSaveCardResponse() {
        return saveCardResponse;
    }

    /**
     * Sets the value of the saveCardResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.SaveCardResponse }
     *     
     */
    public void setSaveCardResponse(ProcessResponse.SaveCardResponse value) {
        this.saveCardResponse = value;
    }

    /**
     * Gets the value of the brmResult property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.BrmResult }
     *     
     */
    public ProcessResponse.BrmResult getBrmResult() {
        return brmResult;
    }

    /**
     * Sets the value of the brmResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.BrmResult }
     *     
     */
    public void setBrmResult(ProcessResponse.BrmResult value) {
        this.brmResult = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Response">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BankingAuthorizerResponse">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="affiliation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="affiliationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bankResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="descriptionResponse" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="standardResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="transactionControlNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="transactionDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txId",
        "response",
        "bankingAuthorizerResponse"
    })
    public static class BankResponse {

        @XmlElement(name = "TxId", required = true)
        protected String txId;
        @XmlElement(name = "Response", required = true)
        protected ProcessResponse.BankResponse.Response response;
        @XmlElement(name = "BankingAuthorizerResponse", required = true)
        protected ProcessResponse.BankResponse.BankingAuthorizerResponse bankingAuthorizerResponse;

        /**
         * Gets the value of the txId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTxId() {
            return txId;
        }

        /**
         * Sets the value of the txId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTxId(String value) {
            this.txId = value;
        }

        /**
         * Gets the value of the response property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.BankResponse.Response }
         *     
         */
        public ProcessResponse.BankResponse.Response getResponse() {
            return response;
        }

        /**
         * Sets the value of the response property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.BankResponse.Response }
         *     
         */
        public void setResponse(ProcessResponse.BankResponse.Response value) {
            this.response = value;
        }

        /**
         * Gets the value of the bankingAuthorizerResponse property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.BankResponse.BankingAuthorizerResponse }
         *     
         */
        public ProcessResponse.BankResponse.BankingAuthorizerResponse getBankingAuthorizerResponse() {
            return bankingAuthorizerResponse;
        }

        /**
         * Sets the value of the bankingAuthorizerResponse property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.BankResponse.BankingAuthorizerResponse }
         *     
         */
        public void setBankingAuthorizerResponse(ProcessResponse.BankResponse.BankingAuthorizerResponse value) {
            this.bankingAuthorizerResponse = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="affiliation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="affiliationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bankResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="descriptionResponse" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="standardResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="transactionControlNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="transactionDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "affiliation",
            "affiliationName",
            "authorizationCode",
            "bankResponseCode",
            "descriptionResponse",
            "standardResponseCode",
            "transactionControlNumber",
            "transactionDate"
        })
        public static class BankingAuthorizerResponse {

            @XmlElement(required = true)
            protected String affiliation;
            @XmlElement(required = true)
            protected String affiliationName;
            @XmlElement(required = true)
            protected String authorizationCode;
            @XmlElement(required = true)
            protected String bankResponseCode;
            @XmlElement(required = true)
            protected String descriptionResponse;
            @XmlElement(required = true)
            protected String standardResponseCode;
            @XmlElement(required = true)
            protected String transactionControlNumber;
            @XmlElement(required = true)
            protected String transactionDate;

            /**
             * Gets the value of the affiliation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAffiliation() {
                return affiliation;
            }

            /**
             * Sets the value of the affiliation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAffiliation(String value) {
                this.affiliation = value;
            }

            /**
             * Gets the value of the affiliationName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAffiliationName() {
                return affiliationName;
            }

            /**
             * Sets the value of the affiliationName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAffiliationName(String value) {
                this.affiliationName = value;
            }

            /**
             * Gets the value of the authorizationCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAuthorizationCode() {
                return authorizationCode;
            }

            /**
             * Sets the value of the authorizationCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAuthorizationCode(String value) {
                this.authorizationCode = value;
            }

            /**
             * Gets the value of the bankResponseCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBankResponseCode() {
                return bankResponseCode;
            }

            /**
             * Sets the value of the bankResponseCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBankResponseCode(String value) {
                this.bankResponseCode = value;
            }

            /**
             * Gets the value of the descriptionResponse property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescriptionResponse() {
                return descriptionResponse;
            }

            /**
             * Sets the value of the descriptionResponse property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescriptionResponse(String value) {
                this.descriptionResponse = value;
            }

            /**
             * Gets the value of the standardResponseCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStandardResponseCode() {
                return standardResponseCode;
            }

            /**
             * Sets the value of the standardResponseCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStandardResponseCode(String value) {
                this.standardResponseCode = value;
            }

            /**
             * Gets the value of the transactionControlNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTransactionControlNumber() {
                return transactionControlNumber;
            }

            /**
             * Sets the value of the transactionControlNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTransactionControlNumber(String value) {
                this.transactionControlNumber = value;
            }

            /**
             * Gets the value of the transactionDate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTransactionDate() {
                return transactionDate;
            }

            /**
             * Sets the value of the transactionDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTransactionDate(String value) {
                this.transactionDate = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "code",
            "description"
        })
        public static class Response {

            @XmlElement(required = true)
            protected String code;
            @XmlElement(required = true)
            protected String description;

            /**
             * Gets the value of the code property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Sets the value of the code property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Response">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BrmResponse">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Poid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Account_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Event">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Event_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Created_T" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Effective_T" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Event_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Item_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Item_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Payment">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Account_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Ach" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Bill_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Sub_trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Payment_failde_info">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Payment_trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "response",
        "brmResponse"
    })
    public static class BrmResult {

        @XmlElement(name = "Response", required = true)
        protected ProcessResponse.BrmResult.Response response;
        @XmlElement(name = "BrmResponse", required = true)
        protected ProcessResponse.BrmResult.BrmResponse brmResponse;

        /**
         * Gets the value of the response property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.BrmResult.Response }
         *     
         */
        public ProcessResponse.BrmResult.Response getResponse() {
            return response;
        }

        /**
         * Sets the value of the response property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.BrmResult.Response }
         *     
         */
        public void setResponse(ProcessResponse.BrmResult.Response value) {
            this.response = value;
        }

        /**
         * Gets the value of the brmResponse property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.BrmResult.BrmResponse }
         *     
         */
        public ProcessResponse.BrmResult.BrmResponse getBrmResponse() {
            return brmResponse;
        }

        /**
         * Sets the value of the brmResponse property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.BrmResult.BrmResponse }
         *     
         */
        public void setBrmResponse(ProcessResponse.BrmResult.BrmResponse value) {
            this.brmResponse = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Poid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Account_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Event">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Event_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Created_T" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Effective_T" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Event_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Item_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Item_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Payment">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Account_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Ach" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Bill_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Sub_trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Payment_failde_info">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Payment_trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "poid",
            "code",
            "description",
            "accountObj",
            "event",
            "createdT",
            "effectiveT",
            "eventNo",
            "itemNo",
            "itemObj",
            "payment",
            "paymentFaildeInfo"
        })
        public static class BrmResponse {

            @XmlElement(name = "Poid", required = true)
            protected String poid;
            @XmlElement(name = "Code", required = true)
            protected String code;
            @XmlElement(name = "Description", required = true)
            protected String description;
            @XmlElement(name = "Account_obj", required = true)
            protected String accountObj;
            @XmlElement(name = "Event", required = true)
            protected ProcessResponse.BrmResult.BrmResponse.Event event;
            @XmlElement(name = "Created_T", required = true)
            protected String createdT;
            @XmlElement(name = "Effective_T", required = true)
            protected String effectiveT;
            @XmlElement(name = "Event_No", required = true)
            protected String eventNo;
            @XmlElement(name = "Item_No", required = true)
            protected String itemNo;
            @XmlElement(name = "Item_obj", required = true)
            protected String itemObj;
            @XmlElement(name = "Payment", required = true)
            protected ProcessResponse.BrmResult.BrmResponse.Payment payment;
            @XmlElement(name = "Payment_failde_info", required = true)
            protected ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo paymentFaildeInfo;

            /**
             * Gets the value of the poid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPoid() {
                return poid;
            }

            /**
             * Sets the value of the poid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPoid(String value) {
                this.poid = value;
            }

            /**
             * Gets the value of the code property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Sets the value of the code property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Gets the value of the accountObj property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAccountObj() {
                return accountObj;
            }

            /**
             * Sets the value of the accountObj property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAccountObj(String value) {
                this.accountObj = value;
            }

            /**
             * Gets the value of the event property.
             * 
             * @return
             *     possible object is
             *     {@link ProcessResponse.BrmResult.BrmResponse.Event }
             *     
             */
            public ProcessResponse.BrmResult.BrmResponse.Event getEvent() {
                return event;
            }

            /**
             * Sets the value of the event property.
             * 
             * @param value
             *     allowed object is
             *     {@link ProcessResponse.BrmResult.BrmResponse.Event }
             *     
             */
            public void setEvent(ProcessResponse.BrmResult.BrmResponse.Event value) {
                this.event = value;
            }

            /**
             * Gets the value of the createdT property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreatedT() {
                return createdT;
            }

            /**
             * Sets the value of the createdT property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreatedT(String value) {
                this.createdT = value;
            }

            /**
             * Gets the value of the effectiveT property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveT() {
                return effectiveT;
            }

            /**
             * Sets the value of the effectiveT property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveT(String value) {
                this.effectiveT = value;
            }

            /**
             * Gets the value of the eventNo property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEventNo() {
                return eventNo;
            }

            /**
             * Sets the value of the eventNo property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEventNo(String value) {
                this.eventNo = value;
            }

            /**
             * Gets the value of the itemNo property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getItemNo() {
                return itemNo;
            }

            /**
             * Sets the value of the itemNo property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setItemNo(String value) {
                this.itemNo = value;
            }

            /**
             * Gets the value of the itemObj property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getItemObj() {
                return itemObj;
            }

            /**
             * Sets the value of the itemObj property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setItemObj(String value) {
                this.itemObj = value;
            }

            /**
             * Gets the value of the payment property.
             * 
             * @return
             *     possible object is
             *     {@link ProcessResponse.BrmResult.BrmResponse.Payment }
             *     
             */
            public ProcessResponse.BrmResult.BrmResponse.Payment getPayment() {
                return payment;
            }

            /**
             * Sets the value of the payment property.
             * 
             * @param value
             *     allowed object is
             *     {@link ProcessResponse.BrmResult.BrmResponse.Payment }
             *     
             */
            public void setPayment(ProcessResponse.BrmResult.BrmResponse.Payment value) {
                this.payment = value;
            }

            /**
             * Gets the value of the paymentFaildeInfo property.
             * 
             * @return
             *     possible object is
             *     {@link ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo }
             *     
             */
            public ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo getPaymentFaildeInfo() {
                return paymentFaildeInfo;
            }

            /**
             * Sets the value of the paymentFaildeInfo property.
             * 
             * @param value
             *     allowed object is
             *     {@link ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo }
             *     
             */
            public void setPaymentFaildeInfo(ProcessResponse.BrmResult.BrmResponse.PaymentFaildeInfo value) {
                this.paymentFaildeInfo = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Event_obj" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "eventObj",
                "result"
            })
            public static class Event {

                @XmlElement(name = "Event_obj", required = true)
                protected String eventObj;
                @XmlElement(name = "Result", required = true)
                protected String result;

                /**
                 * Gets the value of the eventObj property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEventObj() {
                    return eventObj;
                }

                /**
                 * Sets the value of the eventObj property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEventObj(String value) {
                    this.eventObj = value;
                }

                /**
                 * Gets the value of the result property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResult() {
                    return result;
                }

                /**
                 * Sets the value of the result property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResult(String value) {
                    this.result = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Account_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Ach" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Bill_No" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Sub_trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accountNo",
                "ach",
                "amount",
                "billNo",
                "status",
                "transId",
                "subTransId"
            })
            public static class Payment {

                @XmlElement(name = "Account_No", required = true)
                protected String accountNo;
                @XmlElement(name = "Ach", required = true)
                protected String ach;
                @XmlElement(name = "Amount", required = true)
                protected String amount;
                @XmlElement(name = "Bill_No", required = true)
                protected String billNo;
                @XmlElement(name = "Status", required = true)
                protected String status;
                @XmlElement(name = "Trans_id", required = true)
                protected String transId;
                @XmlElement(name = "Sub_trans_id", required = true)
                protected String subTransId;

                /**
                 * Gets the value of the accountNo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAccountNo() {
                    return accountNo;
                }

                /**
                 * Sets the value of the accountNo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAccountNo(String value) {
                    this.accountNo = value;
                }

                /**
                 * Gets the value of the ach property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAch() {
                    return ach;
                }

                /**
                 * Sets the value of the ach property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAch(String value) {
                    this.ach = value;
                }

                /**
                 * Gets the value of the amount property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount() {
                    return amount;
                }

                /**
                 * Sets the value of the amount property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount(String value) {
                    this.amount = value;
                }

                /**
                 * Gets the value of the billNo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBillNo() {
                    return billNo;
                }

                /**
                 * Sets the value of the billNo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBillNo(String value) {
                    this.billNo = value;
                }

                /**
                 * Gets the value of the status property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStatus() {
                    return status;
                }

                /**
                 * Sets the value of the status property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStatus(String value) {
                    this.status = value;
                }

                /**
                 * Gets the value of the transId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTransId() {
                    return transId;
                }

                /**
                 * Sets the value of the transId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTransId(String value) {
                    this.transId = value;
                }

                /**
                 * Gets the value of the subTransId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSubTransId() {
                    return subTransId;
                }

                /**
                 * Sets the value of the subTransId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSubTransId(String value) {
                    this.subTransId = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Payment_trans_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "paymentTransId"
            })
            public static class PaymentFaildeInfo {

                @XmlElement(name = "Payment_trans_id", required = true)
                protected String paymentTransId;

                /**
                 * Gets the value of the paymentTransId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPaymentTransId() {
                    return paymentTransId;
                }

                /**
                 * Sets the value of the paymentTransId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPaymentTransId(String value) {
                    this.paymentTransId = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idResult",
            "code",
            "description"
        })
        public static class Response {

            @XmlElement(name = "IdResult", required = true)
            protected String idResult;
            @XmlElement(name = "Code", required = true)
            protected String code;
            @XmlElement(name = "Description", required = true)
            protected String description;

            /**
             * Gets the value of the idResult property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdResult() {
                return idResult;
            }

            /**
             * Sets the value of the idResult property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdResult(String value) {
                this.idResult = value;
            }

            /**
             * Gets the value of the code property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Sets the value of the code property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Response">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CardsResponsitoryResponse">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="airTimeSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="crmCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="crmDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Ticket" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "response",
        "cardsResponsitoryResponse"
    })
    public static class SaveCardResponse {

        @XmlElement(name = "Response", required = true)
        protected ProcessResponse.SaveCardResponse.Response response;
        @XmlElement(name = "CardsResponsitoryResponse", required = true)
        protected ProcessResponse.SaveCardResponse.CardsResponsitoryResponse cardsResponsitoryResponse;

        /**
         * Gets the value of the response property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.SaveCardResponse.Response }
         *     
         */
        public ProcessResponse.SaveCardResponse.Response getResponse() {
            return response;
        }

        /**
         * Sets the value of the response property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.SaveCardResponse.Response }
         *     
         */
        public void setResponse(ProcessResponse.SaveCardResponse.Response value) {
            this.response = value;
        }

        /**
         * Gets the value of the cardsResponsitoryResponse property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.SaveCardResponse.CardsResponsitoryResponse }
         *     
         */
        public ProcessResponse.SaveCardResponse.CardsResponsitoryResponse getCardsResponsitoryResponse() {
            return cardsResponsitoryResponse;
        }

        /**
         * Sets the value of the cardsResponsitoryResponse property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.SaveCardResponse.CardsResponsitoryResponse }
         *     
         */
        public void setCardsResponsitoryResponse(ProcessResponse.SaveCardResponse.CardsResponsitoryResponse value) {
            this.cardsResponsitoryResponse = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="airTimeSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="crmCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="crmDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Ticket" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "airTimeSequence",
            "crmCode",
            "crmDescription",
            "ticket"
        })
        public static class CardsResponsitoryResponse {

            @XmlElement(required = true)
            protected String airTimeSequence;
            @XmlElement(required = true)
            protected String crmCode;
            @XmlElement(required = true)
            protected String crmDescription;
            @XmlElement(name = "Ticket", required = true)
            protected String ticket;

            /**
             * Gets the value of the airTimeSequence property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAirTimeSequence() {
                return airTimeSequence;
            }

            /**
             * Sets the value of the airTimeSequence property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAirTimeSequence(String value) {
                this.airTimeSequence = value;
            }

            /**
             * Gets the value of the crmCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCrmCode() {
                return crmCode;
            }

            /**
             * Sets the value of the crmCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCrmCode(String value) {
                this.crmCode = value;
            }

            /**
             * Gets the value of the crmDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCrmDescription() {
                return crmDescription;
            }

            /**
             * Sets the value of the crmDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCrmDescription(String value) {
                this.crmDescription = value;
            }

            /**
             * Gets the value of the ticket property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTicket() {
                return ticket;
            }

            /**
             * Sets the value of the ticket property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTicket(String value) {
                this.ticket = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "code",
            "result",
            "description"
        })
        public static class Response {

            @XmlElement(required = true)
            protected String code;
            @XmlElement(name = "Result", required = true)
            protected String result;
            @XmlElement(required = true)
            protected String description;

            /**
             * Gets the value of the code property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Sets the value of the code property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Gets the value of the result property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResult() {
                return result;
            }

            /**
             * Sets the value of the result property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResult(String value) {
                this.result = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

        }

    }

}
