
package com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="ResultDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "result"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(required = true)
    protected ProcessResponse.Result result;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.Result }
     *     
     */
    public ProcessResponse.Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Result }
     *     
     */
    public void setResult(ProcessResponse.Result value) {
        this.result = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="ResultDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idResult",
        "result",
        "resultDescription"
    })
    public static class Result {

        @XmlElement(name = "IdResult")
        protected int idResult;
        @XmlElement(name = "Result")
        protected int result;
        @XmlElement(name = "ResultDescription", required = true)
        protected String resultDescription;

        /**
         * Gets the value of the idResult property.
         * 
         */
        public int getIdResult() {
            return idResult;
        }

        /**
         * Sets the value of the idResult property.
         * 
         */
        public void setIdResult(int value) {
            this.idResult = value;
        }

        /**
         * Gets the value of the result property.
         * 
         */
        public int getResult() {
            return result;
        }

        /**
         * Sets the value of the result property.
         * 
         */
        public void setResult(int value) {
            this.result = value;
        }

        /**
         * Gets the value of the resultDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResultDescription() {
            return resultDescription;
        }

        /**
         * Sets the value of the resultDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResultDescription(String value) {
            this.resultDescription = value;
        }

    }

}
