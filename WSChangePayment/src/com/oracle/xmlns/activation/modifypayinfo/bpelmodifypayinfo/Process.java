
package com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arrLogin">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="User" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Pay_Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ArrInheritedInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="arrInvInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DeliveryPrefer" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="InvInstr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EmailAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="arrCCInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SecurityId" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="DebitExp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="arrDDInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Bankno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ACH" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "arrLogin",
    "token",
    "accountno",
    "payType",
    "arrInheritedInfo",
    "ach"
})
@XmlRootElement(name = "process")
public class Process {

    @XmlElement(required = true)
    protected Process.ArrLogin arrLogin;
    @XmlElementRef(name = "Token", namespace = "http://xmlns.oracle.com/Activation/ModifyPayInfo/BPELModifyPayInfo", type = JAXBElement.class)
    protected JAXBElement<Long> token;
    @XmlElement(name = "ACCOUNT_NO", required = true)
    protected String accountno;
    @XmlElement(name = "Pay_Type", required = true)
    protected String payType;
    @XmlElement(name = "ArrInheritedInfo", required = true)
    protected Process.ArrInheritedInfo arrInheritedInfo;
    @XmlElement(name = "ACH", required = true)
    protected BigInteger ach;

    /**
     * Gets the value of the arrLogin property.
     * 
     * @return
     *     possible object is
     *     {@link Process.ArrLogin }
     *     
     */
    public Process.ArrLogin getArrLogin() {
        return arrLogin;
    }

    /**
     * Sets the value of the arrLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.ArrLogin }
     *     
     */
    public void setArrLogin(Process.ArrLogin value) {
        this.arrLogin = value;
    }

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setToken(JAXBElement<Long> value) {
        this.token = ((JAXBElement<Long> ) value);
    }

    /**
     * Gets the value of the accountno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCOUNTNO() {
        return accountno;
    }

    /**
     * Sets the value of the accountno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCOUNTNO(String value) {
        this.accountno = value;
    }

    /**
     * Gets the value of the payType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayType() {
        return payType;
    }

    /**
     * Sets the value of the payType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayType(String value) {
        this.payType = value;
    }

    /**
     * Gets the value of the arrInheritedInfo property.
     * 
     * @return
     *     possible object is
     *     {@link Process.ArrInheritedInfo }
     *     
     */
    public Process.ArrInheritedInfo getArrInheritedInfo() {
        return arrInheritedInfo;
    }

    /**
     * Sets the value of the arrInheritedInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Process.ArrInheritedInfo }
     *     
     */
    public void setArrInheritedInfo(Process.ArrInheritedInfo value) {
        this.arrInheritedInfo = value;
    }

    /**
     * Gets the value of the ach property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getACH() {
        return ach;
    }

    /**
     * Sets the value of the ach property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setACH(BigInteger value) {
        this.ach = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="arrInvInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DeliveryPrefer" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="InvInstr" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EmailAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="arrCCInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SecurityId" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="DebitExp" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="arrDDInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Bankno" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "arrInvInfo",
        "arrCCInfo",
        "arrDDInfo"
    })
    public static class ArrInheritedInfo {

        protected Process.ArrInheritedInfo.ArrInvInfo arrInvInfo;
        protected Process.ArrInheritedInfo.ArrCCInfo arrCCInfo;
        protected Process.ArrInheritedInfo.ArrDDInfo arrDDInfo;

        /**
         * Gets the value of the arrInvInfo property.
         * 
         * @return
         *     possible object is
         *     {@link Process.ArrInheritedInfo.ArrInvInfo }
         *     
         */
        public Process.ArrInheritedInfo.ArrInvInfo getArrInvInfo() {
            return arrInvInfo;
        }

        /**
         * Sets the value of the arrInvInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link Process.ArrInheritedInfo.ArrInvInfo }
         *     
         */
        public void setArrInvInfo(Process.ArrInheritedInfo.ArrInvInfo value) {
            this.arrInvInfo = value;
        }

        /**
         * Gets the value of the arrCCInfo property.
         * 
         * @return
         *     possible object is
         *     {@link Process.ArrInheritedInfo.ArrCCInfo }
         *     
         */
        public Process.ArrInheritedInfo.ArrCCInfo getArrCCInfo() {
            return arrCCInfo;
        }

        /**
         * Sets the value of the arrCCInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link Process.ArrInheritedInfo.ArrCCInfo }
         *     
         */
        public void setArrCCInfo(Process.ArrInheritedInfo.ArrCCInfo value) {
            this.arrCCInfo = value;
        }

        /**
         * Gets the value of the arrDDInfo property.
         * 
         * @return
         *     possible object is
         *     {@link Process.ArrInheritedInfo.ArrDDInfo }
         *     
         */
        public Process.ArrInheritedInfo.ArrDDInfo getArrDDInfo() {
            return arrDDInfo;
        }

        /**
         * Sets the value of the arrDDInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link Process.ArrInheritedInfo.ArrDDInfo }
         *     
         */
        public void setArrDDInfo(Process.ArrInheritedInfo.ArrDDInfo value) {
            this.arrDDInfo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SecurityId" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="DebitExp" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "address",
            "city",
            "country",
            "name",
            "firstName",
            "lastName",
            "state",
            "zip",
            "securityId",
            "debitExp",
            "debitNum"
        })
        public static class ArrCCInfo {

            @XmlElement(name = "Address", required = true)
            protected String address;
            @XmlElement(name = "City", required = true)
            protected String city;
            @XmlElement(name = "Country", required = true)
            protected String country;
            @XmlElement(name = "Name", required = true)
            protected String name;
            @XmlElement(name = "FirstName", required = true)
            protected String firstName;
            @XmlElement(name = "LastName", required = true)
            protected String lastName;
            @XmlElement(name = "State", required = true)
            protected String state;
            @XmlElement(name = "Zip", required = true)
            protected String zip;
            @XmlElement(name = "SecurityId", required = true)
            protected BigInteger securityId;
            @XmlElement(name = "DebitExp", required = true)
            protected String debitExp;
            @XmlElement(name = "DebitNum", required = true)
            protected BigInteger debitNum;

            /**
             * Gets the value of the address property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddress() {
                return address;
            }

            /**
             * Sets the value of the address property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddress(String value) {
                this.address = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountry() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountry(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the firstName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstName() {
                return firstName;
            }

            /**
             * Sets the value of the firstName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstName(String value) {
                this.firstName = value;
            }

            /**
             * Gets the value of the lastName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastName() {
                return lastName;
            }

            /**
             * Sets the value of the lastName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastName(String value) {
                this.lastName = value;
            }

            /**
             * Gets the value of the state property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getState() {
                return state;
            }

            /**
             * Sets the value of the state property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setState(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the zip property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getZip() {
                return zip;
            }

            /**
             * Sets the value of the zip property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setZip(String value) {
                this.zip = value;
            }

            /**
             * Gets the value of the securityId property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSecurityId() {
                return securityId;
            }

            /**
             * Sets the value of the securityId property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSecurityId(BigInteger value) {
                this.securityId = value;
            }

            /**
             * Gets the value of the debitExp property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDebitExp() {
                return debitExp;
            }

            /**
             * Sets the value of the debitExp property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDebitExp(String value) {
                this.debitExp = value;
            }

            /**
             * Gets the value of the debitNum property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDebitNum() {
                return debitNum;
            }

            /**
             * Sets the value of the debitNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDebitNum(BigInteger value) {
                this.debitNum = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Bankno" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "address",
            "city",
            "country",
            "debitNum",
            "name",
            "firstName",
            "lastName",
            "state",
            "zip",
            "bankno",
            "type"
        })
        public static class ArrDDInfo {

            @XmlElement(name = "Address", required = true)
            protected String address;
            @XmlElement(name = "City", required = true)
            protected String city;
            @XmlElement(name = "Country", required = true)
            protected String country;
            @XmlElement(name = "DebitNum", required = true)
            protected BigInteger debitNum;
            @XmlElement(name = "Name", required = true)
            protected String name;
            @XmlElement(name = "FirstName", required = true)
            protected String firstName;
            @XmlElement(name = "LastName", required = true)
            protected String lastName;
            @XmlElement(name = "State", required = true)
            protected String state;
            @XmlElement(name = "Zip", required = true)
            protected String zip;
            @XmlElement(name = "Bankno", required = true)
            protected String bankno;
            @XmlElement(name = "Type", required = true)
            protected BigInteger type;

            /**
             * Gets the value of the address property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddress() {
                return address;
            }

            /**
             * Sets the value of the address property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddress(String value) {
                this.address = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountry() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountry(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the debitNum property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDebitNum() {
                return debitNum;
            }

            /**
             * Sets the value of the debitNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDebitNum(BigInteger value) {
                this.debitNum = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the firstName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstName() {
                return firstName;
            }

            /**
             * Sets the value of the firstName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstName(String value) {
                this.firstName = value;
            }

            /**
             * Gets the value of the lastName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastName() {
                return lastName;
            }

            /**
             * Sets the value of the lastName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastName(String value) {
                this.lastName = value;
            }

            /**
             * Gets the value of the state property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getState() {
                return state;
            }

            /**
             * Sets the value of the state property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setState(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the zip property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getZip() {
                return zip;
            }

            /**
             * Sets the value of the zip property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setZip(String value) {
                this.zip = value;
            }

            /**
             * Gets the value of the bankno property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBankno() {
                return bankno;
            }

            /**
             * Sets the value of the bankno property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBankno(String value) {
                this.bankno = value;
            }

            /**
             * Gets the value of the type property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getType() {
                return type;
            }

            /**
             * Sets the value of the type property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setType(BigInteger value) {
                this.type = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DeliveryPrefer" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="InvInstr" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EmailAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "address",
            "city",
            "country",
            "deliveryPrefer",
            "invInstr",
            "emailAddr",
            "name",
            "firstName",
            "lastName",
            "state",
            "zip"
        })
        public static class ArrInvInfo {

            @XmlElement(name = "Address", required = true)
            protected String address;
            @XmlElement(name = "City", required = true)
            protected String city;
            @XmlElement(name = "Country", required = true)
            protected String country;
            @XmlElement(name = "DeliveryPrefer", required = true)
            protected BigInteger deliveryPrefer;
            @XmlElement(name = "InvInstr", required = true)
            protected String invInstr;
            @XmlElement(name = "EmailAddr", required = true)
            protected String emailAddr;
            @XmlElement(name = "Name", required = true)
            protected String name;
            @XmlElement(name = "FirstName", required = true)
            protected String firstName;
            @XmlElement(name = "LastName", required = true)
            protected String lastName;
            @XmlElement(name = "State", required = true)
            protected String state;
            @XmlElement(name = "Zip", required = true)
            protected String zip;

            /**
             * Gets the value of the address property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddress() {
                return address;
            }

            /**
             * Sets the value of the address property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddress(String value) {
                this.address = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountry() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountry(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the deliveryPrefer property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDeliveryPrefer() {
                return deliveryPrefer;
            }

            /**
             * Sets the value of the deliveryPrefer property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDeliveryPrefer(BigInteger value) {
                this.deliveryPrefer = value;
            }

            /**
             * Gets the value of the invInstr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInvInstr() {
                return invInstr;
            }

            /**
             * Sets the value of the invInstr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInvInstr(String value) {
                this.invInstr = value;
            }

            /**
             * Gets the value of the emailAddr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEmailAddr() {
                return emailAddr;
            }

            /**
             * Sets the value of the emailAddr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEmailAddr(String value) {
                this.emailAddr = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the firstName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstName() {
                return firstName;
            }

            /**
             * Sets the value of the firstName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstName(String value) {
                this.firstName = value;
            }

            /**
             * Gets the value of the lastName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastName() {
                return lastName;
            }

            /**
             * Sets the value of the lastName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastName(String value) {
                this.lastName = value;
            }

            /**
             * Gets the value of the state property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getState() {
                return state;
            }

            /**
             * Sets the value of the state property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setState(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the zip property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getZip() {
                return zip;
            }

            /**
             * Sets the value of the zip property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setZip(String value) {
                this.zip = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="User" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "user",
        "password"
    })
    public static class ArrLogin {

        @XmlElement(name = "User", required = true)
        protected String user;
        @XmlElement(name = "Password", required = true)
        protected String password;

        /**
         * Gets the value of the user property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUser() {
            return user;
        }

        /**
         * Sets the value of the user property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUser(String value) {
            this.user = value;
        }

        /**
         * Gets the value of the password property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassword() {
            return password;
        }

        /**
         * Sets the value of the password property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassword(String value) {
            this.password = value;
        }

    }

}
