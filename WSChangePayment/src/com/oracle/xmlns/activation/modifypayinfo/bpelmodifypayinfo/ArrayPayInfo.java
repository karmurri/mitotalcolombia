
package com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PAYINFO" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="POID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PAY_TYPE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="PAYMENT_TERM" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="PAYMENT_OFFSET" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="INV_TYPE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="FLAGS" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="INHERITED_INFO">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="INV_INFO" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="DELIVERY_DESCR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="DELIVERY_PREFER" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="INV_INSTR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="INV_TERMS" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="CC_INFO" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="DEBIT_EXP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="DEBIT_NUM" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="SECURITY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="DD_INFO" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="BANK_NO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="DEBIT_NUM" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ACH" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "payinfo"
})
@XmlRootElement(name = "ArrayPayInfo")
public class ArrayPayInfo {

    @XmlElement(name = "PAYINFO", required = true)
    protected List<ArrayPayInfo.PAYINFO> payinfo;

    /**
     * Gets the value of the payinfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the payinfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPAYINFO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayPayInfo.PAYINFO }
     * 
     * 
     */
    public List<ArrayPayInfo.PAYINFO> getPAYINFO() {
        if (payinfo == null) {
            payinfo = new ArrayList<ArrayPayInfo.PAYINFO>();
        }
        return this.payinfo;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="POID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PAY_TYPE" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="PAYMENT_TERM" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="PAYMENT_OFFSET" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="INV_TYPE" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="FLAGS" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="INHERITED_INFO">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="INV_INFO" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="DELIVERY_DESCR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="DELIVERY_PREFER" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="INV_INSTR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INV_TERMS" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="CC_INFO" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="DEBIT_EXP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="DEBIT_NUM" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="SECURITY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="DD_INFO" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="BANK_NO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="DEBIT_NUM" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ACH" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "poid",
        "paytype",
        "paymentterm",
        "paymentoffset",
        "invtype",
        "flags",
        "inheritedinfo",
        "ach",
        "name"
    })
    public static class PAYINFO {

        @XmlElement(name = "POID", required = true)
        protected String poid;
        @XmlElement(name = "PAY_TYPE")
        protected int paytype;
        @XmlElement(name = "PAYMENT_TERM")
        protected int paymentterm;
        @XmlElement(name = "PAYMENT_OFFSET")
        protected int paymentoffset;
        @XmlElement(name = "INV_TYPE")
        protected int invtype;
        @XmlElement(name = "FLAGS")
        protected int flags;
        @XmlElement(name = "INHERITED_INFO", required = true)
        protected ArrayPayInfo.PAYINFO.INHERITEDINFO inheritedinfo;
        @XmlElement(name = "ACH", required = true)
        protected BigInteger ach;
        @XmlElement(name = "NAME", required = true)
        protected String name;
        @XmlAttribute
        protected BigInteger elem;

        /**
         * Gets the value of the poid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPOID() {
            return poid;
        }

        /**
         * Sets the value of the poid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPOID(String value) {
            this.poid = value;
        }

        /**
         * Gets the value of the paytype property.
         * 
         */
        public int getPAYTYPE() {
            return paytype;
        }

        /**
         * Sets the value of the paytype property.
         * 
         */
        public void setPAYTYPE(int value) {
            this.paytype = value;
        }

        /**
         * Gets the value of the paymentterm property.
         * 
         */
        public int getPAYMENTTERM() {
            return paymentterm;
        }

        /**
         * Sets the value of the paymentterm property.
         * 
         */
        public void setPAYMENTTERM(int value) {
            this.paymentterm = value;
        }

        /**
         * Gets the value of the paymentoffset property.
         * 
         */
        public int getPAYMENTOFFSET() {
            return paymentoffset;
        }

        /**
         * Sets the value of the paymentoffset property.
         * 
         */
        public void setPAYMENTOFFSET(int value) {
            this.paymentoffset = value;
        }

        /**
         * Gets the value of the invtype property.
         * 
         */
        public int getINVTYPE() {
            return invtype;
        }

        /**
         * Sets the value of the invtype property.
         * 
         */
        public void setINVTYPE(int value) {
            this.invtype = value;
        }

        /**
         * Gets the value of the flags property.
         * 
         */
        public int getFLAGS() {
            return flags;
        }

        /**
         * Sets the value of the flags property.
         * 
         */
        public void setFLAGS(int value) {
            this.flags = value;
        }

        /**
         * Gets the value of the inheritedinfo property.
         * 
         * @return
         *     possible object is
         *     {@link ArrayPayInfo.PAYINFO.INHERITEDINFO }
         *     
         */
        public ArrayPayInfo.PAYINFO.INHERITEDINFO getINHERITEDINFO() {
            return inheritedinfo;
        }

        /**
         * Sets the value of the inheritedinfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link ArrayPayInfo.PAYINFO.INHERITEDINFO }
         *     
         */
        public void setINHERITEDINFO(ArrayPayInfo.PAYINFO.INHERITEDINFO value) {
            this.inheritedinfo = value;
        }

        /**
         * Gets the value of the ach property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getACH() {
            return ach;
        }

        /**
         * Sets the value of the ach property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setACH(BigInteger value) {
            this.ach = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNAME() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNAME(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the elem property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setElem(BigInteger value) {
            this.elem = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="INV_INFO" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="DELIVERY_DESCR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="DELIVERY_PREFER" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="INV_INSTR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INV_TERMS" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="CC_INFO" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="DEBIT_EXP" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="DEBIT_NUM" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="SECURITY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="DD_INFO" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="BANK_NO" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="DEBIT_NUM" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "invinfo",
            "ccinfo",
            "ddinfo"
        })
        public static class INHERITEDINFO {

            @XmlElement(name = "INV_INFO")
            protected List<ArrayPayInfo.PAYINFO.INHERITEDINFO.INVINFO> invinfo;
            @XmlElement(name = "CC_INFO")
            protected List<ArrayPayInfo.PAYINFO.INHERITEDINFO.CCINFO> ccinfo;
            @XmlElement(name = "DD_INFO")
            protected List<ArrayPayInfo.PAYINFO.INHERITEDINFO.DDINFO> ddinfo;

            /**
             * Gets the value of the invinfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the invinfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getINVINFO().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ArrayPayInfo.PAYINFO.INHERITEDINFO.INVINFO }
             * 
             * 
             */
            public List<ArrayPayInfo.PAYINFO.INHERITEDINFO.INVINFO> getINVINFO() {
                if (invinfo == null) {
                    invinfo = new ArrayList<ArrayPayInfo.PAYINFO.INHERITEDINFO.INVINFO>();
                }
                return this.invinfo;
            }

            /**
             * Gets the value of the ccinfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the ccinfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCCINFO().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ArrayPayInfo.PAYINFO.INHERITEDINFO.CCINFO }
             * 
             * 
             */
            public List<ArrayPayInfo.PAYINFO.INHERITEDINFO.CCINFO> getCCINFO() {
                if (ccinfo == null) {
                    ccinfo = new ArrayList<ArrayPayInfo.PAYINFO.INHERITEDINFO.CCINFO>();
                }
                return this.ccinfo;
            }

            /**
             * Gets the value of the ddinfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the ddinfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDDINFO().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ArrayPayInfo.PAYINFO.INHERITEDINFO.DDINFO }
             * 
             * 
             */
            public List<ArrayPayInfo.PAYINFO.INHERITEDINFO.DDINFO> getDDINFO() {
                if (ddinfo == null) {
                    ddinfo = new ArrayList<ArrayPayInfo.PAYINFO.INHERITEDINFO.DDINFO>();
                }
                return this.ddinfo;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="DEBIT_EXP" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="DEBIT_NUM" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="SECURITY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "address",
                "city",
                "country",
                "debitexp",
                "debitnum",
                "name",
                "securityid",
                "state",
                "zip"
            })
            public static class CCINFO {

                @XmlElement(name = "ADDRESS", required = true)
                protected String address;
                @XmlElement(name = "CITY", required = true)
                protected String city;
                @XmlElement(name = "COUNTRY", required = true)
                protected String country;
                @XmlElement(name = "DEBIT_EXP", required = true)
                protected String debitexp;
                @XmlElement(name = "DEBIT_NUM")
                protected int debitnum;
                @XmlElement(name = "NAME", required = true)
                protected String name;
                @XmlElement(name = "SECURITY_ID")
                protected int securityid;
                @XmlElement(name = "STATE", required = true)
                protected String state;
                @XmlElement(name = "ZIP", required = true)
                protected String zip;
                @XmlAttribute
                @XmlSchemaType(name = "anySimpleType")
                protected String elem;

                /**
                 * Gets the value of the address property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getADDRESS() {
                    return address;
                }

                /**
                 * Sets the value of the address property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setADDRESS(String value) {
                    this.address = value;
                }

                /**
                 * Gets the value of the city property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCITY() {
                    return city;
                }

                /**
                 * Sets the value of the city property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCITY(String value) {
                    this.city = value;
                }

                /**
                 * Gets the value of the country property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCOUNTRY() {
                    return country;
                }

                /**
                 * Sets the value of the country property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCOUNTRY(String value) {
                    this.country = value;
                }

                /**
                 * Gets the value of the debitexp property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDEBITEXP() {
                    return debitexp;
                }

                /**
                 * Sets the value of the debitexp property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDEBITEXP(String value) {
                    this.debitexp = value;
                }

                /**
                 * Gets the value of the debitnum property.
                 * 
                 */
                public int getDEBITNUM() {
                    return debitnum;
                }

                /**
                 * Sets the value of the debitnum property.
                 * 
                 */
                public void setDEBITNUM(int value) {
                    this.debitnum = value;
                }

                /**
                 * Gets the value of the name property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNAME() {
                    return name;
                }

                /**
                 * Sets the value of the name property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNAME(String value) {
                    this.name = value;
                }

                /**
                 * Gets the value of the securityid property.
                 * 
                 */
                public int getSECURITYID() {
                    return securityid;
                }

                /**
                 * Sets the value of the securityid property.
                 * 
                 */
                public void setSECURITYID(int value) {
                    this.securityid = value;
                }

                /**
                 * Gets the value of the state property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSTATE() {
                    return state;
                }

                /**
                 * Sets the value of the state property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSTATE(String value) {
                    this.state = value;
                }

                /**
                 * Gets the value of the zip property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getZIP() {
                    return zip;
                }

                /**
                 * Sets the value of the zip property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setZIP(String value) {
                    this.zip = value;
                }

                /**
                 * Gets the value of the elem property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getElem() {
                    return elem;
                }

                /**
                 * Sets the value of the elem property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setElem(String value) {
                    this.elem = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="BANK_NO" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="DEBIT_NUM" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "address",
                "bankno",
                "city",
                "country",
                "debitnum",
                "name",
                "state",
                "type",
                "zip"
            })
            public static class DDINFO {

                @XmlElement(name = "ADDRESS", required = true)
                protected String address;
                @XmlElement(name = "BANK_NO", required = true)
                protected String bankno;
                @XmlElement(name = "CITY", required = true)
                protected String city;
                @XmlElement(name = "COUNTRY", required = true)
                protected String country;
                @XmlElement(name = "DEBIT_NUM")
                protected int debitnum;
                @XmlElement(name = "NAME", required = true)
                protected String name;
                @XmlElement(name = "STATE")
                protected int state;
                @XmlElement(name = "TYPE")
                protected int type;
                @XmlElement(name = "ZIP", required = true)
                protected String zip;
                @XmlAttribute
                @XmlSchemaType(name = "anySimpleType")
                protected String elem;

                /**
                 * Gets the value of the address property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getADDRESS() {
                    return address;
                }

                /**
                 * Sets the value of the address property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setADDRESS(String value) {
                    this.address = value;
                }

                /**
                 * Gets the value of the bankno property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBANKNO() {
                    return bankno;
                }

                /**
                 * Sets the value of the bankno property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBANKNO(String value) {
                    this.bankno = value;
                }

                /**
                 * Gets the value of the city property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCITY() {
                    return city;
                }

                /**
                 * Sets the value of the city property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCITY(String value) {
                    this.city = value;
                }

                /**
                 * Gets the value of the country property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCOUNTRY() {
                    return country;
                }

                /**
                 * Sets the value of the country property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCOUNTRY(String value) {
                    this.country = value;
                }

                /**
                 * Gets the value of the debitnum property.
                 * 
                 */
                public int getDEBITNUM() {
                    return debitnum;
                }

                /**
                 * Sets the value of the debitnum property.
                 * 
                 */
                public void setDEBITNUM(int value) {
                    this.debitnum = value;
                }

                /**
                 * Gets the value of the name property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNAME() {
                    return name;
                }

                /**
                 * Sets the value of the name property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNAME(String value) {
                    this.name = value;
                }

                /**
                 * Gets the value of the state property.
                 * 
                 */
                public int getSTATE() {
                    return state;
                }

                /**
                 * Sets the value of the state property.
                 * 
                 */
                public void setSTATE(int value) {
                    this.state = value;
                }

                /**
                 * Gets the value of the type property.
                 * 
                 */
                public int getTYPE() {
                    return type;
                }

                /**
                 * Sets the value of the type property.
                 * 
                 */
                public void setTYPE(int value) {
                    this.type = value;
                }

                /**
                 * Gets the value of the zip property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getZIP() {
                    return zip;
                }

                /**
                 * Sets the value of the zip property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setZIP(String value) {
                    this.zip = value;
                }

                /**
                 * Gets the value of the elem property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getElem() {
                    return elem;
                }

                /**
                 * Sets the value of the elem property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setElem(String value) {
                    this.elem = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="DELIVERY_DESCR" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="DELIVERY_PREFER" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="INV_INSTR" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INV_TERMS" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "address",
                "city",
                "country",
                "deliverydescr",
                "deliveryprefer",
                "invinstr",
                "invterms",
                "emailaddr",
                "name",
                "state",
                "zip"
            })
            public static class INVINFO {

                @XmlElement(name = "ADDRESS", required = true)
                protected String address;
                @XmlElement(name = "CITY", required = true)
                protected String city;
                @XmlElement(name = "COUNTRY", required = true)
                protected String country;
                @XmlElement(name = "DELIVERY_DESCR", required = true)
                protected String deliverydescr;
                @XmlElement(name = "DELIVERY_PREFER")
                protected int deliveryprefer;
                @XmlElement(name = "INV_INSTR", required = true)
                protected String invinstr;
                @XmlElement(name = "INV_TERMS")
                protected int invterms;
                @XmlElement(name = "EMAIL_ADDR", required = true)
                protected String emailaddr;
                @XmlElement(name = "NAME", required = true)
                protected String name;
                @XmlElement(name = "STATE", required = true)
                protected String state;
                @XmlElement(name = "ZIP", required = true)
                protected String zip;
                @XmlAttribute
                @XmlSchemaType(name = "anySimpleType")
                protected String elem;

                /**
                 * Gets the value of the address property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getADDRESS() {
                    return address;
                }

                /**
                 * Sets the value of the address property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setADDRESS(String value) {
                    this.address = value;
                }

                /**
                 * Gets the value of the city property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCITY() {
                    return city;
                }

                /**
                 * Sets the value of the city property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCITY(String value) {
                    this.city = value;
                }

                /**
                 * Gets the value of the country property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCOUNTRY() {
                    return country;
                }

                /**
                 * Sets the value of the country property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCOUNTRY(String value) {
                    this.country = value;
                }

                /**
                 * Gets the value of the deliverydescr property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDELIVERYDESCR() {
                    return deliverydescr;
                }

                /**
                 * Sets the value of the deliverydescr property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDELIVERYDESCR(String value) {
                    this.deliverydescr = value;
                }

                /**
                 * Gets the value of the deliveryprefer property.
                 * 
                 */
                public int getDELIVERYPREFER() {
                    return deliveryprefer;
                }

                /**
                 * Sets the value of the deliveryprefer property.
                 * 
                 */
                public void setDELIVERYPREFER(int value) {
                    this.deliveryprefer = value;
                }

                /**
                 * Gets the value of the invinstr property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINVINSTR() {
                    return invinstr;
                }

                /**
                 * Sets the value of the invinstr property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINVINSTR(String value) {
                    this.invinstr = value;
                }

                /**
                 * Gets the value of the invterms property.
                 * 
                 */
                public int getINVTERMS() {
                    return invterms;
                }

                /**
                 * Sets the value of the invterms property.
                 * 
                 */
                public void setINVTERMS(int value) {
                    this.invterms = value;
                }

                /**
                 * Gets the value of the emailaddr property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEMAILADDR() {
                    return emailaddr;
                }

                /**
                 * Sets the value of the emailaddr property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEMAILADDR(String value) {
                    this.emailaddr = value;
                }

                /**
                 * Gets the value of the name property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNAME() {
                    return name;
                }

                /**
                 * Sets the value of the name property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNAME(String value) {
                    this.name = value;
                }

                /**
                 * Gets the value of the state property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSTATE() {
                    return state;
                }

                /**
                 * Sets the value of the state property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSTATE(String value) {
                    this.state = value;
                }

                /**
                 * Gets the value of the zip property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getZIP() {
                    return zip;
                }

                /**
                 * Sets the value of the zip property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setZIP(String value) {
                    this.zip = value;
                }

                /**
                 * Gets the value of the elem property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getElem() {
                    return elem;
                }

                /**
                 * Sets the value of the elem property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setElem(String value) {
                    this.elem = value;
                }

            }

        }

    }

}
