package com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo;

import java.io.File;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (11.1.1.0.0, build 130224.1947.04102)

@WebServiceClient(wsdlLocation="http://soabalanceada.totalplay.com.mx/soa-infra/services/TotalPlayProcessBPEL/ModifyPayInfoClient/bpelmodifypayinfo_client_ep?WSDL",
  targetNamespace="http://xmlns.oracle.com/Activation/ModifyPayInfo/BPELModifyPayInfo",
  name="bpelmodifypayinfo_client_ep")
public class Bpelmodifypayinfo_client_ep
  extends Service
{
  private static URL wsdlLocationURL;

  private static Logger logger;
  static
  {
    try
    {
      logger = Logger.getLogger("com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.Bpelmodifypayinfo_client_ep");
      URL baseUrl = Bpelmodifypayinfo_client_ep.class.getResource(".");
      if (baseUrl == null)
      {
        wsdlLocationURL =
            Bpelmodifypayinfo_client_ep.class.getResource("http://soabalanceada.totalplay.com.mx/soa-infra/services/TotalPlayProcessBPEL/ModifyPayInfoClient/bpelmodifypayinfo_client_ep?WSDL");
        if (wsdlLocationURL == null)
        {
          baseUrl = new File(".").toURL();
          wsdlLocationURL =
              new URL(baseUrl, "http://soabalanceada.totalplay.com.mx/soa-infra/services/TotalPlayProcessBPEL/ModifyPayInfoClient/bpelmodifypayinfo_client_ep?WSDL");
        }
      }
      else
      {
                if (!baseUrl.getPath().endsWith("/")) {
         baseUrl = new URL(baseUrl, baseUrl.getPath() + "/");
}
                wsdlLocationURL =
            new URL(baseUrl, "http://soabalanceada.totalplay.com.mx/soa-infra/services/TotalPlayProcessBPEL/ModifyPayInfoClient/bpelmodifypayinfo_client_ep?WSDL");
      }
    }
    catch (MalformedURLException e)
    {
      logger.log(Level.ALL,
          "Failed to create wsdlLocationURL using http://soabalanceada.totalplay.com.mx/soa-infra/services/TotalPlayProcessBPEL/ModifyPayInfoClient/bpelmodifypayinfo_client_ep?WSDL",
          e);
    }
  }

  public Bpelmodifypayinfo_client_ep()
  {
    super(wsdlLocationURL,
          new QName("http://xmlns.oracle.com/Activation/ModifyPayInfo/BPELModifyPayInfo",
                    "bpelmodifypayinfo_client_ep"));
  }

  public Bpelmodifypayinfo_client_ep(URL wsdlLocation, QName serviceName)
  {
    super(wsdlLocation, serviceName);
  }

  @WebEndpoint(name="BPELModifyPayInfo_pt")
  public com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.BPELModifyPayInfo getBPELModifyPayInfo_pt()
  {
    return (com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.BPELModifyPayInfo) super.getPort(new QName("http://xmlns.oracle.com/Activation/ModifyPayInfo/BPELModifyPayInfo",
                                                                                                                   "BPELModifyPayInfo_pt"),
                                                                                                         com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.BPELModifyPayInfo.class);
  }

  @WebEndpoint(name="BPELModifyPayInfo_pt")
  public com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.BPELModifyPayInfo getBPELModifyPayInfo_pt(WebServiceFeature... features)
  {
    return (com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.BPELModifyPayInfo) super.getPort(new QName("http://xmlns.oracle.com/Activation/ModifyPayInfo/BPELModifyPayInfo",
                                                                                                                   "BPELModifyPayInfo_pt"),
                                                                                                         com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.BPELModifyPayInfo.class,
                                                                                                         features);
  }
}
