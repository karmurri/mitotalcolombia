
package com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProcessToken_QNAME = new QName("http://xmlns.oracle.com/Activation/ModifyPayInfo/BPELModifyPayInfo", "Token");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link ArrayPayInfo }
     * 
     */
    public ArrayPayInfo createArrayPayInfo() {
        return new ArrayPayInfo();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link ParameterAux }
     * 
     */
    public ParameterAux createParameterAux() {
        return new ParameterAux();
    }

    /**
     * Create an instance of {@link ArrayPayInfo.PAYINFO }
     * 
     */
    public ArrayPayInfo.PAYINFO createArrayPayInfoPAYINFO() {
        return new ArrayPayInfo.PAYINFO();
    }

    /**
     * Create an instance of {@link ArrayPayInfo.PAYINFO.INHERITEDINFO }
     * 
     */
    public ArrayPayInfo.PAYINFO.INHERITEDINFO createArrayPayInfoPAYINFOINHERITEDINFO() {
        return new ArrayPayInfo.PAYINFO.INHERITEDINFO();
    }

    /**
     * Create an instance of {@link Process.ArrInheritedInfo }
     * 
     */
    public Process.ArrInheritedInfo createProcessArrInheritedInfo() {
        return new Process.ArrInheritedInfo();
    }

    /**
     * Create an instance of {@link Process.ArrLogin }
     * 
     */
    public Process.ArrLogin createProcessArrLogin() {
        return new Process.ArrLogin();
    }

    /**
     * Create an instance of {@link AuxModify }
     * 
     */
    public AuxModify createAuxModify() {
        return new AuxModify();
    }

    /**
     * Create an instance of {@link ARGS }
     * 
     */
    public ARGS createARGS() {
        return new ARGS();
    }

    /**
     * Create an instance of {@link ProcessResponse.Result }
     * 
     */
    public ProcessResponse.Result createProcessResponseResult() {
        return new ProcessResponse.Result();
    }

    /**
     * Create an instance of {@link ARGSPayInfoAccountObj }
     * 
     */
    public ARGSPayInfoAccountObj createARGSPayInfoAccountObj() {
        return new ARGSPayInfoAccountObj();
    }

    /**
     * Create an instance of {@link ARGSPayInfoOBJ }
     * 
     */
    public ARGSPayInfoOBJ createARGSPayInfoOBJ() {
        return new ARGSPayInfoOBJ();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.DicParameterValuePair }
     * 
     */
    public com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.DicParameterValuePair createDicParameterValuePair() {
        return new com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.DicParameterValuePair();
    }

    /**
     * Create an instance of {@link ARGSPayInfoPOID }
     * 
     */
    public ARGSPayInfoPOID createARGSPayInfoPOID() {
        return new ARGSPayInfoPOID();
    }

    /**
     * Create an instance of {@link ARGSPayInfo }
     * 
     */
    public ARGSPayInfo createARGSPayInfo() {
        return new ARGSPayInfo();
    }

    /**
     * Create an instance of {@link ParameterAux.DicParameterValuePair }
     * 
     */
    public ParameterAux.DicParameterValuePair createParameterAuxDicParameterValuePair() {
        return new ParameterAux.DicParameterValuePair();
    }

    /**
     * Create an instance of {@link ArrayPayInfo.PAYINFO.INHERITEDINFO.INVINFO }
     * 
     */
    public ArrayPayInfo.PAYINFO.INHERITEDINFO.INVINFO createArrayPayInfoPAYINFOINHERITEDINFOINVINFO() {
        return new ArrayPayInfo.PAYINFO.INHERITEDINFO.INVINFO();
    }

    /**
     * Create an instance of {@link ArrayPayInfo.PAYINFO.INHERITEDINFO.CCINFO }
     * 
     */
    public ArrayPayInfo.PAYINFO.INHERITEDINFO.CCINFO createArrayPayInfoPAYINFOINHERITEDINFOCCINFO() {
        return new ArrayPayInfo.PAYINFO.INHERITEDINFO.CCINFO();
    }

    /**
     * Create an instance of {@link ArrayPayInfo.PAYINFO.INHERITEDINFO.DDINFO }
     * 
     */
    public ArrayPayInfo.PAYINFO.INHERITEDINFO.DDINFO createArrayPayInfoPAYINFOINHERITEDINFODDINFO() {
        return new ArrayPayInfo.PAYINFO.INHERITEDINFO.DDINFO();
    }

    /**
     * Create an instance of {@link Process.ArrInheritedInfo.ArrInvInfo }
     * 
     */
    public Process.ArrInheritedInfo.ArrInvInfo createProcessArrInheritedInfoArrInvInfo() {
        return new Process.ArrInheritedInfo.ArrInvInfo();
    }

    /**
     * Create an instance of {@link Process.ArrInheritedInfo.ArrCCInfo }
     * 
     */
    public Process.ArrInheritedInfo.ArrCCInfo createProcessArrInheritedInfoArrCCInfo() {
        return new Process.ArrInheritedInfo.ArrCCInfo();
    }

    /**
     * Create an instance of {@link Process.ArrInheritedInfo.ArrDDInfo }
     * 
     */
    public Process.ArrInheritedInfo.ArrDDInfo createProcessArrInheritedInfoArrDDInfo() {
        return new Process.ArrInheritedInfo.ArrDDInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/Activation/ModifyPayInfo/BPELModifyPayInfo", name = "Token", scope = Process.class)
    public JAXBElement<Long> createProcessToken(Long value) {
        return new JAXBElement<Long>(_ProcessToken_QNAME, Long.class, Process.class, value);
    }

}
