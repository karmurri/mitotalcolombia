
package com.oracle.xmlns.salesforce11g.infocuenta.infocuenta;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResultDes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcountDesc">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Acount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BusinessUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BusinessDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TaxRegime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TaxRegimeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ContractType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CurrencyDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Cluster" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Segmentation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DescSegmentation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BehaivorScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Dn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CreationDateAcount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="IdPackage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Rent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ArrAddress">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Address" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="IdDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Conpany" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="AutdoorNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Colony" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Delegation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Phones" maxOccurs="unbounded">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idResult",
    "result",
    "resultDes",
    "acountDesc"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "IdResult", required = true)
    protected String idResult;
    @XmlElement(name = "Result", required = true)
    protected String result;
    @XmlElement(name = "ResultDes", required = true)
    protected String resultDes;
    @XmlElement(name = "AcountDesc", required = true)
    protected ProcessResponse.AcountDesc acountDesc;

    /**
     * Gets the value of the idResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdResult() {
        return idResult;
    }

    /**
     * Sets the value of the idResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdResult(String value) {
        this.idResult = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResult(String value) {
        this.result = value;
    }

    /**
     * Gets the value of the resultDes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultDes() {
        return resultDes;
    }

    /**
     * Sets the value of the resultDes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultDes(String value) {
        this.resultDes = value;
    }

    /**
     * Gets the value of the acountDesc property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.AcountDesc }
     *     
     */
    public ProcessResponse.AcountDesc getAcountDesc() {
        return acountDesc;
    }

    /**
     * Sets the value of the acountDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.AcountDesc }
     *     
     */
    public void setAcountDesc(ProcessResponse.AcountDesc value) {
        this.acountDesc = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Acount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BusinessUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BusinessDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TaxRegime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TaxRegimeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ContractType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CurrencyDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Cluster" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Segmentation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DescSegmentation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BehaivorScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Dn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CreationDateAcount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="IdPackage" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Rent" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ArrAddress">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Address" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="IdDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Conpany" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="AutdoorNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Colony" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Delegation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Phones" maxOccurs="unbounded">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acount",
        "name",
        "status",
        "businessUnit",
        "businessDescription",
        "taxRegime",
        "taxRegimeDescription",
        "contractType",
        "currency",
        "currencyDescription",
        "cluster",
        "segmentation",
        "descSegmentation",
        "behaivorScore",
        "dn",
        "creationDateAcount",
        "idPackage",
        "packageDescription",
        "rent",
        "arrAddress"
    })
    public static class AcountDesc {

        @XmlElement(name = "Acount", required = true)
        protected String acount;
        @XmlElement(name = "Name", required = true)
        protected String name;
        @XmlElement(name = "Status", required = true)
        protected String status;
        @XmlElement(name = "BusinessUnit", required = true)
        protected String businessUnit;
        @XmlElement(name = "BusinessDescription", required = true)
        protected String businessDescription;
        @XmlElement(name = "TaxRegime", required = true)
        protected String taxRegime;
        @XmlElement(name = "TaxRegimeDescription", required = true)
        protected String taxRegimeDescription;
        @XmlElement(name = "ContractType", required = true)
        protected String contractType;
        @XmlElement(name = "Currency", required = true)
        protected String currency;
        @XmlElement(name = "CurrencyDescription", required = true)
        protected String currencyDescription;
        @XmlElement(name = "Cluster", required = true)
        protected String cluster;
        @XmlElement(name = "Segmentation", required = true)
        protected String segmentation;
        @XmlElement(name = "DescSegmentation", required = true)
        protected String descSegmentation;
        @XmlElement(name = "BehaivorScore", required = true)
        protected String behaivorScore;
        @XmlElement(name = "Dn")
        protected String dn;
        @XmlElement(name = "CreationDateAcount", required = true)
        protected String creationDateAcount;
        @XmlElement(name = "IdPackage", required = true)
        protected String idPackage;
        @XmlElement(name = "PackageDescription", required = true)
        protected String packageDescription;
        @XmlElement(name = "Rent", required = true)
        protected String rent;
        @XmlElement(name = "ArrAddress", required = true)
        protected ProcessResponse.AcountDesc.ArrAddress arrAddress;

        /**
         * Gets the value of the acount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcount() {
            return acount;
        }

        /**
         * Sets the value of the acount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcount(String value) {
            this.acount = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the businessUnit property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBusinessUnit() {
            return businessUnit;
        }

        /**
         * Sets the value of the businessUnit property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBusinessUnit(String value) {
            this.businessUnit = value;
        }

        /**
         * Gets the value of the businessDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBusinessDescription() {
            return businessDescription;
        }

        /**
         * Sets the value of the businessDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBusinessDescription(String value) {
            this.businessDescription = value;
        }

        /**
         * Gets the value of the taxRegime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxRegime() {
            return taxRegime;
        }

        /**
         * Sets the value of the taxRegime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxRegime(String value) {
            this.taxRegime = value;
        }

        /**
         * Gets the value of the taxRegimeDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxRegimeDescription() {
            return taxRegimeDescription;
        }

        /**
         * Sets the value of the taxRegimeDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxRegimeDescription(String value) {
            this.taxRegimeDescription = value;
        }

        /**
         * Gets the value of the contractType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContractType() {
            return contractType;
        }

        /**
         * Sets the value of the contractType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContractType(String value) {
            this.contractType = value;
        }

        /**
         * Gets the value of the currency property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrency() {
            return currency;
        }

        /**
         * Sets the value of the currency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrency(String value) {
            this.currency = value;
        }

        /**
         * Gets the value of the currencyDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyDescription() {
            return currencyDescription;
        }

        /**
         * Sets the value of the currencyDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyDescription(String value) {
            this.currencyDescription = value;
        }

        /**
         * Gets the value of the cluster property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCluster() {
            return cluster;
        }

        /**
         * Sets the value of the cluster property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCluster(String value) {
            this.cluster = value;
        }

        /**
         * Gets the value of the segmentation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSegmentation() {
            return segmentation;
        }

        /**
         * Sets the value of the segmentation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSegmentation(String value) {
            this.segmentation = value;
        }

        /**
         * Gets the value of the descSegmentation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescSegmentation() {
            return descSegmentation;
        }

        /**
         * Sets the value of the descSegmentation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescSegmentation(String value) {
            this.descSegmentation = value;
        }

        /**
         * Gets the value of the behaivorScore property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBehaivorScore() {
            return behaivorScore;
        }

        /**
         * Sets the value of the behaivorScore property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBehaivorScore(String value) {
            this.behaivorScore = value;
        }

        /**
         * Gets the value of the dn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDn() {
            return dn;
        }

        /**
         * Sets the value of the dn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDn(String value) {
            this.dn = value;
        }

        /**
         * Gets the value of the creationDateAcount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreationDateAcount() {
            return creationDateAcount;
        }

        /**
         * Sets the value of the creationDateAcount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreationDateAcount(String value) {
            this.creationDateAcount = value;
        }

        /**
         * Gets the value of the idPackage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdPackage() {
            return idPackage;
        }

        /**
         * Sets the value of the idPackage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdPackage(String value) {
            this.idPackage = value;
        }

        /**
         * Gets the value of the packageDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPackageDescription() {
            return packageDescription;
        }

        /**
         * Sets the value of the packageDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPackageDescription(String value) {
            this.packageDescription = value;
        }

        /**
         * Gets the value of the rent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRent() {
            return rent;
        }

        /**
         * Sets the value of the rent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRent(String value) {
            this.rent = value;
        }

        /**
         * Gets the value of the arrAddress property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.AcountDesc.ArrAddress }
         *     
         */
        public ProcessResponse.AcountDesc.ArrAddress getArrAddress() {
            return arrAddress;
        }

        /**
         * Sets the value of the arrAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.AcountDesc.ArrAddress }
         *     
         */
        public void setArrAddress(ProcessResponse.AcountDesc.ArrAddress value) {
            this.arrAddress = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Address" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="IdDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Conpany" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="AutdoorNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Colony" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Delegation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Phones" maxOccurs="unbounded">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "address"
        })
        public static class ArrAddress {

            @XmlElement(name = "Address", required = true)
            protected List<ProcessResponse.AcountDesc.ArrAddress.Address> address;

            /**
             * Gets the value of the address property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the address property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddress().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ProcessResponse.AcountDesc.ArrAddress.Address }
             * 
             * 
             */
            public List<ProcessResponse.AcountDesc.ArrAddress.Address> getAddress() {
                if (address == null) {
                    address = new ArrayList<ProcessResponse.AcountDesc.ArrAddress.Address>();
                }
                return this.address;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="IdDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Conpany" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="AutdoorNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Colony" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Delegation" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Phones" maxOccurs="unbounded">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "idDescription",
                "firstName",
                "middleName",
                "lastName",
                "conpany",
                "title",
                "gender",
                "birthday",
                "street",
                "autdoorNum",
                "city",
                "colony",
                "delegation",
                "country",
                "email",
                "state",
                "zip",
                "phones"
            })
            public static class Address {

                @XmlElement(name = "Id", required = true)
                protected String id;
                @XmlElement(name = "IdDescription", required = true)
                protected String idDescription;
                @XmlElement(name = "FirstName", required = true)
                protected String firstName;
                @XmlElement(name = "MiddleName", required = true)
                protected String middleName;
                @XmlElement(name = "LastName", required = true)
                protected String lastName;
                @XmlElement(name = "Conpany", required = true)
                protected String conpany;
                @XmlElement(name = "Title", required = true)
                protected String title;
                @XmlElement(name = "Gender", required = true)
                protected String gender;
                @XmlElement(name = "Birthday", required = true)
                protected String birthday;
                @XmlElement(name = "Street", required = true)
                protected String street;
                @XmlElement(name = "AutdoorNum", required = true)
                protected String autdoorNum;
                @XmlElement(name = "City", required = true)
                protected String city;
                @XmlElement(name = "Colony", required = true)
                protected String colony;
                @XmlElement(name = "Delegation", required = true)
                protected String delegation;
                @XmlElement(name = "Country", required = true)
                protected String country;
                @XmlElement(required = true)
                protected String email;
                @XmlElement(name = "State", required = true)
                protected String state;
                @XmlElement(name = "Zip", required = true)
                protected String zip;
                @XmlElement(name = "Phones", required = true)
                protected List<ProcessResponse.AcountDesc.ArrAddress.Address.Phones> phones;

                /**
                 * Gets the value of the id property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Sets the value of the id property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setId(String value) {
                    this.id = value;
                }

                /**
                 * Gets the value of the idDescription property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdDescription() {
                    return idDescription;
                }

                /**
                 * Sets the value of the idDescription property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdDescription(String value) {
                    this.idDescription = value;
                }

                /**
                 * Gets the value of the firstName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFirstName() {
                    return firstName;
                }

                /**
                 * Sets the value of the firstName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFirstName(String value) {
                    this.firstName = value;
                }

                /**
                 * Gets the value of the middleName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMiddleName() {
                    return middleName;
                }

                /**
                 * Sets the value of the middleName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMiddleName(String value) {
                    this.middleName = value;
                }

                /**
                 * Gets the value of the lastName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLastName() {
                    return lastName;
                }

                /**
                 * Sets the value of the lastName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLastName(String value) {
                    this.lastName = value;
                }

                /**
                 * Gets the value of the conpany property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getConpany() {
                    return conpany;
                }

                /**
                 * Sets the value of the conpany property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setConpany(String value) {
                    this.conpany = value;
                }

                /**
                 * Gets the value of the title property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTitle() {
                    return title;
                }

                /**
                 * Sets the value of the title property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTitle(String value) {
                    this.title = value;
                }

                /**
                 * Gets the value of the gender property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGender() {
                    return gender;
                }

                /**
                 * Sets the value of the gender property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGender(String value) {
                    this.gender = value;
                }

                /**
                 * Gets the value of the birthday property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBirthday() {
                    return birthday;
                }

                /**
                 * Sets the value of the birthday property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBirthday(String value) {
                    this.birthday = value;
                }

                /**
                 * Gets the value of the street property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStreet() {
                    return street;
                }

                /**
                 * Sets the value of the street property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStreet(String value) {
                    this.street = value;
                }

                /**
                 * Gets the value of the autdoorNum property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAutdoorNum() {
                    return autdoorNum;
                }

                /**
                 * Sets the value of the autdoorNum property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAutdoorNum(String value) {
                    this.autdoorNum = value;
                }

                /**
                 * Gets the value of the city property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCity() {
                    return city;
                }

                /**
                 * Sets the value of the city property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCity(String value) {
                    this.city = value;
                }

                /**
                 * Gets the value of the colony property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getColony() {
                    return colony;
                }

                /**
                 * Sets the value of the colony property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setColony(String value) {
                    this.colony = value;
                }

                /**
                 * Gets the value of the delegation property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDelegation() {
                    return delegation;
                }

                /**
                 * Sets the value of the delegation property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDelegation(String value) {
                    this.delegation = value;
                }

                /**
                 * Gets the value of the country property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountry() {
                    return country;
                }

                /**
                 * Sets the value of the country property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountry(String value) {
                    this.country = value;
                }

                /**
                 * Gets the value of the email property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEmail() {
                    return email;
                }

                /**
                 * Sets the value of the email property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEmail(String value) {
                    this.email = value;
                }

                /**
                 * Gets the value of the state property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getState() {
                    return state;
                }

                /**
                 * Sets the value of the state property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setState(String value) {
                    this.state = value;
                }

                /**
                 * Gets the value of the zip property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getZip() {
                    return zip;
                }

                /**
                 * Sets the value of the zip property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setZip(String value) {
                    this.zip = value;
                }

                /**
                 * Gets the value of the phones property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the phones property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPhones().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ProcessResponse.AcountDesc.ArrAddress.Address.Phones }
                 * 
                 * 
                 */
                public List<ProcessResponse.AcountDesc.ArrAddress.Address.Phones> getPhones() {
                    if (phones == null) {
                        phones = new ArrayList<ProcessResponse.AcountDesc.ArrAddress.Address.Phones>();
                    }
                    return this.phones;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "phone",
                    "type",
                    "description"
                })
                public static class Phones {

                    @XmlElement(name = "Phone", required = true)
                    protected String phone;
                    @XmlElement(name = "Type", required = true)
                    protected String type;
                    @XmlElement(required = true)
                    protected String description;

                    /**
                     * Gets the value of the phone property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPhone() {
                        return phone;
                    }

                    /**
                     * Sets the value of the phone property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPhone(String value) {
                        this.phone = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                    /**
                     * Gets the value of the description property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDescription() {
                        return description;
                    }

                    /**
                     * Sets the value of the description property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDescription(String value) {
                        this.description = value;
                    }

                }

            }

        }

    }

}
