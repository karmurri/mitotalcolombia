
package com.oracle.xmlns.salesforce11g.infocuenta.infocuenta;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.salesforce11g.infocuenta.infocuenta package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.salesforce11g.infocuenta.infocuenta
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse.AcountDesc }
     * 
     */
    public ProcessResponse.AcountDesc createProcessResponseAcountDesc() {
        return new ProcessResponse.AcountDesc();
    }

    /**
     * Create an instance of {@link ProcessResponse.AcountDesc.ArrAddress }
     * 
     */
    public ProcessResponse.AcountDesc.ArrAddress createProcessResponseAcountDescArrAddress() {
        return new ProcessResponse.AcountDesc.ArrAddress();
    }

    /**
     * Create an instance of {@link ProcessResponse.AcountDesc.ArrAddress.Address }
     * 
     */
    public ProcessResponse.AcountDesc.ArrAddress.Address createProcessResponseAcountDescArrAddressAddress() {
        return new ProcessResponse.AcountDesc.ArrAddress.Address();
    }

    /**
     * Create an instance of {@link Process.Login }
     * 
     */
    public Process.Login createProcessLogin() {
        return new Process.Login();
    }

    /**
     * Create an instance of {@link ProcessResponse.AcountDesc.ArrAddress.Address.Phones }
     * 
     */
    public ProcessResponse.AcountDesc.ArrAddress.Address.Phones createProcessResponseAcountDescArrAddressAddressPhones() {
        return new ProcessResponse.AcountDesc.ArrAddress.Address.Phones();
    }

}
