
package com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;

//import javax.swing.text.Style;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.Holder;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * Oracle JAX-WS 2.1.5
 * Generated source version: 2.1
 * 
 */
@WebService(name = "BPELDetalleCompraIVR", targetNamespace = "http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR")
@XmlSeeAlso({
    ObjectFactory.class
})

@SOAPBinding(style=Style.DOCUMENT, parameterStyle=ParameterStyle.BARE)

public interface BPELDetalleCompraIVR {


    /**
     * 
     * @param result
     * @param detalles
     * @param login
     * @param accountNO
     */
    /*@WebMethod(action = "process")
    @RequestWrapper(localName = "process", targetNamespace = "http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR", className = "com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.Process")
    @ResponseWrapper(localName = "processResponse", targetNamespace = "http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR", className = "com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.ProcessResponse")
    public void process(
        @WebParam(name = "Login", targetNamespace = "http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR")
        com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.Process.Login login,
        @WebParam(name = "ACCOUNT_NO", targetNamespace = "http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR")
        String accountNO,
        @WebParam(name = "Result", targetNamespace = "http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR", mode = WebParam.Mode.OUT)
        Holder<com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.ProcessResponse.Result> result,
        @WebParam(name = "Detalles", targetNamespace = "http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR", mode = WebParam.Mode.OUT)
        Holder<com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.ProcessResponse.Detalles> detalles);
    */
    @WebMethod(action = "process")
    @SOAPBinding(parameterStyle=ParameterStyle.BARE)
    @Action(input="process", output="http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR/ProcessResponse")
    @WebResult(targetNamespace="http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR",
    partName="payload", name="process")
    public com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.ProcessResponse process(@WebParam(targetNamespace="http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR",
        partName="payload", name="process")Process payload
        );
}
