
package com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Result">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ResultDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Detalles">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DetalleCompra" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Poid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CreditAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NumUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Descr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusFlags" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "result",
    "detalles"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "Result", required = true)
    protected ProcessResponse.Result result;
    @XmlElement(name = "Detalles", required = true)
    protected ProcessResponse.Detalles detalles;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.Result }
     *     
     */
    public ProcessResponse.Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Result }
     *     
     */
    public void setResult(ProcessResponse.Result value) {
        this.result = value;
    }

    /**
     * Gets the value of the detalles property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.Detalles }
     *     
     */
    public ProcessResponse.Detalles getDetalles() {
        return detalles;
    }

    /**
     * Sets the value of the detalles property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Detalles }
     *     
     */
    public void setDetalles(ProcessResponse.Detalles value) {
        this.detalles = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DetalleCompra" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Poid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CreditAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NumUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Descr" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusFlags" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "detalleCompra"
    })
    public static class Detalles {

        @XmlElement(name = "DetalleCompra")
        protected List<ProcessResponse.Detalles.DetalleCompra> detalleCompra;

        /**
         * Gets the value of the detalleCompra property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the detalleCompra property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDetalleCompra().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProcessResponse.Detalles.DetalleCompra }
         * 
         * 
         */
        public List<ProcessResponse.Detalles.DetalleCompra> getDetalleCompra() {
            if (detalleCompra == null) {
                detalleCompra = new ArrayList<ProcessResponse.Detalles.DetalleCompra>();
            }
            return this.detalleCompra;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Poid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CreditAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NumUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Descr" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ContractNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusFlags" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "poid",
            "accountNo",
            "amount",
            "creditAmount",
            "numUnits",
            "descr",
            "contractNo",
            "statusFlags"
        })
        public static class DetalleCompra {

            @XmlElement(name = "Poid", required = true)
            protected String poid;
            @XmlElement(name = "AccountNo", required = true)
            protected String accountNo;
            @XmlElement(name = "Amount", required = true)
            protected String amount;
            @XmlElement(name = "CreditAmount", required = true)
            protected String creditAmount;
            @XmlElement(name = "NumUnits", required = true)
            protected String numUnits;
            @XmlElement(name = "Descr", required = true)
            protected String descr;
            @XmlElement(name = "ContractNo", required = true)
            protected String contractNo;
            @XmlElement(name = "StatusFlags", required = true)
            protected String statusFlags;

            /**
             * Gets the value of the poid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPoid() {
                return poid;
            }

            /**
             * Sets the value of the poid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPoid(String value) {
                this.poid = value;
            }

            /**
             * Gets the value of the accountNo property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAccountNo() {
                return accountNo;
            }

            /**
             * Sets the value of the accountNo property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAccountNo(String value) {
                this.accountNo = value;
            }

            /**
             * Gets the value of the amount property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAmount() {
                return amount;
            }

            /**
             * Sets the value of the amount property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAmount(String value) {
                this.amount = value;
            }

            /**
             * Gets the value of the creditAmount property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreditAmount() {
                return creditAmount;
            }

            /**
             * Sets the value of the creditAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreditAmount(String value) {
                this.creditAmount = value;
            }

            /**
             * Gets the value of the numUnits property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumUnits() {
                return numUnits;
            }

            /**
             * Sets the value of the numUnits property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumUnits(String value) {
                this.numUnits = value;
            }

            /**
             * Gets the value of the descr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescr() {
                return descr;
            }

            /**
             * Sets the value of the descr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescr(String value) {
                this.descr = value;
            }

            /**
             * Gets the value of the contractNo property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContractNo() {
                return contractNo;
            }

            /**
             * Sets the value of the contractNo property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContractNo(String value) {
                this.contractNo = value;
            }

            /**
             * Gets the value of the statusFlags property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusFlags() {
                return statusFlags;
            }

            /**
             * Sets the value of the statusFlags property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusFlags(String value) {
                this.statusFlags = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ResultDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "result",
        "idResult",
        "resultDescription"
    })
    public static class Result {

        @XmlElement(name = "Result", required = true)
        protected String result;
        @XmlElement(name = "IdResult", required = true)
        protected String idResult;
        @XmlElement(name = "ResultDescription", required = true)
        protected String resultDescription;

        /**
         * Gets the value of the result property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResult() {
            return result;
        }

        /**
         * Sets the value of the result property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResult(String value) {
            this.result = value;
        }

        /**
         * Gets the value of the idResult property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdResult() {
            return idResult;
        }

        /**
         * Sets the value of the idResult property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdResult(String value) {
            this.idResult = value;
        }

        /**
         * Gets the value of the resultDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResultDescription() {
            return resultDescription;
        }

        /**
         * Sets the value of the resultDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResultDescription(String value) {
            this.resultDescription = value;
        }

    }

}
