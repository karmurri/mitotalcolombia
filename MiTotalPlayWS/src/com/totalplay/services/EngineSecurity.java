package com.totalplay.services;

import com.totalplay.newServices.com.oracle.xmlns.marketplace.infobyuserlogin.infobyuserlogin.InfoByUserLogin;
import com.totalplay.newServices.com.oracle.xmlns.marketplace.infobyuserlogin.infobyuserlogin.InfobyuserloginClientEp;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;

import com.totalplay.utils.VerificaUsuario;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import javax.xml.ws.BindingProvider;

import javax.xml.ws.Holder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*import org.tempuri.AddNewUser;
import org.tempuri.AddNewUserResponse;
import org.tempuri.GetUserByEmployeeNumber;
import org.tempuri.GetUserByEmployeeNumberResponse;
import org.tempuri.LoginUser;
import org.tempuri.LoginUserByEmail;
import org.tempuri.LoginUserByEmailResponse;
import org.tempuri.LoginUserResponse;
import org.tempuri.SecurityUserModel;
import org.tempuri.SecurityUserOnRolModel;
import org.tempuri.Service1;
import org.tempuri.Service1Soap;
import org.tempuri.UpdateUser;
import org.tempuri.UpdateUserResponse;*/

public class EngineSecurity {
    static FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
    //private static Service1 service1;
    private static String rutaWS= fileProperty.getProperty("ENGINESECURITY");
    static Logger logger = LogManager.getLogger(EngineSecurity.class.getName());
    
    public EngineSecurity() {
        super();
    }
    /*public static SecurityUserModel consumeLoginWS(String user, String pass){
        URL wsURL=null;
        try {
            wsURL = new URL(rutaWS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        service1 = new Service1(wsURL, new QName("http://tempuri.org/", "Service1"));
        Service1Soap service1Soap = service1.getService1Soap();
        SecurityUserModel info=null;
        LoginUser us= new LoginUser();
        us.setStrUser(user);      
        us.setStrPWD(pass);   
        try{

             LoginUserResponse respuestaWS= service1Soap.loginUser(us);
             info= respuestaWS.getLoginUserResult();
        
        }catch(Exception e){
            info=null;
        }
        return info;
    }*/
    public static Map consumeLoginWS(String user, String pass){
        logger.error("Estoy en consumeLoginWS");
        Map datos = new HashMap();
        URL wsdlURL = null;
        try{
            String resource = "/infobyuserlogin_client_ep.xml";// C:\Users\ahernandezm\Downloads\infobyuserlogin_client_ep.xml
            URL res = EngineSecurity.class.getResource(resource);
            wsdlURL = res;
            logger.error("url ok");
        }catch(Exception e){
            logger.error("hubo un error al iniciar la url infobyuserlogin_client_ep.xml");
            logger.error("error: "+e.getMessage()+" , "+e.getLocalizedMessage());
        }
        
        logger.error("Generando Client y Port");
        
        InfobyuserloginClientEp ss = null;
        InfoByUserLogin port = null;
        
        try{
            ss = new InfobyuserloginClientEp(wsdlURL,new QName("http://xmlns.oracle.com/Marketplace/InfoByUserLogin/InfoByUserLogin", "InfoByUserLogin_pt"));
            logger.error("cliente creado correctamente");
        }catch(Exception e){
            logger.error("No se pudo crear el cliente InfoByUserLogin");
        }
        
        if(ss != null){
            port = ss.getInfoByUserLoginPt();
            logger.error("Se creo el puerto InfoByUserLogin");
        }
        
        if(port != null){
            logger.error("port diferente de null");
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put("ws-security.username", "ffmapp");
            provider.getRequestContext().put("ws-security.password", "4gend4mi3nto");
            try{
                java.lang.String _process_strLogin = user;
                javax.xml.ws.Holder _process_password = new javax.xml.ws.Holder<java.lang.String>();
                _process_password.value = pass;
                
                javax.xml.ws.Holder _process_result = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_resultId = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_resultDescription = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_userId = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_login = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_employeeNumber = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_firstName = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_lastName = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_email = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_systemId = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_dateReception = new javax.xml.ws.Holder<java.lang.String>();
                javax.xml.ws.Holder _process_status = new javax.xml.ws.Holder<java.lang.String>();
                
                port.process(_process_strLogin, _process_password, 
                             _process_result, _process_resultId, _process_resultDescription, _process_userId, 
                             _process_login, _process_employeeNumber, _process_firstName, _process_lastName, 
                             _process_email, _process_systemId, _process_dateReception, _process_status);
                logger.error("termine process");
                
                datos.put("result", _process_result.value);
                datos.put("idResult", _process_resultId.value);
                datos.put("resultDescription", _process_resultDescription.value);
                
            }catch(Exception e){
                logger.error("no se pudieron pasar los parámetros provider");
            }
        }else{
            logger.error("No se pudo inicializar la variable PORT");
        }
        return datos;
    }
    
      /*public static SecurityUserModel loginByEmailWS(String email, String pass){
          URL wsURL=null;
          try {
              wsURL = new URL(rutaWS);
          } catch (MalformedURLException e) {
              e.printStackTrace();
          }
          service1 = new Service1(wsURL, new QName("http://tempuri.org/", "Service1"));
          Service1Soap service1Soap = service1.getService1Soap();
          SecurityUserModel info=null;
          LoginUserByEmail us= new LoginUserByEmail();
          us.setStrUserEmail(email);
          us.setStrPWD(pass);   
          try{
               LoginUserByEmailResponse respuestaWS= service1Soap.loginUserByEmail(us);
               info= respuestaWS.getLoginUserByEmailResult();
          }catch(Exception e){
              info=null;
          }
          return info;
      }
      public static int updateInfoClient(String userid, String login, String employeenumber, String firstname, String lastname,String email, String password){
          URL wsURL=null;
          try {
              wsURL = new URL(rutaWS);
          } catch (MalformedURLException e) {
              e.printStackTrace();
          }
          service1 = new Service1(wsURL, new QName("http://tempuri.org/", "Service1"));
         Service1Soap service1Soap= service1.getService1Soap();
         UpdateUser usuario= new UpdateUser();
          SecurityUserModel us= new SecurityUserModel();
          us.setUserId(userid);
          us.setLogin(login);
          us.setEmployeeNumber(employeenumber);
          us.setFirstName(firstname);
          us.setLastName(lastname);
          us.setEmail(email);
          us.setPassword(password);
          us.setSystemId("603");
          us.setStatus("1");
          
         usuario.setMdl(us);
         
         UpdateUserResponse respuestaWS= service1Soap.updateUser(usuario);   
         return respuestaWS.getUpdateUserResult();    
      }
      
      public static int addNewUser(String userid, String login,String employeenumber, String firstname, String lastname,String email, String password){
          URL wsURL=null;
          try {
              wsURL = new URL(rutaWS);
          } catch (MalformedURLException e) {
              e.printStackTrace();
          }
          service1 = new Service1(wsURL, new QName("http://tempuri.org/", "Service1"));
          Service1Soap service1Soap= service1.getService1Soap();
          AddNewUser usuario= new AddNewUser();
          SecurityUserModel us= new SecurityUserModel();
          us.setUserId(userid);
          us.setLogin(login);
          us.setEmployeeNumber(employeenumber);
          us.setFirstName(firstname);
          us.setLastName(lastname);
          us.setEmail(email);
          us.setPassword(password);
          us.setSystemId("603");
          us.setStatus("1");
          
          SecurityUserOnRolModel usuarioRol= new SecurityUserOnRolModel();
          usuarioRol.setRoleId("1341");
          usuarioRol.setStatusId("1");
          usuarioRol.setSystemId("603");
          
          
          usuario.setUserModel(us);
          usuario.setUserRolModel(usuarioRol);
          
          
          AddNewUserResponse respuestaWS= service1Soap.addNewUser(usuario);
          return respuestaWS.getAddNewUserResult();
      }
      
      public static SecurityUserModel getNoAccount(String user){
          URL wsURL=null;
          try {
              wsURL = new URL(rutaWS);
          } catch (MalformedURLException e) {
              e.printStackTrace();
          }
          service1 = new Service1(wsURL, new QName("http://tempuri.org/", "Service1"));
          Service1Soap service1Soap= service1.getService1Soap();
          GetUserByEmployeeNumber us= new GetUserByEmployeeNumber();
          us.setEmployeeNumber(user);    
          try{
              GetUserByEmployeeNumberResponse respuestaWS= service1Soap.getUserByEmployeeNumber(us);
              return(respuestaWS.getGetUserByEmployeeNumberResult());
          }catch(Exception e){
              return (null);
          }
      } */
}
