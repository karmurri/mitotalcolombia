package com.totalplay.services;

import ConsultaSaldo.BPELProcess1;
import ConsultaSaldo.Bpelprocess1_client_ep;

import com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1.Process;
import com.oracle.xmlns.balance.consultasaldobrm.bpelprocess1.ProcessResponse;

import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;

import java.net.URL;

import javax.xml.namespace.QName;

public class ConsultaSaldoBRM {
    private static Bpelprocess1_client_ep bpelprocess1_client_ep;
    static FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);

    public ConsultaSaldoBRM() {
        super();
    }
    public static String obtieneSaldoActualReal(String numCuenta){
        String saldoActual=null;
        String rutaWS= fileProperty.getProperty("CONSULTASALDOBRM");

        try{
            URL wsURL = new URL(rutaWS);            
            bpelprocess1_client_ep = new Bpelprocess1_client_ep(wsURL,
          new QName("http://xmlns.oracle.com/Balance/ConsultaSaldoBRM/BPELProcess1",
                    "bpelprocess1_client_ep"));
            BPELProcess1 bPELProcess1 = bpelprocess1_client_ep.getBPELProcess1_pt();
            
            // Add your code to call the desired methods.
            Process payload= new Process();
            payload.setACCOUNTNO(numCuenta);
            
            ProcessResponse respuestaWS= bPELProcess1.process(payload);
            saldoActual= respuestaWS.getArrResult().getArrDatosFactura().getTotalPagar();
        }catch(Exception e){
            e.printStackTrace();
        }
        return saldoActual;
        
    }
}
