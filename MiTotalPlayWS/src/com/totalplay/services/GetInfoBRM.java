package com.totalplay.services;

import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.BPELGetInfoBRM;
import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.Bpelgetinfobrm_client_ep;
import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.Process;
import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.ProcessResponse;

import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;

import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GetInfoBRM {
    private static Bpelgetinfobrm_client_ep bpelgetinfobrm_client_ep;
    static FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);

    public GetInfoBRM() {
        super();
        
    }
    public static ProcessResponse.DatosCliente  obtieneCuenta(String usuario, int bandera){
        ProcessResponse.DatosCliente datos=null;
        String rutaWS= fileProperty.getProperty("GETINFOBRM");
        try{
            URL wsURL = new URL(rutaWS);
            bpelgetinfobrm_client_ep = new Bpelgetinfobrm_client_ep(wsURL, new QName("http://xmlns.oracle.com/BRM/GetInfoBRM/BPELGetInfoBRM","bpelgetinfobrm_client_ep"));
            BPELGetInfoBRM bPELGetInfoBRM = bpelgetinfobrm_client_ep.getBPELGetInfoBRM_pt();
        
            Process payload= new Process();
            if(bandera==1)
              payload.setEmailAddress(usuario);
            else if(bandera==2)
              payload.setDnTotalplay(usuario);
            else
              payload.setAccountNo(usuario);
            ProcessResponse respuestaWS= bPELGetInfoBRM.process(payload);
            datos= respuestaWS.getDatosCliente().get(0); 
        }catch(Exception e){
            e.printStackTrace();
           System.out.println("Error al obtener los datos de la cuenta WS GETINFOBRM "+e.getMessage()+" Causa: "+e.getCause());
        }
        return (datos);
    }
}
