package com.totalplay.services;

import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceRef;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import portabilidad.ConsultaFolio;
import portabilidad.ConsultaFolioResponse;
import portabilidad.ConsultaPortabilidadResponse;

import portabilidad.ConsultaPortabilidad;
import portabilidad.InsertarPortabilidad;
import portabilidad.InsertarPortabilidadResponse;
import portabilidad.MTPPortabilidad;
import portabilidad.MTPPortabilidadService;
import portabilidad.PortabilidadVO;
import portabilidad.ResponseVO;


public final class Portabilidad {
    @WebServiceRef
    private static MTPPortabilidadService mTPPortabilidadService;
    static FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
    private static Logger logger = LogManager.getLogger("Portabilidad");
     
    public Portabilidad() {
        super();
    }
    
    public static ResponseVO insertaPortabilidad( InsertarPortabilidad params ){
        String rutaWS= fileProperty.getProperty("WSPORTABILIDAD");
        try {
             URL wsURL = new URL(rutaWS);
            mTPPortabilidadService = new MTPPortabilidadService(wsURL,  new QName("http://project1/", "MTPPortabilidadService"));
        } catch (MalformedURLException e) {
            logger.error("Error al consultar WS Portabilidad: "+ e.getMessage());
        }
        
        MTPPortabilidad mTPPortabilidad = mTPPortabilidadService.getMTPPortabilidadPort();         
        
        InsertarPortabilidadResponse respuesta= mTPPortabilidad.insertarPortabilidad(params);
        
        return(respuesta.getReturn());
    }
    public static List<PortabilidadVO> consultaPortabilidad(String numCuenta){
        String rutaWS= fileProperty.getProperty("WSPORTABILIDAD");
        try {
             URL wsURL = new URL(rutaWS);
            mTPPortabilidadService = new MTPPortabilidadService(wsURL,  new QName("http://project1/", "MTPPortabilidadService"));
        } catch (MalformedURLException e) {
            logger.error("Error al consultar WS Portabilidad: "+ e.getMessage());
        }
        MTPPortabilidad mTPPortabilidad = mTPPortabilidadService.getMTPPortabilidadPort();
        
        ConsultaPortabilidad parameters= new ConsultaPortabilidad();
        parameters.setNOCUENTA(numCuenta);
        ConsultaPortabilidadResponse respuesta = mTPPortabilidad.consultaPortabilidad(parameters);
        
        return(respuesta.getReturn().getPortaList());
    }
    public static PortabilidadVO consultaFolio(String numCuenta, String folio){
        String rutaWS= fileProperty.getProperty("WSPORTABILIDAD");
        try {
             URL wsURL = new URL(rutaWS);
            mTPPortabilidadService = new MTPPortabilidadService(wsURL,  new QName("http://project1/", "MTPPortabilidadService"));
        } catch (MalformedURLException e) {
            logger.error("Error al consultar WS Portabilidad: "+ e.getMessage());
        }
        MTPPortabilidad mTPPortabilidad = mTPPortabilidadService.getMTPPortabilidadPort();
        
        ConsultaFolio parameters= new ConsultaFolio();
        parameters.setNOCUENTA(numCuenta);
        parameters.setFOLIO(folio);
        ConsultaFolioResponse respuesta = mTPPortabilidad.consultaFolio(parameters);
        
        return(respuesta.getReturn().getPorta());
    }
}
