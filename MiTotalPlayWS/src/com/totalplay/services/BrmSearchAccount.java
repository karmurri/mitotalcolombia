package com.totalplay.services;

import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BPELgetInfoBRM;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Bpelgetinfobrm_client_ep;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Process;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse;

import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;

import java.net.URL;

import javax.xml.namespace.QName;

public class BrmSearchAccount {
    private static Bpelgetinfobrm_client_ep bpelgetinfobrm_client_ep=null;
    static FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);

    public BrmSearchAccount() {
        super();
    }
    public static ProcessResponse.ArrBRM getInfoClient(String numCuenta){
        ProcessResponse.ArrBRM result=null;
        String rutaWS= fileProperty.getProperty("DASHBOARD");

        try{
            URL wsURL = new URL(rutaWS);
          bpelgetinfobrm_client_ep = new Bpelgetinfobrm_client_ep(wsURL,
          new QName("http://xmlns.oracle.com/MiddlewareApplication/BrmSearchAccount/BPELgetInfoBRM",
                    "bpelgetinfobrm_client_ep"));
          BPELgetInfoBRM bPELgetInfoBRM = bpelgetinfobrm_client_ep.getBPELgetInfoBRM_pt();
          // Add your code to call the desired methods.
          Process payload= new Process();
          payload.setACCOUNTNO(numCuenta);
          
          ProcessResponse respuestaWS = bPELgetInfoBRM.process(payload);
           result=respuestaWS.getArrBRM();
        }catch(Exception e){
            e.printStackTrace();
        }
      return (result);
    }
}
