package com.totalplay.services;


import com.oracle.xmlns.clientcards.getclientcards.bpelgetclientcards.BPELGetClientCards;
import com.oracle.xmlns.clientcards.getclientcards.bpelgetclientcards.Bpelgetclientcards_client_ep;
import com.oracle.xmlns.clientcards.getclientcards.bpelgetclientcards.Process;
import com.oracle.xmlns.clientcards.getclientcards.bpelgetclientcards.ProcessResponse;

import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;

import java.net.URL;

import java.util.List;

import javax.xml.namespace.QName;


public class GetClientCards {
    private static Bpelgetclientcards_client_ep bpelgetclientcards_client_ep;
    static FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
    
    public GetClientCards() {
        super();
    }
    public static List<ProcessResponse.ClientCards.ClientCard> obtieneTarjetasCliente(String cuenta){
          List<ProcessResponse.ClientCards.ClientCard> respuesta=null;
          String rutaWS= fileProperty.getProperty("GETCLIENTCARDS");
          try{
              URL wsURL = new URL(rutaWS);
            bpelgetclientcards_client_ep = new Bpelgetclientcards_client_ep(wsURL,
          new QName("http://xmlns.oracle.com/ClientCards/GetClientCards/BPELGetClientCards",
                    "bpelgetclientcards_client_ep"));
            BPELGetClientCards getClientCard= bpelgetclientcards_client_ep.getBPELGetClientCards_pt();
            
            Process payload= new Process();
            payload.setAccountNo(cuenta);
            
            Process.UserPassIp userPass= new Process.UserPassIp();
            userPass.setIp( fileProperty.getProperty("GETCLIENTCARDS_IP") );
            userPass.setUserId( fileProperty.getProperty("GETCLIENTCARDS_ID") );
            userPass.setPassword( fileProperty.getProperty("GETCLIENTCARDS_DROWSSAP") );
            
            payload.setUserPassIp(userPass);
            
            ProcessResponse respuestaWS= getClientCard.process(payload);
            ProcessResponse.ClientCards tarjetaWS= respuestaWS.getClientCards();
              
            respuesta= tarjetaWS.getClientCard();
          }catch(Exception e){
              System.out.println(e.getMessage());
          }
       return(respuesta);     
      }
}
