package com.totalplay.services;

import com.oracle.xmlns.infopagosbaz.infopagosbaz.bpelinfopagosbaz.BPELInfoPagosBaz;
import com.oracle.xmlns.infopagosbaz.infopagosbaz.bpelinfopagosbaz.Bpelinfopagosbaz_client_ep;
import com.oracle.xmlns.infopagosbaz.infopagosbaz.bpelinfopagosbaz.Process;
import com.oracle.xmlns.infopagosbaz.infopagosbaz.bpelinfopagosbaz.ProcessResponse;

import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;

import java.net.URL;

import javax.xml.namespace.QName;

public class InfoPagosBaz {
    private static Bpelinfopagosbaz_client_ep bpelinfopagosbaz_client_ep;
    static FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
    public InfoPagosBaz() {
        super();
    }
    public static ProcessResponse getReferencias(String cuenta)
    {
        ProcessResponse respuestaWS=null;
        String rutaWS= fileProperty.getProperty("GETINFOPAGOSBAZ");
        try{
            URL wsURL = new URL(rutaWS);  
          bpelinfopagosbaz_client_ep = new Bpelinfopagosbaz_client_ep(wsURL,
          new QName("http://xmlns.oracle.com/InfoPagosBaz/InfoPagosBaz/BPELInfoPagosBaz",
                    "bpelinfopagosbaz_client_ep"));
          BPELInfoPagosBaz bPELInfoPagosBaz = bpelinfopagosbaz_client_ep.getBPELInfoPagosBaz_pt();
          // Add your code to call the desired methods.
          Process payload= new Process();
          payload.setAccountNo(cuenta);
                
         respuestaWS = bPELInfoPagosBaz.process(payload);
        }catch(Exception e){
            e.printStackTrace();
            
        }
      return respuestaWS;
    }
}
