package com.totalplay.vo;

public class DetalleRecoVO {
    private  String nombre;
    private  String fecha;
    private  String folio;
    
    public DetalleRecoVO() {
        super();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFolio() {
        return folio;
    }
}
