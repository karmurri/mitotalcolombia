package com.totalplay.vo;

import java.io.Serializable;

public class EstadoCuentaVO implements Serializable{
    @SuppressWarnings("compatibility:-1755015595019679255")
    private static final long serialVersionUID = 1L;
    private String saldoActual;
    private String totalPago;
    private String fechaLim;
    private String totalPagoDesc;
    private String fechaLimDesc;
    private String fechaCorte;
    
    private String estadoCuentaAnt;
    private String pagoRealizado;
    private String notasCred;
    private String totalEstadoActual;

    private String rentaServiciosTerceros;
    
    private String periodo;
    private String rentas;
    private String bonifica;
    private String consumoPeriodo;
    private String subtotal;
    
    private String statusCuenta;
    
    
    public EstadoCuentaVO() {
        super();
    }

    public void setSaldoActual(String saldoActual) {
        this.saldoActual = saldoActual;
    }

    public String getSaldoActual() {
        return saldoActual;
    }

    public void setTotalPago(String totalPago) {
        this.totalPago = totalPago;
    }

    public String getTotalPago() {
        return totalPago;
    }

    public void setFechaLim(String fechaLim) {
        this.fechaLim = fechaLim;
    }

    public String getFechaLim() {
        return fechaLim;
    }

    public void setTotalPagoDesc(String totalPagoDesc) {
        this.totalPagoDesc = totalPagoDesc;
    }

    public String getTotalPagoDesc() {
        return totalPagoDesc;
    }

    public void setFechaLimDesc(String fechaLimDesc) {
        this.fechaLimDesc = fechaLimDesc;
    }

    public String getFechaLimDesc() {
        return fechaLimDesc;
    }

    public void setFechaCorte(String fechaCorte) {
        this.fechaCorte = fechaCorte;
    }

    public String getFechaCorte() {
        return fechaCorte;
    }

    public void setEstadoCuentaAnt(String estadoCuentaAnt) {
        this.estadoCuentaAnt = estadoCuentaAnt;
    }

    public String getEstadoCuentaAnt() {
        return estadoCuentaAnt;
    }

    public void setPagoRealizado(String pagoRealizado) {
        this.pagoRealizado = pagoRealizado;
    }

    public String getPagoRealizado() {
        return pagoRealizado;
    }

    public void setNotasCred(String notasCred) {
        this.notasCred = notasCred;
    }

    public String getNotasCred() {
        return notasCred;
    }

    public void setTotalEstadoActual(String totalEstadoActual) {
        this.totalEstadoActual = totalEstadoActual;
    }

    public String getTotalEstadoActual() {
        return totalEstadoActual;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setRentas(String rentas) {
        this.rentas = rentas;
    }

    public String getRentas() {
        return rentas;
    }

    public void setBonifica(String bonifica) {
        this.bonifica = bonifica;
    }

    public String getBonifica() {
        return bonifica;
    }

    public void setConsumoPeriodo(String consumoPeriodo) {
        this.consumoPeriodo = consumoPeriodo;
    }

    public String getConsumoPeriodo() {
        return consumoPeriodo;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setStatusCuenta(String statusCuenta) {
        this.statusCuenta = statusCuenta;
    }

    public String getStatusCuenta() {
        return statusCuenta;
    }

    public void setRentaServiciosTerceros(String rentaServiciosTerceros) {
        this.rentaServiciosTerceros = rentaServiciosTerceros;
    }

    public String getRentaServiciosTerceros() {
        return rentaServiciosTerceros;
    }
}
