package com.totalplay.vo;

public class FacturaVO {
    private String fechaPago;
    private String monto;
    private String liga;
    public FacturaVO() {
        super();
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getMonto() {
        return monto;
    }

    public void setLiga(String liga) {
        this.liga = liga;
    }

    public String getLiga() {
        return liga;
    }
}
