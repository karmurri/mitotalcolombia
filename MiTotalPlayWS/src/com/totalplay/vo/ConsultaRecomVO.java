package com.totalplay.vo;

public class ConsultaRecomVO {
    int status;
    int numContactar;
    int numInstalar;
    int numNoExito;
    int numIlocalizable;
    int numinstalados;
    
    public ConsultaRecomVO() {
        super();
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setNumContactar(int numContactar) {
        this.numContactar = numContactar;
    }

    public int getNumContactar() {
        return numContactar;
    }

    public void setNumInstalar(int numInstalar) {
        this.numInstalar = numInstalar;
    }

    public int getNumInstalar() {
        return numInstalar;
    }

    public void setNumNoExito(int numNoExito) {
        this.numNoExito = numNoExito;
    }

    public int getNumNoExito() {
        return numNoExito;
    }

    public void setNumIlocalizable(int numIlocalizable) {
        this.numIlocalizable = numIlocalizable;
    }

    public int getNumIlocalizable() {
        return numIlocalizable;
    }

    public void setNuminstalados(int numinstalados) {
        this.numinstalados = numinstalados;
    }

    public int getNuminstalados() {
        return numinstalados;
    }
}
