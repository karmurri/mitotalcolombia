package com.totalplay.vo;

import java.io.Serializable;

public class RespuestaVO implements Serializable{
    private String result;
    private String descripcion;
    
    
    public RespuestaVO() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
