package com.totalplay.vo;

import java.io.Serializable;

public class DetalleCompraVO implements Serializable{
    @SuppressWarnings("compatibility:-1755015595019679255")
    private static final long serialVersionUID = 1L;
    private String montoMensual; //Amount
    private String total; //CreditAmount
    private String mesesFinanciados; //NumUnits
    private String nombre; //Descr
    private String NoContrato; //ContractNo
    
    public DetalleCompraVO() {
        super();
    }

    public void setMontoMensual(String montoMensual) {
        this.montoMensual = montoMensual;
    }

    public String getMontoMensual() {
        return montoMensual;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public void setMesesFinanciados(String mesesFinanciados) {
        this.mesesFinanciados = mesesFinanciados;
    }

    public String getMesesFinanciados() {
        return mesesFinanciados;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNoContrato(String NoContrato) {
        this.NoContrato = NoContrato;
    }

    public String getNoContrato() {
        return NoContrato;
    }
}
