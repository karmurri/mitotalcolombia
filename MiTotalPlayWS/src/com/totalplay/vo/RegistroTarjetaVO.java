package com.totalplay.vo;

public class RegistroTarjetaVO {
    private String cliente;
    private String numCuenta;
    private String numTarjeta;
    private String titular;
    private String banco;
    private String fechaVen;
    private String servicio;
    private String fechaSolCR;
    private String fechaCorte;
    private String fechaCobro;
    
    public RegistroTarjetaVO() {
        super();
        this.cliente=" ";
        this.numCuenta=" ";
        this.numTarjeta=" ";
        this.titular=" ";
        this.banco=" ";
        this.fechaVen=" ";
        this.servicio=" ";
        this.fechaSolCR=" ";
        this.fechaCorte=" ";
        this.fechaCobro=" ";
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getTitular() {
        return titular;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getBanco() {
        return banco;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setFechaSolCR(String fechaSolCR) {
        this.fechaSolCR = fechaSolCR;
    }

    public String getFechaSolCR() {
        return fechaSolCR;
    }

    public void setFechaCorte(String fechaCorte) {
        this.fechaCorte = fechaCorte;
    }

    public String getFechaCorte() {
        return fechaCorte;
    }

    public void setFechaCobro(String fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public String getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaVen(String fechaVen) {
        this.fechaVen = fechaVen;
    }

    public String getFechaVen() {
        return fechaVen;
    }
}
