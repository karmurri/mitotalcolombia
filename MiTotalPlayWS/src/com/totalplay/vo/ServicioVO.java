package com.totalplay.vo;

import java.util.List;

public class ServicioVO {
    private String tipoServicio;
    private String descripcion;
    private List<ProductoVO> productos;
    public ServicioVO() {
        super();
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }


    public void setProductos(List<ProductoVO> productos) {
        this.productos = productos;
    }

    public List<ProductoVO> getProductos() {
        return productos;
    }
}
