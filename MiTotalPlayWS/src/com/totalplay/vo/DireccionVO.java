package com.totalplay.vo;

import java.io.Serializable;

public class DireccionVO implements Serializable{
    @SuppressWarnings("compatibility:-3646781119500486671")
    private static final long serialVersionUID = 1L;
    private String billing_name;
    private String billing_rfc;
    private String billing_street_address;
    private String billing_xtnum;
    private String billing_intnum;
    private String billing_state;
    private String billing_suburb;
    private String billing_city;
    private String billing_postcode;
    private String billing_between1;
    private String billing_between2;
    public DireccionVO() {
        super();
         billing_name=" ";
         billing_rfc=" ";
         billing_street_address=" ";
         billing_xtnum=" ";
         billing_intnum=" ";
         billing_state=" ";
         billing_suburb=" ";
         billing_city=" ";
         billing_postcode=" ";
         billing_between1=" ";
         billing_between2=" ";
    }

    public void setBilling_name(String billing_name) {
        this.billing_name = billing_name;
    }

    public String getBilling_name() {
        return billing_name;
    }

    public void setBilling_rfc(String billing_rfc) {
        this.billing_rfc = billing_rfc;
    }

    public String getBilling_rfc() {
        return billing_rfc;
    }

    public void setBilling_street_address(String billing_street_address) {
        this.billing_street_address = billing_street_address;
    }

    public String getBilling_street_address() {
        return billing_street_address;
    }

    public void setBilling_xtnum(String billing_xtnum) {
        this.billing_xtnum = billing_xtnum;
    }

    public String getBilling_xtnum() {
        return billing_xtnum;
    }

    public void setBilling_intnum(String billing_intnum) {
        this.billing_intnum = billing_intnum;
    }

    public String getBilling_intnum() {
        return billing_intnum;
    }

    public void setBilling_state(String billing_state) {
        this.billing_state = billing_state;
    }

    public String getBilling_state() {
        return billing_state;
    }

    public void setBilling_suburb(String billing_suburb) {
        this.billing_suburb = billing_suburb;
    }

    public String getBilling_suburb() {
        return billing_suburb;
    }

    public void setBilling_city(String billing_city) {
        this.billing_city = billing_city;
    }

    public String getBilling_city() {
        return billing_city;
    }

    public void setBilling_postcode(String billing_postcode) {
        this.billing_postcode = billing_postcode;
    }

    public String getBilling_postcode() {
        return billing_postcode;
    }

    public void setBilling_between1(String billing_between1) {
        this.billing_between1 = billing_between1;
    }

    public String getBilling_between1() {
        return billing_between1;
    }

    public void setBilling_between2(String billing_between2) {
        this.billing_between2 = billing_between2;
    }

    public String getBilling_between2() {
        return billing_between2;
    }
    public String toString(){
        return(       
         billing_name+"\n"+
         billing_rfc+"\n"+
         billing_street_address+"\n"+
         billing_xtnum+"\n"+
         billing_intnum+"\n"+
         billing_state+"\n"+
         billing_suburb+"\n"+
         billing_city+"\n"+
         billing_postcode+"\n"+
         billing_between1+"\n"+
         billing_between2);
    }
}
