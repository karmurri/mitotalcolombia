package com.totalplay.vo;

import java.io.Serializable;

import java.util.List;


public class PlanVO implements Serializable{
    @SuppressWarnings("compatibility:-254492530448282307")
    private static final long serialVersionUID = 1L;
    private List<ServicioVO> servicios;
    private String precioLista;
    public PlanVO() {
        super();
    }
    
    public void setServicios(List<ServicioVO> servicios) {
        this.servicios = servicios;
    }

    public List<ServicioVO> getServicios() {
        return servicios;
    }

    public void setPrecioLista(String precioLista) {
        this.precioLista = precioLista;
    }

    public String getPrecioLista() {
        return precioLista;
    }

}
