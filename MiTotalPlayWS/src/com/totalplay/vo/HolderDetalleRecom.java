package com.totalplay.vo;

import java.util.ArrayList;

public class HolderDetalleRecom {
    int status;
    ArrayList<DetalleRecoVO> contactos= new ArrayList<DetalleRecoVO>();
    public HolderDetalleRecom() {
        super();
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setContactos(ArrayList<DetalleRecoVO> contactos) {
        this.contactos = contactos;
    }

    public ArrayList<DetalleRecoVO> getContactos() {
        return contactos;
    }
}
