package com.totalplay.vo;

import java.io.Serializable;

import java.util.List;


public class HistoricoFacturaVO implements Serializable{
    @SuppressWarnings("compatibility:-1338407409715639192")
    private static final long serialVersionUID = 1L;
    private List<FacturaVO> historicoFac;
    public HistoricoFacturaVO() {
        super();
    }

    public void setHistoricoFac(List<FacturaVO> historicoFac) {
        this.historicoFac = historicoFac;
    }

    public List<FacturaVO> getHistoricoFac() {
        return historicoFac;
    }
}
