package com.totalplay.vo;

public class ProductoVO {
    private String descripcionAdicional;
    public ProductoVO() {
        super();
    }

    public void setDescripcionAdicional(String descripcionAdicional) {
        this.descripcionAdicional = descripcionAdicional;
    }

    public String getDescripcionAdicional() {
        return descripcionAdicional;
    }
}
