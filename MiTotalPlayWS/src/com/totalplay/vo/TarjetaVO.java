package com.totalplay.vo;

public class TarjetaVO {
    private String bank;
    private String bankId;
    private String endingCardNumber;
    private String idCard;
    private String cardNumber;
    private String expirationYear;
    private String expirationMonth;
    private String idUser;
    private String fullName;
    private String status;
    private String razonStatus;
    private String provider;
    private String current;
    private String code;
    
    public TarjetaVO() {
        super();
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBank() {
        return bank;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setEndingCardNumber(String endingCardNumber) {
        this.endingCardNumber = endingCardNumber;
    }

    public String getEndingCardNumber() {
        return endingCardNumber;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setRazonStatus(String razonStatus) {
        this.razonStatus = razonStatus;
    }

    public String getRazonStatus() {
        return razonStatus;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getCurrent() {
        return current;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
}
