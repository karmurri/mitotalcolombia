
package com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Result">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ResultDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tokenExpityDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "result",
    "token",
    "tokenExpityDate"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "Result", required = true)
    protected ProcessResponse.Result result;
    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected String tokenExpityDate;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.Result }
     *     
     */
    public ProcessResponse.Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Result }
     *     
     */
    public void setResult(ProcessResponse.Result value) {
        this.result = value;
    }

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the tokenExpityDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenExpityDate() {
        return tokenExpityDate;
    }

    /**
     * Sets the value of the tokenExpityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenExpityDate(String value) {
        this.tokenExpityDate = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ResultDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idResult",
        "result",
        "resultDescription"
    })
    public static class Result {

        @XmlElement(name = "IdResult", required = true)
        protected String idResult;
        @XmlElement(name = "Result", required = true)
        protected String result;
        @XmlElement(name = "ResultDescription", required = true)
        protected String resultDescription;

        /**
         * Gets the value of the idResult property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdResult() {
            return idResult;
        }

        /**
         * Sets the value of the idResult property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdResult(String value) {
            this.idResult = value;
        }

        /**
         * Gets the value of the result property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResult() {
            return result;
        }

        /**
         * Sets the value of the result property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResult(String value) {
            this.result = value;
        }

        /**
         * Gets the value of the resultDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResultDescription() {
            return resultDescription;
        }

        /**
         * Sets the value of the resultDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResultDescription(String value) {
            this.resultDescription = value;
        }

    }

}
