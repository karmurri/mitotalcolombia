
package com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * Oracle JAX-WS 2.1.5
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "bpelgettokennetflixiptv_client_ep", targetNamespace = "http://soa.totalplay.com/Middleware/GeneraTokenNetflix/BPELGetTokenNetflixIPTV", wsdlLocation = "http://soabalanceada.totalplay.com.mx/soa-infra/services/TotalPlayProcessBPEL/GeneraTokenNetflix/bpelgettokennetflixiptv_client_ep?WSDL#%7Bhttp%3A%2F%2Fsoa.totalplay.com%2FMiddleware%2FGeneraTokenNetflix%2FBPELGetTokenNetflixIPTV%7Dbpelgettokennetflixiptv_client_ep")
public class BpelgettokennetflixiptvClientEp
    extends Service
{

    private final static URL BPELGETTOKENNETFLIXIPTVCLIENTEP_WSDL_LOCATION;
    private final static Logger logger = Logger.getLogger(com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv.BpelgettokennetflixiptvClientEp.class.getName());

    static {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv.BpelgettokennetflixiptvClientEp.class.getResource(".");
            url = new URL(baseUrl, "http://soabalanceada.totalplay.com.mx/soa-infra/services/TotalPlayProcessBPEL/GeneraTokenNetflix/bpelgettokennetflixiptv_client_ep?WSDL#%7Bhttp%3A%2F%2Fsoa.totalplay.com%2FMiddleware%2FGeneraTokenNetflix%2FBPELGetTokenNetflixIPTV%7Dbpelgettokennetflixiptv_client_ep");
        } catch (MalformedURLException e) {
            logger.warning("Failed to create URL for the wsdl Location: 'http://soabalanceada.totalplay.com.mx/soa-infra/services/TotalPlayProcessBPEL/GeneraTokenNetflix/bpelgettokennetflixiptv_client_ep?WSDL#%7Bhttp%3A%2F%2Fsoa.totalplay.com%2FMiddleware%2FGeneraTokenNetflix%2FBPELGetTokenNetflixIPTV%7Dbpelgettokennetflixiptv_client_ep', retrying as a local file");
            logger.warning(e.getMessage());
        }
        BPELGETTOKENNETFLIXIPTVCLIENTEP_WSDL_LOCATION = url;
    }

    public BpelgettokennetflixiptvClientEp(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public BpelgettokennetflixiptvClientEp() {
        super(BPELGETTOKENNETFLIXIPTVCLIENTEP_WSDL_LOCATION, new QName("http://soa.totalplay.com/Middleware/GeneraTokenNetflix/BPELGetTokenNetflixIPTV", "bpelgettokennetflixiptv_client_ep"));
    }

    /**
     * 
     * @return
     *     returns BPELGetTokenNetflixIPTV
     */
    @WebEndpoint(name = "BPELGetTokenNetflixIPTV_pt")
    public BPELGetTokenNetflixIPTV getBPELGetTokenNetflixIPTVPt() {
        return super.getPort(new QName("http://soa.totalplay.com/Middleware/GeneraTokenNetflix/BPELGetTokenNetflixIPTV", "BPELGetTokenNetflixIPTV_pt"), BPELGetTokenNetflixIPTV.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns BPELGetTokenNetflixIPTV
     */
    @WebEndpoint(name = "BPELGetTokenNetflixIPTV_pt")
    public BPELGetTokenNetflixIPTV getBPELGetTokenNetflixIPTVPt(WebServiceFeature... features) {
        return super.getPort(new QName("http://soa.totalplay.com/Middleware/GeneraTokenNetflix/BPELGetTokenNetflixIPTV", "BPELGetTokenNetflixIPTV_pt"), BPELGetTokenNetflixIPTV.class, features);
    }

}
