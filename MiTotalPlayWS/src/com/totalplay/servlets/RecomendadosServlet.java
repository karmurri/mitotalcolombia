package com.totalplay.servlets;

import com.totalplay.utils.FileProperty;

import encrypt.DecryptJSON;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RecomendadosServlet extends HttpServlet {
    private static final String CONTENT_TYPE = "application/json";
    @SuppressWarnings("compatibility:3160771019153168651")
    private static final long serialVersionUID = 1L;
    Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        HttpSession sesion = request.getSession(false);
        String numCuenta = (String)sesion.getAttribute("numCuenta");
        String[] datos = new String[3];
        String data = request.getParameter("data");
        if (data != null) {
            datos = DecryptJSON.DecryptJSONRecomendado(data);
        }
        String nombre = datos[0];
        String tel = datos[1];
        String email = datos[2];
        PrintWriter out = response.getWriter();
        FileProperty fileProp = new FileProperty("com.totalplay.prop.Recom");


        if (nombre != null && tel != null) {
            try {
                String URLServicio = fileProp.getProperty("guardarProspecto");
                HttpResponse respon = null;
                HttpPost post = new HttpPost(URLServicio);

                JSONObject jsonLogin = new JSONObject();
                JSONObject jsonDatos = new JSONObject();
                JSONArray jsonarrayL = new JSONArray();

                if (post != null) {
                    HttpClient httpclient = new DefaultHttpClient();
                    try {
                        jsonLogin.put("User", fileProp.getProperty("usr"));
                        jsonLogin.put("Password", fileProp.getProperty("pwd"));
                        jsonLogin.put("Ip", fileProp.getProperty("ip"));
                        jsonarrayL.put(jsonLogin);

                        jsonDatos.put("Login", jsonarrayL);
                        jsonDatos.put("canalReferido", "MiTotalplay");
                        jsonDatos.put("subCanalReferido", "Cliente Referido");
                        jsonDatos.put("codigoPostal", "");
                        jsonDatos.put("cuentaReferente", numCuenta);
                        jsonDatos.put("email", email);
                        jsonDatos.put("estatus", "Candidato");
                        jsonDatos.put("nombre", nombre);
                        jsonDatos.put("IdTecnico", "");
                        jsonDatos.put("IdAuxiliar", "");
                        jsonDatos.put("OTFFM", "");
                        jsonDatos.put("origen", "Call Center BTL");
                        jsonDatos.put("telefono", tel);
                    } catch (JSONException ex) {
                        logger.error("No se puede generar json " + ex.getMessage());
                    }

                    post.setHeader("Content-type", "application/json");
                    post.setHeader("Authorization", "Basic ZmZtYXBwOjRnZW5kNG1pM250bw==");
                    post.setHeader("cache-control", "no-cache");
                    post.setHeader("Content-Type", "application/json; charset=UTF-8");
                    post.setHeader("Accept", "application/json");

                    try {
                        StringEntity jsonFinal = new StringEntity(new String(jsonDatos.toString().getBytes(), "UTF-8"));
                        post.setEntity(jsonFinal);
                        respon = httpclient.execute(post);
                        String body = EntityUtils.toString(respon.getEntity(), "UTF-8");
                        JSONObject jsons = new JSONObject(body);
                        String resultRespone = jsons.get("result").toString();
                        String resultDescrip = jsons.get("resultDescription").toString();

                        if (respon != null) {
                            if (resultRespone.equalsIgnoreCase("0")) {
                                out.print("{\"status\": 0 }");
                            } else if (resultRespone.equalsIgnoreCase("1") && resultDescrip.contains("existe")) {
                                out.print("{\"status\": 3 }");
                            } else {
                                out.print("{\"status\": 1 }");
                            }
                        } else {
                            out.print("{\"status\": 2 }");
                        }
                    } catch (Exception ex) {
                        logger.error("No se puede establecer la conexi�n " + ex.getMessage());
                        out.print("{\"status\": -3 }");
                    } finally {
                        cerrar(httpclient);
                    }
                } else {
                    out.print("{\"status\": 4 }");
                }
            } catch (Exception e) {
                this.logger.error("Error al guardar los datos del lead " + e.getMessage());
                out.print("{\"status\": -2 }");
            }
        } else {
            out.print("{\"status\": -1 }");
        }

        out.close();
    }
    
    public void cerrar(HttpClient httpclient){
       try{
           if(httpclient!=null){
               httpclient.getConnectionManager().shutdown();
           }
       }catch(Exception ex){
           logger.error("no se pudo cerrar el cliente");
       }
   }
}
