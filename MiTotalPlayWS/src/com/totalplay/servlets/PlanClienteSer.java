package com.totalplay.servlets;


import com.totalplay.services.BrmSearchAccount;
import com.totalplay.vo.PlanVO;
import com.totalplay.vo.ProductoVO;
import com.totalplay.vo.ServicioVO;

import java.io.IOException;
import java.io.PrintWriter;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PlanClienteSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request,response);
    }

    public void doPost(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        response.setContentType(CONTENT_TYPE);
        DecimalFormat formatoMoneda = new DecimalFormat("$#,##0.00 MXN");
        try{
       // logger.info("Inicia ejecucion PlanClienteSer");
        HttpSession sesion= request.getSession(false);
        String numCuenta= (String)sesion.getAttribute("numCuenta");
        int contProd=0;
        //hardcode
        //numCuenta="1.5127525";
        
        String tipoMoneda= (String)sesion.getAttribute("tipoMoneda")!=null ?(String)sesion.getAttribute("tipoMoneda"):"";
        if(tipoMoneda!="" && !tipoMoneda.equals("484")){
            formatoMoneda = new DecimalFormat("$#,##0.00 USD");
        }
        
        com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM objArrBRM = BrmSearchAccount.getInfoClient(numCuenta);
            
            
        //informaci�n del plan contratado y los servicios incluidos
        PlanVO infoPlan= new PlanVO();
        List<ServicioVO> serviciosPlan= new ArrayList<ServicioVO>();
        com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.ArrServices serviciosWS = objArrBRM.getArrServices();
        for(com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.ArrServices.Servicio servicio: serviciosWS.getServicio()){
            contProd=0;
            ServicioVO servicioVo= new ServicioVO();
                if(servicio.getService().contains("tv"))
                    servicioVo.setTipoServicio("Televisi�n");
                else if (servicio.getService().contains("ip"))
                    servicioVo.setTipoServicio("Internet");
                else if(servicio.getService().contains("telephony"))
                    servicioVo.setTipoServicio("Tel�fono");
            servicioVo.setDescripcion(servicio.getPackageDescription());
            if(servicio.getArrProduct()!=null){
                List<ProductoVO> productos= new ArrayList<ProductoVO>();
                for (com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct.Product product: servicio.getArrProduct().getProduct()){
                    ProductoVO productoVo= new ProductoVO();
                    contProd++;
                    if(contProd==servicio.getArrProduct().getProduct().size())
                        productoVo.setDescripcionAdicional(product.getAdditionalDescription());
                    else
                        productoVo.setDescripcionAdicional(product.getAdditionalDescription()+",");
                    productos.add(productoVo);
                }
                servicioVo.setProductos(productos);
            }
            serviciosPlan.add(servicioVo);
        }
        infoPlan.setServicios(serviciosPlan);
            infoPlan.setPrecioLista(formatoMoneda.format(objArrBRM.getBillInfo().getLastInvoiceSummary().getRent()!=null?objArrBRM.getBillInfo().getLastInvoiceSummary().getRent():new Double("0.0")));
        sesion.setAttribute("infoPlan", infoPlan);
       // response.sendRedirect("jsp/planclientevista.jsp"); 
        request.getRequestDispatcher("jsp/miplancontratado.jsp").forward(request, response);
        }catch(Exception e){
            logger.error("Error al consultar el plan contratado por el cliente: "+e.getMessage()+" Origen de error en: "+e.getStackTrace()[0]);
        }
    }
}
