package com.totalplay.servlets;


import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.BPELObtenerCorreo;
import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Bpelobtenercorreo_client_ep;

import com.totalplay.services.EngineSecurity;
import com.totalplay.services.GetInfoBRM;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.EmailUtility;
import com.totalplay.utils.FileProperty;
import com.totalplay.utils.ProcesaHtml;
import com.totalplay.utils.Rands;
import com.totalplay.utils.VerificaUsuario;

import java.io.IOException;
import java.io.PrintWriter;

import java.net.URL;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import org.tempuri.SecurityUserModel;


public class RememberSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    Logger logger = LogManager.getLogger(this.getClass().getName());
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request,response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
       // logger.info("-----Entro a RememberSer----");
       response.setContentType(CONTENT_TYPE);
       request.setCharacterEncoding("UTF-8");
        
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
        HttpSession sesion= request.getSession(false);
        PrintWriter out = response.getWriter(); 
        /*EmailUtility wsEmail= new EmailUtility();
        
        String numCuenta=null;
        String parametroCuenta=request.getParameter("username");
        int bandera=0;
        if(VerificaUsuario.isNumCuentaVarios(parametroCuenta)){
            bandera=0;
        }else if(VerificaUsuario.isNumTelefono(parametroCuenta)){
            bandera=2;
        }
        com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.ProcessResponse.DatosCliente datos = GetInfoBRM.obtieneCuenta(parametroCuenta, bandera);
        numCuenta=datos.getAccountNo();
        SecurityUserModel infoUser = EngineSecurity.getNoAccount(numCuenta);
            
        String password = Rands.generaPassword(12);
        String title="";
        String msj="";
       /* logger.info(infoUser.getUserId());
        logger.info(infoUser);*/
       /*try{
           String rutaWS= fileProperty.getProperty("OBTIENECORREOFACT");
            URL wsURL= new URL(rutaWS);
           Bpelobtenercorreo_client_ep bpelobtenercorreo_client_ep = new Bpelobtenercorreo_client_ep(wsURL,
           new QName("http://xmlns.oracle.com/SeguridadBRM/ObtenerCorreo/BPELObtenerCorreo",
                    "bpelobtenercorreo_client_ep"));
          BPELObtenerCorreo bPELObtenerCorreo = bpelobtenercorreo_client_ep.getBPELObtenerCorreo_pt();
          
          com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process paramCorreo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process();
          com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin authCorreo=  new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin();
          authCorreo.setIp("0.0.0.1");
          authCorreo.setUserId("452710");
          authCorreo.setPassword("Portales789$");
          paramCorreo.setApplicationLogin(authCorreo);
          
          com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo paramClientInfo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo();
          paramClientInfo.setAccountOrDn(numCuenta);
          paramCorreo.setClientInfo(paramClientInfo);
          
          com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.ProcessResponse respCorreo= bPELObtenerCorreo.process(paramCorreo);

          if(respCorreo.getResult().equals("0")){ 
             infoUser.setEmail(respCorreo.getEmail());
          }
       }catch(Exception e){
           e.printStackTrace();
           logger.error("Error al consultar servicio de obtenci�n de correo: "+e.getMessage()+ " causa: "+e.getCause());
       }
        
        
        if (infoUser.getUserId()!=null){
            if(infoUser.getEmail()!=null){
                String ruta= this.getServletContext().getRealPath("/assets/mail/password.htm");
                ruta=ruta!=null?ruta:getServletContext().getResource("/assets/mail/password.htm").getPath();

               int result = EngineSecurity.updateInfoClient(infoUser.getUserId(), numCuenta, numCuenta, 
                                 infoUser.getFirstName(), infoUser.getLastName(), infoUser.getEmail(), password);
              // System.out.println("Password: "+password);
               if(result==0){
               //hardcode
               // infoUser.setEmail("cfloresc@elektra.com.mx");
                String bodyEmail = ProcesaHtml.creaCadenaHTML(ruta,infoUser.getFirstName()+" "+infoUser.getLastName(),infoUser.getEmployeeNumber(),password);
                boolean emailResul= wsEmail.sendEmail(infoUser.getEmail(), Constantes.EMAILCC, Constantes.EMAILBCC, Constantes.REPLY_TO, Constantes.SUBJECT, bodyEmail);
                    if(emailResul==true){
                         out.print(EmailUtility.paddingMail(infoUser.getEmail())); 
                       /* title= "�Listo!";
                        msj= "Te enviamos una contrase�a temporal al correo electr�nico que tienes registrado, es necesario cambiarla una vez que ingreses al portal de Mi Cuenta Totalplay.";
                        request.setAttribute("title", title);
                        request.setAttribute("msj", msj);
                         RequestDispatcher despachador =
                             getServletContext().getRequestDispatcher("/jsp/accountCreated.jsp");
                         despachador.include(request, response);*/
                       
                     /*}
                     else{
                         out.print("failcorreo");
                        /* title= "�Error!";
                         msj= "Ocurri� un error al actualizar la contrase�a dela cuenta ingresada.";
                         request.setAttribute("title", title);
                         request.setAttribute("msj", msj);
                          RequestDispatcher despachador =
                              getServletContext().getRequestDispatcher("/jsp/accountCreated.jsp");
                          despachador.include(request, response);*/
                     /*}
                }
                else{
                   out.print("fail");
                }
            }
        }
        else{
           // logger.info("No se encontr� la cuenta");
            out.print("notfound");
          
        }
           // out.close();

        }catch(Exception e){
            logger.error("Error: "+e.getMessage());
        }*/
        out.print("fail");
    }
}
