package com.totalplay.servlets;


import com.totalplay.utils.Constantes;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ValCaptSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    Logger logger = LogManager.getLogger(this.getClass().getName());
    

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doGet(request, response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
       // logger.info("Inicia ejecución de servlet ValidaCaptcha");
             
            
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        
        try{
            String remoteAddr = request.getRemoteAddr();
        
            ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
            reCaptcha.setPrivateKey(Constantes.PRIVATE_KEY_RECAPTCHA);

            String challenge = request.getParameter("recaptcha_challenge_field");
            String uresponse = request.getParameter("recaptcha_response_field");
           
            ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(
                            remoteAddr, challenge, uresponse);
            if (reCaptchaResponse.isValid()) {
               // logger.info("recaptcha valido");
                    out.print("success");
            } else {
                
                logger.error("Error captcha: "+reCaptchaResponse.getErrorMessage());
                out.print("fail");
            }
            out.close();
        }catch(Exception e){
            logger.error("Error al validar Recaptcha: "+e.getMessage());
        }
    }
}
