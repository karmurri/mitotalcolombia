package com.totalplay.servlets;


import ConsultaSaldo.BPELProcess1_ptClient;

import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.ArrAddress;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.ArrAddress.Address;
import com.oracle.xmlns.salesforce11g.infocuenta.infocuenta.InfoCuenta;
import com.oracle.xmlns.salesforce11g.infocuenta.infocuenta.Infocuenta_client_ep;
import com.oracle.xmlns.salesforce11g.infocuenta.infocuenta.Process;

import com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.BpeldetallecompraivrClientEp;
import com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.ProcessResponse;

import com.totalplay.services.BrmSearchAccount;
import com.totalplay.services.ConsultaSaldoBRM;
import com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.BPELDetalleCompraIVR;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.DateTime;
import com.totalplay.utils.FileProperty;
import com.totalplay.vo.DireccionVO;
import com.totalplay.vo.EstadoCuentaVO;
import com.totalplay.vo.DetalleCompraVO;

import java.io.IOException;

import java.math.BigDecimal;

import java.net.URL;

import java.text.DecimalFormat;

import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class InfoClienteSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    @SuppressWarnings("compatibility:-3682375780556269327")
    private static final long serialVersionUID = 1L;
    private Logger logger = LogManager.getLogger(this.getClass().getName());


    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request,response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
        response.setContentType(CONTENT_TYPE);
        request.setCharacterEncoding("UTF-8");
        DecimalFormat formatoMoneda = new DecimalFormat("$#,##0.00 MXN");
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);

        HttpSession sesion= request.getSession(false);
        
        com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.BillInfo infoCuenta;
        
        String numCuenta = (String)sesion.getAttribute("numCuenta");
//        String numCuenta=usuario.getEmployeenumber();   
       
        
            //Obtener información del tipo de moneda asociada a la cuenta
        try{
            String rutaWS= fileProperty.getProperty("GETTIPOMONEDA");
             URL wsURL= new URL(rutaWS);
            Infocuenta_client_ep infocuenta_client_ep = new Infocuenta_client_ep(wsURL, new QName("http://xmlns.oracle.com/SalesForce11g/InfoCuenta/InfoCuenta",
                    "infocuenta_client_ep"));
            InfoCuenta infoCuentaMoneda = infocuenta_client_ep.getInfoCuenta_pt();
            
            com.oracle.xmlns.salesforce11g.infocuenta.infocuenta.Process payload= new com.oracle.xmlns.salesforce11g.infocuenta.infocuenta.Process();
            Process.Login login= new Process.Login();
            login.setIp( fileProperty.getProperty("GETTIPOMONEDA_IP") );//login.setIp("1.1.1.1");
            login.setPassword( fileProperty.getProperty("GETTIPOMONEDA_PWD") );//login.setPassword(Constantes.P_USD);
            login.setUserId( fileProperty.getProperty("GETTIPOMONEDA_USR") );//login.setUserId(Constantes.U_USD);
                      
            payload.setLogin(login);
            payload.setAcountNo(numCuenta);
            
            com.oracle.xmlns.salesforce11g.infocuenta.infocuenta.ProcessResponse respMoneda= infoCuentaMoneda.process(payload);
            if(respMoneda.getResult().trim().equals("0"))
            {
            logger.error("getCurrency: " + respMoneda.getAcountDesc().getCurrency());
                if(!respMoneda.getAcountDesc().getCurrency().equals("484")){
                    formatoMoneda = new DecimalFormat("$#,##0.00 USD");
                }
            }else{
                logger.error("Respuesta servicio tipo de moneda asociada a la cuenta: "+respMoneda.getResultDes());
            }
            sesion.setAttribute("tipoMoneda", respMoneda.getAcountDesc().getCurrency());
            
        }catch(Exception e){
            logger.error("Error al consultar tipo de moneda asociada a la cuenta: "+ e.getMessage());
        }

        try{
            ArrBRM objArrBRM = BrmSearchAccount.getInfoClient(numCuenta);
            infoCuenta= objArrBRM.getBillInfo();
            
            //información del último estado de cuenta  
            com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.BillInfo.LastBillBalance ultBalance= infoCuenta.getLastBillBalance(); 
            EstadoCuentaVO balance= new EstadoCuentaVO();
            DetalleCompraVO detalleDeCompra = new DetalleCompraVO();
            
            //Obtención de saldo actual ajustado
            BPELProcess1_ptClient consultaEstadoSaldo= new BPELProcess1_ptClient();
            BigDecimal saldoActual= BigDecimal.valueOf(Double.parseDouble(ConsultaSaldoBRM.obtieneSaldoActualReal(numCuenta)));
            
            if(ultBalance!=null){
                try{ 
                    //Saldo actual obtenido de WS dashboard
                    //balance.setSaldoActual(formatoMoneda.format(ultBalance.getCurrentBilling()));
                    balance.setSaldoActual(formatoMoneda.format(saldoActual));
                    balance.setTotalPago(formatoMoneda.format(ultBalance.getTotalPay()));
                    balance.setFechaLim(DateTime.convierteFechaSimpleVista(ultBalance.getEndT()));
                    balance.setTotalPagoDesc(formatoMoneda.format(ultBalance.getTotalPaydiscount()));
                    balance.setFechaLimDesc(DateTime.convierteFechaSimpleVista(ultBalance.getEndTDiscount()));
                    balance.setFechaCorte(DateTime.convierteFechaSimpleVista(ultBalance.getCourtT()));
                }catch(Exception e){
                    sesion.removeAttribute("balance");
                }
            }
            //información del resumen del estado de cuenta del cliente
            
            try{
                com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.BillInfo.SummaryStatement estadoCuenta= infoCuenta.getSummaryStatement();
                balance.setEstadoCuentaAnt(formatoMoneda.format(estadoCuenta.getPreviiousBill()));
                balance.setPagoRealizado(formatoMoneda.format(estadoCuenta.getPayment()));
                balance.setNotasCred(formatoMoneda.format(estadoCuenta.getCreditNote()));
                balance.setTotalEstadoActual(formatoMoneda.format(estadoCuenta.getTotalPay()));
            }catch(IllegalArgumentException e){
                sesion.removeAttribute("balance");
            }
            //información del último estado de cuenta
            com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.BillInfo.LastInvoiceSummary ultResumen=infoCuenta.getLastInvoiceSummary();
            if(ultBalance !=null && ultBalance.getEndT()!=null){
                try{
                    String []convertirfecha=ultResumen.getPeriod().split("-");
                    String fecha1 = DateTime.convierteFechaSimpleVista(convertirfecha[0]);
                    String fecha2 = DateTime.convierteFechaSimpleVista(convertirfecha[1]);
                    balance.setPeriodo(fecha1+" al "+fecha2);
                    //balance.setPeriodo(ultResumen.getPeriod().replace("-", " al "));
                    balance.setRentas(formatoMoneda.format(ultResumen.getRent()));
                    balance.setBonifica(formatoMoneda.format(ultResumen.getBonosesPromotion()));
                    balance.setConsumoPeriodo(formatoMoneda.format(ultResumen.getPeriodConsum()));
                    balance.setSubtotal(formatoMoneda.format(ultResumen.getSubtotal()));
                }catch(IllegalArgumentException e){
                   
                    sesion.removeAttribute("balance");
                }
                catch(NullPointerException e){
                    e.printStackTrace();
                    sesion.removeAttribute("balance");
                }
                //inicia información para servicios rentados sobre IPTV    
                double suma = 0.00;
                try{
                    
                    String rutaWSDC= fileProperty.getProperty("GETDETALLECOMPRA");
                     URL wsURLDC= new URL(rutaWSDC);
                    
                    BpeldetallecompraivrClientEp bpel = new BpeldetallecompraivrClientEp(wsURLDC,new QName("http://soa.totalplay.com/TiendasTotalPlay/DetalleCompraIVR/BPELDetalleCompraIVR",
                                                                                         "bpeldetallecompraivr_client_ep"));
                    BPELDetalleCompraIVR detalleCompra = bpel.getBPELDetalleCompraIVRPt();
                    
                    com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.Process payloadDetalle = new com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.Process();
                    com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.Process.Login loginDetalle = new com.totalplay.soa.tiendastotalplay.detallecompraivr.bpeldetallecompraivr.Process.Login();
                    
                    loginDetalle.setIP(fileProperty.getProperty("GETTIPOMONEDA_IP"));
                    loginDetalle.setPassword(fileProperty.getProperty("GETTIPOMONEDA_PWD"));
                    loginDetalle.setUser(fileProperty.getProperty("GETTIPOMONEDA_USR"));
                    
                    payloadDetalle.setLogin(loginDetalle);
                    payloadDetalle.setACCOUNTNO(numCuenta);
                    
                    ProcessResponse responseDetalleCompra = detalleCompra.process(payloadDetalle);
                                        
                    //Encontro una cuenta
                    if(responseDetalleCompra.getResult().getResult().equals("0")){
                        String descripcionPeticion = responseDetalleCompra.getResult().getResultDescription();
                        if(descripcionPeticion.startsWith("Petición")){
                            for(ProcessResponse.Detalles.DetalleCompra listaDetallecompra : responseDetalleCompra.getDetalles().getDetalleCompra()){
                                suma += Double.parseDouble(listaDetallecompra.getAmount());
                                detalleDeCompra.setMontoMensual(listaDetallecompra.getAmount());
                                detalleDeCompra.setTotal(listaDetallecompra.getCreditAmount());
                                detalleDeCompra.setMesesFinanciados(listaDetallecompra.getNumUnits());
                                detalleDeCompra.setNombre(listaDetallecompra.getDescr());
                                detalleDeCompra.setNoContrato(listaDetallecompra.getContractNo());                                
                            }
                            logger.error("--MONTO MENSUAL: "+suma+"--");
                        }else{
                            logger.error("--NO TIENE COMPRAS"+suma);
                        }
                        balance.setRentaServiciosTerceros(formatoMoneda.format(suma));
                    }
                }catch(Exception e){
                    logger.error("--ERROR al consultar detalle de compra: "+e.getMessage());
                    balance.setRentaServiciosTerceros(formatoMoneda.format(suma));
                }
                //termina información para servicios rentados sobre IPTV
                
                if(Double.parseDouble(saldoActual.toString())<=0){
                    balance.setStatusCuenta("sinsaldo");
                }else{
                    if(!ultBalance.getEndTDiscount().equals("") && !ultBalance.getEndT().equals("")){
                        Date prontoPago = DateTime.convierteFechaSimple(ultBalance.getEndTDiscount());
                        Date vencimiento = DateTime.convierteFechaSimple(ultBalance.getEndT());
                        if(DateTime.obtieneFechaActual("dd/MM/yyyy").before(prontoPago)){
                            balance.setStatusCuenta("prontopago");
                        }
                        else if(DateTime.obtieneFechaActual("dd/MM/yyyy").after(vencimiento)){
                            balance.setStatusCuenta("moroso");
                        }
                        else {
                            balance.setStatusCuenta("limitepago");
                        }
                    }
                }
            }
            //obtener información de la dirección de instalación del cliente
            DireccionVO direccion=getInstalaAddr(objArrBRM);
            DireccionVO dirFiscal=getAddrFiscal(objArrBRM);
            //información del Plan del Cliente

            //información de las facturas        
          
            //obtener tarjetas ya registradas del cliente 
            
            
            
            
            //Obtener referencias bancarias
            if(direccion!=null)
                sesion.setAttribute("direccion", direccion);
            if(dirFiscal!=null)
                sesion.setAttribute("dirFiscal", dirFiscal);
            sesion.setAttribute("numCuenta",numCuenta);                   
            sesion.setAttribute("balance",balance );
            sesion.setAttribute("detalleCompra", detalleDeCompra);
           // sesion.setAttribute("infoPlan", infoPlan);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al obtener informacion del usuario: "+e.getMessage());
        }  
      
        /*RequestDispatcher despachador =
        getServletContext().getRequestDispatcher("/jsp/EstadoCta.jsp");
        despachador.include(request, response);
        */        
        
        request.getRequestDispatcher("facturasser.html").forward(request, response);
        
    }
    private DireccionVO getInstalaAddr( ArrBRM objArrBRM){
        ArrAddress direcciones= objArrBRM.getArrAddress();
        DireccionVO direcUser= null;
        if(direcciones!=null){
            direcUser=new DireccionVO();
            for(Address dir: direcciones.getAddress()){
                if(dir.getIdDescription().equalsIgnoreCase("Instalacion")){
                    direcUser.setBilling_name(!(dir.getRazonSocial().equals(""))?dir.getRazonSocial():dir.getFirstName()+" "+dir.getMiddleName()+" "+dir.getLastName());
                    direcUser.setBilling_rfc((!dir.getRFC().equals(" ") && !dir.getRFC().isEmpty())?dir.getRFC():" ");
                    direcUser.setBilling_street_address((!dir.getStreet().equals(" ") && !dir.getStreet().isEmpty())?dir.getStreet():" ");
                    direcUser.setBilling_xtnum((!dir.getNumExterior().equals(" ") && !dir.getNumExterior().isEmpty())?dir.getNumExterior():" ");
                    direcUser.setBilling_intnum((!dir.getNumInterior().equals(" ") && !dir.getNumInterior().isEmpty())?dir.getNumInterior():" ");
                    direcUser.setBilling_state((!dir.getState().equals(" ") && !dir.getState().isEmpty())?dir.getState():" ");
                    direcUser.setBilling_suburb((!dir.getColonia().equals(" ") && !dir.getColonia().isEmpty())?dir.getColonia():" ");
                    direcUser.setBilling_city((!dir.getCity().equals(" ") && !dir.getCity().isEmpty())?dir.getCity():" ");
                    direcUser.setBilling_postcode((!dir.getZip().equals(" ") && !dir.getZip().isEmpty())?dir.getZip():" ");
                    direcUser.setBilling_between1((!dir.getEntreCalles().equals(" ") && !dir.getEntreCalles().isEmpty())?dir.getEntreCalles():" ");
                    direcUser.setBilling_between2((!dir.getDelegacion().equals(" ") && !dir.getDelegacion().isEmpty())?dir.getDelegacion():" ");
                }
            }
        }
        return direcUser;
    }
    private DireccionVO getAddrFiscal(ArrBRM objArrBRM){
        ArrAddress direcciones= objArrBRM.getArrAddress();
        DireccionVO direcUser= null;
        if(direcciones!=null){
            direcUser=new DireccionVO();
            for(Address dir: direcciones.getAddress()){
                if(dir.getIdDescription().equalsIgnoreCase("Fiscal")){
                    direcUser.setBilling_name(!(dir.getRazonSocial().equals(""))?dir.getRazonSocial():dir.getFirstName()+" "+dir.getMiddleName()+" "+dir.getLastName());
                    direcUser.setBilling_rfc((!dir.getRFC().equals(" ") && !dir.getRFC().isEmpty())?dir.getRFC():" ");
                    direcUser.setBilling_street_address((!dir.getStreet().equals(" ") && !dir.getStreet().isEmpty())?dir.getStreet():" ");
                    direcUser.setBilling_xtnum((!dir.getNumExterior().equals(" ") && !dir.getNumExterior().isEmpty())?dir.getNumExterior():" ");
                    direcUser.setBilling_intnum((!dir.getNumInterior().equals(" ") && !dir.getNumInterior().isEmpty())?dir.getNumInterior():" ");
                    direcUser.setBilling_state((!dir.getState().equals(" ") && !dir.getState().isEmpty())?dir.getState():" ");
                    direcUser.setBilling_suburb((!dir.getColonia().equals(" ") && !dir.getColonia().isEmpty())?dir.getColonia():" ");
                    direcUser.setBilling_city((!dir.getCity().equals(" ") && !dir.getCity().isEmpty())?dir.getCity():" ");
                    direcUser.setBilling_postcode((!dir.getZip().equals(" ") && !dir.getZip().isEmpty())?dir.getZip():" ");
                    direcUser.setBilling_between1((!dir.getEntreCalles().equals(" ") && !dir.getEntreCalles().isEmpty())?dir.getEntreCalles():" ");
                    direcUser.setBilling_between2((!dir.getDelegacion().equals(" ") && !dir.getDelegacion().isEmpty())?dir.getDelegacion():" ");
                }
            }
        }
        return direcUser;
    }
}
