package com.totalplay.servlets;

import com.bea.wls.ejbgen.support.PrintWriterFactory;

import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;

import java.io.IOException;
import java.io.PrintWriter;

import java.net.URL;

import javax.servlet.*;
import javax.servlet.http.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;

import org.json.JSONObject;
/*
import project1.BinVO;
import project1.ConsultaBines;
import project1.ConsultaBinesResponse;
import project1.MiTotalplay;
import project1.MiTotalplayService;
*/
/**
 * Servlet que consulta el m�todo consultaBines del servicio http://soabalanceada.totalplay.com.mx/WSMiTotalplay/MiTotalplay?wsdl                    
 */
public class BinSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
    
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request, response);

    }
    /**
     * @param N�mero de bin bancaria de la tarjeta consultada(Primeros 6 d�gitos) "binParam"
     * @return Objeto de la clase BinVO que contiene los datos de la tarjeta "binres":
     *          cargosAutomaticos
     *          comercioElectronico
     *          idBanco
     *          idMarca
     *          idTipoTarjeta
     *          nombreBanco
     *          nombreMarca
     *          nombreProducto
     *          tipoTarjeta    
     */
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
        response.setContentType(CONTENT_TYPE);
        HttpSession sesion= request.getSession(false);
        
        PrintWriter out = response.getWriter();
        
        /*String bin = request.getParameter("binParam")!=null?request.getParameter("binParam"):"";
        
        String rutaWS= fileProperty.getProperty("MITOTALPLAYWS");
        URL wsURL= new URL(rutaWS);
        MiTotalplayService miTotalplayService = new MiTotalplayService(wsURL,
          new QName("http://project1/", "MiTotalplayService"));
        MiTotalplay miTotalplay = miTotalplayService.getMiTotalplayPort();
        
        ConsultaBines consultaBin = new ConsultaBines();
        consultaBin.setBin(bin);
        
        ConsultaBinesResponse res = miTotalplay.consultaBines(consultaBin);
       
        if(res.getReturn().getRecomResponse().getCode().equals("0")){
            BinVO binres= res.getReturn().getBin();
            JSONObject binObtenido= new JSONObject(binres);
            out.print(binObtenido);

        }else{
            BinVO binres= res.getReturn().getBin();
            JSONObject binObtenido= new JSONObject(binres);
            out.print(binObtenido);
        }*/
        out.print("fail");

        out.close();
    }
}
