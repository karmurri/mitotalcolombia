package com.totalplay.servlets;



import com.totalplay.services.Portabilidad;
import com.totalplay.utils.VerificaUsuario;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import portabilidad.PortabilidadVO;

public class ObtienePortados extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
            doPost(request, response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
        try{
            response.setContentType(CONTENT_TYPE);
            PrintWriter out = response.getWriter();
            HttpSession sesion= request.getSession(false);
            String numCuenta=(String)sesion.getAttribute("numCuenta");
            List<PortabilidadVO> solicitudes=new ArrayList<PortabilidadVO>();
            if(numCuenta!=null)
                solicitudes = Portabilidad.consultaPortabilidad(numCuenta);
            
            for(PortabilidadVO x : solicitudes){
                x.setRUTAACTACONSTITUTIVA("");
                x.setRUTACARTACANCELACION("");
                x.setRUTASOLICITUD("");
                x.setRUTAIDENTIFICACION("");
            }
            
            JSONArray respJSON= new JSONArray(solicitudes);
            out.print(respJSON);
            out.close();
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al consultar informacion de numero portado: "+e.getMessage());
        }
    }
}
