package com.totalplay.servlets;


import com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv.BPELGetTokenNetflixIPTV;
import com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv.BpelgettokennetflixiptvClientEp;
import com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv.Process;
import com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv.Process.Login;
import com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv.ProcessResponse;
import com.totalplay.soa.middleware.generatokennetflix.bpelgettokennetflixiptv.ProcessResponse.Result;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;
import com.totalplay.vo.EstadoCuentaVO;

import java.io.IOException;
import java.io.PrintWriter;

import java.net.URL;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;
import javax.xml.ws.Holder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class GetTokenNF extends HttpServlet {
    private static final String CONTENT_TYPE = "application/json";
    private Logger logger = LogManager.getLogger(this.getClass().getName());
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType(CONTENT_TYPE);
        request.setCharacterEncoding("UTF-8");
        FileProperty fileProperty = new FileProperty(Constantes.CFGNETFLIX);
        HttpSession sesion= request.getSession(false);
        String numCuenta= (sesion.getAttribute("numCuenta")!=null)?(String)sesion.getAttribute("numCuenta"):null;
        EstadoCuentaVO balanceVO = (sesion.getAttribute("balance")!=null)?(EstadoCuentaVO)sesion.getAttribute("balance"):null;
        boolean moroso=false;
        if(balanceVO!=null){
            if(balanceVO.getStatusCuenta().equalsIgnoreCase("moroso")){
                moroso=true;
            }
        }else{
            moroso=true;
        }
        
        PrintWriter out = response.getWriter();
        
        if(numCuenta!=null && !moroso){
            BpelgettokennetflixiptvClientEp netflixClient = null;
            
            try{
                String rutaWS= fileProperty.getProperty("netflixWSDL");
                URL wsURL= new URL(rutaWS);
                netflixClient = new BpelgettokennetflixiptvClientEp(wsURL,
                new QName("http://soa.totalplay.com/Middleware/GeneraTokenNetflix/BPELGetTokenNetflixIPTV",
                         "bpelgettokennetflixiptv_client_ep"));
                
            }catch(Exception e){
                logger.error("no se puedo acceder al Servicio de Generatoken Netflix");
            }
            
            if(netflixClient!=null){
                BPELGetTokenNetflixIPTV netflixPort = netflixClient.getBPELGetTokenNetflixIPTVPt();
                if(netflixPort!=null){
                        Process.Login loginData= new Process.Login();
                        loginData.setUser(fileProperty.getProperty("netflixUserWS"));
                        loginData.setPassword(fileProperty.getProperty("netflixsecretWS"));
                        loginData.setIp(request.getLocalAddr());
                        Holder<ProcessResponse.Result> result= new Holder<ProcessResponse.Result>();
                        Holder<String> token=new Holder<String>();
                        Holder<String> tokenExpityDate= new Holder<String>();
                        netflixPort.process(loginData, numCuenta,fileProperty.getProperty("netflixURLRelay"), result, token, tokenExpityDate);
                        ProcessResponse.Result rest=result.value;
                    
                    if(rest.getResult().equals("0")){
                        if(token.value.length()>0){
                            out.print("{\"result\":1," +
                                "\"token\":\""+token.value+"\"}");
                        }else{
                            out.print("{\"result\":-1," +
                                "\"desc\":\"failNF noToken\"}");
                        }
                    }else{
                            out.print("{\"result\":-1," +
                                "\"desc\":\"failNF No Code\"}");
                    }
                }else{
                    out.print("{\"result\":-1," +
                        "\"desc\":\"fail - netflixPort\"}");
                }
            }else{
                out.print("{\"result\":-1," +
                    "\"desc\":\"fail - netflixClient\"}");
            }
        }else{
            if(moroso){
                out.print("{\"result\":-2," +
                    "\"desc\":\"failNFMoroso\"}");
            }else{
                out.print("{\"result\":-1," +
                    "\"desc\":\"failNFNC\"}");
            }
            
        }
        
        out.close();
    }
}