package com.totalplay.servlets;


import com.google.gson.Gson;

import com.totalplay.utils.FileProperty;
import com.totalplay.vo.DetalleRecoVO;
import com.totalplay.vo.HolderDetalleRecom;

import java.io.IOException;
import java.io.PrintWriter;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ConsultaDetalleReco extends HttpServlet {
    private static final String CONTENT_TYPE = "application/json";
    Logger logger = LogManager.getLogger(this.getClass().getName());
    @SuppressWarnings("compatibility:746740207771054938")
    private static final long serialVersionUID = 1L;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession sesion= request.getSession(false);        
        String numCuenta = (String)sesion.getAttribute("numCuenta");
        String tipo =(String) request.getParameter("tipo");
        //numCuenta =(String) request.getParameter("numCuenta");//quitar para Prod
        FileProperty fileProp = new FileProperty("com.totalplay.prop.Recom");        
        String salida="{\"status\": -2 }";
        boolean success=false;
        
        if(numCuenta!=null && tipo!=null){
            String URLServicio= fileProp.getProperty("detalleIndicador"); 
            HttpResponse respon = null;     
                
            JSONObject jsonLogin = new JSONObject();
            JSONObject jsonDatos = new JSONObject();
            JSONArray jsonarrayL = new JSONArray();   
                
            HttpPost post = new HttpPost(URLServicio);
            if(post != null){
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date hoy= new Date();
                Calendar c= Calendar.getInstance();
                c.setTime(hoy);
                c.add(Calendar.MONTH, -3);
                Date pasado=c.getTime();
                String fechaInicio=dateFormat.format(pasado);
                String fechaFin=dateFormat.format(hoy);
                String estatusP  = getStatusDR(tipo);
                HttpClient httpclient = new DefaultHttpClient();
                    
                try{
                    jsonLogin.put("User", fileProp.getProperty("usr"));
                    jsonLogin.put("Password", fileProp.getProperty("pwd"));
                    jsonLogin.put("IP", fileProp.getProperty("ip"));   
                    jsonarrayL.put(jsonLogin);
                    
                    jsonDatos.put("login", jsonarrayL);
                    jsonDatos.put("idTecnico","");
                    jsonDatos.put("numeroCuenta",numCuenta);
                    jsonDatos.put("estatus",estatusP);
                    jsonDatos.put("Tipo","recomendado");
                    jsonDatos.put("fechaInicio",fechaInicio);
                    jsonDatos.put("fechaFin",fechaFin);
                    
                }catch(JSONException ex){                    
                    this.logger.error("No se puede generar json " + ex.getMessage());
                    //ex.printStackTrace(response.getWriter());ex.printStackTrace();out.print(ex.getLocalizedMessage() + ex.getMessage());
                }
                
                post.setHeader("Content-type", "application/json");
                post.setHeader("Authorization", "Basic ZmZtYXBwOjRnZW5kNG1pM250bw==");
                post.setHeader("cache-control", "no-cache");
                post.setHeader("Content-Type", "application/json; charset=UTF-8");
                post.setHeader("Accept", "application/json");
                    
                try{
                    StringEntity jsonFinal = new StringEntity(new String(jsonDatos.toString().getBytes(),"UTF-8"));             
                    post.setEntity(jsonFinal);
                    respon = httpclient.execute(post);       
                    String body = EntityUtils.toString(respon.getEntity(), "UTF-8");
                    JSONObject jsons = new JSONObject(body);
                    String resultRespone = jsons.get("result").toString();
                    String resultDescrip = jsons.get("resultDescripcion").toString();
                        
                    if(respon != null && resultRespone.equalsIgnoreCase("0")){   
                        HolderDetalleRecom contactos= new HolderDetalleRecom();
                        JSONArray detallesRecoM = new JSONArray(jsons.get("detalleIndicador").toString());
                        JSONObject objeto = null; String fechaSet = "";                    
                        if(detallesRecoM != null){
                            for(int i = 0; i < detallesRecoM.length(); i ++){
                                DetalleRecoVO contacto= new DetalleRecoVO();   
                                objeto = detallesRecoM.getJSONObject(i);
                                contacto.setNombre(objeto.getString("nombreC"));
                                contacto.setFolio(objeto.getString("codigoReferido"));
                                if(objeto.isNull("fechaEnvio")){
                                    fechaSet="";
                                    contacto.setFecha(fechaSet);
                                }else{
                                    contacto.setFecha(objeto.getString("fechaEnvio"));
                                }                                
                                if(getStatusDR(tipo).equals("instalados")){
                                    contacto.setFecha(objeto.getString("fechaInstalacion"));
                                }
                                
                                contactos.getContactos().add(contacto);
                            }
                                
                            if(contactos.getContactos().size()==0){
                                DetalleRecoVO contacto= new DetalleRecoVO();
                                contacto.setNombre("No cuentas con Recomendados");
                                contacto.setFolio("Gracias");
                                contacto.setFecha("en este status");
                                contactos.getContactos().add(contacto);
                                contactos.setStatus(1);
                                logger.error("No cuentas con Recomendados");
                            }
                                
                            Gson json=new Gson();
                            out.print(json.toJson(contactos));
                            success=true;                                
                        }else{
                            salida=("{\"status\": 2 }");
                        }
                            
                    }else{
                        if(resultRespone.equalsIgnoreCase("1")&& resultDescrip.contains("No se encontro informacion")){
                            salida="{\"status\": 4 }";
                        }else{
                            salida="{\"status\": 3 }";
                        }   
                    }
                }catch(Exception ex){                                             
                    logger.error("No se puede establecer la conexi�n " + ex.getMessage());
                    out.print( "{\"status\": 5 }"); 
                    //ex.printStackTrace(response.getWriter());ex.printStackTrace();out.print(ex.getLocalizedMessage() + ex.getMessage());
                }                
                finally{
                    cerrar(httpclient);
                }
            }else{
                salida="{\"status\": -1 }";
            }
            if(!success){
                out.print(salida);
            }
        }else{
            out.print(salida);    
        }         
        out.close();
    }

    private String getStatusDR(String cadena) {
        String response="";
        if(cadena.equals("1")){
            response="porContactar";
        }
        if(cadena.equals("2")){
            response="porInstalar";
        }
        if(cadena.equals("3")){
            response="noExito";
        }        
        if(cadena.equals("4")){
            response="instalados";
        }
        if(cadena.equals("5")){
            response="ilocalizable";
        }
        
        
        return response;
    }
    
    public void cerrar(HttpClient httpclient){
        try{
            if(httpclient!=null){
               httpclient.getConnectionManager().shutdown();
            }
        }catch(Exception ex){
            logger.error("no se pudo cerrar el cliente");
        }
   }
}
