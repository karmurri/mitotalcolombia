package com.totalplay.servlets;


import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.ProcessResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.ProcessResponse.BankResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.ProcessResponse.BankResponse.BankingAuthorizerResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.ProcessResponse.BrmResult;
import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.BPELObtenerCorreo;
import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Bpelobtenercorreo_client_ep;

import com.totalplay.db.dao.GeneralDAO;
import com.totalplay.db.dbo.TransaccionDBO;
import com.totalplay.implement.ImpPaymentEngineExTDC;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.DateTime;
import com.totalplay.utils.EmailUtility;
import com.totalplay.utils.FileProperty;
import com.totalplay.utils.ProcesaHtml;
import com.totalplay.vo.TarjetaVO;
import com.totalplay.vo.UsuarioVO;
import com.totalplay.ws.vo.ClientCardVO;

import java.io.IOException;
import java.io.PrintWriter;

import java.net.URL;

import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class PayExtCardSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
       doPost(request,response);
    }
    public void doPost(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
       //logger.error("-----------Entro a servlet PayExtCardSer------------ ");
        response.setContentType(CONTENT_TYPE);
        request.setCharacterEncoding("UTF-8");
        ImpPaymentEngineExTDC pagoExtCard= new ImpPaymentEngineExTDC();
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);

        HttpSession sesion= request.getSession(false);
        
        UsuarioVO usuario= (UsuarioVO)sesion.getAttribute("usuario");
        
        PrintWriter out = response.getWriter();

        try{
            String rutaWS= fileProperty.getProperty("OBTIENECORREOFACT");
             URL wsURL= new URL(rutaWS);
            Bpelobtenercorreo_client_ep bpelobtenercorreo_client_ep = new Bpelobtenercorreo_client_ep(wsURL,
            new QName("http://xmlns.oracle.com/SeguridadBRM/ObtenerCorreo/BPELObtenerCorreo",
                     "bpelobtenercorreo_client_ep"));
           BPELObtenerCorreo bPELObtenerCorreo = bpelobtenercorreo_client_ep.getBPELObtenerCorreo_pt();
           
           com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process paramCorreo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process();
           com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin authCorreo=  new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin();
           authCorreo.setIp("0.0.0.1");
           authCorreo.setUserId("452710");
           authCorreo.setPassword("Portales789$");
           paramCorreo.setApplicationLogin(authCorreo);
           
           com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo paramClientInfo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo();
           paramClientInfo.setAccountOrDn(usuario.getLogin());
           paramCorreo.setClientInfo(paramClientInfo);
           
           com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.ProcessResponse respCorreo= bPELObtenerCorreo.process(paramCorreo);
          
           if(respCorreo.getResult().equals("0")){ 
              usuario.setEmail(respCorreo.getEmail());
           }
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al consultar servicio de obtenci�n de correo: "+e.getMessage()+ " causa: "+e.getCause());
        }
        
        
        String codigo=request.getParameter("sec");
        String errorText=null;
        int errorType=0;
        HashMap<String,String> respBancoMap=new HashMap<String,String>();;
        boolean errores=false;
        
        String idTarjeta=request.getParameter("idCardList");
        String amount= (request.getParameter("amm")!=null )?request.getParameter("amm"):" ";
        double new_variable = Math.round((Double.parseDouble(amount))*100) / 100.0;
        amount=new_variable+"";
        
        String fechaExpiracion=null;
        if(request.getParameter("Month_"+idTarjeta)!=null && request.getParameter("Year_"+idTarjeta) !=null){
            fechaExpiracion=request.getParameter("Month_"+idTarjeta)+request.getParameter("Year_"+idTarjeta);
        }else{
            fechaExpiracion="";
        }
        
        String ipCliente= request.getRemoteAddr();
        String browserClient= request.getHeader("User-Agent");
        TransaccionDBO transact= new TransaccionDBO();
        GeneralDAO transactBO= new GeneralDAO(3);
        EmailUtility wsEmail= new EmailUtility();
        
        List<TarjetaVO> listTarjetas=(List<TarjetaVO>)sesion.getAttribute("tarjetas");
        if(listTarjetas!=null && listTarjetas.size()>0){
            String resul=null;
            for(TarjetaVO tarjeta: listTarjetas){
                if(codigo.equals(tarjeta.getCode()) && tarjeta.getStatus().equals("ACTIVA") && fechaExpiracion.equals(tarjeta.getExpirationMonth()+tarjeta.getExpirationYear())){
                    
                    ClientCardVO tarjetaVo= new ClientCardVO();
                    tarjetaVo.setCvv2(request.getParameter("CCV_"+idTarjeta));
                    tarjetaVo.setIdCard(tarjeta.getIdCard());
                    
                    ProcessResponse pagoExtCardResp= pagoExtCard.payExTDC(usuario.getEmployeenumber(), amount, tarjetaVo, ipCliente);
                    
                    if(pagoExtCardResp==null){
                        errores=true;
                        logger.error("ENGPAYNC0001: No se pudo conectar");
                        errorText = Constantes.ERROR24+ " Error de conexion WS PagoNTC";
                        String res="{\"error\": \"2\", \"cliente\": \""+usuario.getName()+"\"}";
                        out.print(res);
                        errorType=2;
                        res=null;
                    }
                    else{
                        BankResponse respuestaBanco= pagoExtCardResp.getBankResponse();
                        BrmResult respuestaBRM= pagoExtCardResp.getBrmResult();
                        
                        BankingAuthorizerResponse autorizacionBanco= respuestaBanco.getBankingAuthorizerResponse();
                        
                        if(autorizacionBanco!=null){
                            errores=false;
                            
                            String StatusBanco= respuestaBanco.getResponse().getCode();
                            if(!StatusBanco.isEmpty() && StatusBanco.equals("0")){
                                respBancoMap.put("authorizationCode",autorizacionBanco.getAuthorizationCode());
                                respBancoMap.put("bankResponseCode", autorizacionBanco.getBankResponseCode());
                                respBancoMap.put("descriptionResponse", autorizacionBanco.getDescriptionResponse());
                                respBancoMap.put("transactionControlNumber", autorizacionBanco.getTransactionControlNumber());
                                respBancoMap.put("standardResponseCode", autorizacionBanco.getStandardResponseCode());
                                respBancoMap.put("affiliation",autorizacionBanco.getAffiliation());
                                respBancoMap.put("affiliationName",autorizacionBanco.getAffiliationName());
                                respBancoMap.put("transactionDate", autorizacionBanco.getTransactionDate());
                                
                                if(respuestaBRM!=null){
                                    String StatusBRM = respuestaBRM.getResponse().getCode();
                                    if(!StatusBRM.equals("0")){
                                        errores=true;
                                         logger.error("ENGPAYNC0005: Error al actualizar BRM: "+respuestaBRM.getResponse().getDescription());                           
                                         errorText = Constantes.ERROR25;
                                         respBancoMap.put("descriptionResponse", respuestaBRM.getResponse().getDescription());
                                         errorType=3;
                                    }
                                }
                            }else{
                                errores =true;
                                logger.error("ENGPAYNC0003: No se autoriz� el pago: "+ autorizacionBanco.getDescriptionResponse());
                                errorText = Constantes.ERROR21+" "+ autorizacionBanco.getDescriptionResponse();
                                respBancoMap.put("descriptionResponse", autorizacionBanco.getDescriptionResponse());
                                String res="{\"error\": \"1\",\"cliente\": \""+usuario.getName()+"\"}";
                                out.print(res);
                                res=null;
                                errorType=1;
                            }
                        }else{
                            errores =true;
                            logger.error("ENGPAYNC0003: No se autoriz� el pago: "+ respuestaBanco.getResponse().getDescription());
                            errorText = Constantes.ERROR21+" "+ respuestaBanco.getResponse().getDescription();
                            respBancoMap.put("descriptionResponse", respuestaBanco.getResponse().getDescription());
                            String res="{\"error\": \"1\",\"cliente\": \""+usuario.getName()+"\"}";
                            out.print(res);
                            res=null;
                            errorType=1;      
                        }  
                    }
      
                   
                    transact.setUserID(usuario.getEmployeenumber());
                    transact.setMessage("|EXISTENTE|"+respBancoMap.get("descriptionResponse")+"|"+tarjeta.getFullName());
                    transact.setTitular(usuario.getName()+" "+usuario.getLastname());
                    transact.setAmmount(amount);
                    transact.setAuthBank(respBancoMap.get("authorizationCode")!=null?respBancoMap.get("authorizationCode"):" ");
                    transact.setBankName(tarjeta.getBank()!=null?tarjeta.getBank():" ");
                    String tarjetaFin= tarjeta.getEndingCardNumber()!=null?tarjeta.getEndingCardNumber().substring(1):" ";
                    transact.setCEndNumber(tarjetaFin);
                    transact.setError(errorType);
                    transact.setIp(ipCliente);
                    transact.setBrowser(browserClient);
                    
                    transact.setLogdate(DateTime.obtieneFechaHoraActual("yyyy-MM-dd hh:mm:ss"));
                    //inserci�n de registro del pago
                    
                    if(!transactBO.insertTransaction(transact))
                         logger.error("Problema al insertar transacci�n en BD: "+transact.getUserID());
                    
                    String rutaSoporte= this.getServletContext().getRealPath("/assets/mail/errores.htm");
                    rutaSoporte=rutaSoporte!=null?rutaSoporte:getServletContext().getResource("/assets/mail/errores.htm").getPath();
                    
                    String statusMsg="Autorizado";
                    switch(errorType){
                        //usuario.setEmail("cfloresc@elektra.com.mx");

                         case 1: statusMsg= "El pago no fu� autorizado: "+respBancoMap.get("descriptionResponse");
                                 break;
                         case 2: statusMsg="Error al obtener respuesta de Web Service PagoNTC";
                                 break;
                         case 3: statusMsg= "Pago autorizado pero no se actualiz� la cuenta en BRM "+respBancoMap.get("descriptionResponse");
                                 break;
                    
                    }
                    
                    //hardcode
                    //errores=false;
                    if(errores){
                        statusMsg="(EXTCARD)Cuenta: "+usuario.getEmployeenumber()+" Error: "+errorText +"("+respBancoMap.get("descriptionResponse")+")";
                        sesion.setAttribute("AuthBancaria", null);
                        String bodyEmail = ProcesaHtml.creaCadenaHTMLSoporte(rutaSoporte,statusMsg);
                        wsEmail.sendEmail(Constantes.EMAIL_SUPORT, Constantes.EMAIL_SUPPORTCC, "?", "?", Constantes.SUBJECT_SUPPORT, bodyEmail);
                        String res="{\"error\": \"1\",\"cliente\": \""+usuario.getName()+"\"}";
                        out.print(res);
                        res=null;
                    }
                    else{
                        //hardcode
                        //respBancoMap.put("authorizationCode","012345678");
                        //errorType=0;
                        transact.setAuthBank(respBancoMap.get("authorizationCode"));
                        transact.setLogdate(DateTime.obtieneFechaHoraActual("yyyy-MM-dd hh:mm:ss"));
                        transact.setMessage(statusMsg);
                        sesion.setAttribute("AuthBancaria", respBancoMap.get("authorizationCode")); 
                        
                         //Envio de correo de confirmaci�n de pago exitoso al usuario 
                     
                        String ruta= this.getServletContext().getRealPath("/assets/mail/mailing.htm");
                        ruta=ruta!=null?ruta:getServletContext().getResource("/assets/mail/mailing.htm").getPath();
                        //hardcode
                        //usuario.setEmail("cfloresc@elektra.com.mx");
                        String bodyEmail = ProcesaHtml.creaCadenaHTMLPago(ruta,usuario.getName()+" "+usuario.getLastname(),respBancoMap.get("authorizationCode"),amount,tarjetaFin);
                        boolean emailResul= wsEmail.sendEmail(usuario.getEmail(), Constantes.EMAILCC, Constantes.EMAILBCC, Constantes.REPLY_TO, Constantes.SUBJECTPAGO, bodyEmail);
                        if(emailResul==true){
                            sesion.setAttribute("sendEmail", "1");
                            String res="{ \"error\": \"0\", \"cliente\": \""+usuario.getName()+"\" , \"auth\": \""+respBancoMap.get("authorizationCode")+"\", \"email\": \""+usuario.getEmail()+"\" }";
                            out.print(res);
                            res=null;
                        }else{
                            String res="{ \"error\": \"4\", \"cliente\": \""+usuario.getName()+"\" , \"auth\": \""+respBancoMap.get("authorizationCode")+"\"}";
                            out.print(res);
                            res=null;
                        }
                        
                        transactBO.insertTransaction(transact);
                    }
                    resul=null;
                }
                else{
                   // logger.info("Tarjeta no v�lida");
                    errorType=6;
                    transact.setUserID(usuario.getEmployeenumber());
                    transact.setMessage("|EXISTENTE|INTENTO DE PAGO CON DATOS DE TARJETA NO VALIDOS|");
                    transact.setTitular(usuario.getName()+" "+usuario.getLastname());
                    transact.setAmmount(amount);
                    transact.setAuthBank(respBancoMap.get("authorizationCode")!=null?respBancoMap.get("authorizationCode"):" ");
                    transact.setBankName(tarjeta.getBank()!=null?tarjeta.getBank():" ");
                    String tarjetaFin= tarjeta.getEndingCardNumber()!=null?tarjeta.getEndingCardNumber().substring(1):" ";
                    transact.setCEndNumber(tarjetaFin);
                    transact.setError(errorType);
                    transact.setIp(ipCliente);
                    transact.setBrowser(browserClient);
                    
                    transact.setLogdate(DateTime.obtieneFechaHoraActual("yyyy-MM-dd hh:mm:ss"));
                    
                    transactBO.insertTransaction(transact);
                    resul="{\"error\": \"6\",\"cliente\": \""+usuario.getName()+"\"}";
                    
                    
                   /* sesion.setAttribute("errorType", errorType+"");
                    request.getRequestDispatcher("infoclienteser.html").forward(request, response);  */
                }
                
            }
            if(resul!=null){
                out.print(resul);
            }
            out.close();
        }
    }
}
