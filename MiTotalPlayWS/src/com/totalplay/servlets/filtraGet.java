package com.totalplay.servlets;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class filtraGet implements Filter {
    private FilterConfig _filterConfig = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        _filterConfig = filterConfig;
    }

    public void destroy() {
        _filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException,
                                                   ServletException {
        
        HttpServletRequest req=(HttpServletRequest)request;
       // System.out.println("metodo: "+req.getMethod());
        if(req.getMethod().equals("GET")){
            req.getRequestDispatcher("home.jsp").forward(req, response);
        }else{
            chain.doFilter(request, response);
        }
    }
}
