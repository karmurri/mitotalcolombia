package com.totalplay.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class descargaContrato extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
            doPost(request,response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
            HttpSession sesion= request.getSession(false);
            String numCuenta = (String)sesion.getAttribute("numCuenta");
            PrintWriter out=null;
            InputStream fis=null;
            if(numCuenta!=null){
               
               try {
                String fileName = "Contrato" + numCuenta.replace(".", "") + ".pdf";
                File file = new File("/Ventas/Totalplay/" + numCuenta + "/Documentos/" + fileName);
                if (file.exists()) {
                    response.setContentType("application/pdf");
                    response.setHeader("Cache-Control", "no-cache"); // HTTP 1.1
                    response.setHeader("Cache-Control", "max-age=0");
                    response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + "\"");
                    ServletOutputStream stream = response.getOutputStream();
                    fis = new FileInputStream(file);
                    byte[] bufferData = new byte[1024];
                    int read = 0;
                    while ((read = fis.read(bufferData)) != -1) {
                        stream.write(bufferData, 0, read);
                    }
                    stream.flush();
                    stream.close();
                    


                } else {
                    response.setContentType("text/html; charset=windows-1252");
                    out = response.getWriter();
                    out.print("notFound");
                    out.close();
                }
            } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                response.setContentType("text/html; charset=windows-1252");
                out = response.getWriter();
                out.print("notFound");
                
            } catch (IOException ioe) {
                response.setContentType("text/html; charset=windows-1252");
                out = response.getWriter();
                out.print("notFound");
                
            } finally {
                   safeClose(fis);
                   if(out==null){
                       out = response.getWriter();
                       out.print("notFound");
                       out.close();
                   }
                   
                   
            }
            
            }
        }
    
    public void safeClose(InputStream is){
        if(is!=null){
            try {
                is.close();
            } catch (IOException ioe) {
                logger.error("Error no se cierra el FIS:", ioe.getCause());
            }
            
        }
    }
}
