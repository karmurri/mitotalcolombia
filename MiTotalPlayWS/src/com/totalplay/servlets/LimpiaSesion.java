package com.totalplay.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

public class LimpiaSesion extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
        try{
            response.setContentType(CONTENT_TYPE);
            PrintWriter out = response.getWriter();
            HttpSession sesion= request.getSession(false);
            
            sesion.removeAttribute("errorType");
            sesion.removeAttribute("AuthBancaria");
            out.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
