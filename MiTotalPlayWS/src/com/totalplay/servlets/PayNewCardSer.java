 package com.totalplay.servlets;


import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse.BankResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse.BankResponse.BankingAuthorizerResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse.BrmResult;
import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.BPELObtenerCorreo;
import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Bpelobtenercorreo_client_ep;

import com.totalplay.db.dao.GeneralDAO;
import com.totalplay.db.dbo.EventoDBO;
import com.totalplay.db.dbo.RegistroTCRDBO;
import com.totalplay.db.dbo.TarjetaDBO;
import com.totalplay.db.dbo.TransaccionDBO;
import com.totalplay.implement.ImpPaymentEngineNewTDC;
import com.totalplay.utils.CambioMetodoPago;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.DateTime;
import com.totalplay.utils.EmailUtility;
import com.totalplay.utils.FileProperty;
import com.totalplay.utils.ProcesaHtml;
import com.totalplay.utils.ValidateForm;
import com.totalplay.vo.UsuarioVO;
import com.totalplay.ws.vo.newCardVO;

import encrypt.AesUtil;

import java.io.IOException;
import java.io.PrintWriter;

import java.math.BigInteger;

import java.net.URL;

import java.text.DecimalFormat;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class PayNewCardSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    @SuppressWarnings("compatibility:-8523887124550261856")
    private static final long serialVersionUID = 1L;
    transient Logger logger = LogManager.getLogger(this.getClass().getName());
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request,response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
       logger.info("-----------Entro a servlet PayNewCardSer------------ ");
        response.setContentType(CONTENT_TYPE);
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession();
        try{
        ImpPaymentEngineNewTDC pagoNewCard= new ImpPaymentEngineNewTDC();
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
        String res=null;
        UsuarioVO usuario= (UsuarioVO)sesion.getAttribute("usuario");
        PrintWriter out = response.getWriter();
        if(sesion.getAttribute("errorType")==null){
        //actualiza SessionSSO
        String tarjetaDecryp = null;
        
        //consulta de correo
         try{
            String rutaWS= fileProperty.getProperty("OBTIENECORREOFACT");
             URL wsURL= new URL(rutaWS);
            Bpelobtenercorreo_client_ep bpelobtenercorreo_client_ep = new Bpelobtenercorreo_client_ep(wsURL,
            new QName("http://xmlns.oracle.com/SeguridadBRM/ObtenerCorreo/BPELObtenerCorreo",
                     "bpelobtenercorreo_client_ep"));
           BPELObtenerCorreo bPELObtenerCorreo = bpelobtenercorreo_client_ep.getBPELObtenerCorreo_pt();
           
           com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process paramCorreo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process();
           com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin authCorreo=  new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin();
           authCorreo.setIp("0.0.0.1");
           authCorreo.setUserId("452710");
           authCorreo.setPassword("Portales789$");
           paramCorreo.setApplicationLogin(authCorreo);
           
           com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo paramClientInfo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo();
           paramClientInfo.setAccountOrDn(usuario.getLogin());
           paramCorreo.setClientInfo(paramClientInfo);
           
           com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.ProcessResponse respCorreo= bPELObtenerCorreo.process(paramCorreo);
         
           if(respCorreo.getResult().equalsIgnoreCase("0")){ 
              usuario.setEmail(respCorreo.getEmail());
           }
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al consultar servicio de obtenci�n de correo: "+e.getMessage()+ " causa: "+e.getCause());
        } 
        
        /* try{
            ConsultarSesionResponse sso= sessionSSO.consultarSesion(sessid);
            if(sso.getReturn().getResponseVO().getCode().equals("0")&& sso.getReturn().getSessionVO().getStatus().equalsIgnoreCase("inactiva")){
                sessionSSO.actualizaSesion(sessid);
            }
        }catch(java.lang.NullPointerException nex){
            logger.error("Error: " + nex.getMessage() + " Causa: " + nex.getCause());
        } */
           
        newCardVO newCard = new newCardVO();
        String ipCliente= request.getRemoteAddr();
        String browserClient= request.getHeader("User-Agent"); 
        boolean errores=false;
               
        //newCard.setAddress(request.getParameter("cCardAdd").toUpperCase());
        newCard.setBank(request.getParameter("cCardBank"));
        
        String typeCard=request.getParameter("Ctype");
        String BankIdProvider= "";
        EmailUtility wsEmail= new EmailUtility();
        if(typeCard.equalsIgnoreCase("Mastercard"))
            BankIdProvider="3";
        else if(typeCard.equalsIgnoreCase("Visa"))
            BankIdProvider="2";
        else if(typeCard.equalsIgnoreCase("Amex"))
            BankIdProvider="1";
        else
            BankIdProvider="";
        newCard.setBankIdProvider(BankIdProvider);
        
        //************Desencriptar No. Tarjeta**********///
        String tarjetaEncryp=request.getParameter("cCardNumberEnc");
        if(tarjetaEncryp!= null){            
            //String phrase = Constantes.PHRASE;
            //GnuPG pgp = new GnuPG ();
            //pgp.setGnuPGRuntime(Constantes.GNUGPGRUNTIME);
            
            //pgp.decrypt(tarjetaEncryp, phrase);
            
            //tarjetaDecryp= pgp.getResult();
            tarjetaDecryp = decTarjeta(tarjetaEncryp);
            // System.out.println("Tarjeta desencriptada: "+tarjetaDecryp);
            newCard.setCardNumber(tarjetaDecryp);
        }

        String calle="calle";
        String colonia="colonia";
        String country="pais";
        String cvv2=request.getParameter("cCardSec")!=null?request.getParameter("cCardSec").toString().trim()!=""?request.getParameter("cCardSec").toString().trim().toUpperCase():"":"";
        String expirationMonth=request.getParameter("expMonth")!=null?request.getParameter("expMonth").toString().trim()!=""?request.getParameter("expMonth").toString().trim().toUpperCase():"":"";
        String expirationYear=request.getParameter("expYear")!=null?request.getParameter("expYear").toString().trim()!=""?request.getParameter("expYear").toString().trim().toUpperCase():"":"";
        String firstName=request.getParameter("cCardname")!=null?request.getParameter("cCardname").toString().trim()!=""?request.getParameter("cCardname").toString().trim().toUpperCase():"":"";
        String lastName=request.getParameter("cCardLastname")!=null?request.getParameter("cCardLastname").toString().trim()!=""?request.getParameter("cCardLastname").toString().trim().toUpperCase()+" "+request.getParameter("cCardLastname1").toString().trim().toUpperCase():"":"";
        String municipality="del";
        String provider=request.getParameter("Ctype")!=null?request.getParameter("Ctype").toString().trim()!=""?request.getParameter("Ctype").toString().trim().toUpperCase():"":"";
        String state="estado";
        String zip="00000";
        String guardarTarjeta = request.getParameter("SaveCard")!=null?request.getParameter("SaveCard").toString().trim()!=""?request.getParameter("SaveCard").toString().trim().toUpperCase():"":"";
        String tipoTarjeta = request.getParameter("cTipoTarjeta")!=null?request.getParameter("cTipoTarjeta").toString().trim()!=""?request.getParameter("cTipoTarjeta").toString().trim().toUpperCase():"":"";
        String cargoRecurrente = request.getParameter("cCargoRecurrent")!=null?request.getParameter("cCargoRecurrent").toString().trim()!=""?request.getParameter("cCargoRecurrent").toString().trim().toUpperCase():"":"";
        //String idBanko = request.getParameter("idBancoInput")!=null?request.getParameter("idBancoInput").toString().trim()!=""?request.getParameter("idBancoInput").toString().trim().toUpperCase():"":"";
    
        newCard.setColony((colonia!=null && !colonia.equalsIgnoreCase(" ") && ValidateForm.soloLetrasNums(colonia))?colonia:" ");
        newCard.setCountry((country!=null && !country.equalsIgnoreCase(" ") && ValidateForm.soloLetras(country))?country:" ");
        newCard.setCvv2((cvv2!=null && !cvv2.equalsIgnoreCase(" ") && ValidateForm.soloNumeros(cvv2))?cvv2:" ") ;
        newCard.setExpirationMonth((expirationMonth!=null && !expirationMonth.equalsIgnoreCase(" "))?expirationMonth:" ");
        newCard.setExpirationYear((expirationYear!=null && !expirationYear.equalsIgnoreCase(" "))?expirationYear:" ");
        newCard.setFirstName((firstName!=null && !firstName.equalsIgnoreCase(" ") && ValidateForm.soloLetras(firstName))?firstName:" ");
        newCard.setLastName((lastName!=null && !lastName.equalsIgnoreCase(" ") && ValidateForm.soloLetras(lastName))?lastName:" ");
        newCard.setMunicipality((municipality!=null && !municipality.equalsIgnoreCase(" ") && ValidateForm.soloLetrasNums(municipality))?municipality:" ");
        newCard.setProvider( (provider!=null && !provider.equalsIgnoreCase(" "))?provider:" ");
        newCard.setState( (state!=null && !state.equalsIgnoreCase(" "))?state:" ");
        newCard.setZip((zip!=null && !zip.equalsIgnoreCase(" ") && ValidateForm.soloNumeros(zip))?zip:" ");
        
        DecimalFormat df = new DecimalFormat("#.##");
        
        String amount= (request.getParameter("amm")!=null && ValidateForm.decimales(request.getParameter("amm")))?request.getParameter("amm").trim():"";
        double new_variable = Math.round((Double.parseDouble(amount))*100) / 100.0;
        amount=new_variable+"";
        //Procesamiento del pago con nueva tarjeta utilizando el Web Service correspondiente
        ProcessResponse pagoNewCardResp= pagoNewCard.payNewTDC(usuario.getEmployeenumber(), amount, newCard, ipCliente);
        
        String errorText=null;
        String atuCode = "";
        int errorType=0;
        HashMap<String,String> respBancoMap=null;
        respBancoMap=new HashMap<String,String>();
        if(pagoNewCardResp==null){
            errores=true;
            logger.error("ENGPAYNC0001: No se pudo conectar");
            errorText = Constantes.ERROR24+ " Error de conexion WS PagoNTC";
            out.flush();
            out.print("");
            res="{\"error\": \"2\", \"cliente\": \""+usuario.getName()+"\"}";
            errorType=2;
        }
        else{
            BankResponse respuestaBanco = pagoNewCardResp.getBankResponse();            
            BrmResult respuestaBRM = pagoNewCardResp.getBrmResult();        
          //  SaveCardResponse respuestaGuardaTC= pagoNewCardResp.getSaveCardResponse();
            
            BankingAuthorizerResponse autorizacionBanco= respuestaBanco.getBankingAuthorizerResponse();
            if(autorizacionBanco!=null){
                errores=false;
                
                String StatusBanco= respuestaBanco.getResponse().getCode();                                
                if(!StatusBanco.isEmpty() && StatusBanco.equalsIgnoreCase("0")){
                    respBancoMap.put("authorizationCode",autorizacionBanco.getAuthorizationCode());
                    atuCode = autorizacionBanco.getAuthorizationCode();
                    respBancoMap.put("bankResponseCode", autorizacionBanco.getBankResponseCode());
                    respBancoMap.put("descriptionResponse", autorizacionBanco.getDescriptionResponse());
                    respBancoMap.put("transactionControlNumber", autorizacionBanco.getTransactionControlNumber());
                    respBancoMap.put("standardResponseCode", autorizacionBanco.getStandardResponseCode());
                    respBancoMap.put("affiliation",autorizacionBanco.getAffiliation());
                    respBancoMap.put("affiliationName",autorizacionBanco.getAffiliationName());
                    respBancoMap.put("transactionDate", autorizacionBanco.getTransactionDate());
                    if(respuestaBRM!=null){
                        String StatusBRM = respuestaBRM.getResponse().getCode();                        
                        if(!StatusBRM.equalsIgnoreCase("0")){
                            errores=true;
                             logger.error("ENGPAYNC0005: Error al actualizar BRM: "+respuestaBRM.getResponse().getDescription());                           
                             errorText = Constantes.ERROR25;
                             respBancoMap.put("descriptionResponse", respuestaBRM.getResponse().getDescription());
                             errorType=3;
                        }
                    }
                    else{
                        logger.error("Error, respuestaBRM null");
                        }
                    /*if(respuestaGuardaTC!=null){
                         String StatusRecordCard=respuestaGuardaTC.getResponse().getCode();
                         if(!StatusRecordCard.equals("0")){
                             errores=true;
                              logger.error("ENGPAYNC0006: Error al guardar la tarjeta: "+respuestaGuardaTC.getResponse().getDescription());
                              errorText=Constantes.ERROR26;
                              respBancoMap.put("descriptionResponse", respuestaGuardaTC.getResponse().getDescription());
                              errorType=4;
                         }
                    } */                   
                }else{
                    errores =true;
                    logger.error("ENGPAYNC0003: No se autoriz� el pago: "+ autorizacionBanco.getDescriptionResponse());
                    errorText = Constantes.ERROR21+" "+ autorizacionBanco.getDescriptionResponse();
                    respBancoMap.put("descriptionResponse", autorizacionBanco.getDescriptionResponse());
                    res="{\"error\": \"1\",\"cliente\": \""+usuario.getName()+"\"}";
                    errorType=1;
                }
            }else{
                //respBancoMap=new HashMap<String,String>();
                errores =true;
                logger.error("ENGPAYNC0003: No se autoriz� el pago: "+ respuestaBanco.getResponse().getDescription());
                errorText = Constantes.ERROR21+" "+ respuestaBanco.getResponse().getDescription();
                respBancoMap.put("descriptionResponse", respuestaBanco.getResponse().getDescription());
               
                res="{\"error\": \"1\",\"cliente\": \""+usuario.getName()+"\"}";
                
                errorType=1;      
            }
        }
           
        
           
        String rutaSoporte= this.getServletContext().getRealPath("/assets/mail/errores.htm");
        rutaSoporte=rutaSoporte!=null?rutaSoporte:getServletContext().getResource("/assets/mail/errores.htm").getPath();
        String statusMsg="Autorizado";
        switch(errorType){
                case 1: 
                        statusMsg= "El pago no fu� autorizado: ";
                        if(respBancoMap!=null){
                            statusMsg+=respBancoMap.get("descriptionResponse");
                            }
                        else{
                            logger.error( "case: 1, Error respBancoMap null" );
                            }
                        break;
                case 2: statusMsg= "Error al obtener respuesta de Web Service PagoNTC";
                        break;
                case 3: statusMsg= "Pago autorizado pero no se actualiz� la cuenta en BRM "+respBancoMap.get("descriptionResponse");
                        break;
                case 4: statusMsg= "Pago autorizado pero no se guard� la tarjeta  "+ respBancoMap.get("descriptionResponse");      
                        break;
                case 5: statusMsg= "Pago no fue autorizado, por un problema interno "+ respBancoMap.get("descriptionResponse");      
                        break;
                case 6: statusMsg= "El pago fue autorizado y la tarjeta se guard� exitosamente"+ respBancoMap.get("descriptionResponse");      
                        break;
               default: logger.error("case: default, Error con el pago");
                       break;
                }
           //hardcode///////////////
           //errores=false;
           ////////////////////////
           TransaccionDBO transact= new TransaccionDBO();
            GeneralDAO transactBO= new GeneralDAO(3);
            String tarjetaFin= newCard.getCardNumber()!=null?newCard.getCardNumber().substring(newCard.getCardNumber().length()-4,newCard.getCardNumber().length()):" ";
           if(errores){
               //respBancoMap=new HashMap<String,String>();
               transact.setUserID(usuario.getEmployeenumber());
               String respuestabanco ="";
               if (respBancoMap!=null){
                   respuestabanco+=respBancoMap.get("descriptionResponse");
                   
               transact.setMessage("|NUEVA|"+respuestabanco+"|"+newCard.getFirstName()+" "+newCard.getLastName()+"|"+calle+"|"+colonia+
                                   "|"+municipality+"|"+state+"|"+country+"|"+zip+"|");
               transact.setTitular(usuario.getName()+" "+usuario.getLastname());
               transact.setAmmount(amount);               
               transact.setAuthBank(respBancoMap.get("authorizationCode")!=null?respBancoMap.get("authorizationCode"):" ");
               transact.setBankName(newCard.getBank()!=null?newCard.getBank():" ");
               transact.setCEndNumber(tarjetaFin);
               transact.setError(errorType);
               transact.setIp(ipCliente);
               transact.setBrowser(browserClient);
               
               transact.setLogdate(DateTime.obtieneFechaHoraActual("yyyy-MM-dd hh:mm:ss"));
               }
               
               else{
                    logger.error("Error en: respBancoMap, no se inicio");
                   }
               //inserci�n de registro del pago
                
                if(!transactBO.insertTransaction(transact)){
                    logger.error("Problema al insertar transacci�n en BD: "+transact.getUserID());
                }
           
               statusMsg="(NEWCARD)Cuenta: "+usuario.getEmployeenumber()+" Error: "+errorText +"("+respBancoMap.get("descriptionResponse")+")";
               sesion.setAttribute("AuthBancaria", null);
               String bodyEmail = ProcesaHtml.creaCadenaHTMLSoporte(rutaSoporte,statusMsg);
               wsEmail.sendEmail(Constantes.EMAIL_SUPORT, Constantes.EMAIL_SUPPORTCC, "?", "?", Constantes.SUBJECT_SUPPORT, bodyEmail);
               res="{\"error\": \"1\",\"cliente\": \""+usuario.getName()+"\"}";
           }
           else{                
               //hardcode
               //respBancoMap.put("authorizationCode","012345678");
               //errorType=0;
               //respBancoMap=new HashMap<String,String>();
               transact.setUserID(usuario.getEmployeenumber());               
               //transact.setAuthBank(respBancoMap.get("authorizationCode"));
               transact.setAuthBank(atuCode);
               transact.setLogdate(DateTime.obtieneFechaHoraActual("yyyy-MM-dd hh:mm:ss"));
                String respuestabanco="";
                if (respBancoMap!=null){
                    respuestabanco+=respBancoMap.get("descriptionResponse");
                    }                
               transact.setMessage("|NUEVA|"+respuestabanco+"|"+newCard.getFirstName()+" "+newCard.getLastName()+"|"+colonia+
                               "|"+municipality+"|"+state+"|"+country+"|"+zip+"|");
               sesion.setAttribute("AuthBancaria", atuCode); 
               transact.setTitular(usuario.getName()+" "+usuario.getLastname());
                transact.setAmmount(amount);
                transact.setBankName(newCard.getBank()!=null?newCard.getBank():" ");
                transact.setCEndNumber(tarjetaFin);
                transact.setIp(ipCliente);
                transact.setBrowser(browserClient);

                //Envio de correo de confirmaci�n de pago exitoso al usuario 
            
               String ruta= this.getServletContext().getRealPath("/assets/mail/mailing.htm");
               ruta=ruta!=null?ruta:getServletContext().getResource("/assets/mail/mailing.htm").getPath();
               
               String ruta2 = this.getServletContext().getRealPath("/assets/mail/mailing-empy.htm"); 
               ruta2 = ruta2 != null ? ruta2:getServletContext().getResource("/assets/mail/mailing-empy.htm").getPath(); 
    
               //hardcode///////////////////////
               ///usuario.setEmail("cfloresc@elektra.com.mx");
               ////////////////////////////////
               boolean emailResul;
               if(amount.isEmpty() || tarjetaFin.isEmpty() || atuCode.isEmpty()){
                   String bodyEmailVa = ProcesaHtml.creaCadenaHTMLPagoVacio(ruta2,usuario.getName()+" "+usuario.getLastname());
                   emailResul= wsEmail.sendEmail(usuario.getEmail(), Constantes.EMAILCC, Constantes.EMAILBCC, Constantes.REPLY_TO, Constantes.SUBJECTPAGO, bodyEmailVa);                   
               }else{
                   //String bodyEmail = ProcesaHtml.creaCadenaHTMLPago(ruta,usuario.getName()+" "+usuario.getLastname(),respBancoMap.get("authorizationCode"),amount,tarjetaFin);
                   String bodyEmail = ProcesaHtml.creaCadenaHTMLPago(ruta,usuario.getName()+" "+usuario.getLastname(),atuCode,amount,tarjetaFin);
                   emailResul= wsEmail.sendEmail(usuario.getEmail(), Constantes.EMAILCC, Constantes.EMAILBCC, Constantes.REPLY_TO, Constantes.SUBJECTPAGO, bodyEmail);                   
               }
               
               if(emailResul == true && !atuCode.isEmpty()){
                   sesion.setAttribute("sendEmail", "1");
                    //La respuesta se muestra cuando existen los 3 par�metros.
                    //res="{ \"error\": \"0\", \"cliente\": \""+usuario.getName()+"\" , \"auth\": \""+respBancoMap.get("authorizationCode")+"\", \"email\": \""+usuario.getEmail()+"\" }";
                    res="{ \"error\": \"0\", \"cliente\": \""+usuario.getName()+"\" , \"auth\": \""+atuCode+"\", \"email\": \""+usuario.getEmail()+"\" }";
	            
                }else{
                   if(atuCode.isEmpty()){
                        //Respuesta se muestra cuando el cod. de autorizacion es vacia o nula
                        res="{ \"error\": \"5\", \"cliente\": \""+usuario.getName()+"\"}";
                   }else{
                       //Respuesta se env�a cuando existio algun error al enviar el correo, pero existen datos en la respuesta
                        res="{ \"error\": \"4\", \"cliente\": \""+usuario.getName()+"\" , \"auth\": \""+atuCode+"\"}";
                   }
            	    
                }
               transactBO.insertTransaction(transact);
                               
               if(guardarTarjeta != null && tipoTarjeta != null){
               logger.error("if guardaTarjeta y tipoTarjeta, cargoRecurrente: "+cargoRecurrente);
                   logger.info("guardarTarjeta: "+guardarTarjeta+" tipoTarjeta: "+tipoTarjeta);
                   if(guardarTarjeta.equalsIgnoreCase("1") && cargoRecurrente.equalsIgnoreCase("SI")){
                       
                       String ACCOUNT_NO = usuario.getEmployeenumber();
                       String PAY_TYPE = provider;       
                       BigInteger ACH = null;
                       if(PAY_TYPE.equalsIgnoreCase("AMEX")){
                           ACH = BigInteger.valueOf(1L);
                       }else{
                           ACH = BigInteger.valueOf(2L);
                       }
                       
                       //String Address =  calle+" "+colonia+" "+municipality+" "+zip+" "+state+" "+country;
                       String City = colonia;  
                       String Country = country;
                       //BigInteger DeliveryPrefer =BigInteger.valueOf(0L);
                       //String InvInstr ="";
                       //String EmailAddr=usuario.getEmail();        
                       String Name = firstName+" "+lastName;   
                       //String LastName = lastName;
                       String FirstName = firstName;        
                       String State = state;
                       String Zip = zip;
                       String DebitExp = expirationMonth+expirationYear;
                       BigInteger DebitNum = BigInteger.valueOf(Long.parseLong(tarjetaDecryp));
                       BigInteger SecurityId = BigInteger.valueOf(Long.parseLong(cvv2));
                      
                       //BigInteger Type =BigInteger.valueOf(0L);
                       //String idBanco= request.getParameter("cCardBankId")!=null?request.getParameter("cCardBankId"):"";
                       
                      CambioMetodoPago cambioMetodoPago = new CambioMetodoPago();
                      
                        RegistroTCRDBO registro= new RegistroTCRDBO();
                        registro.setBanco(newCard.getBank()!=null?newCard.getBank().toUpperCase():" ");
                        registro.setBrowser(browserClient);
                        registro.setComentario("");
                        registro.setEmail(request.getParameter("correo")!=null?request.getParameter("correo").toString().trim():"");
                        registro.setIp(ipCliente);
                        registro.setNombre_cliente(usuario.getName()+" "+usuario.getLastname());
                        registro.setNom_tarhab(newCard.getFirstName()+" "+newCard.getLastName());
                        registro.setNum_cuenta(usuario.getEmployeenumber());
                        registro.setPromocion(request.getParameter("promocion")!=null?request.getParameter("promocion").trim().toUpperCase():"");
                        registro.setTel_fijo(request.getParameter("tel")!=null?request.getParameter("tel").trim().toUpperCase():"");
                        registro.setTel_movil(request.getParameter("cel")!=null?request.getParameter("cel").trim().toUpperCase():"");
                        
                        
                        
                    if(tarjetaDecryp!=null){
                        registro.setTermtarj(tarjetaDecryp.substring(12));
                        TarjetaDBO tarjeta= new TarjetaDBO();
                        tarjeta.setApellido_materno(request.getParameter("cCardLastname1").toUpperCase());
                        tarjeta.setApellido_paterno(request.getParameter("cCardLastname").toUpperCase());      
                        tarjeta.setCalle(calle);
                        tarjeta.setCodigo_postal(Zip);
                        tarjeta.setColonia(colonia);
                        tarjeta.setDelegacion(City);
                        tarjeta.setEstado(country);
                        tarjeta.setFecha_vencimiento("01/"+expirationMonth+"/"+expirationYear);
                        tarjeta.setId_promocion(request.getParameter("promocion")!=null?request.getParameter("promocion").trim().toUpperCase():"");
                        tarjeta.setNombre(FirstName);
                        tarjeta.setNum_tarjeta(tarjetaDecryp.substring(12));
                        tarjeta.setNum_tarjeta_mascara(tarjetaDecryp.substring(12));
                        tarjeta.setNumero_serguridad(cvv2);
                        tarjeta.setPais(country);
                              
                        registro.setTarjeta(tarjeta);
                        
                        EventoDBO evento= new EventoDBO();
                        evento.setEstatus(respBancoMap.get("descriptionResponse"));      
                        evento.setId_canal("1");//se establece como el canal 1 como predeterminado (Portal MiTotalPlay)      
                        evento.setMonto(amount);
                        registro.setEvento(evento);
                        }
                        
                        
                    if(errorType == 3 || errorType == 2 || errorType == 1){ 
                      
                    }else{
                        if(tipoTarjeta.equalsIgnoreCase("credito")){
                            if(cambioMetodoPago.MetodoPagoCredito(ACCOUNT_NO, "10003", ACH, calle, City, Country, 
                                 Name, State, Zip,DebitExp, DebitNum, SecurityId)){
                                res="{ \"error\": \"7\", \"cliente\": \""+usuario.getName()+"\" , \"auth\": \""+respBancoMap.get("authorizationCode")+"\", \"email\": \""+usuario.getEmail()+"\" }";
                                errorType = 7;
                               if(!transactBO.insertRegistroCompleto(registro))
                                    logger.error("Error al insertar registro de tarjeta para cargo recurrente");
                               
                            }else{
                                res="{ \"error\": \"8\", \"cliente\": \""+usuario.getName()+"\" , \"auth\": \""+respBancoMap.get("authorizationCode")+"\", \"email\": \""+usuario.getEmail()+"\" }";
                                errorType = 8;    
                            }
                        }else{
                            if(tipoTarjeta.equalsIgnoreCase("debito")){
                                
                                if(cambioMetodoPago.MetodoPagoCredito(ACCOUNT_NO, "10003", ACH, calle, City, Country, 
                                         Name, State, Zip,DebitExp, DebitNum, SecurityId)){
                                
                                    res="{ \"error\": \"7\", \"cliente\": \""+usuario.getName()+"\" , \"auth\": \""+respBancoMap.get("authorizationCode")+"\", \"email\": \""+usuario.getEmail()+"\" }";
                                    errorType = 7;
                                    if(!transactBO.insertRegistroCompleto(registro))
                                        logger.error("Error al insertar registro de tarjeta para cargo recurrente");
                                }else{
                                    res="{ \"error\": \"8\", \"cliente\": \""+usuario.getName()+"\" , \"auth\": \""+respBancoMap.get("authorizationCode")+"\", \"email\": \""+usuario.getEmail()+"\" }";
                                    errorType = 8;    
                                }
                            }
                        }
                        
                    }
                }        
               
               
           }
            //sesion.setAttribute("errorType", errorType+"");
            //request.getRequestDispatcher("infoclienteser.html").forward(request, response);
            
            }
            out.print(res);
            out.close();
        }  
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al realizar pago: "+e.getMessage()+" "+e.getStackTrace());
        }
    }
    
    private String decTarjeta(String strEnc){
        String resp = null;
        
        try{
            FileProperty fp = new FileProperty("com.totalplay.prop.cfg");
            String iv = fp.getProperty("VI");
            String salt = fp.getProperty("TLAS");
            String passphrase = fp.getProperty("ESARHPSSAP");
            int ic = Integer.decode(fp.getProperty("IC"));
            int ks = Integer.decode(fp.getProperty("KS"));
            
            AesUtil au = new AesUtil(ks,ic);
            
            resp = au.decrypt(salt, iv, passphrase, strEnc);
        }catch(Exception e){
            logger.error("Error al desencriptar la tarjeta : " + e.getMessage());
        }
        
        return resp;        
    }
}
