package com.totalplay.servlets;


import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse.BankResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse.BankResponse.BankingAuthorizerResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse.BrmResult;
import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.BPELObtenerCorreo;
import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Bpelobtenercorreo_client_ep;

import com.totalplay.db.dao.GeneralDAO;
import com.totalplay.db.dbo.EventoDBO;
import com.totalplay.db.dbo.RegistroTCRDBO;
import com.totalplay.db.dbo.TarjetaDBO;
import com.totalplay.db.dbo.TransaccionDBO;
import com.totalplay.implement.ImpPaymentEngineNewTDC;
import encrypt.AesUtil;
import com.totalplay.utils.CambioMetodoPago;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.DateTime;
import com.totalplay.utils.EmailUtility;
import com.totalplay.utils.FileProperty;
import com.totalplay.utils.ProcesaHtml;
import com.totalplay.utils.ValidateForm;
import com.totalplay.vo.UsuarioVO;
import com.totalplay.ws.vo.newCardVO;

import java.io.IOException;
import java.io.PrintWriter;

import java.math.BigInteger;

import java.net.URL;

import java.text.DecimalFormat;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class SaveCardSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    @SuppressWarnings("compatibility:-8523887124550261856")
    private static final long serialVersionUID = 1L;
    transient Logger logger = LogManager.getLogger(this.getClass().getName());
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request,response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {

       try{
           FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
           response.setContentType(CONTENT_TYPE);
           request.setCharacterEncoding("UTF-8");
           HttpSession sesion= request.getSession();
           UsuarioVO usuario= (UsuarioVO)sesion.getAttribute("usuario");
           PrintWriter out = response.getWriter();
           
           
           ImpPaymentEngineNewTDC pagoNewCard= new ImpPaymentEngineNewTDC();
           String tarjetaDecryp = null;
           
           //consulta de correo
           try{
               String rutaWS= fileProperty.getProperty("OBTIENECORREOFACT");
                URL wsURL= new URL(rutaWS);
               Bpelobtenercorreo_client_ep bpelobtenercorreo_client_ep = new Bpelobtenercorreo_client_ep(wsURL,
               new QName("http://xmlns.oracle.com/SeguridadBRM/ObtenerCorreo/BPELObtenerCorreo",
                        "bpelobtenercorreo_client_ep"));
              BPELObtenerCorreo bPELObtenerCorreo = bpelobtenercorreo_client_ep.getBPELObtenerCorreo_pt();
              
              com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process paramCorreo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process();
              com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin authCorreo=  new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin();
              authCorreo.setIp("0.0.0.1");
              authCorreo.setUserId("452710");
              authCorreo.setPassword("Portales789$");
              paramCorreo.setApplicationLogin(authCorreo);
              
              com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo paramClientInfo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo();
              paramClientInfo.setAccountOrDn(usuario.getLogin());
              paramCorreo.setClientInfo(paramClientInfo);
              
              com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.ProcessResponse respCorreo= bPELObtenerCorreo.process(paramCorreo);
             
              if(respCorreo.getResult().equals("0")){ 
                 usuario.setEmail(respCorreo.getEmail());
              }
           }catch(Exception e){
               e.printStackTrace();
               logger.error("Error al consultar servicio de obtenci�n de correo: "+e.getMessage()+ " causa: "+e.getCause());
           }
           
         /*  try{
               ConsultarSesionResponse sso= sessionSSO.consultarSesion(sessid);
               if(sso.getReturn().getResponseVO().getCode().equals("0")&& sso.getReturn().getSessionVO().getStatus().equalsIgnoreCase("inactiva")){
                   sessionSSO.actualizaSesion(sessid);
               }
           }catch(java.lang.NullPointerException nex){
               logger.error("Error: " + nex.getMessage() + " Causa: " + nex.getCause());
           }*/
           
           newCardVO newCard = new newCardVO();
           String ipCliente= request.getRemoteAddr();
           String browserClient= request.getHeader("User-Agent"); 
           boolean errores=false;
                  
           newCard.setAddress(request.getParameter("cCardAdd").toUpperCase());
           newCard.setBank(request.getParameter("cCardBank"));
           
           String typeCard=request.getParameter("Ctype");
           String BankIdProvider= "";
           EmailUtility wsEmail= new EmailUtility();
           if(typeCard.equalsIgnoreCase("Mastercard"))
               BankIdProvider="3";
           else if(typeCard.equalsIgnoreCase("Visa"))
               BankIdProvider="2";
           else if(typeCard.equalsIgnoreCase("Amex"))
               BankIdProvider="1";
           else
               BankIdProvider="";
           newCard.setBankIdProvider(BankIdProvider);
           
           //************Desencriptar No. Tarjeta**********///
           String tarjetaEncryp=request.getParameter("cCardNumberEnc");
           if(tarjetaEncryp!= null){
               //String phrase = Constantes.PHRASE;
               //GnuPG pgp = new GnuPG ();
               //pgp.setGnuPGRuntime(Constantes.GNUGPGRUNTIME);
               
               //pgp.decrypt(tarjetaEncryp, phrase);
               //tarjetaDecryp= pgp.getResult();
               tarjetaDecryp = decTarjeta(tarjetaEncryp);
               newCard.setCardNumber(tarjetaDecryp);
           }

           String calle=request.getParameter("cCardAdd")!=null?request.getParameter("cCardAdd").toString().trim()!=""?request.getParameter("cCardAdd").toString().trim().toUpperCase():"":"";
           String colonia=request.getParameter("cCardCol")!=null?request.getParameter("cCardCol").toString().trim()!=""?request.getParameter("cCardCol").toString().trim().toUpperCase():"":"";
           String country=request.getParameter("cCardCtr")!=null?request.getParameter("cCardCtr").toString().trim()!=""?request.getParameter("cCardCtr").toString().trim().toUpperCase():"":"";
           String cvv2=request.getParameter("cCardSec")!=null?request.getParameter("cCardSec").toString().trim()!=""?request.getParameter("cCardSec").toString().trim().toUpperCase():"":"";
           String expirationMonth=request.getParameter("expMonth")!=null?request.getParameter("expMonth").toString().trim()!=""?request.getParameter("expMonth").toString().trim().toUpperCase():"":"";
           String expirationYear=request.getParameter("expYear")!=null?request.getParameter("expYear").toString().trim()!=""?request.getParameter("expYear").toString().trim().toUpperCase():"":"";
           String firstName=request.getParameter("cCardname")!=null?request.getParameter("cCardname").toString().trim()!=""?request.getParameter("cCardname").toString().trim().toUpperCase():"":"";
           String lastName=request.getParameter("cApellidoPat")!=null?request.getParameter("cApellidoPat").toString().trim().toUpperCase()+" "+request.getParameter("cApellidoMat").toString().trim().toUpperCase():"";
           String municipality=request.getParameter("cCardDel")!=null?request.getParameter("cCardDel").toString().trim()!=""?request.getParameter("cCardDel").toString().trim().toUpperCase():"":"";
           String provider=request.getParameter("Ctype")!=null?request.getParameter("Ctype").toString().trim()!=""?request.getParameter("Ctype").toString().trim().toUpperCase():"":"";
           String state=request.getParameter("cCardEdo")!=null?request.getParameter("cCardEdo").toString().trim()!=""?request.getParameter("cCardEdo").toString().trim().toUpperCase():"":"";
           String zip=request.getParameter("cCardCP")!=null?request.getParameter("cCardCP").toString().trim()!=""?request.getParameter("cCardCP").toString().trim().toUpperCase():"":"";
           String guardarTarjeta = request.getParameter("SaveCard")!=null?request.getParameter("SaveCard").toString().trim()!=""?request.getParameter("SaveCard").toString().trim().toUpperCase():"":"";
           String tipoTarjeta = request.getParameter("cTipoTarjeta")!=null?request.getParameter("cTipoTarjeta").toString().trim()!=""?request.getParameter("cTipoTarjeta").toString().trim().toUpperCase():"":"";
           String cargoRecurrente = request.getParameter("cCargoRecurrent")!=null?request.getParameter("cCargoRecurrent").toString().trim()!=""?request.getParameter("cCargoRecurrent").toString().trim().toUpperCase():"":"";
           String idBanko = request.getParameter("idBancoInput")!=null?request.getParameter("idBancoInput").toString().trim()!=""?request.getParameter("idBancoInput").toString().trim().toUpperCase():"":"";
           
           newCard.setColony((colonia!=null && !colonia.equalsIgnoreCase(" ") && ValidateForm.soloLetrasNums(colonia))?colonia:" ");
           newCard.setCountry((country!=null && !country.equalsIgnoreCase(" ") && ValidateForm.soloLetras(country))?country:" ");
           newCard.setCvv2((cvv2!=null && !cvv2.equalsIgnoreCase(" ") && ValidateForm.soloNumeros(cvv2))?cvv2:" ") ;
           newCard.setExpirationMonth((expirationMonth!=null && !expirationMonth.equalsIgnoreCase(" "))?expirationMonth:" ");
           newCard.setExpirationYear((expirationYear!=null && !expirationYear.equalsIgnoreCase(" "))?expirationYear:" ");
           newCard.setFirstName((firstName!=null && !firstName.equalsIgnoreCase(" ") && ValidateForm.soloLetras(firstName))?firstName:" ");
           newCard.setLastName((lastName!=null && !lastName.equalsIgnoreCase(" "))?lastName:" ");
           newCard.setMunicipality((municipality!=null && !municipality.equalsIgnoreCase(" ") && ValidateForm.soloLetrasNums(municipality))?municipality:" ");
           newCard.setProvider( (provider!=null && !provider.equalsIgnoreCase(" "))?provider:" ");
           newCard.setState( (state!=null && !state.equalsIgnoreCase(" "))?state:" ");
           newCard.setZip((zip!=null && !zip.equalsIgnoreCase(" ") && ValidateForm.soloNumeros(zip))?zip:" ");
           
           DecimalFormat df = new DecimalFormat("#.##");
           
           String amount= (request.getParameter("amm")!=null && ValidateForm.decimales(request.getParameter("amm")))?request.getParameter("amm").trim():"";
           double new_variable = Math.round((Double.parseDouble(amount))*100) / 100.0;
           amount=new_variable+"";
           //Procesamiento del pago con nueva tarjeta utilizando el Web Service correspondiente
           ProcessResponse pagoNewCardResp= pagoNewCard.payNewTDC(usuario.getEmployeenumber(), amount, newCard, ipCliente);
           
           String errorText=null;
           int errorType=0;
           HashMap<String,String> respBancoMap=null;
           if(pagoNewCardResp==null){
               errores=true;
               logger.error("ENGPAYNC0001: No se pudo conectar");
               errorText = Constantes.ERROR24+ " Error de conexion WS PagoNTC";
               errorType=2;
           }
           else{
               BankResponse respuestaBanco = pagoNewCardResp.getBankResponse();
               BrmResult respuestaBRM = pagoNewCardResp.getBrmResult();
             //  SaveCardResponse respuestaGuardaTC= pagoNewCardResp.getSaveCardResponse();
               
               BankingAuthorizerResponse autorizacionBanco= respuestaBanco.getBankingAuthorizerResponse();
               if(autorizacionBanco!=null){
                   errores=false;
                   respBancoMap=new HashMap<String,String>();
                   String StatusBanco= respuestaBanco.getResponse().getCode();
                   if(!StatusBanco.isEmpty() && StatusBanco.equals("0")){
                       respBancoMap.put("authorizationCode",autorizacionBanco.getAuthorizationCode());
                       respBancoMap.put("bankResponseCode", autorizacionBanco.getBankResponseCode());
                       respBancoMap.put("descriptionResponse", autorizacionBanco.getDescriptionResponse());
                       respBancoMap.put("transactionControlNumber", autorizacionBanco.getTransactionControlNumber());
                       respBancoMap.put("standardResponseCode", autorizacionBanco.getStandardResponseCode());
                       respBancoMap.put("affiliation",autorizacionBanco.getAffiliation());
                       respBancoMap.put("affiliationName",autorizacionBanco.getAffiliationName());
                       respBancoMap.put("transactionDate", autorizacionBanco.getTransactionDate());
                       
                       if(respuestaBRM!=null){
                           String StatusBRM = respuestaBRM.getResponse().getCode();
                           if(!StatusBRM.equals("0")){
                               errores=true;
                                logger.error("ENGPAYNC0005: Error al actualizar BRM: "+respuestaBRM.getResponse().getDescription());                           
                                errorText = Constantes.ERROR25;
                                respBancoMap.put("descriptionResponse", respuestaBRM.getResponse().getDescription());
                                errorType=3;
                           }
                       }
                       else{
                           logger.error("Error, respuestaBRM null");
                           }
                       /*if(respuestaGuardaTC!=null){
                            String StatusRecordCard=respuestaGuardaTC.getResponse().getCode();
                            if(!StatusRecordCard.equals("0")){
                                errores=true;
                                 logger.error("ENGPAYNC0006: Error al guardar la tarjeta: "+respuestaGuardaTC.getResponse().getDescription());
                                 errorText=Constantes.ERROR26;
                                 respBancoMap.put("descriptionResponse", respuestaGuardaTC.getResponse().getDescription());
                                 errorType=4;
                            }
                       } */                   
                   }else{
                       respBancoMap=new HashMap<String,String>();
                       errores =true;
                       logger.error("ENGPAYNC0003: No se autoriz� el pago: "+ autorizacionBanco.getDescriptionResponse());
                       errorText = Constantes.ERROR21+" "+ autorizacionBanco.getDescriptionResponse();
                       respBancoMap.put("descriptionResponse", autorizacionBanco.getDescriptionResponse());
                       errorType=1;
                   }
               }else{
                   respBancoMap=new HashMap<String,String>();
                   errores =true;
                   logger.error("ENGPAYNC0003: No se autoriz� el pago: "+ respuestaBanco.getResponse().getDescription());
                   errorText = Constantes.ERROR21+" "+ respuestaBanco.getResponse().getDescription();
                   respBancoMap.put("descriptionResponse", respuestaBanco.getResponse().getDescription());
                   errorType=1;      
               }
           }
           TransaccionDBO transact= new TransaccionDBO();
           transact.setUserID(usuario.getEmployeenumber());
           String textRBM="";
           if(respBancoMap!=null){
                respBancoMap=new HashMap<String,String>();
                textRBM+=respBancoMap.get("descriptionResponse");
                transact.setMessage("|NUEVA|"+textRBM+"|"+newCard.getFirstName()+" "+newCard.getLastName()+"|"+calle+"|"+colonia+
                                  "|"+municipality+"|"+state+"|"+country+"|"+zip+"|");
                transact.setTitular(usuario.getName()+" "+usuario.getLastname());
                transact.setAmmount(amount);
                transact.setAuthBank(respBancoMap.get("authorizationCode")!=null?respBancoMap.get("authorizationCode"):" ");
                transact.setBankName(newCard.getBank()!=null?newCard.getBank():" ");
                String tarjetaFin= newCard.getCardNumber()!=null?newCard.getCardNumber().substring(newCard.getCardNumber().length()-4,newCard.getCardNumber().length()):" ";
                transact.setCEndNumber(tarjetaFin);
                transact.setError(errorType);
                transact.setIp(ipCliente);
                transact.setBrowser(browserClient);
                transact.setLogdate(DateTime.obtieneFechaHoraActual("yyyy-MM-dd hh:mm:ss"));
                }
           
           
           else{
                logger.error("Error en: respBancoMap null");
               }
           
           
           GeneralDAO transactBO= new GeneralDAO(3);
           if(!transactBO.insertTransaction(transact))
               logger.error("Problema al insertar transacci�n en BD: "+transact.getUserID());
                    
              
              String rutaSoporte= this.getServletContext().getRealPath("/assets/mail/errores.htm");
               rutaSoporte=rutaSoporte!=null?rutaSoporte:getServletContext().getResource("/assets/mail/errores.htm").getPath();
              String statusMsg="Autorizado";
              switch(errorType){
                   case 1: statusMsg= "El pago no fu� autorizado: "+respBancoMap.get("descriptionResponse");
                           break;
                   case 2: statusMsg= "Error al obtener respuesta de Web Service PagoNTC";
                           break;
                   case 3: statusMsg= "Pago autorizado pero no se actualiz� la cuenta en BRM "+respBancoMap.get("descriptionResponse");
                           break;
                   case 4: statusMsg= "Pago autorizado pero no se guard� la tarjeta  "+ respBancoMap.get("descriptionResponse");      
                           break;
                   case 5: statusMsg= "Pago no fue autorizado, por un problema interno "+ respBancoMap.get("descriptionResponse");      
                           break;
                   case 6: statusMsg= "El pago fue autorizado y la tarjeta se guard� exitosamente"+ respBancoMap.get("descriptionResponse");      
                           break;
                    default: logger.error("case: default, Error con el pago");
                            break;
              
              }
              //hardcode///////////////
              //errores=false;
              ////////////////////////
              if(errores){
                  respBancoMap=new HashMap<String,String>();
                  statusMsg="(RTCR)Cuenta: "+usuario.getEmployeenumber()+" Error: "+"("+respBancoMap.get("descriptionResponse")+")";
                  sesion.setAttribute("AuthBancaria", null);
                  String bodyEmail = ProcesaHtml.creaCadenaHTMLSoporte(rutaSoporte,statusMsg);
                  wsEmail.sendEmail(Constantes.EMAIL_SUPORT, Constantes.EMAIL_SUPPORTCC, "?", "?", Constantes.SUBJECT_SUPPORT, bodyEmail);
                  out.println("errorPago");
              }
              else{
                  //hardcode
                  //respBancoMap.put("authorizationCode","012345678");
                  //errorType=0;
                  respBancoMap=new HashMap<String,String>();
                  transact.setAuthBank(respBancoMap.get("authorizationCode"));
                  transact.setLogdate(DateTime.obtieneFechaHoraActual("yyyy-MM-dd hh:mm:ss"));
                  transact.setMessage("|NUEVA|"+respBancoMap.get("descriptionResponse")+"|"+newCard.getFirstName()+" "+newCard.getLastName()+"|"+colonia+
                                  "|"+municipality+"|"+state+"|"+country+"|"+zip+"|");
                  sesion.setAttribute("AuthBancaria", respBancoMap.get("authorizationCode")); 

           String nombre= request.getParameter("cCardname")!=null?request.getParameter("cCardname").trim().toUpperCase():"";
           String apellPaterno= request.getParameter("cApellidoPat")!=null?request.getParameter("cApellidoPat").trim().toUpperCase():"";
           String apellMaterno= request.getParameter("cApellidoMat")!=null?request.getParameter("cApellidoMat").trim().toUpperCase():"";
            
            //String tarjetaEncryp=request.getParameter("cCardNumberEnc");
            //String tarjetaDecryp="";
            if(tarjetaEncryp!= null){
                /*String phrase = Constantes.PHRASE;
                GnuPG pgp = new GnuPG ();
                pgp.setGnuPGRuntime(Constantes.GNUGPGRUNTIME);
                
                pgp.decrypt(tarjetaEncryp, phrase);
                tarjetaDecryp= pgp.getResult();*/
                tarjetaDecryp = decTarjeta(tarjetaEncryp);
            } 
            String mesExpiracion=request.getParameter("expMonth")!=null?request.getParameter("expMonth").trim():"";
            String anioExpiracion=request.getParameter("expYear")!=null?request.getParameter("expYear").trim():"";
            String tipoPago=request.getParameter("cTipoPago")!=null?request.getParameter("cTipoPago").trim():"";
            //String tipoTarjeta=request.getParameter("cTipoTarjeta")!=null?request.getParameter("cTipoTarjeta").trim():"";
            String aceptaCargos=request.getParameter("cCargoRecurrent")!=null?request.getParameter("cCargoRecurrent").trim():"";
            String direccion= request.getParameter("cCardAdd")!=null?request.getParameter("cCardAdd").trim():"";
            String ciudad= request.getParameter("cCardCol")!=null?request.getParameter("cCardCol").trim():"";
            String estado=request.getParameter("cCardEdo")!=null?request.getParameter("cCardEdo").trim():"";
            String cp=request.getParameter("cCardCP")!=null?request.getParameter("cCardCP").trim():"";
            //String country=request.getParameter("cCardCtr")!=null?request.getParameter("cCardCtr").trim():"";
           String idBanco= request.getParameter("cCardBankId")!=null?request.getParameter("cCardBankId").trim():"";
            
           String numCuenta= usuario.getEmployeenumber(); 
            
           String nombreCompleto= nombre+" "+apellPaterno+" "+apellMaterno; 
           String PAY_TYPE = tipoPago;       
           BigInteger ACH = null;
           if(PAY_TYPE.equalsIgnoreCase("AMEX")){
               ACH = BigInteger.valueOf(1L);
           }else{
               ACH = BigInteger.valueOf(2L);
           } 
            
            RegistroTCRDBO registro= new RegistroTCRDBO();
            registro.setBanco(newCard.getBank()!=null?newCard.getBank().toUpperCase():" ");
            registro.setBrowser(browserClient);
            registro.setComentario("");
            registro.setEmail(request.getParameter("email")!=null?request.getParameter("email").toString().trim().toUpperCase():"");
            registro.setIp(ipCliente);
            registro.setNombre_cliente(usuario.getName()+" "+usuario.getLastname());
            registro.setNom_tarhab(nombreCompleto);
            registro.setNum_cuenta(usuario.getEmployeenumber());
            registro.setPromocion(request.getParameter("promocion")!=null?request.getParameter("promocion").trim().toUpperCase():"");
            registro.setTel_fijo(request.getParameter("telFijo")!=null?request.getParameter("telFijo").trim().toUpperCase():"");
            registro.setTel_movil(request.getParameter("celular")!=null?request.getParameter("celular").trim().toUpperCase():"");
            
            
            if(tarjetaDecryp!=null){
                registro.setTermtarj(tarjetaDecryp.substring(12));
                
            TarjetaDBO tarjeta= new TarjetaDBO();
            tarjeta.setApellido_materno(apellMaterno);
            tarjeta.setApellido_paterno(apellPaterno);      
            tarjeta.setCalle(calle);
            tarjeta.setCodigo_postal(cp);
            tarjeta.setColonia(colonia);
            tarjeta.setDelegacion(ciudad);
            tarjeta.setEstado(estado);
            tarjeta.setFecha_vencimiento("01/"+mesExpiracion+"/"+anioExpiracion);
            tarjeta.setId_promocion(request.getParameter("promocion")!=null?request.getParameter("promocion").trim().toUpperCase():"");
            tarjeta.setNombre(nombre);
            tarjeta.setNum_tarjeta(tarjetaDecryp.substring(12));
            tarjeta.setNum_tarjeta_mascara(tarjetaDecryp.substring(12));
            tarjeta.setNumero_serguridad(cvv2);
            tarjeta.setPais(country);
                  
            registro.setTarjeta(tarjeta);
            }
                  
                  
            EventoDBO evento= new EventoDBO();
            evento.setEstatus(respBancoMap.get("descriptionResponse"));      
            evento.setId_canal("1");//se establece como el canal 1 como predeterminado (Portal MiTotalPlay)      
            evento.setMonto(amount);
            registro.setEvento(evento);
            
           if(aceptaCargos.trim().equalsIgnoreCase("si")){
               country="MX";
               CambioMetodoPago cambioMetodo= new CambioMetodoPago();
               if(tipoTarjeta.equalsIgnoreCase("credito") && evento.getEstatus().contains("Aprobada")){
                   if(cambioMetodo.MetodoPagoCredito(numCuenta, "10003", ACH, direccion, ciudad, country, nombreCompleto, estado, cp, mesExpiracion+anioExpiracion, new BigInteger(tarjetaDecryp), BigInteger.valueOf(Long.parseLong(cvv2)))){
                        if(!transactBO.insertRegistroCompleto(registro))
                           logger.error("Error al insertar registro de tarjeta para cargo recurrente");
                        out.println("save");
                   }else{
//                        errorType = 10;    
                       out.println("dontsave");
                   }
               }else{
                   if(tipoTarjeta.equalsIgnoreCase("debito") && evento.getEstatus().contains("Aprobada")){
                        if(cambioMetodo.MetodoPagoCredito(numCuenta, "10003", ACH, direccion, ciudad, country, nombreCompleto, estado, cp, mesExpiracion+anioExpiracion, new BigInteger(tarjetaDecryp), BigInteger.valueOf(Long.parseLong(cvv2)))){
                            if(!transactBO.insertRegistroCompleto(registro))
                                logger.error("Error al insertar registro de tarjeta para cargo recurrente");
                            out.println("save");
                        }else{
                            out.println("dontsave");                           
                        }
                   }
               }
               
           }
        }
           out.close();
       }catch(Exception e){
           e.printStackTrace();
           logger.error("Error al guardar tarjeta para cargo recurrente: "+e.getMessage()+" "+e.getCause());
       }

    } 
    
    private String decTarjeta(String strEnc){
        String resp = null;
        
        try{
            FileProperty fp = new FileProperty("com.totalplay.prop.cfg");
            String iv = fp.getProperty("VI");
            String salt = fp.getProperty("TLAS");
            String passphrase = fp.getProperty("ESARHPSSAP");
            int ic = Integer.decode(fp.getProperty("IC"));
            int ks = Integer.decode(fp.getProperty("KS"));
            
            AesUtil au = new AesUtil(ks,ic);
            
            resp = au.decrypt(salt, iv, passphrase, strEnc);
        }catch(Exception e){
            logger.error("Error al desencriptar la tarjeta : " + e.getMessage());
        }
        
        return resp;        
    }
  
}