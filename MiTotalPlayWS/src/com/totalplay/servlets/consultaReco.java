package com.totalplay.servlets;

import org.apache.http.client.methods.HttpPost;

import com.google.gson.Gson;

import com.totalplay.utils.FileProperty;
import com.totalplay.vo.ConsultaRecomVO;

import java.io.IOException;
import java.io.PrintWriter;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.http.client.HttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class consultaReco extends HttpServlet {
    private static final String CONTENT_TYPE = "application/json";
    Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        
        HttpSession sesion= request.getSession(false);        
        String numCuenta = (String)sesion.getAttribute("numCuenta");
        FileProperty fileProp = new FileProperty("com.totalplay.prop.Recom");        
        boolean cont=true;        
        
        if(numCuenta!=null && cont){
            String host = fileProp.getProperty("host");
            
            JSONObject jsonLogin = new JSONObject();
            JSONObject jsonDatos = new JSONObject();
            JSONArray jsonarrayL = new JSONArray();            
            
            HttpResponse respon = null;     
            HttpPost post = new HttpPost(host);
                
            if(cont && post != null){
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date hoy= new Date();
                Calendar c= Calendar.getInstance();
                c.setTime(hoy);                        
                c.add(Calendar.MONTH, -3);                        
                Date pasado=c.getTime();
                String fechaInicio=dateFormat.format(pasado);
                String fechaFin=dateFormat.format(hoy);
                HttpClient httpclient = new DefaultHttpClient();
                
                try{
                    jsonLogin.put("User", fileProp.getProperty("usr"));
                    jsonLogin.put("Password", fileProp.getProperty("pwd"));
                    jsonLogin.put("Ip", fileProp.getProperty("ip"));   
                    jsonarrayL.put(jsonLogin);
                    
                    jsonDatos.put("login", jsonarrayL);
                    jsonDatos.put("idTecnico","");
                    jsonDatos.put("noCuenta",numCuenta);
                    jsonDatos.put("fechaInicio",fechaInicio);
                    jsonDatos.put("fechaFin",fechaFin);
                }catch(JSONException ex){
                    logger.error("No se puede generar json " + ex.getMessage());                    
                }
                        
                post.setHeader("Content-type", "application/json");
                post.setHeader("Authorization", "Basic ZmZtYXBwOjRnZW5kNG1pM250bw==");
                post.setHeader("cache-control", "no-cache");
                post.setHeader("Content-Type", "application/json; charset=UTF-8");
                post.setHeader("Accept", "application/json");
                
                try{                    
                    ConsultaRecomVO recomData=new ConsultaRecomVO();
                    StringEntity jsonFinal = new StringEntity(new String(jsonDatos.toString().getBytes(),"UTF-8"));             
                    post.setEntity(jsonFinal);
                    respon = httpclient.execute(post);       
                    String body = EntityUtils.toString(respon.getEntity(), "UTF-8");
                    JSONObject jsons = new JSONObject(body);
                    String resultRespone = jsons.get("result").toString();
                    
                    if(respon != null && resultRespone.equalsIgnoreCase("0")){   
                        JSONObject misrecomendados = new JSONObject(jsons.get("MisRecomendados").toString());
                        recomData.setNumContactar(Integer.parseInt((misrecomendados.get("numContactar").toString())));
                        recomData.setNumInstalar(Integer.parseInt((misrecomendados.get("numInstalar").toString())));
                        recomData.setNumNoExito(Integer.parseInt((misrecomendados.get("numNoExito").toString())));
                        recomData.setNumIlocalizable(Integer.parseInt((misrecomendados.get("numIlocalizable").toString())));
                        recomData.setNuminstalados(Integer.parseInt((misrecomendados.get("numinstalados").toString())));
                        recomData.setStatus(0);                            
                        
                        Gson json=new Gson();
                        out.print(json.toJson(recomData));                        
                    }else{
                        out.print( "{\"status\": 1 }");   
                    }
                }catch(Exception ex){ 
                    logger.error("No se puede establecer la conexi�n " + ex.getMessage());
                    out.print( "{\"status\": 2 }"); 
                }                
                finally{
                    cerrar(httpclient);
                }
            }else{
                out.print( "{\"status\": 3 }");    
            }            
        }else{
            out.print( "{\"status\": 4 }");    
        }
        out.close();
    }
    
    public void cerrar(HttpClient httpclient){
           try{
               if(httpclient!=null){
                   httpclient.getConnectionManager().shutdown();
               }
           }catch(Exception ex){
               logger.error("no se pudo cerrar el cliente");
           }
       }
}
