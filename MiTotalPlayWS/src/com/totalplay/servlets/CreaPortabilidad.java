package com.totalplay.servlets;


import com.totalplay.services.Portabilidad;
import com.totalplay.vo.UsuarioVO;

import java.io.IOException;
import java.io.PrintWriter;

import java.security.SecureRandom;

import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import portabilidad.InsertarPortabilidad;
import portabilidad.ResponseVO;

import java.text.SimpleDateFormat;

import java.text.DateFormat;

import java.util.Date;


public class CreaPortabilidad extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
        try{
            
            response.setContentType(CONTENT_TYPE);
            PrintWriter out = response.getWriter();
            HttpSession sesion= request.getSession(false);
            String numCuenta= (String)sesion.getAttribute("numCuenta");
            UsuarioVO usuario= (UsuarioVO)sesion.getAttribute("usuario");
            boolean archValid=true;
            ServletFileUpload up= new ServletFileUpload(new DiskFileItemFactory());
            up.setHeaderEncoding("UTF-8");
            List<FileItem> items = up.parseRequest(request); 
                       
            java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy");
            java.util.Date date = new java.util.Date();
            String superFecha=dateFormat.format(date); 
            
            InsertarPortabilidad parameters= new InsertarPortabilidad();             
            parameters.setNOCUENTA(numCuenta);   
            parameters.setESTATUS("En proceso");
            parameters.setFECHAPROGRAMACION(dateFormat.format(date));
            
            //Random rng= new Random();
            //String folio=rng.nextInt(9000)+1000+"";
            SecureRandom sr = new SecureRandom();
            sr = SecureRandom.getInstance("SHA1PRNG");
            byte[] bytes = new byte[512];
            sr.nextBytes(bytes);
            
            String folio = (int)(sr.nextDouble() * 9000)+1000+"";//String folio = sr.nextInt(9000)+1000+"";
                
            String respuestaFinal="";
            
            while(!Portabilidad.consultaFolio(numCuenta, folio).getFOLIO().equals("")){
                folio = (int)(sr.nextDouble() * 9000)+1000+"";//folio = sr.nextInt(9000)+1000+"";
                if(Portabilidad.consultaFolio(numCuenta, folio).getFOLIO().equals("")){
                    //System.out.println("Se genera folio: "+folio);
                    break;
                }
            }
            
            parameters.setFOLIO(folio);
            for (FileItem item : items) { 
                
                    if(item.getFieldName().equals("DN_0"))
                        parameters.setPORTADO(item.getString());
                    if(item.getFieldName().equals("telCont"))
                        parameters.setPROVISIONAL(item.getString());
                    if(item.getFieldName().equals("nip_0"))
                        parameters.setNIP(item.getString());
                    if(item.getFieldName().equals("emailPorta"))
                        parameters.setEMAIL(item.getString());
                    if(item.getFieldName().equals("fechaPort"))
                        parameters.setFECHAPROGRAMACION(item.getString());
                    if(item.getFieldName().equals("razonSocial"))
                        parameters.setTIPOPERSONA(item.getString());
                
                
                //persona fisica
                if (!item.isFormField()) {
                    if(item.getFieldName().equals("btnIdenPorta") && item.get()!=null){
                        String input=Base64.encodeBase64String(item.get());
                        String cabecera = input.substring(0,15).toUpperCase();
                        //if(cabecera.contains("JVBERI0") || cabecera.contains("/9J/4AA")){
                        if(true){
                            int indice=item.getName().length()-3;
                            String extension=item.getName().substring(indice);
                            if(extension.equals("pdf")||extension.equals("jpg") || item.getName().endsWith(".pdf") || item.getName().endsWith(".jpg")){
                                parameters.setARCHIVOIDENTIFICACION(input);
                            }else{
                                //respuestaFinal="{ \"code\":\"1\"}";
                                respuestaFinal="{ \"code\":\"1\",\"desc\":'no es el tipo de archivo'}";
                                archValid=false;
                            }
                        }else{
                            //respuestaFinal="{ \"code\":\"1\"}";
                            respuestaFinal="{ \"code\":\"1\",\"desc\":'la cabezera falla'}";
                            archValid=false;
                        }
                    }
                    
                    //moral
                    if(item.getFieldName().equals("btnSolPorta") && item.get()!=null){
                        String input=Base64.encodeBase64String(item.get());
                        String cabecera = input.substring(0,15).toUpperCase();
                        //if(cabecera.contains("JVBERI0") || cabecera.contains("/9J/4AA")){
                        if(true){
                            int indice=item.getName().length()-3;
                            String extension=item.getName().substring(indice);
                            if(extension.equals("pdf")||extension.equals("jpg") || item.getName().endsWith(".pdf") || item.getName().endsWith(".jpg")){
                                parameters.setARCHIVOSOLICITUD(input);
                                
                            }else{
                                //respuestaFinal="{ \"code\":\"2\"}";
                                respuestaFinal="{ \"code\":\"2\",\"desc\":'no es el tipo de archivo'}";
                                archValid=false;
                            }
                        }else{
                            respuestaFinal="{ \"code\":\"2\"}";
                            respuestaFinal="{ \"code\":\"2\",\"desc\":'la cabezera falla'}";
                            archValid=false;
                        }
                    }
                    
                    if(item.getFieldName().equals("btnCartaCancel") && item.get()!=null){
                        String input=Base64.encodeBase64String(item.get());
                        String cabecera = input.substring(0,15).toUpperCase();
                        //if(cabecera.contains("JVBERI0") || cabecera.contains("/9J/4AA")){
                        if(true){
                            int indice=item.getName().length()-3;
                            String extension=item.getName().substring(indice);
                            if(extension.equals("pdf")||extension.equals("jpg") || item.getName().endsWith(".pdf") || item.getName().endsWith(".jpg")){
                               parameters.setARCHIVOCARTACAN(input);
                               
                            }else{
                                //respuestaFinal="{ \"code\":\"3\"}";
                                respuestaFinal="{ \"code\":\"3\",\"desc\":'no es el tipo de archivo'}";
                                archValid=false;
                            }
                        }else{
                            respuestaFinal="{ \"code\":\"3\"}";
                            respuestaFinal="{ \"code\":\"3\",\"desc\":'la cabezera falla'}";
                            archValid=false;
                        }
                    }
                    
                    if(item.getFieldName().equals("btnActaCons") && item.get()!=null){
                        String input=Base64.encodeBase64String(item.get());
                        String cabecera = input.substring(0,15).toUpperCase();
                        //if(cabecera.contains("JVBERI0") || cabecera.contains("/9J/4AA")){
                        if(true){
                            int indice=item.getName().length()-3;
                            String extension=item.getName().substring(indice);
                            if(extension.equals("pdf")||extension.equals("jpg") || item.getName().endsWith(".pdf") || item.getName().endsWith(".jpg")){
                                parameters.setARCHIVOACTACONS(input);
                                
                            }else{
                                respuestaFinal="{ \"code\":\"4\"}";
                                respuestaFinal="{ \"code\":\"4\",\"desc\":'no es el tipo de archivo'}";
                                archValid=false;
                            }
                        }else{
                            respuestaFinal="{ \"code\":\"4\"}";
                            respuestaFinal="{ \"code\":\"4\",\"desc\":'la cabezera falla'}";
                            archValid=false;
                       }
                   }
                    
                }//persona fisica
                
            }
            
            
            /*parameters.setNOMBRES(usuario.getName());
            parameters.setAPELLIDOS(usuario.getLastname());
            parameters.setCOMENTARIO("Tu solicitud est� en proceso");
            parameters.setARCHIVOACTACONS("");
            parameters.setARCHIVOCARTACAN("");
            parameters.setARCHIVOIDENTIFICACION("");
            parameters.setARCHIVOSOLICITUD("");*/
            
            
            
            parameters.setNOMBRES(usuario.getName());
            parameters.setAPELLIDOS(usuario.getLastname());
            parameters.setCOMENTARIO("Tu solicitud est� en proceso");
            parameters.setFECHAPROGRAMACION(superFecha);
            
            
            if(archValid==true){
                ResponseVO respuesta = Portabilidad.insertaPortabilidad(parameters);
                
                logger.error("getARCHIVOACTACONS"+parameters.getARCHIVOACTACONS());
                logger.error("getARCHIVOCARTACAN"+parameters.getARCHIVOCARTACAN());
                logger.error("getARCHIVOIDENTIFICACION"+parameters.getARCHIVOIDENTIFICACION());
                logger.error("getARCHIVOSOLICITUD"+parameters.getARCHIVOSOLICITUD());
                
                if(respuesta.getCode().trim().equalsIgnoreCase("0")){
                    
                    respuestaFinal="{ \"code\":\"0\", \"folio\" :\""+folio+"\" ,\"numeroPortado\": \""+parameters.getPORTADO()+"\"}";
                }else{
                    respuestaFinal="{ \"code\":\"-1\"}";
                }
            }
            out.print(respuestaFinal);
            out.close();
        }catch(Exception e){
            e.printStackTrace();    
        }
    }
}
