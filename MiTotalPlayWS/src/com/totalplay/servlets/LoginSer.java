package com.totalplay.servlets;


import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.ProcessResponse;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BPELgetInfoBRM;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Bpelgetinfobrm_client_ep;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Process;

import com.totalplay.services.EngineSecurity;
import com.totalplay.services.GetInfoBRM;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;
import com.totalplay.utils.VerificaUsuario;
import com.totalplay.vo.UsuarioVO;

import encrypt.DecryptJSON;

import java.io.IOException;

import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import org.tempuri.SecurityUserModel;


public class LoginSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
 
   Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws  ServletException, 
                                                            IOException { 
        doPost(request, response);
    }
  
    public void doPost(HttpServletRequest request,
                      HttpServletResponse response) throws  ServletException, 
                                                            IOException {    
        
        response.setContentType(CONTENT_TYPE);
        try{
        HttpSession sesion= request.getSession(false);
        String sessionJava=null;
            
        if(request.getParameter("secdat")!=null)
            sesion.setAttribute("secdat", request.getParameter("secdat"));
        
        String encriptado= (String)sesion.getAttribute("secdat");
        
        if(sesion!=null){
                try {
                    String sessid= sesion.getId();
                    String usuario= null;
                    String pass=null;    
                    //SecurityUserModel infoUser= null;
                    String infoUser = null;
                    String numCuenta=null;
                    ProcessResponse.DatosCliente infoCliente=null;
                    
                    if(encriptado!=null){
                        String [] datosUser = DecryptJSON.DecryptJSONString(encriptado);
                        usuario=datosUser[0];
                        pass=datosUser[1];
                    }
                    else{
                        usuario = ((String)sesion.getAttribute("numCuenta")!=null)?(String)sesion.getAttribute("numCuenta"):request.getParameter("numCuenta");
                        if(VerificaUsuario.isNumTelefono(usuario)){
                            infoCliente = GetInfoBRM.obtieneCuenta(usuario, 2);
                            usuario= infoCliente.getAccountNo();
                        }
                        else{
                            logger.error("Error, isNumTelefono");
                            }
                        infoCliente = null;
                        //SecurityUserModel info = EngineSecurity.getNoAccount(usuario);   
                        //pass=info.getPassword();
                       // response.sendRedirect("infoclienteser?numCuenta="+numCuenta);
                    }
                    
                    UsuarioVO user= new UsuarioVO();
                    //hardcode
                   /* usuario=request.getParameter("iUser");
                    pass= request.getParameter("iPwd");*/   
                    
                  FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
                    String parametroCuenta=usuario;
                    int bandera=0;
                    if(VerificaUsuario.isNumCuentaVarios(parametroCuenta)){
                        bandera=0;
                    }else if(VerificaUsuario.isNumTelefono(parametroCuenta)){
                        bandera=2;
                    }else if(VerificaUsuario.isEmail(parametroCuenta)){
                        bandera=1;
                    }
                    com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.ProcessResponse.DatosCliente datos = GetInfoBRM.obtieneCuenta(parametroCuenta, bandera);
                    String cia= "";
                    com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM result = new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM();
                    
                    //obtenemos la compa�ia
                    if(datos!=null){
                        numCuenta=datos.getAccountNo();
                        logger.error("numero de cuenta obtenido:"+numCuenta);
                        String wsRuta= fileProperty.getProperty("DASHBOARD");
                        URL urlWS= new URL(wsRuta);
                        Bpelgetinfobrm_client_ep bpelgetinfobrm_client_ep = new Bpelgetinfobrm_client_ep(urlWS,
                            new QName("http://xmlns.oracle.com/MiddlewareApplication/BrmSearchAccount/BPELgetInfoBRM",
                            "bpelgetinfobrm_client_ep"));
                       BPELgetInfoBRM bPELgetInfoBRM = bpelgetinfobrm_client_ep.getBPELgetInfoBRM_pt();
                        Process payload= new Process();
                        payload.setACCOUNTNO(numCuenta);
                   
                        com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse respuestaWS = bPELgetInfoBRM.process(payload);
                        result =respuestaWS.getArrBRM();
                                                                   
                        cia= result.getCiaPerteneciente();
                    }
                    //Fin obtenemos la compa�ia
                    
                //hardacoede mientras no accesamos al servicio de login/
                   if(VerificaUsuario.isNumCuentaVarios(usuario)){
                        if(cia.equalsIgnoreCase("totalplay")){
                            //infoUser= new SecurityUserModel();
                            //infoUser.setEmployeeNumber(usuario);
                            //infoUser= EngineSecurity.consumeLoginWS(usuario,pass);
                            /*if(infoUser!=null){
                            
                                numCuenta=infoUser.getEmployeeNumber();
                                //numCuenta=usuario;                                
                                infoCliente = GetInfoBRM.obtieneCuenta(usuario, 0);
                                if(infoCliente!=null){
                                    infoUser.setLogin(infoCliente.getAccountNo());
                                    infoUser.setFirstName(infoCliente.getName());
                                    infoUser.setLastName(infoCliente.getLastName());
                                }
                                infoCliente = null;
                            }else{
                                logger.error("Error infoUser");
                            } */                           
                        }else{
                            infoUser=null;
                            logger.error("Error, cia = null");
                        }
                        cia = "";
                        //infoCliente = null;
                    }
                    else if(VerificaUsuario.isEmail(usuario)){
                        if(cia.equalsIgnoreCase("totalplay")){
                            /*infoUser = EngineSecurity.loginByEmailWS(usuario, pass);    
                            if(infoUser!=null ){
                                numCuenta= infoUser.getEmployeeNumber();
                                infoCliente = GetInfoBRM.obtieneCuenta(usuario, 1);
                                if(infoCliente!=null){
                                    infoUser.setFirstName(infoCliente.getName());
                                    infoUser.setLastName(infoCliente.getLastName());
                                }
                            }*/
                        }else{
                            infoUser=null;
                        }
                    }
                    
                    
                    else{
                        if(VerificaUsuario.isNumTelefono(usuario)){
                            if(cia.equalsIgnoreCase("totalplay")){
                                infoCliente = GetInfoBRM.obtieneCuenta(usuario, 2);
                                if(infoCliente!=null){
                                    numCuenta= infoCliente.getAccountNo();
                                    //infoUser = EngineSecurity.consumeLoginWS(numCuenta,pass);
                                }
                            }
                        }else{
                            infoUser=null;
                        }
                    }
                    
                    logger.error("infoUser: " + infoUser);
                    
                    //if(infoUser!=null && numCuenta!=null){
                    if(numCuenta!=null){
                        logger.error("numero de cuenta diferente a null");
                        user.setUserid("");
                        user.setLogin(numCuenta);
                        user.setEmployeenumber(numCuenta);
                        user.setName(datos.getName());
                        user.setLastname(datos.getLastName());
                        user.setEmail("pruebamitotal@gmail.com");
                        user.setPass("123456");
                        /*user.setUserid(infoUser.getUserId());
                        user.setLogin(numCuenta);
                        user.setEmployeenumber(infoUser.getEmployeeNumber());
                        user.setName(infoUser.getFirstName().toUpperCase());
                        user.setLastname(infoUser.getLastName().toUpperCase());
                        user.setEmail(infoUser.getEmail());
                        user.setPass(infoUser.getPassword());*/
                        sesion.setAttribute("numCuenta", numCuenta);
                            
                        if(infoCliente !=null){
                            user.setTelephone(infoCliente.getDn());
                        }else{
                            user.setTelephone("5555555555");
                        }
                        
                        user.setStatus(result.getStatus());
                        
                        sesion.setAttribute("usuario", user);
                        sesion.setAttribute("sesionID", sessionJava);
                        
                        request.getRequestDispatcher("infoclienteser.html").forward(request, response);
                       
                    }else {
                        logger.error("Error, infoUser = null");
                        infoUser = null;
                        sesion.setAttribute("tipoError", "1");
                        logger.error("No se encontraron datos del cliente: "+numCuenta+" "+user.getLogin());
                        RequestDispatcher despachador =
                        getServletContext().getRequestDispatcher("/index.jsp");
                        despachador.include(request, response);
                    }
                    infoCliente = null;
                } catch (Exception e) {
                    sesion.setAttribute("tipoError", "1");
                    logger.error("Error: "+e.getMessage()+ " Causa: "+e.getCause());
                    
                    RequestDispatcher despachador =
                    getServletContext().getRequestDispatcher("/index.jsp");
                    despachador.include(request, response);
                }
                
           // }        
            
        }
        else{
            RequestDispatcher despachador =
                getServletContext().getRequestDispatcher("/index.jsp");
            despachador.include(request, response);
        }
        }catch(Exception e){
            
            logger.error("Error: "+e.getMessage()+ " Causa: "+e.getCause());
            e.printStackTrace();
            RequestDispatcher despachador =
                getServletContext().getRequestDispatcher("/index.jsp");
            despachador.include(request, response);
        }
    }
}
