package com.totalplay.servlets;


import com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.BPELBalanceBRM;
import com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Bpelbalancebrm_client_ep;

import com.totalplay.services.BrmSearchAccount;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.DateTime;
import com.totalplay.utils.FileProperty;
import com.totalplay.vo.FacturaVO;
import com.totalplay.vo.HistoricoFacturaVO;

import encrypt.encriptaInterfactura;

import java.io.IOException;
import java.io.PrintWriter;

import java.net.URL;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.JSONException;
import org.json.JSONObject;

public class FacturasSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html;charset=windows-1252";
    private Logger logger = LogManager.getLogger(this.getClass().getName());
    
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
        try{
            response.setContentType(CONTENT_TYPE);
            request.setCharacterEncoding("UTF-8");
            HttpSession sesion= request.getSession(false);
            com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.BillInfo infoCuenta;
        
            String numCuenta= (String)sesion.getAttribute("numCuenta");
            DecimalFormat formatoMoneda = new DecimalFormat("$#,##0.00 MXN");
           
            com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM objArrBRM = BrmSearchAccount.getInfoClient(numCuenta);
            infoCuenta= objArrBRM.getBillInfo();
            
            String tipoMoneda= (String)sesion.getAttribute("tipoMoneda")!=null ?(String)sesion.getAttribute("tipoMoneda"):"";
            if(tipoMoneda!="" && !tipoMoneda.equals("484")){
                formatoMoneda = new DecimalFormat("$#,##0.00 USD");
            }
            
            DecimalFormat df= new DecimalFormat("0.00");
            df.setMaximumFractionDigits(2);
            HistoricoFacturaVO facturas= new HistoricoFacturaVO();
            List<FacturaVO> listFacturas= new ArrayList<FacturaVO>();
            try{
                FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
                /*com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion infoFacturas= infoCuenta.getHistoricoFacturacion();
                
                for(com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion.Factura factWS: infoFacturas.getFactura()){
                    FacturaVO factura= new FacturaVO();
                    Float monto= Float.parseFloat(factWS.getAmount());                        
                    factura.setFechaPago(DateTime.obtieneMes(factWS.getBilingDate()));                    
                    factura.setMonto(df.format(monto));
                    factura.setLiga(encriptaInterfactura.getLigaInterfactura(numCuenta,DateTime.obtieneMesAnioDigit(factWS.getBilingDate())[0],DateTime.obtieneMesAnioDigit(factWS.getBilingDate())[1]));
                    listFacturas.add(factura);
                }*/
                String rutaWS= fileProperty.getProperty("GETFACTURAS");
                URL wsURL= new URL(rutaWS);
                
                Bpelbalancebrm_client_ep bpelbalancebrm_client_ep = new Bpelbalancebrm_client_ep(wsURL, new QName("http://xmlns.oracle.com/BalanceBRM/BalanceBRM/BPELBalanceBRM","bpelbalancebrm_client_ep"));
                BPELBalanceBRM bPELBalanceBRM = bpelbalancebrm_client_ep.getBPELBalanceBRM_pt();
                
                com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Process facturasWS= new com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Process();
                facturasWS.setAcountNo(numCuenta);
                com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.ProcessResponse respFactWS= bPELBalanceBRM.process(facturasWS);
                if(respFactWS.getFacturas().size()>=1){
                    for( int i=0; i<respFactWS.getFacturas().size() && i<3; i++ ){ //s�lo se muestran las 3 �ltimas facturas
                        com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.ProcessResponse.Facturas factWS=respFactWS.getFacturas().get(i);
                        FacturaVO factura= new FacturaVO();
                        Float monto= !factWS.getMontoFacturado().equals("") && !factWS.getMontoFacturado().isEmpty() ?Float.parseFloat(factWS.getMontoFacturado()):0.0f;                        
                        factura.setFechaPago(DateTime.obtieneMes(factWS.getFechaOperacion()));                    
                        factura.setMonto(formatoMoneda.format(monto));
                        factura.setLiga(encriptaInterfactura.getLigaInterfactura(numCuenta, DateTime.obtieneMesAnioDigit(factWS.getFechaOperacion())[0], DateTime.obtieneMesAnioDigit(factWS.getFechaOperacion())[1]));
                        listFacturas.add(factura);
                    } 
                }
            }catch(Exception e){e.printStackTrace(); logger.error("Error al obtener facturas del cliente: "+e.getMessage());}   
            if(listFacturas!=null && listFacturas.size()>0){
                facturas.setHistoricoFac(listFacturas);
                sesion.setAttribute("facturas", facturas);
                sesion.setAttribute("sizeListFact", listFacturas.size());
            }
            //request.getRequestDispatcher("jsp/facturasVista.jsp").forward(request, response);
            //response.sendRedirect("jsp/facturasVista.jsp"); 
            RequestDispatcher despachador =
            getServletContext().getRequestDispatcher("/jsp/EstadoCta.jsp");
            despachador.include(request, response);
        }catch(Exception e)     {
            logger.error("Error al obtener facturas del cliente: "+e.getMessage());
        }
        
    }
}
