package com.totalplay.servlets;


/*import com.oracle.xmlns.ffmsoporte.testtr.testtr.TestTR;
import com.oracle.xmlns.ffmsoporte.testtr.testtr.Testtr_client_ep;
import com.oracle.xmlns.utilerias.changewifi.changewifi.ChangeWiFi;
import com.oracle.xmlns.utilerias.changewifi.changewifi.Changewifi_client_ep;
import com.oracle.xmlns.utilerias.changewifi.changewifi.Process;
import com.oracle.xmlns.utilerias.changewifi.changewifi.ProcessResponse;
import com.oracle.xmlns.utilerias.syncssid.syncssid.SyncSSID;
import com.oracle.xmlns.utilerias.syncssid.syncssid.Syncssid_client_ep;*/

import com.totalplay.soa.getsearchserialred.GetSearchSerialRed;
import com.totalplay.soa.getsearchserialred.GetSearchSerialRedPort;
import com.totalplay.soa.getsearchserialred.GetSearchSerialRedRqType;
import com.totalplay.soa.getsearchserialred.GetSearchSerialRedRsType;
import com.totalplay.soa.getsearchserialred.Logintype;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;

import java.io.IOException;
import java.io.PrintWriter;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class RedWiFiSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private static Logger logger = LogManager.getLogger(RedWiFiSer.class);

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pWriter = response.getWriter();
        response.setContentType(CONTENT_TYPE); 
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
        
        
        logger.error("SSID: " + request.getParameter("ssid"));
        logger.error("Tipo de seguridad: " + request.getParameter("seguridad"));
        logger.error("Contraseņa: " + request.getParameter("contrasena"));
        
        HttpSession session = request.getSession(false);
        
        String nCuenta = (String)session.getAttribute("numCuenta");
        /*try{
            String wsSync = fileProperty.getProperty("SYNCSSID");
            URL urlSync = new URL(wsSync);
            Syncssid_client_ep sSSIDC = new Syncssid_client_ep(urlSync, new QName("http://xmlns.oracle.com/Utilerias/SyncSSID/SyncSSID","syncssid_client_ep"));
            SyncSSID sSSID = sSSIDC.getSyncSSID_pt();
            
            com.oracle.xmlns.utilerias.syncssid.syncssid.Process payload_ = new com.oracle.xmlns.utilerias.syncssid.syncssid.Process();
            com.oracle.xmlns.utilerias.syncssid.syncssid.Process.Loggin login_ = new com.oracle.xmlns.utilerias.syncssid.syncssid.Process.Loggin();
            
            login_.setIp( fileProperty.getProperty("SYNCSSID_IP") );//login_.setIp("0.0.0.1");
            login_.setLoggin( fileProperty.getProperty("SYNCSSID_LOGGIN") );//login_.setLoggin("452710");
            login_.setPassword( fileProperty.getProperty("SYNCSSID_DROWSSAP") );//login_.setPassword("Portales789$");
            payload_.setLoggin(login_);
            payload_.setCuenta(nCuenta/*"0210000749"*///);
            
            /*logger.error(nCuenta);
            logger.error(request.getParameter("seguridad"));
            com.oracle.xmlns.utilerias.syncssid.syncssid.ProcessResponse srSSID = sSSID.process(payload_);
            
            
            
            logger.error("Contiene >>> " + srSSID.getCONTIENE());
            if(srSSID.getCONTIENE() == 1){ 
                String wsChange = fileProperty.getProperty("CHANGEWIFI");
                URL urlChange = new URL(wsChange);
                Changewifi_client_ep cWiFiC = new Changewifi_client_ep(urlChange, new QName("http://xmlns.oracle.com/Utilerias/ChangeWiFi/ChangeWiFi","changewifi_client_ep"));
                ChangeWiFi cWiFi = cWiFiC.getChangeWiFi_pt();
                
                Process payload = new Process();
                Process.Loggin login = new Process.Loggin();
                login.setIp( fileProperty.getProperty("SYNCSSID_IP") );
                login.setLoggin( fileProperty.getProperty("SYNCSSID_LOGGIN") );
                login.setPassword( fileProperty.getProperty("SYNCSSID_DROWSSAP") ); 
                
                String wsSerial = fileProperty.getProperty("GETSERIAL");
                URL urlSerial = new URL(wsSerial);
                GetSearchSerialRed gSerial = new GetSearchSerialRed(urlSerial,new QName("http://soa.totalplay.com/GetSearchSerialRed", "GetSearchSerialRed"));
                GetSearchSerialRedPort serial = gSerial.getGetSearchSerialRedPort_pt();
                
                GetSearchSerialRedRqType pSerial = new GetSearchSerialRedRqType();
                Logintype lType = new Logintype();

                lType.setIp( fileProperty.getProperty("SYNCSSID_IP") );//lType.setIp("0.0.0.1");
                lType.setLogin( fileProperty.getProperty("SYNCSSID_LOGGIN") );//lType.setLogin("452710");
                lType.setPassword( fileProperty.getProperty("SYNCSSID_DROWSSAP") );//lType.setPassword("Portales789$");
                
                pSerial.setCuenta(nCuenta);
                pSerial.setLiginArg(lType);
                
                GetSearchSerialRedRsType sResponse = serial.getSearchSerialRed(pSerial); 
                
                if(sResponse.getResult().getResult().equals("0")){
                    srSSID.setIDONT(sResponse.getIPONT());
                    srSSID.setSERIALNUMBER(sResponse.getSerialNumber());
                }
                
                payload.setLoggin(login);    
                payload.setAUTENTICATIONMODE(request.getParameter("seguridad"));  // Valor del JSP WPA 
                payload.setAUTOSCAN("true");//Revisar valor
                payload.setBROADCASTSSID(/*srSSID.getBROADCASTSSID().equals("0") ? true : false*/ //true);
                //payload.setCHANNEL(srSSID.getCHANNEL());
                //payload.setCHANNEL4K(true);//Revisar valor
                /*payload.setCountNum(nCuenta);
                payload.setDISPCONECTED(srSSID.getDISPCONECTED());
                if(request.getParameter("seguridad").equals("WPAand11i"))
                    payload.setENCRIPTATION("TKIPandAESEncryption");
                if(request.getParameter("seguridad").equals("WPA"))
                    payload.setENCRIPTATION("TKIPEncryption");
                if(request.getParameter("seguridad").equals("11i"))
                    payload.setENCRIPTATION("AESEncryption");
                payload.setFRECUENCY("0");
                payload.setIPONT(sResponse.getIPONT()); //Validar contrato de interfaz menciona que no es necesario, pero en request productivo solicita el valor
                payload.setIdentificador(/*1*///srSSID.getCONTIENE());
                /*payload.setLENGUAGE("MX");
                payload.setPASSWORD(request.getParameter("contrasena")); // Valor del JSP 
                payload.setSERIALNUMBER(sResponse.getSerialNumber());
                payload.setSSIDNAME(request.getParameter("ssid")); // Valor del JSP 
                payload.setTXPOWER("100"); //no es obligatorio
                payload.setVMMM(/*srSSID.getVMMM().equals("0") ? true : false*/ //true);// no es obligatorio
                /*payload.setWPS(/*srSSID.getWPS().equals("0") ? true : false*/ //true); //no es obligatorio
                  
                /*ProcessResponse crWiFi = cWiFi.process(payload); 
                
                if(crWiFi.getResult().trim().equals("1")  || crWiFi.getResult().trim().equals("0")){
                    if(crWiFi.getResultONT().contains("no pertenecen al catalogo")){
                        pWriter.print("other");
                        logger.error(crWiFi.getResult());
                        logger.error(crWiFi.getResultONT());
                        logger.error(crWiFi.getResultID());
                        logger.error(crWiFi.getResultInvLog());
                    } else{
                        pWriter.print("ok");
                        logger.error(crWiFi.getResult());
                        logger.error(crWiFi.getResultONT());
                        logger.error(crWiFi.getResultID());
                        logger.error(crWiFi.getResultInvLog());
                        session.setAttribute("ssid", request.getParameter("ssid"));
                        session.setAttribute("sec", request.getParameter("seguridad"));
                        session.setAttribute("pwd", request.getParameter("contrasena"));
                        
                    }
                        
                } else {
                    logger.error(crWiFi.getResult());
                    logger.error(crWiFi.getResultONT());
                    logger.error(crWiFi.getResultID());
                    logger.error(crWiFi.getResultInvLog());
                    pWriter.print("nsaved");
                }
                
            } else if(srSSID.getCONTIENE() == 2){
                String wsTest = fileProperty.getProperty("TESTTRONT");
                URL urlTest = new URL(wsTest);
                Testtr_client_ep vProtocoloTR069 = new Testtr_client_ep(urlTest, new QName("http://xmlns.oracle.com/FFMSoporte/TestTR/TestTR", "testtr_client_ep"));
                TestTR vTestTR069 = vProtocoloTR069.getTestTR_pt();
                
                com.oracle.xmlns.ffmsoporte.testtr.testtr.Process payload_v = new com.oracle.xmlns.ffmsoporte.testtr.testtr.Process();
                com.oracle.xmlns.ffmsoporte.testtr.testtr.Process.Login login_v = new com.oracle.xmlns.ffmsoporte.testtr.testtr.Process.Login();
                login_v.setIp( fileProperty.getProperty("SYNCSSID_IP") );
                login_v.setUserId( fileProperty.getProperty("SYNCSSID_LOGGIN") );
                login_v.setPassword( fileProperty.getProperty("SYNCSSID_DROWSSAP") );
                
                payload_v.setLogin(login_v);
                payload_v.setAccount(nCuenta); 
                
                com.oracle.xmlns.ffmsoporte.testtr.testtr.ProcessResponse vResponseTR069 = vTestTR069.process(payload_v);
                
                if(vResponseTR069.getResult().equals("0")){
                    
                        com.oracle.xmlns.utilerias.syncssid.syncssid.ProcessResponse srSSID_rM = sSSID.process(payload_);
                        ProcessResponse rMChangeWiFi = changeWiFi(srSSID_rM, request.getParameter("seguridad"), request.getParameter("contrasena"), request.getParameter("ssid"), nCuenta);
                        
                        if(rMChangeWiFi.getResult().trim().equals("1") || rMChangeWiFi.getResult().trim().equals("0")){
                            if(rMChangeWiFi.getResultONT().contains("no pertenecen al catalogo")){
                                pWriter.print("other");
                                logger.error(rMChangeWiFi.getResult());
                                logger.error(rMChangeWiFi.getResultONT());
                                logger.error(rMChangeWiFi.getResultID());
                                logger.error(rMChangeWiFi.getResultInvLog());
                            } else{
                                pWriter.print("ok");
                                logger.error(rMChangeWiFi.getResult());
                                logger.error(rMChangeWiFi.getResultONT());
                                logger.error(rMChangeWiFi.getResultID());
                                logger.error(rMChangeWiFi.getResultInvLog());
                                session.setAttribute("ssid", request.getParameter("ssid"));
                                session.setAttribute("sec", request.getParameter("seguridad"));
                                session.setAttribute("pwd", request.getParameter("contrasena"));
                            } 
                        } else {
                            logger.error(rMChangeWiFi.getResult());
                            logger.error(rMChangeWiFi.getResultONT());
                            logger.error(rMChangeWiFi.getResultID());
                            logger.error(rMChangeWiFi.getResultInvLog());
                            pWriter.print("nsaved");
                        }
                    /*} else {
                        logger.error(rMigrate.getIdResult());
                        logger.error(rMigrate.getResult());
                        logger.error(rMigrate.getResultDescription());
                        pWriter.print("nmigrate");
                    } */
                /*}else{
                    logger.error(vResponseTR069.getResult()); 
                    logger.error(vResponseTR069.getResultDescription());
                    pWriter.print("nsupport");
                } 
            } else if(srSSID.getCONTIENE() == 0){
                String wsTest = fileProperty.getProperty("TESTTRONT");
                URL urlTest = new URL(wsTest);
                Testtr_client_ep vProtocoloTR069 = new Testtr_client_ep(urlTest, new QName("http://xmlns.oracle.com/FFMSoporte/TestTR/TestTR", "testtr_client_ep"));
                TestTR vTestTR069 = vProtocoloTR069.getTestTR_pt();
                
                com.oracle.xmlns.ffmsoporte.testtr.testtr.Process payload_v = new com.oracle.xmlns.ffmsoporte.testtr.testtr.Process();
                com.oracle.xmlns.ffmsoporte.testtr.testtr.Process.Login login_v = new com.oracle.xmlns.ffmsoporte.testtr.testtr.Process.Login();
                login_v.setIp( fileProperty.getProperty("SYNCSSID_IP") );
                login_v.setUserId( fileProperty.getProperty("SYNCSSID_LOGGIN") );
                login_v.setPassword( fileProperty.getProperty("SYNCSSID_DROWSSAP") );
                
                payload_v.setLogin(login_v);
                payload_v.setAccount(nCuenta); 
                
                com.oracle.xmlns.ffmsoporte.testtr.testtr.ProcessResponse vResponseTR069 = vTestTR069.process(payload_v);
                
                if(vResponseTR069.getResult().equals("0")){
                    com.oracle.xmlns.utilerias.syncssid.syncssid.ProcessResponse srSSID_rM = sSSID.process(payload_);
                    ProcessResponse rMChangeWiFi = changeWiFi(srSSID_rM, request.getParameter("seguridad"), request.getParameter("contrasena"), request.getParameter("ssid"), nCuenta);
                    
                    if(rMChangeWiFi.getResult().trim().equals("1") || rMChangeWiFi.getResult().trim().equals("0")){ 
                        if(rMChangeWiFi.getResultONT().contains("no pertenecen al catalogo")){
                            pWriter.print("other");
                            logger.error(rMChangeWiFi.getResult());
                            logger.error(rMChangeWiFi.getResultONT());
                            logger.error(rMChangeWiFi.getResultID());
                            logger.error(rMChangeWiFi.getResultInvLog());
                        } else{
                            pWriter.print("ok");
                            logger.error(rMChangeWiFi.getResult());
                            logger.error(rMChangeWiFi.getResultONT());
                            logger.error(rMChangeWiFi.getResultID());
                            logger.error(rMChangeWiFi.getResultInvLog());
                            session.setAttribute("ssid", request.getParameter("ssid"));
                            session.setAttribute("sec", request.getParameter("seguridad"));
                            session.setAttribute("pwd", request.getParameter("contrasena"));
                        } 
                    } else {
                        logger.error(rMChangeWiFi.getResult());
                        logger.error(rMChangeWiFi.getResultONT());
                        logger.error(rMChangeWiFi.getResultID());
                        logger.error(rMChangeWiFi.getResultInvLog());
                        pWriter.print("nsaved");
                    }
                }else{
                    logger.error(vResponseTR069.getResult()); 
                    logger.error(vResponseTR069.getResultDescription());
                    pWriter.print("nsupport");
                }
            }
            
        }catch(Exception ex){
            logger.error("Excepcion >>> " + ex.getMessage());
            pWriter.print("other");
        }finally{
            pWriter.close();   
        } */  
                pWriter.print("nsaved");                                                                           
    }
    
    /*private ProcessResponse changeWiFi(com.oracle.xmlns.utilerias.syncssid.syncssid.ProcessResponse srSSID, String seguridad,  String contrasena, String ssid, String nCuenta) throws MalformedURLException {
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
        String wsChange = fileProperty.getProperty("CHANGEWIFI");
        URL urlChange = new URL(wsChange);
        Changewifi_client_ep cWiFiC = new Changewifi_client_ep(urlChange, new QName("http://xmlns.oracle.com/Utilerias/ChangeWiFi/ChangeWiFi", "changewifi_client_ep"));
        ChangeWiFi cWiFi = cWiFiC.getChangeWiFi_pt();
        
        Process payload = new Process();
        Process.Loggin login = new Process.Loggin();
        login.setIp( fileProperty.getProperty("SYNCSSID_IP") );
        login.setLoggin( fileProperty.getProperty("SYNCSSID_LOGGIN") );
        login.setPassword( fileProperty.getProperty("SYNCSSID_DROWSSAP") );
        
        String wsSerial = fileProperty.getProperty("GETSERIAL");
        URL urlSerial = new URL(wsSerial);
        GetSearchSerialRed gSerial = new GetSearchSerialRed(urlSerial,new QName("http://soa.totalplay.com/GetSearchSerialRed", "GetSearchSerialRed"));
        GetSearchSerialRedPort serial = gSerial.getGetSearchSerialRedPort_pt();
        
        GetSearchSerialRedRqType pSerial = new GetSearchSerialRedRqType();
        Logintype lType = new Logintype();
        
        lType.setIp( fileProperty.getProperty("IP") );
        lType.setLogin( fileProperty.getProperty("USRGSSR") );
        lType.setPassword( fileProperty.getProperty("PWDGSSR") );
        
        pSerial.setCuenta(nCuenta);
        pSerial.setLiginArg(lType);
        
        GetSearchSerialRedRsType sResponse = serial.getSearchSerialRed(pSerial);
        
        if(sResponse.getResult().getResult().equals("0")){
            srSSID.setIDONT(sResponse.getIPONT());
            srSSID.setSERIALNUMBER(sResponse.getSerialNumber());
        }
        
        logger.error(seguridad);
        payload.setLoggin(login);    
        payload.setAUTENTICATIONMODE(seguridad);  // Valor del JSP WPA 
        payload.setAUTOSCAN("true");//Revisar valor
        payload.setBROADCASTSSID(true);
        payload.setCHANNEL("11");
        //payload.setCHANNEL4K("");//Revisar valor
        payload.setCountNum(nCuenta);
        payload.setDISPCONECTED("32");
        if(seguridad.equals("WPAand11i"))
            payload.setENCRIPTATION("TKIPandAESEncryption");
        if(seguridad.equals("WPA"))
            payload.setENCRIPTATION("TKIPEncryption");
        if(seguridad.equals("11i"))
            payload.setENCRIPTATION("AESEncryption");
        payload.setFRECUENCY("0");
        payload.setIPONT(sResponse.getIPONT()); //Validar contrato de interfaz menciona que no es necesario, pero en request productivo solicita el valor
        payload.setIdentificador(/*1*///srSSID.getCONTIENE());
        /*payload.setLENGUAGE("MX");
        payload.setPASSWORD(contrasena); 
        payload.setSERIALNUMBER(sResponse.getSerialNumber());
        payload.setSSIDNAME(ssid); // Valor del JSP pancho
        payload.setTXPOWER("100"); //no es obligatorio
        payload.setVMMM(true);// no es obligatorio
        payload.setWPS(true); //no es obligatorio
          
        ProcessResponse crWiFi = cWiFi.process(payload);
        
        
        return crWiFi;
    }*/
    
    
}
