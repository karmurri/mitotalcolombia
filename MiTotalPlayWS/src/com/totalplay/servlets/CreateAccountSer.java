package com.totalplay.servlets;


import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM;
import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.BPELObtenerCorreo;
import com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Bpelobtenercorreo_client_ep;

import com.totalplay.services.BrmSearchAccount;
import com.totalplay.services.EngineSecurity;
import com.totalplay.services.GetInfoBRM;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.EmailUtility;
import com.totalplay.utils.FileProperty;
import com.totalplay.utils.ProcesaHtml;
import com.totalplay.utils.Rands;
import com.totalplay.utils.VerificaUsuario;

import java.io.IOException;
import java.io.PrintWriter;

import java.net.URL;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*import org.tempuri.SecurityUserModel;
import org.tempuri.Service1SoapClient;*/


public class CreateAccountSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    Logger logger = LogManager.getLogger(this.getClass().getName());
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request,response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
       // logger.info("-----Entro a CreateAccountSer----");
        response.setContentType(CONTENT_TYPE);
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
        HttpSession sesion= request.getSession(false);
           PrintWriter out = response.getWriter();  
        /*try{
            Service1SoapClient wsUser = new Service1SoapClient();
            EmailUtility wsEmail= new EmailUtility();
            String numCuenta=null;
            String parametroCuenta=request.getParameter("username");
            int bandera=0;
            if(VerificaUsuario.isNumCuentaVarios(parametroCuenta)){
                bandera=0;
            }else if(VerificaUsuario.isNumTelefono(parametroCuenta)){
                bandera=2;
            }
            com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.ProcessResponse.DatosCliente datos = GetInfoBRM.obtieneCuenta(parametroCuenta, bandera);
            numCuenta=datos.getAccountNo();
            SecurityUserModel infoUser = EngineSecurity.getNoAccount(numCuenta);
            
            String title="";
            String msj="";
            String password = Rands.generaPassword(12);
            try{
    
               String rutaWS= fileProperty.getProperty("OBTIENECORREOFACT");
                URL wsURL= new URL(rutaWS);
               Bpelobtenercorreo_client_ep bpelobtenercorreo_client_ep = new Bpelobtenercorreo_client_ep(wsURL,
              new QName("http://xmlns.oracle.com/SeguridadBRM/ObtenerCorreo/BPELObtenerCorreo",
                        "bpelobtenercorreo_client_ep"));
               BPELObtenerCorreo bPELObtenerCorreo = bpelobtenercorreo_client_ep.getBPELObtenerCorreo_pt();
               
               com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process paramCorreo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process();
               com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin authCorreo=  new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ApplicationLogin();
               authCorreo.setIp( fileProperty.getProperty("OBTIENECORREOFACT_IP") );
               authCorreo.setUserId( fileProperty.getProperty("OBTIENECORREOFACT_LOGGIN") );
               authCorreo.setPassword( fileProperty.getProperty("OBTIENECORREOFACT_DROWSSAP") );
               paramCorreo.setApplicationLogin(authCorreo);
               
               com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo paramClientInfo= new com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.Process.ClientInfo();
               paramClientInfo.setAccountOrDn(numCuenta);
               paramCorreo.setClientInfo(paramClientInfo);
               
               com.oracle.xmlns.seguridadbrm.obtenercorreo.bpelobtenercorreo.ProcessResponse respCorreo= bPELObtenerCorreo.process(paramCorreo);
               
               if(respCorreo.getResult().equals("0")){ 
                  infoUser.setEmail(respCorreo.getEmail());
               }
            }catch(Exception e){
                e.printStackTrace();
                logger.error("Error al consultar servicio de obtenci�n de correo: "+e.getMessage()+ " causa: "+e.getCause());
            }
                   
           String ruta= this.getServletContext().getRealPath("/assets/mail/password.htm");
           ruta=ruta!=null?ruta:getServletContext().getResource("/assets/mail/password.htm").getPath();        
            
            int result=-1;
            if (infoUser.getUserId()!=null){                 
                result = EngineSecurity.updateInfoClient(infoUser.getUserId(), numCuenta, numCuenta, 
                                        infoUser.getFirstName(), infoUser.getLastName(), infoUser.getEmail(), password);
            }
            else{ //no se encontr� la cuenta
                ArrBRM objArrBRM = BrmSearchAccount.getInfoClient(numCuenta);
                result = EngineSecurity.addNewUser(null, numCuenta, numCuenta, objArrBRM.getName(), "", infoUser.getEmail(), password);
                infoUser.setFirstName(objArrBRM.getName());
                infoUser.setLastName("");
                infoUser.setEmployeeNumber(numCuenta);
            }
            if(result==0){
               //hardcode
                //infoUser.setEmail("cfloresc@elektra.com.mx");
                String bodyEmail = ProcesaHtml.creaCadenaHTML(ruta,infoUser.getFirstName()+" "+infoUser.getLastName(),infoUser.getEmployeeNumber(),password);
                boolean emailResul= wsEmail.sendEmail(infoUser.getEmail(), Constantes.EMAILCC, Constantes.EMAILBCC, Constantes.REPLY_TO, Constantes.SUBJECT, bodyEmail);
                if(emailResul==true){
                   if(infoUser.getEmail()!=null && !infoUser.getEmail().equals(""))
                       out.print(EmailUtility.paddingMail(infoUser.getEmail()));
                   else
                       out.print("success");
                }
                else{
                    out.print("failcorreo");
                }
            }
            else if(result==2){
               out.print("exists");
            }
            else{
               out.print("fail");
            }
                        
       }catch(Exception ex){
            ex.printStackTrace();
           logger.error("Error al crear cuenta: "+ex.getMessage());
       }*/
           out.print("fail");
    }
}
