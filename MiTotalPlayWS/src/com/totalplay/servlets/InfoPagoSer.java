package com.totalplay.servlets;


import com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.BPELGetBillsBRM;
import com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Bpelgetbillsbrm_client_ep;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse;

import com.totalplay.db.dao.GeneralDAO;
import com.totalplay.db.dbo.PromotionDBO;
import com.totalplay.services.BrmSearchAccount;
import com.totalplay.services.GetClientCards;
import com.totalplay.services.InfoPagosBaz;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;
import com.totalplay.vo.TarjetaVO;
import com.totalplay.ws.vo.ReferenceBankVO;

import encrypt.Encrypt3Des;

import java.io.IOException;

import java.net.URL;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class InfoPagoSer extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private Logger logger = LogManager.getLogger(this.getClass().getName());
    
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
                                                           IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                            IOException {
        response.setContentType(CONTENT_TYPE);
        request.setCharacterEncoding("UTF-8");
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
        HttpSession sesion= request.getSession(false);
        String numCuenta= (String)sesion.getAttribute("numCuenta");
        
        com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM objArrBRM = BrmSearchAccount.getInfoClient(numCuenta);
        
        try{
           
            List<com.oracle.xmlns.clientcards.getclientcards.bpelgetclientcards.ProcessResponse.ClientCards.ClientCard> tarjetasRespuesta = GetClientCards.obtieneTarjetasCliente(numCuenta);
            
            List<TarjetaVO> listTarjetas = new ArrayList<TarjetaVO>();
            for(com.oracle.xmlns.clientcards.getclientcards.bpelgetclientcards.ProcessResponse.ClientCards.ClientCard tarjetaws: tarjetasRespuesta){
               TarjetaVO tarjetaVo= new TarjetaVO();
               tarjetaVo.setBank((tarjetaws.getBank()!=null)?tarjetaws.getBank():null);    
               tarjetaVo.setBankId((tarjetaws.getBankId()!=null)?tarjetaws.getBankId():null);    
               tarjetaVo.setEndingCardNumber((tarjetaws.getEndingCardNumber()!=null)?tarjetaws.getEndingCardNumber():null);    
               tarjetaVo.setIdCard((tarjetaws.getIdCard()!=null)?tarjetaws.getIdCard():null);    
               tarjetaVo.setCardNumber((tarjetaws.getCardNumber()!=null)?tarjetaws.getCardNumber():null);               
               tarjetaVo.setExpirationMonth((tarjetaws.getExpirationMonth()!=null)?tarjetaws.getExpirationMonth():null);               
               tarjetaVo.setExpirationYear((tarjetaws.getExpirationYear()!=null)?tarjetaws.getExpirationYear():null);    
               tarjetaVo.setIdUser((tarjetaws.getIdUser()!=null)?tarjetaws.getIdUser():null);                           
               tarjetaVo.setFullName(tarjetaws.getFirstName()+" "+tarjetaws.getLastName());
               tarjetaVo.setStatus((tarjetaws.getStatus()!=null)?tarjetaws.getStatus():null);           
               tarjetaVo.setRazonStatus((tarjetaws.getStatus()!=null)?tarjetaws.getStatus():null);
               tarjetaVo.setProvider((tarjetaws.getProvider()!=null)?tarjetaws.getProvider():null);
               tarjetaVo.setCurrent((tarjetaws.getCurrent()!=null)?tarjetaws.getCurrent():null);                       
               
               String encryptInfo=Encrypt3Des.getEncrypt3des(tarjetaVo.getEndingCardNumber()+"|"+tarjetaVo.getBank()+"|"+tarjetaVo.getFullName());
               tarjetaVo.setCode(encryptInfo);                     
                listTarjetas.add(tarjetaVo);
            }
            
            sesion.setAttribute("tarjetas", listTarjetas);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al obtener listado de tarjetas del cliente: "+e.getMessage());
        }
        //obtener informaci�n de referencias bancarias
        
         List<ReferenceBankVO> listaReferencias= new ArrayList<ReferenceBankVO>();
        
        ProcessResponse.ArrBRM.ReferenciasBancarias refsBank = objArrBRM.getReferenciasBancarias()!=null?objArrBRM.getReferenciasBancarias():null;
        
        ReferenceBankVO referencia= new ReferenceBankVO();
        referencia.setBanco("BANCO AZTECA");
        ReferenceBankVO referencia2= new ReferenceBankVO();
        referencia2.setBanco("BANCOMER");
        ReferenceBankVO referencia3= new ReferenceBankVO();
        referencia3.setBanco("HSBC");
        ReferenceBankVO referencia4= new ReferenceBankVO();
        referencia4.setBanco("BANORTE");
        ReferenceBankVO referencia5= new ReferenceBankVO();        
        referencia5.setBanco("SCOTIABANK");
        ReferenceBankVO referencia6= new ReferenceBankVO();    
        referencia6.setBanco("SANTANDER");
        ReferenceBankVO referencia7= new ReferenceBankVO();  
        referencia7.setBanco("BANAMEX");
        ReferenceBankVO referencia8= new ReferenceBankVO();  
        referencia8.setBanco("SPEI a BANCO AZTECA con tu CLABE personalizada");
        //logger.error("Referencias: "+ refsBank+" banco azteca: "+refsBank.getBANCOAZTECA());
        if(refsBank!=null){
           if(refsBank.getBANCOAZTECA()!=null || !refsBank.getBANCOAZTECA().isEmpty() || !refsBank.getBANCOAZTECA().equals("")){ 
                referencia.setReferencia(refsBank.getBANCOAZTECA());              
                referencia2.setReferencia(refsBank.getBANCOMER());                
                referencia3.setReferencia(refsBank.getHSBC());
                referencia4.setReferencia(refsBank.getBANORTE());
                referencia5.setReferencia(refsBank.getSCOTIABANK());
                referencia6.setReferencia(refsBank.getSANTANDER());
                referencia7.setReferencia(refsBank.getBANAMEX());
           }
        }else{
            logger.error("Error, refsBank");
            try{
                com.oracle.xmlns.infopagosbaz.infopagosbaz.bpelinfopagosbaz.ProcessResponse refsBankWS2 = InfoPagosBaz.getReferencias(numCuenta);
                String ref=refsBankWS2.getReferenciaBancariaAzteca();
             
                if(ref!=null && !ref.isEmpty()){
                    referencia.setReferencia("Ref: "+ref);
                    referencia2.setReferencia("Conv: 1167081 Ref: "+ref);
                    referencia3.setReferencia("Trans: 5503 Cve. 6227 Ref: "+ref);
                    referencia4.setReferencia("Emisora: 2052 Ref: "+ref);
                    referencia5.setReferencia("No. Serv: 3748 Ref: "+ref);
                    referencia6.setReferencia("Conv: 3998 Ref: "+ref);
                    referencia7.setReferencia(null);
                }
            }   
            catch(Exception e){
                e.printStackTrace();
                logger.error("Error al obtener informacion de referencias del usuario: "+e.getMessage());
            }
        }
        
        
        try{
            String rutaGetBillsWS= fileProperty.getProperty("GETBILLSBRM");
            URL wsURL= new URL(rutaGetBillsWS);
            Bpelgetbillsbrm_client_ep bpelgetbillsbrm_client_ep = new Bpelgetbillsbrm_client_ep(wsURL,
          new QName("http://xmlns.oracle.com/Balance/GetBillsBRM/BPELGetBillsBRM",
                    "bpelgetbillsbrm_client_ep"));
            BPELGetBillsBRM bPELGetBillsBRM = bpelgetbillsbrm_client_ep.getBPELGetBillsBRM_pt();
            
            com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Process payload= new com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Process();
            
            com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Process.UserPassIp user= new com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Process.UserPassIp();
            user.setPassword( fileProperty.getProperty("InfoPagoSer_DROWSSAP") );
            user.setUserId( fileProperty.getProperty("InfoPagoSer_UserId") );
            
            payload.setAccountNo(numCuenta);
            payload.setUserPassIp(user);
            
            com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.ProcessResponse respuestaSPEI= bPELGetBillsBRM.process(payload);
            
            String SPEI=respuestaSPEI.getReferenciasBancarias()!=null?respuestaSPEI.getReferenciasBancarias().getSPEI():null;
            referencia8.setReferencia(SPEI!=null && !SPEI.equals("")?SPEI:"");
            if(!respuestaSPEI.getResponse().getCode().equals("0")){
                logger.error("Error al consultar SPEI: "+respuestaSPEI.getResponse().getDescription());
            }
        }catch(Exception e){
            referencia8.setReferencia("");
            referencia8.setBanco("");
            logger.error("Error al consultar SPEI: "+e.getMessage());
        }
        listaReferencias.add(referencia8);
        listaReferencias.add(referencia);
        listaReferencias.add(referencia2);
        listaReferencias.add(referencia3);
        listaReferencias.add(referencia4);
        listaReferencias.add(referencia5);
        listaReferencias.add(referencia6);
        listaReferencias.add(referencia7);
        
        sesion.setAttribute("referencias", listaReferencias);
        
        ///Registro de tarjeta CR
        GeneralDAO consultas= new GeneralDAO(3);
        List<PromotionDBO> promos;
        try{
            promos=consultas.getPromociones();
        }catch(NullPointerException e){
            promos= new ArrayList<PromotionDBO>();
            PromotionDBO promo= new PromotionDBO();
            promo.setDescripcion("no hay promociones disponibles");
            promo.setId_promocion("0");
            promo.setPromocion("No hay promociones disponibles");
            promos.add(promo);
                        
        }
        if(promos!=null && !promos.isEmpty())
            request.setAttribute("promociones", promos);
        request.getRequestDispatcher("jsp/pagosVistaMod.jsp").forward(request, response);
        
        logger.error("Error, getReferenciasBancarias");
        refsBank = null;
        
    }
}
