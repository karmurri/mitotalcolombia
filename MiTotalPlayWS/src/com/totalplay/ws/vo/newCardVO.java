package com.totalplay.ws.vo;

import javax.xml.bind.annotation.XmlElement;

public class newCardVO {
    protected String blSaveCard;
    protected String address;
    protected String bank;
    protected String bankIdProvider;
    protected String cardNumber;
    protected String city;
    protected String colony;
    protected String country;
    protected String cvv2;
    protected String expirationMonth;
    protected String expirationYear;
    protected String firstName;
    protected String lastName;
    protected String municipality;
    protected String provider;
    protected String rfc;
    protected String state;
    protected String status;
    protected String telephone;
    protected String trackCard;
    protected String trackCardDos;
    protected String zip;
    
    public newCardVO(){
        this.blSaveCard="?";
        this.address="?";
        this.bank="?";
        this.bankIdProvider="?";
        this.cardNumber="?";
        this.city="?";
        this.colony="?";
        this.country="?";
        this.cvv2="?";
        this.expirationMonth="?";
        this.expirationYear="?";
        this.firstName="?";
        this.lastName="?";
        this.municipality="?";
        this.provider="?";
        this.rfc="?";
        this.state="?";
        this.status="?";
        this.telephone="?";
        this.trackCard="?";
        this.trackCardDos="?";
        this.zip="?";
    }

    public void setBlSaveCard(String blSaveCard) {
        this.blSaveCard = blSaveCard;
    }

    public String getBlSaveCard() {
        return blSaveCard;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBank() {
        return bank;
    }

    public void setBankIdProvider(String bankIdProvider) {
        this.bankIdProvider = bankIdProvider;
    }

    public String getBankIdProvider() {
        return bankIdProvider;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public String getColony() {
        return colony;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getRfc() {
        return rfc;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTrackCard(String trackCard) {
        this.trackCard = trackCard;
    }

    public String getTrackCard() {
        return trackCard;
    }

    public void setTrackCardDos(String trackCardDos) {
        this.trackCardDos = trackCardDos;
    }

    public String getTrackCardDos() {
        return trackCardDos;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getZip() {
        return zip;
    }
    public String toString(){
        return ("GuardarTarj: "+ blSaveCard+"\n"+
        "Direccion: "+address+"\n"+
        "Banco: "+bank+"\n"+
        "BankIdProvider: "+ bankIdProvider+"\n"+
        "Numero tarjeta "+cardNumber+"\n"+
        "ciudad: "+city+"\n"+
        "colonia: "+colony+"\n"+
        "Pais: "+country+"\n"+
        "C�digo de seguridad "+cvv2+"\n"+
        "Mes de expiraci�n "+expirationMonth+"\n"+
        "A�o de expiraci�n "+expirationYear+"\n"+
        "Nombre "+firstName+"\n"+
        "Apellido "+lastName+"\n"+
        "Municipio: "+municipality+"\n"+
        "Proveedor: "+provider+"\n"+
        "RFC: "+rfc+"\n"+
        "Estado: "+state+"\n"+
        "Estado de tarjeta: "+status+"\n"+
        "Tel�fono: "+telephone+"\n"+
        "Datos para el autorizador bancario: "+trackCard+"\n"+
        "Datos para el autorizador bancario2: "+trackCardDos+"\n"+
        "C�digo Postal: "+zip);
    }
    
}
