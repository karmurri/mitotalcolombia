package com.totalplay.ws.vo;

public class ClientCardVO {
    private String idCard;
    private String monthsNoInterest;
    private String cvv2;
    public ClientCardVO() {
        super();
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setMonthsNoInterest(String monthsNoInterest) {
        this.monthsNoInterest = monthsNoInterest;
    }

    public String getMonthsNoInterest() {
        return monthsNoInterest;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getCvv2() {
        return cvv2;
    }
}
