package com.totalplay.ws.vo;

public class ReferenceBankVO {
    private String banco;
    private String referencia;
    public ReferenceBankVO() {
        super();
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getBanco() {
        return banco;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getReferencia() {
        return referencia;
    }
}
