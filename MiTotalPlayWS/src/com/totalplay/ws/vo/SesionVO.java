package com.totalplay.ws.vo;

public class SesionVO {
    private String browser;
    private String email;
    private String hashed;
    private String ip;
    private String lastUpdate;
    private String logdate;
    private String nickname;
    private String salt;
    private String secid;
    private String sessid;
    private String status;
    private String systemLogin;
    private String userID;
    private String userName;

    public SesionVO() {
        super();
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getBrowser() {
        return browser;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setHashed(String hashed) {
        this.hashed = hashed;
    }

    public String getHashed() {
        return hashed;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLogdate(String logdate) {
        this.logdate = logdate;
    }

    public String getLogdate() {
        return logdate;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getSalt() {
        return salt;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getSecid() {
        return secid;
    }

    public void setSessid(String sessid) {
        this.sessid = sessid;
    }

    public String getSessid() {
        return sessid;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setSystemLogin(String systemLogin) {
        this.systemLogin = systemLogin;
    }

    public String getSystemLogin() {
        return systemLogin;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
