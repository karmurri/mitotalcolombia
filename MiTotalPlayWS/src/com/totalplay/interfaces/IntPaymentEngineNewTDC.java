package com.totalplay.interfaces;

import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse;

import com.totalplay.ws.response.ResponsePaymentEngineNewTDC;
import com.totalplay.ws.vo.newCardVO;

public interface IntPaymentEngineNewTDC {
   
    public ProcessResponse payNewTDC(String accountNo,String amount,newCardVO newCardVO,String ipCliente);
}
