package com.totalplay.interfaces;

import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.ProcessResponse;
import com.totalplay.ws.vo.ClientCardVO;

public interface IntPaymentEngineExTDC {
     public ProcessResponse payExTDC(String cuenta, String monto, ClientCardVO tarjetaVo, String ipCliente) ;
}
