package com.totalplay.utils;


import com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.BPELModifyPayInfo;
import com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.Bpelmodifypayinfo_client_ep;
import com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.Process;
import com.oracle.xmlns.activation.modifypayinfo.bpelmodifypayinfo.ProcessResponse;

import java.io.Serializable;

import java.math.BigInteger;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class CambioMetodoPago implements Serializable {
    
    private static final long serialVersionUID = 6299497296194490667L;
    FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
    Logger logger = LogManager.getLogger(this.getClass().getName());

    public CambioMetodoPago() {}
    
    public boolean MetodoPagoCredito(
        String ACCOUNT_NO, 
        String PAY_TYPE,	 
        BigInteger ACH, 
        String Address, 
        String City, 	 
        String Country,	 
        String Name,        
        String State,	 
        String Zip,
        String DebitExp,         
        BigInteger DebitNum,
        BigInteger SecurityId) {

        boolean b = false;
        try{
            String rutaWS= fileProperty.getProperty("MODIFYPAYIINFOCLIENT");
            URL urlWS=null;
    
            try {
                urlWS = new URL(rutaWS);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Bpelmodifypayinfo_client_ep bpelmodifypayinfo_client_ep = new Bpelmodifypayinfo_client_ep(urlWS,
              new QName("http://xmlns.oracle.com/Activation/ModifyPayInfo/BPELModifyPayInfo",
                        "bpelmodifypayinfo_client_ep"));
            BPELModifyPayInfo bPELModifyPayInfo = bpelmodifypayinfo_client_ep.getBPELModifyPayInfo_pt();        
    
            Process.ArrInheritedInfo value = new Process.ArrInheritedInfo();
            
            Process.ArrInheritedInfo.ArrCCInfo value3 = new Process.ArrInheritedInfo.ArrCCInfo();
        
            value3.setAddress(Address);
            value3.setCity(City);
            value3.setCountry(Country);
            value3.setDebitExp(DebitExp);
            value3.setDebitNum(DebitNum);
            value3.setName(Name);
            value3.setSecurityId(SecurityId);
            value3.setState(State);
            value3.setZip(Zip);
            
            value.setArrCCInfo(value3);
            
            Process.ArrLogin value2 = new Process.ArrLogin();
            
            value2.setUser( fileProperty.getProperty("MODIFYPAYIINFOCLIENT_USER") );
            value2.setPassword( fileProperty.getProperty("MODIFYPAYIINFOCLIENT_DROWSSAP") );
                   
            Process process = new Process();
            process.setACCOUNTNO(ACCOUNT_NO);
            process.setPayType(PAY_TYPE);
            process.setACH(ACH);
            process.setArrInheritedInfo(value);
            process.setArrLogin(value2);
            
            ProcessResponse proResp = bPELModifyPayInfo.process(process);
                        
            if (proResp.getResult().getResult() == 0){
                b = true;
            }
            else{    
                logger.error("Fall� respuesta de servicio:"+proResp.getResult().getResultDescription());  
                b = false;
            }        
        }catch(Exception e){
            logger.error("Error al ejecutar cambio de m�todo de pago: "+e.getMessage()+ " "+e.getCause());
        }
        return b;
    }

    public boolean MetodoPagoDebito(
        String ACCOUNT_NO, 
        String PAY_TYPE,         
        BigInteger ACH, 
        String Address, 
        String City,     
        String Country,  
        String Name,        
        String State,    
        String Zip,
        BigInteger DebitNum,
        String Bankno,   
        BigInteger Type) {

        String rutaWS= fileProperty.getProperty("MODIFYPAYIINFOCLIENT");
        URL urlWS=null;

        try {
            urlWS = new URL(rutaWS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Bpelmodifypayinfo_client_ep bpelmodifypayinfo_client_ep = new Bpelmodifypayinfo_client_ep(urlWS,
          new QName("http://xmlns.oracle.com/Activation/ModifyPayInfo/BPELModifyPayInfo",
                    "bpelmodifypayinfo_client_ep"));
        BPELModifyPayInfo bPELModifyPayInfo = bpelmodifypayinfo_client_ep.getBPELModifyPayInfo_pt();        

        Process.ArrInheritedInfo value = new Process.ArrInheritedInfo();
        
        Process.ArrInheritedInfo.ArrDDInfo value4 = new Process.ArrInheritedInfo.ArrDDInfo();
    
        
        value4.setAddress(Address);
        value4.setBankno(Bankno);
        value4.setCity(City);
        value4.setCountry(Country);
        value4.setDebitNum(DebitNum);
        value4.setName(Name);
        value4.setState(State);
        value4.setType(Type);
        value4.setZip(Zip);
        
        value.setArrDDInfo(value4);
        
        Process.ArrLogin value2 = new Process.ArrLogin();
        
        value2.setUser( fileProperty.getProperty("MODIFYPAYIINFOCLIENT_USER") );
        value2.setPassword( fileProperty.getProperty("MODIFYPAYIINFOCLIENT_DROWSSAP") );
               
        Process process = new Process();
        process.setACCOUNTNO(ACCOUNT_NO);
        process.setPayType(PAY_TYPE);
        process.setACH(ACH);
        process.setArrInheritedInfo(value);
        process.setArrLogin(value2);
        
        ProcessResponse proResp = bPELModifyPayInfo.process(process);
        
        boolean b = false;

        if (proResp.getResult().equals("0")){
            System.out.println("Exito");
            b = true;
        }
        else{
            System.out.println("Fall� respuesta de servicio:"+proResp.getResult().getResultDescription());                       
            b = false;
        }        

        return b;
    }

}
