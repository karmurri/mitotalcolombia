package com.totalplay.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ValidateForm {
    public ValidateForm() {
        super();
    }
    
    public static boolean soloLetras(String valor){
        boolean valido= false;
        Pattern pat= Pattern.compile("^[a-zA-Z����������������������\\s]+$");  
        Matcher mat= pat.matcher(valor);
        if(mat.matches()) valido=true;
        return valido;
    }
    
    public static boolean soloLetrasNums(String valor){
        boolean valido= false;
        Pattern pat= Pattern.compile("^[a-zA-Z����������������������\\s\\d#\\.&()\\/-]+$");  
        Matcher mat= pat.matcher(valor);
        if(mat.matches()) valido=true;
        return valido;
    }
    
    public static boolean soloNumeros(String valor){
        boolean valido= false;
        Pattern pat= Pattern.compile("^[0-9]+$");  
        Matcher mat= pat.matcher(valor);
        if(mat.matches()) valido=true;
        return valido;
    }
    
    public static boolean decimales (String valor){
            boolean valido= false;
            Pattern pat= Pattern.compile("^[0-9]+([\\.][0-9]+)?$");  
            Matcher mat= pat.matcher(valor);
            if(mat.matches()) valido=true;
            return valido;
    }
    
   /* public static void main(String args[]){
        System.out.println(ValidateForm.soloLetras("ret L�pez"));
        System.out.println(ValidateForm.soloLetrasNums("ret L�pez 89 - .()"));
        System.out.println(ValidateForm.soloN�meros("657656445"));
        System.out.println(ValidateForm.decimales("66.56445"));

    }*/
}
