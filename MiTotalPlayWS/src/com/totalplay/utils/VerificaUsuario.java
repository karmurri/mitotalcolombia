package com.totalplay.utils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public final class VerificaUsuario {
    static Logger logger = LogManager.getLogger(VerificaUsuario.class.getName());
    public VerificaUsuario() {
        super();
    }
    public static boolean isNumCuenta(String cuenta ){
        boolean isCuenta= false;
        Pattern pat= Pattern.compile("^[\\d]{1}\\.[\\d]{7}");  
        Matcher mat= pat.matcher(cuenta);
        if(mat.matches()) isCuenta=true;
        return isCuenta;
    }
    
    public static boolean isEmail(String email){
        boolean isEmail=false;
        Pattern pat= Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mat=pat.matcher(email.trim());
        if(mat.matches()) isEmail=true;
        return isEmail;
    }
    
    public static boolean isNumTelefono(String telefono){
        boolean isTelefono= false;
        Pattern pat= Pattern.compile("^[\\d]{9,10}");
        Matcher mat= pat.matcher(telefono);
        if(mat.matches()) isTelefono=true;
        return isTelefono;
    }
    
    
    public static boolean verificaPagoHoy(String userID){
        boolean pagoHoy= false;
        /*GeneralDAO bo= new GeneralDAO(3);
        List<TransaccionDBO> pagos;
        try{
            pagos=(bo.getPagosHoy(userID)!=null)?bo.getPagosHoy(userID):null;
            if(pagos!=null && !pagos.isEmpty()){
                for (TransaccionDBO trans: pagos){
                    if(trans.getAuthBank()!=null && !trans.getAuthBank().equals("")){
                        pagoHoy= true;
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al verificar pagosHoy: "+e.getMessage());
            pagoHoy=false;
        }*/
        return pagoHoy;
    }
    
    public static boolean isNumCuentaVarios(String cuenta ){
        boolean isCuenta= false;
        Pattern pat= Pattern.compile("^(([\\d]{1}\\.[\\d]{7})|((01|02)[\\d]{8}))");  
        Matcher mat= pat.matcher(cuenta);
        if(mat.matches()) isCuenta=true;
        return isCuenta;
    }
    
   
    
   
    
}
