package com.totalplay.utils;

import java.io.IOException;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class SendRedirectOverloadedResponse extends HttpServletResponseWrapper
{
   private Logger Log = LogManager.getLogger(this.getClass().getName());
   private HttpServletRequest m_request;
   private String prefix = null;
   public SendRedirectOverloadedResponse(HttpServletRequest inRequest,HttpServletResponse response)
   {
      super(response);
      m_request = inRequest;
      prefix = getPrefix(inRequest);
   }
   
   public void sendRedirect(String location) throws IOException
   {
      Log.trace("Going originally to:" + location);
      String finalurl = null;
      
      if (isUrlAbsolute(location))
      {
         Log.trace("This url is absolute. No scheme changes will be attempted");
         finalurl = location;
      }
      else
      {
         finalurl = fixForScheme(prefix + location);
         Log.trace("Going to absolute url:" + finalurl);
      }
      super.sendRedirect(finalurl);
   }
   
   public boolean isUrlAbsolute(String url)
   {
      String lowercaseurl = url.toLowerCase(Locale.getDefault());
      if (lowercaseurl.startsWith("http") == true)
      {
         return true;
      }
      else
      {
         return false;
      }
   }
   public String fixForScheme(String url)
   {
      //alter the url here if you were to change the scheme
      return url;
   }
   
   public String getPrefix(HttpServletRequest request)
   {
      StringBuffer str = request.getRequestURL();
      String url = str.toString();
      String uri = request.getRequestURI();
      Log.trace("requesturl:" + url);
      Log.trace("uri:" + uri);
      int offset = url.indexOf(uri);
      String prefix = url.substring(0,offset);
      Log.trace("prefix:" + prefix);
      return prefix;
   }
}