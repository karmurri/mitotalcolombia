package com.totalplay.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public final class DateTime {
    private static Logger logger = LogManager.getLogger(DateTime.class);

    public DateTime() {
        super();
    }
    public static Date convierteFechaSimple(String fecha){
        SimpleDateFormat sdf=null;
        if(fecha.contains("/")){
            sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("es","MX"));
        }else if(fecha.contains("-")){
            sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("es","MX"));
        }
        
        Date result=null;
        try {
            if(sdf!=null){
                result=sdf.parse(fecha);
            }
        } catch (ParseException e) {
            logger.error("Error en conversión de fecha: "+e.getMessage());
        }
       
       return (result);
        
        
    }
    public static Date obtieneFechaActual(String formato){
        SimpleDateFormat sdf= new SimpleDateFormat(formato, new Locale("es","MX"));
        Date fecha= new Date();
        String fechaF=sdf.format(fecha);
        Date result=null;
        try {
            result =sdf.parse(fechaF);
        } catch (ParseException e) {
             logger.error("Error en conversión de fecha: "+e.getMessage());
        }
        return result;
    }
    public static String obtieneFechaHoraActual(String formato){
        SimpleDateFormat sdf= new SimpleDateFormat(formato, new Locale("es","MX"));
        Date fecha= new Date();
        return sdf.format(fecha);
    }
    public static String convierteFechaSimpleVista(String fecha){
        SimpleDateFormat formateador=null;
        if(fecha.contains("/")){
            formateador= new SimpleDateFormat("dd/MM/yyyy");
        }else if(fecha.contains("-")){
            formateador= new SimpleDateFormat("yyyy-MM-dd");
        }
        DateFormat dateForm= DateFormat.getDateInstance(DateFormat.LONG, new Locale("es","MX"));
        Date newFecha=null;
        try {
            if(formateador!=null){
                newFecha = formateador.parse(fecha);
            }
        } catch (ParseException e) {
             logger.error("Error al convertir fechas: "+e.getMessage());
        }
       String result= dateForm.format(newFecha);
       result=result.replaceAll(" de "," ");
        return result;
    }
    public static String obtieneMes(String fecha){
        DateFormat formateador =null;
        if(fecha.contains("/")){
            formateador= new SimpleDateFormat("dd/MM/yyyy", new Locale("es","MX"));
        }else if(fecha.contains("-")){
            formateador= new SimpleDateFormat("yyyy-MM-dd", new Locale("es","MX")); 
        }
        DateFormat formatMes = new SimpleDateFormat("MMM yyyy", new Locale("es","MX"));
        Date date;
        String mes="";
        try {
            if(formateador!=null){
                date = formateador.parse(fecha);
                formateador =null;
            
            mes= formatMes.format(date);
            mes=mes.substring(0,1).toUpperCase()+mes.substring(1,mes.length());
            }
        } catch (ParseException e) {
             logger.error("Error al convertir fechas: "+e.getMessage());
        }
        return mes;
    }
    
    public static String[] obtieneMesAnioDigit(String fecha){
        DateFormat formateador =null;
        if(fecha.contains("/")){
            formateador= new SimpleDateFormat("dd/MM/yyyy", new Locale("es","MX"));
        }else if(fecha.contains("-")){
            formateador= new SimpleDateFormat("yyyy-MM-dd", new Locale("es","MX")); 
        }
        DateFormat formatMes = new SimpleDateFormat("MM,yyyy", new Locale("es","MX"));
        Date date;
        String mes="";
        try {
            if(formateador!=null){
                date = formateador.parse(fecha);
                formateador =null;
                mes= formatMes.format(date);
            }
        } catch (ParseException e) {
             logger.error("Error al convertir fechas: "+e.getMessage());
        }
        return mes.split(",");
    }
    /*public static void main(String []args){
        System.out.println("Fecha actual: "+DateTime.obtieneFechaActual("dd/MM/yyyy") );
        System.out.println("Fecha con formato para la vista: "+ DateTime.obtieneMes("22/08/2014"));
        System.out.println("Fecha 19/02/2015: "+DateTime.convierteFechaSimpleVista("19/02/2015"));
    }*/
}
