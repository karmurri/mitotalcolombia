package com.totalplay.utils;

import java.net.InetAddress;



import java.util.Date;
import java.util.List;
import java.util.Properties;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmailUtility {
    private Logger logger = LogManager.getLogger(this.getClass().getName());
    FileProperty filePropertyCFG = new FileProperty(Constantes.CFGPROP);

	public boolean sendEmail(String toAddress,String ccAddress,String bccAddress, String replyto,
			String subject, String message){
                
            boolean enviado=false;        
	    final String p="";
            try{
		// sets SMTP server properties
		Properties properties = new Properties();
           
                String host= filePropertyCFG.getProperty("SMTP");
                String port= filePropertyCFG.getProperty("PORT");
                final String userName=filePropertyCFG.getProperty("SENDERMAIL");
                
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "false");
		//properties.put("mail.smtp.starttls.enable", "true");

		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, p);
			}
		};

            Session session = Session.getInstance(properties, auth);

		Message msg = new MimeMessage(session);
                
                
		msg.setFrom(new InternetAddress(userName));
                
                toAddress=toAddress.trim();
                if(isEmail(toAddress)){
                    if(ccAddress!=null && isEmail(ccAddress)){
                        if(bccAddress!=null && isEmail(bccAddress)){
                            InternetAddress[] toAddresses = { new InternetAddress(toAddress), new InternetAddress(ccAddress), new InternetAddress(bccAddress)};
                            msg.setRecipients(Message.RecipientType.TO, toAddresses);
                            msg.setRecipients(Message.RecipientType.CC, toAddresses);
                            msg.setRecipients(Message.RecipientType.BCC, toAddresses);
                            msg.setReplyTo(new javax.mail.Address[]{new javax.mail.internet.InternetAddress(replyto)});
                            msg.setSubject(subject);
                            msg.setSentDate(new Date());
                            msg.setContent(message, "text/html; charset=utf-8");
                            //msg.setText(message);
                        }
                        else {
                            InternetAddress[] toAddresses = { new InternetAddress(toAddress), new InternetAddress(ccAddress)};
                            msg.setRecipients(Message.RecipientType.TO, toAddresses);
                            msg.setRecipients(Message.RecipientType.CC, toAddresses);
                            msg.setReplyTo(new javax.mail.Address[]{new javax.mail.internet.InternetAddress(replyto)});
                            msg.setSubject(subject);
                            msg.setSentDate(new Date());
                            msg.setContent(message, "text/html; charset=utf-8");
                
                        }
                    }
                    else{
                        InternetAddress[] toAddresses = { new InternetAddress(toAddress)};
                        msg.setRecipients(Message.RecipientType.TO, toAddresses);
                        msg.setReplyTo(new javax.mail.Address[]{new javax.mail.internet.InternetAddress(replyto)});
                        msg.setSubject(subject);
                        msg.setSentDate(new Date());
                        msg.setContent(message, "text/html; charset=utf-8");    
                    }
                    
                    // sends the e-mail
                    Transport.send(msg);
                    enviado=true;
                }
                else{
                    logger.error("Error al enviar correo emailutility no es correo "+toAddress);   
                    enviado=false;
                }
            }catch(Exception ex){
                    logger.error("Error durante el envio de correo: "+ex.getMessage());
                    enviado=false;
            }
            return enviado;
	}
        
        public static boolean isEmail(String email){
            boolean isEmail=false;
            Pattern pat= Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher mat=pat.matcher(email);
            if(mat.matches()) isEmail=true;
            return isEmail;
        }
        
    public static String paddingMail(String correo){
        String[] partesMail=correo.split("@");
        if(partesMail.length==2){
                int mitad=partesMail[0].length()/2;
                String newMail=String.format("%0" + mitad + "d", 0).replace("0", "*")+partesMail[0].substring(mitad,partesMail[0].length())+"@"+partesMail[1];
                return newMail;       
        }else{
            return "failcorreo";       
        }
        
    }
        public static void main(String []args){
            System.out.println(EmailUtility.isEmail("a-ndy.qaf-@gmail-algo.otracosa.com"));
        }
        
}
