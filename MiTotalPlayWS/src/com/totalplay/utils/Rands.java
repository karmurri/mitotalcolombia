package com.totalplay.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public final class Rands {
    
    private static Logger logger = LogManager.getLogger( Rands.class );
    
    public Rands() {
        super();
    }
    
    public static String generaPassword(int longitud){
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        String [] palabras= new String[]{"Abaco","Abate","Aboli","Acaso","Acata","Acato","Asea","Acera","Acero","Actua","Adobe","Agape","Agata","Agora","Agote","Ahito","Aires","Airon","Alabe","Alean","Alero","Aliar","Alias","Aloja","Altos","Amaga","Amara","Amole","Anexo","Angel","Aovar","Apela","Apice","Apnea","Apoca","Apodo","Arcos","Ardor","Arduo","Arena","Argon","Arras","Arrea","Arreo","Asilo","Astil","Ataco","Atado","Atajo","Atico","Atole","Atona","Atono","Atrapa","Aureo","Aviar","Axial","Ayala","Azala","Azora","Azote","Babel","Balin","Balon","Banal","Bario","Baron","Beato","Berna","Betel","Betun","Bidon","Bilar","Bosco","Bozal","Breve","Brida","Broca","Bromo","Budin","Burla","Caida","Calar","Calas","Canal","Canon","Capea","Casal","Casta","Casto","Catan","Catar","Catre","Celar","Celio","Cerda","Cerdo","Cesto","Chapa","Cieno","Clase","Clima","Cloro","Colmo","Copar","Copos","Coral","Coran","Coxal","Craneo","Craso","Crema","Criba","Cromo","Cupon","Dalia","Deben","Deles","Densa","Diodo","Diosa","Docto","Doler","Domar","Drena","Efebo","Efeso","Ejido","Elote","Eludo","Emana","Emane","Emite","Emulo","Enano","Eneas","Enema","Epico","Error","Espia","Etica","Etico","Evade","Evito","Exile","Fanal","Farsa","Fasto","Feral","Finca","Forma","Freno","Gaita","Galan","Gales","Galga","Gallo","Galon","Gases","Gaste","Gema","Gesta","Gibas","Gnomo","Grada","Grapa","Graso","Grata","Grial","Grumo","Habil","Hacer","Hadas","Halar","Helio","Hilar","Himno","Honor","Horas","Horda","Humor","Ibera","Icono","Idead","Ideal","Ideas","Ideos","Igneo","Inste","Inuit","Jalon","Jubon","Juro","Labia","Labre","Laico","Lazos","Lemur","Leon","Leona","Liana","Limbo","Lince","Linfa","Liron","Llama","Llana","Logia","Lozas","Lucir","Mamey","Mamut","Manar","Mania","Maori","Marea","Mason","Maulla","Medir","Mente","Miedo","Migas","Miras","Misal","Nadar","Nadas","Natal","Negra","Netas","Nieto","Ninfa","Nodal","Noria","Notar","Novel","Ogros","Oliva","Omiso","Opaco","Opalo","Opima","Orate","Orean","Oreja","Oreos","Orfeo","Oriol","Oruga","Osmio","Ostia","Ostra","Otero","Ozono","Pacas","Pacer","Palas","Panel","Paris","Pausa","Pauta","Perno","Pesar","Piola","Pique","Placer","Pleca","Polis","Polka","Polos","Pomar","Prior","Prisa","Prole","Radar","Radon","Rafia","Ralos","Ramal","Rango","Ravel","Rayad","Razon","Reata","Recio","Redil","Remar","Rengo","Retar","Retro","Rinde","Rivas","Robar","Robin","Rodar","Roera","Roido","Rolon","Ronco","Rondo","Rubro","Rulos","Rumbo","Rupia","Saeta","Sajon","Salas","Sales","Salud","Sanos","Santa","Saten","Secta","Segun","Sello","Sigla","Sinai","Siria","Sodio","Solar","Sonda","Sopor","Spray","Suave","Suero","Tacha","Talar","Talco","Talon","Tamal","Tanto","Tapir","Temor","Tener","Tenue","Tieso","Tilde","Tirso","Titan","Tomar","Tonal","Torca","Torce","Toril","Total","Traba","Traer","Trago","Trama","Trapo","Trata","Treta","Trias","Triple","Trole","Trova","Troza","Ufano","Unias","Unire","Untar","Untos","U�ero","Urico","Usted","Valuo","Vanos","Varia","Variar","Vario","Vatio","Vedar","Venir","Veras","Virar","Visir","Visor","Vista","Vital","Voraz","Vulgo","Yelmo","Yermo","Yerno","Yerto","Zanja","Zarco"};
        String nums= "1234567890";
        String specialChars=".-$@";
        StringBuffer caracteres=new StringBuffer();
        StringBuffer numeros= new StringBuffer();
        StringBuffer special= new StringBuffer();        
        StringBuffer cadenaAleatoria=new StringBuffer();
        
        SecureRandom secureRandom = null;
        try{
        secureRandom = secureRandom.getInstance("SHA1PRNG");
        byte[] bytes = new byte[512];
        secureRandom.nextBytes(bytes);
        
        for(int i=0; i<chars.length(); i++){
            int random= (int)(secureRandom.nextDouble() * chars.length());
            caracteres.append(chars.charAt(random));
        }
        for(int i=0; i<palabras.length; i++){
           int random=(int)(secureRandom.nextDouble() *palabras.length);
            palabras[0]=palabras[random];
        }
        for (int i=0; i<nums.length(); i++){
            int random=(int)(secureRandom.nextDouble() *nums.length());
            numeros.append(nums.charAt(random));
        }
        for (int i=0; i<specialChars.length(); i++){
            int random=(int)(secureRandom.nextDouble() *specialChars.length());
            special.append(specialChars.charAt(random));
        }
        
        }
        catch( NoSuchAlgorithmException e ){
            logger.error(" No Such Algorithm exists " + e);
            }
        
        cadenaAleatoria.append(caracteres.substring(0,longitud-2));
        cadenaAleatoria.append(palabras[0]);
        cadenaAleatoria.append(special.substring(0, 1));
        cadenaAleatoria.append(numeros.substring(0,3)); 
        return (cadenaAleatoria.toString());
        
    }
    
    /*public static void main(String args[]){
        System.out.println(Rands.generaPassword(12));
        //System.out.println("jorge".substring(0,1).toUpperCase() + "jorge".substring(1));
    }*/
}
