package com.totalplay.utils;

public interface Constantes {    
    //Encrypt
   final String PHRASE = "B2JmXXbOHMbebopgM3pC";
   //Local GNUPGRUNTIME 
   //local final String GNUGPGRUNTIME = "E:\\GnuPG\\gpg";
   
   //RUNTIME servidor DMZ
   final String GNUGPGRUNTIME="gpg --homedir /home/oracle/.gnupg";
   
   //RUNTIME SERVIDOR 47
   //final String GNUGPGRUNTIME="gpg --homedir /var/www/.gnupg";
   
    final String CFGNETFLIX="com.totalplay.prop.propNF";
   
   final String WSROUTEPROP="com.totalplay.prop.MTPWS";
   final String CFGPROP="com.totalplay.prop.cfg";
   final String MANTENIMIENTO="com.totalplay.prop.ApplicationResources";
   
  
   //KEYs www.mitotalplay.com.mx
   final String PUBLIC_KEY_RECAPTCHA ="6Le7WfkSAAAAANxfvJNqwWGanbnafVhaBe_Zcd0R";
   final String PRIVATE_KEY_RECAPTCHA="6Le7WfkSAAAAAO-6oAu72Ia44zaYryFBB1iwAIJv";  
   
   //Correos env�o de contrase�a
   final String REPLY_TO ="informacion@totalplay.com.mx";
   final String EMAILCC="?";
   
   final String EMAILBCC="";
   final String SUBJECT= "Env�o de contrase�a temporal Mi Cuenta Totalplay";
   final String SUBJECTPAGO= "Confirmaci�n de pago de servicio Mi Totalplay";
   final String SUBJECTVENTAIPAD="Status de pago venta de experiencia Totalplay Donde sea";
   
   //Errores
   final String ERROR21="El pago no fue autorizado";
   final String ERROR24="No se pudo realizar el pago";
   final String ERROR25="Se present&oacute; un error al actualizar la cuenta";
   final String ERROR26="Se autoriz� el pago, pero no se pudo guardar la tarjeta";
   
    
    final String TBL_ACTLOG="act_log";
    final String TBL_FBCLIENTS="Client_fb";
    final String TBL_RELFRIENDS="Clients_friends";
    final String TBL_FRIENDS="friends";
    final String TBL_RELACTIVITIES="Clients_activities";
    final String TBL_ACTIVITY="activities";
    final String TBL_RELINTERESTS="Client_interests";
    final String TBL_INTERESTS="interests";
    final String TBL_RELLIKES="Client_likes";
    final String TBL_LIKES="likes";
    final String TBL_RELVIDEOS="Client_actions_video";
    final String TBL_VIDEOS="actions_video";
    final String TBL_RELMUSIC="Client_actions_music";
    final String TBL_MUSIC="actions_music";
    final String TBL_RELFITNESS="Client_actions_fitness";
    final String TBL_FITNESS="actions_fitness";
    final String TBL_RELNEWS="Client_actions_news";
    final String TBL_NEWS="actions_news";
    
   //Correo soporte pagos
   final String EMAIL_SUPORT="jrmorenos@totalplay.com.mx";
   final String EMAIL_SUPPORTCC="jrmorenos@totalplay.com.mx";
   final String SUBJECT_SUPPORT="Error portal Mi Totalplay!!";
   
   //Correo sincronizaci�n de cuenta Facebook
   final String SUBJECT_FACEBOOK= "Enhorabuena, tu cuenta de Facebook a sido sincronizada a tu cuenta TotalPlay";
   
   //Obtenci�n de tipo de moneda asociada a la cuenta
   final String P_USD="Middle100$";
   final String U_USD="25631";    
}
