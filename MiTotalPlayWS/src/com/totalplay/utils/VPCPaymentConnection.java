package com.totalplay.utils;


import com.totalplay.db.mybatis.ConnectionFactory;

import java.io.UnsupportedEncodingException;

import java.math.BigInteger;

import java.net.URLEncoder;

import java.nio.charset.Charset;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import utils.system;

public class VPCPaymentConnection {

    private static Logger logger = LogManager.getLogger(ConnectionFactory.class.getName());
    private String postData="";
    private String hashInput="";
    private String secureHashSecret;
    public VPCPaymentConnection() {
        super();
        
    }
    
    public boolean addDigitalOrderField(String field, String value){
       // System.out.println("Datos a ser concatenados:"+field+ " "+value );
        try{
        if(field.length()==0) return false;
        if(value.length()==0) return false;
        this.postData+=((this.postData.equals(""))?"":"&")+URLEncoder.encode(field,"UTF-8")+"="+URLEncoder.encode(value,"UTF-8");
        this.hashInput+=field+"="+value+"&";
        }catch(Exception e){
            e.printStackTrace();
            //System.out.println(e.getMessage());
        }
        return true;
    }
    
    public void setSecureSecret(String secret) {              
        this.secureHashSecret = secret;
    }
      
    public  String hashAllFields(){
        FileProperty filePropertyCFG=new FileProperty("com.totalplay.prop.ApplicationResources");
       // System.out.println("Input hash antes de replace:"+this.hashInput);
        this.hashInput= this.hashInput.replaceAll("&+$","");
       // System.out.println("Input hash despu�s de replace:"+this.hashInput);
        String res= hash_hmac3(this.hashInput,this.secureHashSecret);
        
       return res.toUpperCase();
    }
      
    private static String pack1(byte[] rawSHA){
        return( new String(Hex.encodeHex(rawSHA)));
    }
      
    private static String pack(String hex) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hex.length(); i += 2) {
            String str = hex.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }
        return output.toString();
    }
    
    public static byte[] hexToString(String hex) {
        // hexToString that works at a byte level, not a character level
        byte[] output = new byte[(hex.length() + 1) / 2];
        for (int i = hex.length() - 1; i >= 0; i -= 2) {
            int from = i - 1;
            if (from < 0) {
                from = 0;
            }
            String str = hex.substring(from, i + 1);
            output[i/2] = (byte)Integer.parseInt(str, 16);
        }
        return output;
    }
    
    public static String hexify(byte[] data) {
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < data.length; i++) {
    sb.append(Integer.toHexString(((int) data[i]) & 0xFF));
    }
    return sb.toString();
    }
    
    
    private static String hash_hmac2(String data, String key) {
       // System.out.println("Datos para ser udados Encryp: "+data+key);
        byte[] digest=null;
        try{
        Mac hmac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(Charset.forName("UTF-8").encode(key).array(), "HmacSHA256");
           // System.out.println("Llave secreta: "+secret_key.getFormat()+secret_key.getAlgorithm()+secret_key.getEncoded());
        hmac.init(secret_key);
         digest = hmac.doFinal(data.getBytes("UTF-8"));
        //System.out.println(hexify(digest));
        }catch(Exception e){
            e.printStackTrace();
        }
        return hexify(digest);
    }
    
    private static String hash_hmac3(String data,String key){
        Mac mac;
            String result ="";
            try
            {
                final SecretKeySpec secretKey = new SecretKeySpec( DatatypeConverter.parseHexBinary(key),"HmacSHA256");
                mac =Mac.getInstance("HmacSHA256");
                mac.init( secretKey );
                final byte[] macData = mac.doFinal( data.getBytes());
                byte[] hex =new Hex().encode( macData );
                result =new String( hex,"UTF-8");
            }catch(Exception e){
                e.printStackTrace();
            }

            return result.toUpperCase();
    }
    
  
    
    
    public String getDigitalOrder(String vpcURL){
        String redirectURL= vpcURL+"?"+this.postData;
        return redirectURL;
    }
    
   /* public static void main(String []args) throws Exception{
        VPCPaymentConnection obj= new VPCPaymentConnection();
        //obj.hash_hmac();
        //obj.hash_hmac( "0A4973B35D62A9AF62D59DFB3068271C","hola");
        System.out.println("Resultado: "+ obj.hash_hmac3("vpc_AccessCode=5439CB3F&vpc_Amount=768500&vpc_Command=pay&vpc_Currency=MXN&vpc_CustomPaymentPlanPlanId=BPWOI7&vpc_Locale=es_MX&vpc_MerchTxnRef=15127525-hk29pld4fc34vudstfjgi0gm60&vpc_Merchant=1030534&vpc_OrderInfo=hk29pld4fc34vudstfjgi0gm60&vpc_ReturnURL=http://www.mitotalplay.com.mx/respbmx.do&vpc_Version=1", "79F7E1E533BAEB321E295B48618C78FF"));
        
        System.out.println("pack: "+obj.pack("79F7E1E533BAEB321E295B48618C78FF"));
    }*/
    
}
