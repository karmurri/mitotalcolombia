package com.totalplay.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public final class ProcesaHtml {
    public ProcesaHtml() {
        super();
    }
    public static String creaCadenaHTML(String archivoHtm, String usuario, String account, String password){
        
   // public static void main(String []args){
       
        StringBuffer result= new StringBuffer();
        File archivo = null;
             FileReader fr = null;
             BufferedReader br = null;
        
             try {
                
                archivo = new File (archivoHtm);
                // System.out.println(archivo.getAbsolutePath());
                fr = new FileReader (archivo);
                br = new BufferedReader(fr);

                String linea=br.readLine();
                 while((linea)!=null){    
                     if(linea.contains("{usuario}"))
                            linea=linea.replace("{usuario}", usuario);
                     else if(linea.contains("{account}"))
                            linea=linea.replace("{account}", account);
                     else if(linea.contains("{password}"))
                            linea=linea.replace("{password}", password);
                     result.append(linea);
                     linea=br.readLine();
                 }
             }
             catch(Exception e){
                e.printStackTrace();
             }finally{
            
                try{                    
                   if( null != fr ){   
                      fr.close();     
                   }                  
                }catch (Exception e2){ 
                   e2.printStackTrace();
                }
             }
        return(result.toString());
    }
    
    public static String creaCadenaHTMLPagoVacio(String archivoHtm, String usuario){
        
    // public static void main(String []args){
       
        StringBuffer result= new StringBuffer();
        File archivo = null;
             FileReader fr = null;
             BufferedReader br = null;
        
             try {
                
                archivo = new File (archivoHtm);
                // System.out.println(archivo.getAbsolutePath());
                fr = new FileReader (archivo);
                br = new BufferedReader(fr);

                String linea=br.readLine();
                 while((linea)!=null){    
                     if(linea.contains("{usuario}"))
                            linea=linea.replace("{usuario}", usuario);
                     result.append(linea);
                     linea=br.readLine();
                 }
             }
             catch(Exception e){
                e.printStackTrace();
             }finally{
            
                try{                    
                   if( null != fr ){   
                      fr.close();     
                   }                  
                }catch (Exception e2){ 
                   e2.printStackTrace();
                }
             }
        return(result.toString());
    }
    
    public static String creaCadenaHTMLPago(String archivoHtm, String usuario, String referencia, String adeudo, String terminacion){
        
    // public static void main(String []args){
       
        StringBuffer result= new StringBuffer();
        File archivo = null;
             FileReader fr = null;
             BufferedReader br = null;
        
             try {
                
                archivo = new File (archivoHtm);
                // System.out.println(archivo.getAbsolutePath());
                fr = new FileReader (archivo);
                br = new BufferedReader(fr);

                String linea=br.readLine();
                 while((linea)!=null){    
                     if(linea.contains("{usuario}"))
                            linea=linea.replace("{usuario}", usuario);
                     else if(linea.contains("{referencia}"))
                            linea=linea.replace("{referencia}", referencia);
                     else if(linea.contains("{adeudo}"))
                            linea=linea.replace("{adeudo}", adeudo);
                     else if(linea.contains("{terminacion}"))
                            linea=linea.replace("{terminacion}", terminacion);
                     result.append(linea);
                     linea=br.readLine();
                 }
             }
             catch(Exception e){
                e.printStackTrace();
             }finally{
            
                try{                    
                   if( null != fr ){   
                      fr.close();     
                   }                  
                }catch (Exception e2){ 
                   e2.printStackTrace();
                }
             }
        return(result.toString());
    }
    public static String creaCadenaHTMLSoporte(String archivoHtm, String message){
        
    // public static void main(String []args){
       
        StringBuffer result= new StringBuffer();
        File archivo = null;
             FileReader fr = null;
             BufferedReader br = null;
        
             try {
                
                archivo = new File (archivoHtm);
                // System.out.println(archivo.getAbsolutePath());
                fr = new FileReader (archivo);
                br = new BufferedReader(fr);

                String linea=br.readLine();
                 while((linea)!=null){    
                     if(linea.contains("{message}"))
                            linea=linea.replace("{message}", message);
                    
                     result.append(linea);
                     linea=br.readLine();
                 }
             }
             catch(Exception e){
                e.printStackTrace();
             }finally{
            
                try{                    
                   if( null != fr ){   
                      fr.close();     
                   }                  
                }catch (Exception e2){ 
                   e2.printStackTrace();
                }
             }
        return(result.toString());
    }
    public static String creaCadenaHTMLFacebook(String archivoHtm, String usuario, String linkFacebook, String account){
        
    // public static void main(String []args){
       
        StringBuffer result= new StringBuffer();
        File archivo = null;
             FileReader fr = null;
             BufferedReader br = null;
        
             try {
                
                archivo = new File (archivoHtm);
                // System.out.println(archivo.getAbsolutePath());
                fr = new FileReader (archivo);
                br = new BufferedReader(fr);

                String linea=br.readLine();
                 while((linea)!=null){    
                     if(linea.contains("{usuario}"))
                            linea=linea.replace("{usuario}", usuario);
                     else if(linea.contains("{linkFacebook}"))
                            linea=linea.replace("{linkFacebook}", linkFacebook);
                     else if(linea.contains("{account}"))
                            linea=linea.replace("{account}", account);
                    
                     result.append(linea);
                     linea=br.readLine();
                 }
             }
             catch(Exception e){
                e.printStackTrace();
             }finally{
            
                try{                    
                   if( null != fr ){   
                      fr.close();     
                   }                  
                }catch (Exception e2){ 
                   e2.printStackTrace();
                }
             }
        return(result.toString());
    }
    
    public static String creaCadenaHTMLVentaIPAD(String archivoHtm, String dominio, String usuario, String fecha, String detalle, String meses, String total, String totalcompras,String diasentrega){
        
    
       
        StringBuffer result= new StringBuffer();
        File archivo = null;
             FileReader fr = null;
             BufferedReader br = null;
        
             try {
                
                archivo = new File (archivoHtm);
                // System.out.println(archivo.getAbsolutePath());
                fr = new FileReader (archivo);
                br = new BufferedReader(fr);

                String linea=br.readLine();
                 while((linea)!=null){    
                    if(linea.contains("{dominio}"))
                        linea=linea.replace("{dominio}", dominio);
                    if (linea.contains("{usuario}"))
                        linea = linea.replace("{usuario}", usuario);
                    if (linea.contains("{fecha}"))
                        linea = linea.replace("{fecha}", fecha);
                    if (linea.contains("{detalle}"))
                        linea = linea.replace("{detalle}", detalle);
                    if (linea.contains("{meses}"))
                        linea = linea.replace("{meses}", meses);
                    if (linea.contains("{total}"))
                        linea = linea.replace("{total}", total);
                    if (linea.contains("{totalcompras}"))
                        linea = linea.replace("{totalcompras}", totalcompras);
                    if (linea.contains("{diasentrega}"))
                        linea = linea.replace("{diasentrega}", diasentrega);
                     
                     result.append(linea);
                     linea=br.readLine();
                 }
             }
             catch(Exception e){
                e.printStackTrace();
             }finally{
            
                try{                    
                   if( null != fr ){   
                      fr.close();     
                   }                  
                }catch (Exception e2){ 
                   e2.printStackTrace();
                }
             }
        return(result.toString());
    }
    
    
}
