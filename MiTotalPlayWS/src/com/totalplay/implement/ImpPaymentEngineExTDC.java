package com.totalplay.implement;

import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.PaymentProcessing;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.Process;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.ProcessResponse;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.Process.ChargeModel;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.Process.ChargeModel.ClientCard;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.Process.UserPassIp;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessing.Paymentprocessing_client_ep;
import com.totalplay.interfaces.IntPaymentEngineExTDC;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;
import com.totalplay.ws.response.ResponsePaymentEngineExTDC;

import com.totalplay.ws.vo.ClientCardVO;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implementacion para pagar con TDC
 * @author: MTP
 * @version: 26/08/2014
 */

public class ImpPaymentEngineExTDC implements IntPaymentEngineExTDC{
   
    /**
      * M�todo que realiza el pago con Tarjeta ya existente
      * @param ninguno
      * @return ResponsePaymentEngine
      */ 
    Logger logger = LogManager.getLogger(this.getClass().getName());
    public ProcessResponse payExTDC(String cuenta, String monto, ClientCardVO tarjetaVo, String ipCliente) {
        
        ResponsePaymentEngineExTDC responsePaymentEngineExTDC = new ResponsePaymentEngineExTDC();
        
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
        FileProperty filePropertyCFG = new FileProperty(Constantes.CFGPROP);
        
        URL wsURL=null;
        try {
            wsURL = new URL(fileProperty.getProperty("PAYEXSTDC_BRM"));
        } catch (MalformedURLException e) {
            logger.error("Error al construir la cadena:"+fileProperty.getProperty("PAYEXSTDC_BRM"),e);
        }
        Paymentprocessing_client_ep  paymentprocessing_client_ep = new Paymentprocessing_client_ep(wsURL,new QName("http://xmlns.oracle.com/MiddlewareApplication/PaymentsEngine/PaymentProcessing",
                    "paymentprocessing_client_ep"));
        PaymentProcessing paymentProcessing = paymentprocessing_client_ep.getPaymentProcessing_pt();

        Process payload = new Process();
      
        UserPassIp userPassIp = new UserPassIp();
        userPassIp.setUserId(filePropertyCFG.getProperty("PAY_TPROVUSER2"));
        userPassIp.setPassword(filePropertyCFG.getProperty("PAY_TPROVPWD"));
        userPassIp.setIp("10.213.24.41");
        payload.setUserPassIp(userPassIp);
        
        ChargeModel chargeModel = new ChargeModel();
        chargeModel.setAccountNo(cuenta);          ////////parametro cuenta
        chargeModel.setAmount(monto);                 ////////////parametro monto 
        chargeModel.setBussinesId(filePropertyCFG.getProperty("PAY_BUSSINES_ID2"));
        chargeModel.setClientIp(ipCliente);
        chargeModel.setPaymentType(filePropertyCFG.getProperty("PAYMENTTYPE")); 
        
        ClientCard tarjeta= new ClientCard();
        tarjeta.setIdCard(tarjetaVo.getIdCard());     /////////////////////////////parametro numtarjeta
        tarjeta.setMonthsNoInterest(filePropertyCFG.getProperty("MESESDIFERIDO"));    
        tarjeta.setCvv2(tarjetaVo.getCvv2());
    
        chargeModel.setClientCard(tarjeta);
        
        
        payload.setChargeModel(chargeModel);

        ProcessResponse responseWS = paymentProcessing.process(payload);

     return responseWS;
    }
}
