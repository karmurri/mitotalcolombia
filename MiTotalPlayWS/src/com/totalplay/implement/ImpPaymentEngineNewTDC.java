package com.totalplay.implement;


import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.PaymentProcessingNC;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.Paymentprocessingnc_client_ep;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.Process;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.Process.ChargeModel;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.Process.NewCard;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.Process.UserPassIp;
import com.oracle.xmlns.middlewareapplication.paymentsengine.paymentprocessingnc.ProcessResponse;

import com.totalplay.interfaces.IntPaymentEngineNewTDC;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;
import com.totalplay.ws.response.ResponsePaymentEngineNewTDC;
import com.totalplay.ws.vo.newCardVO;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Implementacion para pagar con TDC
 * @author: MTP
 * @version: 26/08/2014
 */

public class ImpPaymentEngineNewTDC implements IntPaymentEngineNewTDC{

  /**
    * M�todo que realiza el pago de Nueva Tarjeta 
    * @param ninguno
    * @return ResponsePaymentEngine
    */
   Logger logger = LogManager.getLogger(this.getClass().getName());
    public ProcessResponse payNewTDC(String accountNo,String amount,newCardVO newcardVO, String ipCliente) {
      
      
        
        ResponsePaymentEngineNewTDC responsePaymentEngine = new ResponsePaymentEngineNewTDC();
        
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
        FileProperty filePropertyCFG = new FileProperty(Constantes.CFGPROP);
        URL wsURL=null;
        try {
            wsURL = new URL(fileProperty.getProperty("PAYNEWTDC_BRM"));
        } catch (MalformedURLException e) {
            logger.error("Error al construir la cadena:"+fileProperty.getProperty("PAYEXSTDC_BRM"),e);
        }
        Paymentprocessingnc_client_ep paymentprocessingnc_client_ep = new Paymentprocessingnc_client_ep(wsURL, new QName("http://xmlns.oracle.com/MiddlewareApplication/PaymentsEngine/PaymentProcessingNC",
                    "paymentprocessingnc_client_ep"));
        
        PaymentProcessingNC paymentProcessingNC = paymentprocessingnc_client_ep.getPaymentProcessingNC_pt();

        Process payload = new Process();
        
        UserPassIp userPassIp = new UserPassIp();
        userPassIp.setUserId(filePropertyCFG.getProperty("PAY_TPROVUSER2"));
        userPassIp.setPassword(filePropertyCFG.getProperty("PAY_TPROVPWD"));
        userPassIp.setIp("10.213.24.41");
        payload.setUserPassIp(userPassIp);
        
        ChargeModel chargeModel = new ChargeModel();
        chargeModel.setAccountNo(accountNo);
        chargeModel.setAmount(amount);
        chargeModel.setBussinesId(filePropertyCFG.getProperty("PAY_BUSSINES_ID2"));
        chargeModel.setClientIp(ipCliente);
        chargeModel.setMonthsNoInterest(filePropertyCFG.getProperty("MESESDIFERIDO"));
        chargeModel.setPaymentType(filePropertyCFG.getProperty("PAYMENTTYPE"));
        
        payload.setChargeModel(chargeModel);

        NewCard newCard = new NewCard();
        newCard.setBlSaveCard("false");
        newCard.setAddress( (newcardVO.getAddress()!=null)?newcardVO.getAddress():"");
        newCard.setBank(newcardVO.getBank());
        newCard.setBankIdProvider(newcardVO.getBankIdProvider()); //Identificador Amex 1, visa 2, mastercard 3
        newCard.setCardNumber(newcardVO.getCardNumber());
        newCard.setCity(newcardVO.getCity());
        newCard.setColony(newcardVO.getColony());
        newCard.setCountry(newcardVO.getCountry());
        newCard.setCvv2(newcardVO.getCvv2());
        newCard.setExpirationMonth(newcardVO.getExpirationMonth());
        newCard.setExpirationYear(newcardVO.getExpirationYear());
        newCard.setFirstName(newcardVO.getFirstName());
        newCard.setLastName(newcardVO.getLastName());
        newCard.setMunicipality(newcardVO.getMunicipality());
        newCard.setProvider(newcardVO.getProvider());
        newCard.setRfc(newcardVO.getRfc());
        newCard.setState(newcardVO.getState());
        newCard.setStatus(newcardVO.getStatus());
        newCard.setTelephone(newcardVO.getTelephone());
        newCard.setTrackCard(newcardVO.getTrackCard());
        newCard.setTrackCardDos(newcardVO.getTrackCardDos());
        newCard.setZip(newcardVO.getZip());
        
        payload.setNewCard(newCard);

       

        ProcessResponse responseWS = paymentProcessingNC.process(payload);
//        System.out.println("Respuesta desde WEB Service: "+responseWS.getBrmResult().getResponse().getDescription() );
         responseWS.getBankResponse();
         responseWS.getBrmResult();
         responseWS.getSaveCardResponse();
        
        return  responseWS;
    }
}
