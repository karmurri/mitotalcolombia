package com.totalplay.db.dbo;

public class EventoDBO {
    private String id_evento;
    private String id_canal;
    private String id_tarjeta;
    private String fecha_evento;
    private String monto;
    private String estatus;
    
    public EventoDBO(){
        super();
        this.id_evento=" ";
        this.id_canal=" ";
        this.id_tarjeta=" ";
        this.fecha_evento=" ";
        this.monto=" ";
        this.estatus=" ";
    }

    public void setId_evento(String id_evento) {
        this.id_evento = id_evento;
    }

    public String getId_evento() {
        return id_evento;
    }

    public void setId_tarjeta(String id_tarjeta) {
        this.id_tarjeta = id_tarjeta;
    }

    public String getId_tarjeta() {
        return id_tarjeta;
    }

    public void setFecha_evento(String fecha_evento) {
        this.fecha_evento = fecha_evento;
    }

    public String getFecha_evento() {
        return fecha_evento;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getMonto() {
        return monto;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setId_canal(String id_canal) {
        this.id_canal = id_canal;
    }

    public String getId_canal() {
        return id_canal;
    }
}
