package com.totalplay.db.dbo;

public class RegistroTCRDBO {
    private String num_cuenta;
    private String nombre_cliente;
    private String tel_fijo;
    private String tel_movil;
    private String email;
    private String promocion;
    private String fecha_reg;
    private String browser;
    private String ip;
    private String termtarj;
    private String banco;
    private String comentario;
    private String nom_tarhab;
    private TarjetaDBO tarjeta;
    private EventoDBO evento;
    
    public RegistroTCRDBO() {
        super();
        this.num_cuenta=" ";
        this.nombre_cliente=" ";
        this.tel_fijo=" ";
        this.tel_movil=" ";
        this.email=" ";
        this.promocion=" ";
        this.fecha_reg=" ";
        this.browser=" ";
        this.ip=" ";
        this.termtarj=" ";
        this.banco=" ";
        this.comentario=" ";
        this.nom_tarhab=" ";
    }

    public void setNum_cuenta(String num_cuenta) {
        this.num_cuenta = num_cuenta;
    }

    public String getNum_cuenta() {
        return num_cuenta;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setTel_fijo(String tel_fijo) {
        this.tel_fijo = tel_fijo;
    }

    public String getTel_fijo() {
        return tel_fijo;
    }

    public void setTel_movil(String tel_movil) {
        this.tel_movil = tel_movil;
    }

    public String getTel_movil() {
        return tel_movil;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPromocion(String promocion) {
        this.promocion = promocion;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setFecha_reg(String fecha_reg) {
        this.fecha_reg = fecha_reg;
    }

    public String getFecha_reg() {
        return fecha_reg;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getBrowser() {
        return browser;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }

    public void setTermtarj(String termtarj) {
        this.termtarj = termtarj;
    }

    public String getTermtarj() {
        return termtarj;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getBanco() {
        return banco;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getComentario() {
        return comentario;
    }

    public void setNom_tarhab(String nom_tarhab) {
        this.nom_tarhab = nom_tarhab;
    }

    public String getNom_tarhab() {
        return nom_tarhab;
    }

    public void setTarjeta(TarjetaDBO tarjeta) {
        this.tarjeta = tarjeta;
    }

    public TarjetaDBO getTarjeta() {
        return tarjeta;
    }

    public void setEvento(EventoDBO evento) {
        this.evento = evento;
    }

    public EventoDBO getEvento() {
        return evento;
    }
}
