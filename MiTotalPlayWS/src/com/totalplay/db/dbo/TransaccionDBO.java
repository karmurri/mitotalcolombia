package com.totalplay.db.dbo;

import java.util.Date;

public class TransaccionDBO {
    private String userID; 
    private String message;
    private String titular;
    private String ammount;
    private String AuthBank;
    private String BankName;
    private String cEndNumber;
    private int error;
    private String ip;
    private String browser;
    private String logdate;
        
    public TransaccionDBO() {
        super();
        this.userID=" "; 
        this.message=" ";
        this.titular=" ";
        this.ammount=" ";
        this.AuthBank=" ";
        this.BankName=" ";
        this.cEndNumber=" ";
        this.error=0;
        this.ip=" ";
        this.browser=" ";
        this.logdate=" ";
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getTitular() {
        return titular;
    }

    public void setAmmount(String ammount) {
        this.ammount = ammount;
    }

    public String getAmmount() {
        return ammount;
    }

    public void setAuthBank(String AuthBank) {
        this.AuthBank = AuthBank;
    }

    public String getAuthBank() {
        return AuthBank;
    }

    public void setBankName(String BankName) {
        this.BankName = BankName;
    }

    public String getBankName() {
        return BankName;
    }

    public void setCEndNumber(String cEndNumber) {
        this.cEndNumber = cEndNumber;
    }

    public String getCEndNumber() {
        return cEndNumber;
    }

    public void setError(int error) {
        this.error = error;
    }

    public int getError() {
        return error;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getBrowser() {
        return browser;
    }

  

    public String toString(){
        return( "userID: "+this.userID+" message: "+this.message+" titular: "+
                this.titular+" authBank: "+this.AuthBank+" bankName: "+
                this.BankName+" cEndNumber: "+this.cEndNumber+" error: "+
                this.error+" ip: "+this.ip+"  browser: "+this.browser+" logdate: "+this.logdate);
    }

    public void setLogdate(String logdate) {
        this.logdate = logdate;
    }

    public String getLogdate() {
        return logdate;
    }
}
