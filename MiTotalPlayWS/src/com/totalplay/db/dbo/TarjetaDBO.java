package com.totalplay.db.dbo;

public class TarjetaDBO {
    private String id_tarjeta;
    private String id_promocion;
    private String num_tarjeta;
    private String num_tarjeta_mascara;
    private String numero_serguridad;
    private String fecha_vencimiento;
    private String nombre;
    private String apellido_paterno;
    private String apellido_materno;
    private String calle;
    private String colonia;
    private String codigo_postal;
    private String delegacion;
    private String estado;
    private String pais;
    
    public TarjetaDBO() {
        super();
    }

    public void setId_tarjeta(String id_tarjeta) {
        this.id_tarjeta = id_tarjeta;
    }

    public String getId_tarjeta() {
        return id_tarjeta;
    }

    public void setId_promocion(String id_promocion) {
        this.id_promocion = id_promocion;
    }

    public String getId_promocion() {
        return id_promocion;
    }

    public void setNum_tarjeta(String num_tarjeta) {
        this.num_tarjeta = num_tarjeta;
    }

    public String getNum_tarjeta() {
        return num_tarjeta;
    }

    public void setNum_tarjeta_mascara(String num_tarjeta_mascara) {
        this.num_tarjeta_mascara = num_tarjeta_mascara;
    }

    public String getNum_tarjeta_mascara() {
        return num_tarjeta_mascara;
    }

    public void setNumero_serguridad(String numero_serguridad) {
        this.numero_serguridad = numero_serguridad;
    }

    public String getNumero_serguridad() {
        return numero_serguridad;
    }

    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCalle() {
        return calle;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getColonia() {
        return colonia;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public String getCodigo_postal() {
        return codigo_postal;
    }

    public void setDelegacion(String delegacion) {
        this.delegacion = delegacion;
    }

    public String getDelegacion() {
        return delegacion;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getPais() {
        return pais;
    }
}
