package com.totalplay.db.dbo;

public class PromocionDBO {
    private String promocion;
    private String promid;
    private String activa;
    public PromocionDBO() {
        super();
    }

    public void setPromocion(String descripcion) {
        this.promocion = descripcion;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setPromid(String promoID) {
        this.promid = promoID;
    }

    public String getPromid() {
        return promid;
    }

    public void setActiva(String activa) {
        this.activa = activa;
    }

    public String getActiva() {
        return activa;
    }
}
