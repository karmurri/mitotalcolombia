package com.totalplay.db.dbo;

public class PromotionDBO {
    private String id_promocion;
    private String promocion;
    private String descripcion;
    public PromotionDBO() {
        super();
    }

    public void setId_promocion(String id_promocion) {
        this.id_promocion = id_promocion;
    }

    public String getId_promocion() {
        return id_promocion;
    }

    public void setPromocion(String promocion) {
        this.promocion = promocion;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
