package com.totalplay.db.mybatis;

import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConnectionFactory {
    private static Logger logger = LogManager.getLogger(ConnectionFactory.class.getName());
    private static SqlSessionFactory sqlMapper;
            private static Reader reader; 

            static{
                    try{
                        reader    = Resources.getResourceAsReader("com/totalplay/db/mybatis/configuration.xml");
                        if(sqlMapper==null){
                            sqlMapper = new SqlSessionFactoryBuilder().build(reader);
                        }
                        
                    }catch(Exception e){
                            e.printStackTrace();
                            logger.error("Error de conexion a BD: "+e.getMessage());
                    }
            }

            public static SqlSessionFactory getSession(){
                    return sqlMapper;
            }
}
