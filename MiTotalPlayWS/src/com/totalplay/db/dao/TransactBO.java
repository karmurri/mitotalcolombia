package com.totalplay.db.dao;

import com.totalplay.db.dbo.EventoDBO;
import com.totalplay.db.dbo.PromocionDBO;
import com.totalplay.db.dbo.PromotionDBO;
import com.totalplay.db.dbo.RegistroTCRDBO;
import com.totalplay.db.dbo.TarjetaDBO;
import com.totalplay.db.mybatis.ConnectionFactory;

import com.totalplay.db.dbo.TransaccionDBO;

import com.totalplay.utils.DateTime;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TransactBO {
    Logger logger = LogManager.getLogger(this.getClass().getName());
    private SqlSessionFactory sqlSessionFactory;
    
    public TransactBO(SqlSessionFactory sqlSessionFactory) {
       this.sqlSessionFactory=sqlSessionFactory;
    }
    public List<TransaccionDBO> getTransacts() throws Exception{
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);
        List<TransaccionDBO> transacciones=null;
        try{
        transacciones= dao.getTransactions();
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al listar transacciones: "+e.getMessage());
        }finally{
            session.commit();
            session.close();
        }
        return transacciones;
    }
    
    
    public boolean insertTransaction(TransaccionDBO transact){
        boolean insertado= false;
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);

        try {
            if(dao.insertTransact(transact)==1) insertado= true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error al insertar transaccion: "+e.getMessage());
        }
        finally{
            session.commit();
            session.close();
        }
        return insertado;
    }
    
    public List<TransaccionDBO> getPagosHoy(String userID) throws Exception {
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);
        List<TransaccionDBO> transacciones=null;
        try{
            transacciones = dao.getPagosHoy(userID);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al obtener pagos hoy: "+e.getMessage());
        }finally{
            session.commit();
            session.close();
        }
        return transacciones;
    }
    
    public List<PromocionDBO> getPromos() {
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);
        List<PromocionDBO> promociones=null;
        try{
        promociones= dao.getPromotions();
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al listar promociones: "+e.getMessage());
        }finally{
            session.commit();
            session.close();
        }
        return promociones;
    }
    
   /* public boolean insertRegistroTCR(RegistroTCRDBO registro){
        boolean insertado= false;
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);

        try {
            if(dao.insertRegistroTCR(registro)==1) insertado= true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error al insertar registro de tarjeta CR: "+e.getMessage());
        }
        finally{
            session.commit();
            session.close();
        }
        return insertado;
    }*/
    
    
    public List<PromotionDBO> getPromociones() {
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);
        List<PromotionDBO> promociones=null;
        try{
        promociones= dao.getPromos();
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al listar promociones: "+e.getMessage());
        }finally{
            session.commit();
            session.close();
        }
        return promociones;
    }
    
    public String getLastIDTarjeta(){
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);
        String lastID=null;
        try{
            lastID = dao.lastIdTarjeta();
            if(lastID==null || lastID.equalsIgnoreCase("0")){
                lastID="1";
            }else{
                int idInt=(Integer.parseInt(lastID))+1;
                lastID=idInt+"";
            }
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al obtener ID a insertar: "+e.getMessage());
        }finally{
            session.commit();
            session.close();
        }
        return lastID;
    }
    
    public String getLastIDEvento(){
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);
        String lastID=null;
        try{
            lastID = dao.lastIdEvento();
            if(lastID==null || lastID.equalsIgnoreCase("0")){
                lastID="1";
            }else{
                int idInt=(Integer.parseInt(lastID))+1;
                lastID=idInt+"";
            }
        }catch(Exception e){
            e.printStackTrace();
            logger.error("Error al obtener ID a insertar: "+e.getMessage());
        }finally{
            session.commit();
            session.close();
        }
        return lastID;
    }
    
    public boolean insertTarjeta(TarjetaDBO tarjeta){
        boolean insertado= false;
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);
        try {
            if(dao.insertTarjeta(tarjeta)==1) insertado= true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error al insertar transaccion: "+e.getMessage());
        }
        finally{
            session.commit();
            session.close();
        }
        return insertado;
    }
    
    public boolean insertEvento(EventoDBO evento){
        boolean insertado= false;
        SqlSession session= sqlSessionFactory.openSession();
        TransactDAO dao= session.getMapper(TransactDAO.class);
        try {
            if(dao.insertEvento(evento)==1) insertado= true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error al insertar transaccion: "+e.getMessage());
        }
        finally{
            session.commit();
            session.close();
        }
        return insertado;
    }
   /* public static void main(String [] args)throws Exception{
      /*  TransactBO bo= new TransactBO();
        for(TransaccionDBO trans: bo.getTransacts())
            System.out.println("Transaccion: "+trans.getTitular());
        
        TransaccionDBO transact= new TransaccionDBO();
        transact.setAmmount("1,200");
        transact.setUserID("1.5127525");
        transact.setMessage("Test");
        transact.setTitular("Jorge Test");
        transact.setLogdate("2014-10-09 02:11:00");
    System.out.println(bo.insertTransaction(transact));
    
      System.out.println(DateTime.obtieneFechaHoraActual("yyyy-MM-dd hh:mm:ss"));
        for(TransaccionDBO trans: bo.getPagosHoy("1.5127525"))
            System.out.println("Transaccion: "+trans.getLogdate());
    }
*/
    
}
