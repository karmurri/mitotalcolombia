package com.totalplay.db.dao;

import com.totalplay.db.dbo.EventoDBO;
import com.totalplay.db.dbo.PromocionDBO;
import com.totalplay.db.dbo.PromotionDBO;
import com.totalplay.db.dbo.RegistroTCRDBO;
import com.totalplay.db.dbo.TarjetaDBO;
import com.totalplay.db.dbo.TransaccionDBO;

import com.totalplay.utils.Constantes;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

public interface TransactDAO {
 
    String GET_ALL_TRANSACTIONS="SELECT * FROM act_log";
    String INSERT_TRANSACTIONS="INSERT INTO " + Constantes.TBL_ACTLOG+" (userID, message,titular, " +
                                                    "ammount, AuthBank, BankName, cEndNumber, error, ip, " +
                                                    "browser, logdate ) " +
                                                    "values ( #{userID}, utl_raw.cast_to_raw(convert(#{message},'WE8ISO8859P1')),#{titular}," + 
                                                    "#{ammount}, #{AuthBank}, #{BankName},#{cEndNumber}, #{error}, #{ip}," + 
                                                    "#{browser}, sysdate)";
    //Mysql   now() en lugar de sysdate
   /* String GET_PAGOSHOY = "SELECT AuthBank, ammount FROM "+Constantes.TBL_ACTLOG+
                          " WHERE  DAY(logdate) = (DAY(NOW())) AND MONTH(logdate) = MONTH(NOW()) AND YEAR(logdate) = YEAR(NOW()) AND userID = #{userID}";*/
    String GET_PAGOSHOY = "SELECT AuthBank, ammount FROM " + Constantes.TBL_ACTLOG+
                             " WHERE to_char(logdate,'DD/MM/YYYY')=to_char(sysdate,'DD/MM/YYYY')  AND userID = #{userID}";  

    //Obtiene cat�logo de promociones  
    String GET_PROMOCIONES="SELECT * FROM MTP_CATPROM_RTCR";
    
 /*   String INSERT_REGTCR="INSERT INTO MTP_REGTCR (num_cuenta, nombre_cliente, tel_fijo, tel_movil, email, promocion, fecha_reg," +
                                                "browser, ip, termtarj, banco, comentario,nom_tarhab) values (#{num_cuenta}, #{nombre_cliente}," +
                                                "#{tel_fijo}, #{tel_movil}, #{email}, #{promocion}, sysdate, #{browser}, #{ip}, #{termtarj}," +
                                                "#{banco}, #{comentario}, #{nom_tarhab})";
    */
    String GET_PROMOS="SELECT * FROM RTCR_CAT_PROMOCION";

    String INSERT_TARJETA="INSERT INTO RTCR_TARJETA(ID_TARJETA, ID_PROMOCION, NUM_TARJETA, NUM_TARJETA_MASCARA, FECHA_VENCIMIENTO, NOMBRE, APELLIDO_PATERNO, APELLIDO_MATERNO, CALLE, COLONIA, CODIGO_POSTAL, DELEGACION, ESTADO, PAIS) VALUES(#{id_tarjeta}, #{id_promocion}, #{num_tarjeta}, #{num_tarjeta_mascara}, TO_DATE(#{fecha_vencimiento},'dd/mm/yy'), #{nombre}, #{apellido_paterno}, #{apellido_materno}, #{calle}, #{colonia}, #{codigo_postal}, #{delegacion}, #{estado}, #{pais})";
    
    String INSERT_EVENTO="INSERT INTO RTCR_EVENTO(ID_EVENTO, ID_CANAL, ID_TARJETA, FECHA_EVENTO, MONTO, ESTATUS) VALUES(#{id_evento}, #{id_canal}, #{id_tarjeta}, sysdate, #{monto}, #{estatus})";
    
    String GET_LASTID_TARJETA="SELECT MAX(id_tarjeta) FROM RTCR_TARJETA";
    
    String GET_LASTID_EVENTO="SELECT MAX(id_evento) FROM RTCR_EVENTO";
        
    @Select(GET_ALL_TRANSACTIONS)
    public List<TransaccionDBO> getTransactions() throws Exception;
    
    @Select(GET_PAGOSHOY)
    public List<TransaccionDBO> getPagosHoy(String userID) throws Exception;
    
    @Insert(INSERT_TRANSACTIONS)
    @Options(useGeneratedKeys=true, keyProperty="act_log.id")
    public int insertTransact(TransaccionDBO t) throws Exception;
    
    @Select(GET_PROMOCIONES)
    public List<PromocionDBO> getPromotions() throws Exception;
    
        
   /* @Insert(INSERT_REGTCR)
    @Options(useGeneratedKeys=true, keyProperty="MTP_REGTCR.id")
    public int insertRegistroTCR(RegistroTCRDBO t) throws Exception;
   */
    @Insert(INSERT_TARJETA)
    public int insertTarjeta(TarjetaDBO t) throws Exception;
    
    @Insert(INSERT_EVENTO)
    public int insertEvento(EventoDBO t) throws Exception;
    
    @Select(GET_LASTID_TARJETA)
    public String lastIdTarjeta() throws Exception;
    
    @Select(GET_LASTID_EVENTO)
    public String lastIdEvento() throws Exception;
    
    
    @Select(GET_PROMOS)
    public List<PromotionDBO> getPromos() throws Exception;
    
}
