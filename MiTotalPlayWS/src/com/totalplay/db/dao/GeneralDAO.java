package com.totalplay.db.dao;


import com.totalplay.db.dbo.PromocionDBO;
import com.totalplay.db.dbo.PromotionDBO;
import com.totalplay.db.dbo.RegistroTCRDBO;
import com.totalplay.db.dbo.TransaccionDBO;
import com.totalplay.db.mybatis.ConnectionFactory;

import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class GeneralDAO {
    private static Logger logger = LogManager.getLogger(ConnectionFactory.class.getName());
    TransactBO transactDAO;
    SqlSessionFactory sesionUnica= new ConnectionFactory().getSession();
    public GeneralDAO(int opc){
        switch(opc){
        case 3: transactDAO= new TransactBO(sesionUnica);break;
        }
    }
    
    /**
     * M�todo que inserta en BD la informaci�n de las trasacciones realidzadas por el cliente
     * dichas transacciones son intentos de pago desde el portal
     * @param transact
     * @return
     */
    public boolean insertTransaction(TransaccionDBO transact){
        return  transactDAO.insertTransaction(transact);
    }
    
    /**
     * M�todo que devuelve los pagos realizados el d�a actual por el cliente identificado por su userID
     * en caso de que no existan pagos realizados regresa una lista vac�a
     * @param userID
     * @return lista de pagos realizados o lista vac�a 
     * @throws Exception
     */
    public List<TransaccionDBO> getPagosHoy(String userID) throws Exception {
        return transactDAO.getPagosHoy(userID);
    }
    
    /**
     * * M�todo que lista las promociones a las que un cliente puede hacerse acreedor por registrar su tarjeta para cargo recurrente.
     * @param 
     * @return Lista de objetos con la informaci�n de las promociones
     * @throws Exception
     */
    
    public List<PromocionDBO> getPromotions(){//m�todo usado con la primer version de la base de datos
        return transactDAO.getPromos();
    }
    
    public List<PromotionDBO> getPromociones(){
        return transactDAO.getPromociones();
    }
    
    

    /**
     * M�todo que inserta un registro de tarjeta para cargo recurrente.
     * @param registro (objeto de tipo RegistroTCRDBO con los datos del cliente y su registro de tarjeta CR)
     * @return boolean que indica si se insert� correctamente el registro
     */

    public boolean insertRegistroCompleto(RegistroTCRDBO registro){
        String id_tarjeta=transactDAO.getLastIDTarjeta();
        String id_evento= transactDAO.getLastIDEvento();
        boolean insertado= false;
        System.out.println("Id de Tarjeta a ser insertada: "+id_tarjeta);
        registro.getTarjeta().setId_tarjeta(id_tarjeta);
        registro.getEvento().setId_tarjeta(id_tarjeta);
        registro.getEvento().setId_evento(id_evento);
        //Se inserta tarjeta
        if(transactDAO.insertTarjeta(registro.getTarjeta())){
            if(!transactDAO.insertEvento(registro.getEvento())){
                logger.error("Ocurri� un error al insertar el registro de tarjeta completo");
            }else{
                insertado=true;
            }
        }
        
        
        return insertado;
    }

}
