package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import org.tempuri.SecurityUserModel;
import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.ProcessResponse;

import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BPELgetInfoBRM;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Bpelgetinfobrm_client_ep;

import com.totalplay.services.EngineSecurity;
import com.totalplay.services.GetInfoBRM;
import com.totalplay.utils.Constantes;
import com.totalplay.utils.FileProperty;
import com.totalplay.utils.VerificaUsuario;

import com.totalplay.vo.UsuarioVO;

import encrypt.DecryptJSON;

import java.net.URL;

import java.util.Map;

import javax.xml.namespace.QName;

public class newLogin extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.error("Estoy en nuevo servlet de login");
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.close();
        try{
            HttpSession sesion= request.getSession(false);
            String sessionJava=null;
                
            if(request.getParameter("secdat")!=null)
                sesion.setAttribute("secdat", request.getParameter("secdat"));
            
            String encriptado= (String)sesion.getAttribute("secdat");
            
            if(sesion != null){
                try{
                    String sessid = sesion.getId();
                    String usuario = null;
                    String pass = null;
                    //SecurityUserModel infoUser = null;
                    Map infoUser = null;
                    String numCuenta = null;
                    ProcessResponse.DatosCliente infoCliente = null;
                    
                    if(encriptado != null){
                        String [] datosUser = DecryptJSON.DecryptJSONString(encriptado);
                        usuario = datosUser[0];
                        pass = datosUser[1];
                    }/*else{
                        usuario = ((String)sesion.getAttribute("numCuenta") != null)?(String)sesion.getAttribute("numCuenta"):request.getParameter("numCuenta");
                        if(VerificaUsuario.isNumTelefono(usuario)){
                            infoCliente = GetInfoBRM.obtieneCuenta(usuario, 2);
                            usuario = infoCliente.getAccountNo();
                        }else{
                            logger.error("Error, isNumTelefono");
                        }
                        infoCliente = null;
                        SecurityUserModel info = EngineSecurity.getNoAccount(usuario);
                        pass = info.getPassword();
                    }*/
                    
                    UsuarioVO user = new UsuarioVO();
                    FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
                    String parametroCuenta = usuario;
                    int bandera = 0;
                    
                    if(VerificaUsuario.isNumCuenta(parametroCuenta)){
                        bandera = 0;
                    }else if(VerificaUsuario.isNumTelefono(parametroCuenta)){
                        bandera = 2;
                    }else if(VerificaUsuario.isEmail(parametroCuenta)){
                        bandera = 1;
                    }
                    
                    ProcessResponse.DatosCliente datos = GetInfoBRM.obtieneCuenta(parametroCuenta, bandera);
                    String cia = "";
                    com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM result = new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM();
                    
                    if(datos != null){
                        numCuenta = datos.getAccountNo();
                        logger.error("numero de cuenta obtenido: "+numCuenta);
                        String wsRuta = fileProperty.getProperty("DASHBOARD");
                        URL urlWS = new URL(wsRuta);
                        Bpelgetinfobrm_client_ep bpelgetinfobrm_client_ep = new Bpelgetinfobrm_client_ep(urlWS,new QName("http://xmlns.oracle.com/MiddlewareApplication/BrmSearchAccount/BPELgetInfoBRM",
                            "bpelgetinfobrm_client_ep"));
                        BPELgetInfoBRM bPELgetInfoBRM = bpelgetinfobrm_client_ep.getBPELgetInfoBRM_pt();
                        com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Process payload = new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Process();
                        payload.setACCOUNTNO(numCuenta);
                        
                        com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse respuestaWS = bPELgetInfoBRM.process(payload);
                        result = respuestaWS.getArrBRM();
                        cia = result.getCiaPerteneciente();
                    }
                    
                    if(VerificaUsuario.isNumCuentaVarios(usuario)){
                        if(cia.equalsIgnoreCase("totalplay")){
                            infoUser= EngineSecurity.consumeLoginWS(usuario,pass);
                        }
                    }
                    
                }catch (Exception e) {
                    sesion.setAttribute("tipoError", "1");
                    logger.error("Error: "+e.getMessage()+ " Causa: "+e.getCause());
                    
                    RequestDispatcher despachador =
                    getServletContext().getRequestDispatcher("/index.jsp");
                    despachador.include(request, response);
                }
            }else{
                RequestDispatcher despachador = getServletContext().getRequestDispatcher("/index.jsp");
                despachador.include(request, response);
            }
            //request.getRequestDispatcher("infoclienteser.html").forward(request, response);//************
        }catch(Exception e){
            logger.error("Error: "+e.getMessage()+ " Causa: "+e.getCause());
            e.printStackTrace();
            RequestDispatcher despachador = getServletContext().getRequestDispatcher("/index.jsp");
            despachador.include(request, response);
        }
    }
}
