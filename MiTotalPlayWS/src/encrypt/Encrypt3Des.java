package encrypt;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Encrypt3Des
{
  private static Logger logger = LogManager.getLogger(Encrypt3Des.class);

  public static String getEncrypt3des(String string)
  {
      String cipherTextB64 = "";
    try
    {
        String myKey = "facturas";
        byte[] plaintext = string != null ? string.getBytes("UTF8") : new byte[16];
      
   
      Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
      SecretKey shared_key = new SecretKeySpec(myKey.getBytes("UTF-8"), "DES");
      IvParameterSpec ivspec = new IvParameterSpec(new byte[] { 123, 29, 2, -98, -7, 15, 80, 92 });
      cipher.init(1, shared_key, ivspec);
      byte[] ciphertext = cipher.doFinal(plaintext);
      byte[] encoded = Base64.encodeBase64(ciphertext);
      cipherTextB64 = new String(encoded);
    }
    catch (Exception localException) {
        logger.error(localException.getMessage());
    }
    return cipherTextB64;
  }
}