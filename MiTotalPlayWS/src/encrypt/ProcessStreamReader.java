package encrypt;

import java.io.*;

/**
 * Reads stream buffers
 * @author <a href="wsgrah@wm.edu">Wayne Graham</a>
 * @author John Anderson, original code (http://lists.gnupg.org/pipermail/gnupg-devel/2002-February/018098.html)
 */
public class ProcessStreamReader extends Thread{
  private StringBuffer stream;
  private InputStreamReader in;
  private BufferedReader br;
  private final static int BUFFER_SIZE = 1024;
  
  /**
   * Constructor code to create a ProcessStreamReader object
   * @param in Input stream
   */
  ProcessStreamReader(InputStream in){
    super();
    this.in = new InputStreamReader(in);
    this.stream = new StringBuffer();
  }//ProcessStreamReader
  
  /**
   * Run the process and read stdOut
   */
  public void run(){
    try{
      int read;
      char[] c = new char[BUFFER_SIZE];
      
      while((read = in.read(c, 0, BUFFER_SIZE - 1)) > 0){
        stream.append(c, 0, read);
        if(read < BUFFER_SIZE - 1)
          break;
      }
    }catch(IOException io){io.printStackTrace();}
                    
                        
  }//run
  
  /**
   * Returns output stream information
   * @return Output stream
   */
  public String toString(){
    return stream.toString();
  }//toString
}//class ProcessStreamReader 
