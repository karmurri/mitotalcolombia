package encrypt;

import com.totalplay.utils.FileProperty;

public final class encriptaInterfactura
{
  public static String getLigaInterfactura(String cuenta, String mes, String anio){
    FileProperty fileProperty = new FileProperty("com.totalplay.prop.propNF");
    
    StringBuffer parameters = new StringBuffer();
    String url = "https://facturastpe.interfactura.com/?";
    String usr = "consulta";
    String pwd = fileProperty.getProperty("drowssap");
    //String pwd = "asdfg123";

    parameters.append("user");
    parameters.append("=");
    parameters.append(usr);
    parameters.append("&");
    parameters.append("password");
    parameters.append("=");
    parameters.append(pwd);
    parameters.append("&");
    parameters.append("key2");
    parameters.append("=");
    parameters.append(cuenta);
    parameters.append("&");
    parameters.append("key4");
    parameters.append("=");
    parameters.append(mes);
    parameters.append("&");
    parameters.append("key5");
    parameters.append("=");
    parameters.append(anio);
    try
    {
      String encriptado = Encrypt3Des.getEncrypt3des(parameters.toString());
      return url + encriptado;
    } catch (Exception localException) {
        localException.printStackTrace();
    }
    return "";
  }
  
  
  /*public static void main(String args[]){
      
      System.out.println( new encriptaInterfactura().getLigaInterfactura("1.5127525", "07", "2014"));
      /*String prueba="1234567891234567";
      System.out.println(prueba.substring(prueba.length()-4, prueba.length()));
  }*/
}