    <style> 
      body{ padding-top: .5em;}
      #imgheaderlogo{
            height: 25px;
            width: 29px;
            margin-top: -.6em;
        }

     
    .tpr-header-texto 
    {  
        font-family: 'Montserrat-semibold' !important;
        font-size: 1em;
        color:#3B4559;  
    } 
 
    .tpr-font 
    { 
        font-family: 'Montserrat-semibold' !important;
        font-size: 1em;
    } 
    #uno{
         margin-right: -6%; 
         margin-left:6%;   
    }
    #dos{
        margin-right: 2%; 
        margin-left: 2%;
    }
    #tres{
        margin-top: -5px;
        margin-right: 4%;
        font-family: 'Montserrat';
        font-size: 1.25em;
    }
    .fontmedium{
        font-family: 'Montserrat-medium' !important;
        color:   #3B4559;
    }
    .fontbold{
        font-family: 'Montserrat-bold' !important;
        color:   #3B4559;
    }
 
    </style> 
    <br>
    <div class="d-none d-lg-block main container cabecera-ini">
        <div class="row color-header">
            <div class="col-2 .col-auto" id="uno">
                <a class="" href="/"> 
                    <span class="font-weight-bold tpr-font tpr-header-texto" >RESIDENCIAL</span> 
                </a>
            </div>
            <div class="col-1 .col-auto" id="dos">
                <a class="" href="https://www.negociostotalplay.com.mx/"> 
                    <span class="font-weight-bold tpr-font tpr-header-texto" >NEGOCIOS</span> 
                </a>
            </div>
            <div class="col-1 .col-auto" >
                <a class="" href="https://www.totalplayempresarial.com.mx"> 
                    <span class="font-weight-bold tpr-font tpr-header-texto" >EMPRESARIAL</span>
                </a>
            </div>
            <div class="col" ></div>
            <div class="col-1 .col-auto">
                <a class="" href="https://www.totalplay.com.mx/ayuda"> 
                    <span class="font-weight-bold tpr-font tpr-header-texto" ><p>AYUDA</p></span> 
                </a>
            </div>
            <div class="col-3 .col-auto" id="tres">
                <a class="" href="<%=request.getContextPath()%>/" target="_blank"> 
                   <img id="imgheaderlogo" src="<%=request.getContextPath()%>/img/Menus/carrito_mini.png">
                    <span class="fontmedium" >Mi Cuenta <b class="fontbold">total</b></span> 
                </a>
            </div>
        </div>  
    </div>