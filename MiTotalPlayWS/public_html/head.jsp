<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
    <!--modales -->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/fancybox2/jquery.fancybox.css" media="screen" />
    
    <script src="<%=request.getContextPath()%>/js/jquery-3.3.1.min.js"></script>
    <script src="<%=request.getContextPath()%>/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/estadoCta.css">
    <title>Mi Cuenta Totalplay</title>
    <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/icn/favicon.ico"/>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/Login.css">  
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/estylosCJ.css">
    
    <script type="text/javascript" src="<%=request.getContextPath()%>/fancybox2/jquery.fancybox.js"></script>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/footer.css">
    
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/eventos.js"></script>
    
    <!-- Etiqueta global de sitio (gtag.js) de Google Analytics begind -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-7618852-11');
    </script>
    <!-- Etiqueta global de sitio (gtag.js) de Google Analytics end -->
    <!-- Cxense script begin -->
    <script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['setSiteId', '1132899846499742441']);//test: 1132899846499742441 prod: 1131772398957904842
        cX.callQueue.push(['cint', '200']);
        cX.callQueue.push(['sendPageViewEvent']);
        // one time initialization for all sendEvent calls
        cX.callQueue.push(['setEventAttributes', 
        { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
        </script>
        <script type="text/javascript">
        (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
        e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
        t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
    </script>        
    <!-- Cxense script end --> 
</head>