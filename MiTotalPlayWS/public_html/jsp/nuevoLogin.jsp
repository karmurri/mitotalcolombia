<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="es">
    <jsp:include page="/head.jsp"/>
    <body style="">
        <jsp:include page="/header.jsp"/>
        <jsp:include page="/jsp/menu.jsp"/>
        <div id="tablet" class="">  
            <div id="login">
                <div class="container" style="margin-top:-6em;">
                    <div id="login-row" class="row justify-content-center align-items-center">
                        <div id="login-column" class="col-lg-7">
                            <div id="login-box" class="col-md-12">
                                <form action="<%=request.getContextPath()%>/loginser.html" method="POST" id="loginform" autocomplete="off" >
                                <!--form action="<%=request.getContextPath()%>/newlogin." method="POST" id="loginform" autocomplete="off" -->
                                <input type="hidden" id="secdat" name="secdat" value="" />
                                    <!--h3 class="text-center text-info pb-4"-->
                                    <div class="form-group contienelogo">
                                        <img src="<%=request.getContextPath()%>/img/Menus/Logo_mini.png" id="logo">  
                                    </div>
                                    <div class="form-group pt-4">
                                        <label for="iUser" class="text-label">Ingresa tu e-mail, número de cuenta o teléfono Totalplay a 10 dígitos:</label><br>
                                        <input type="text" name="iUser" id="iUser" class="form-control" placeholder="E-mail / no cuenta / Teléfono">
                                    </div>
                                    <div class="form-group">
                                        <label for="iPwd" class="text-label">Ingresa tu contraseña:</label><br>
                                        <input type="password" id="iPwd" name="iPwd" class="form-control" placeholder="Contraseña" autocomplete="off">
                                    </div><br>
                                    <div class="form-group col-sm-12 col-md-8 col-lg-6" style="text-align:center; max-width:100%;">
                                        <input type="submit" id="createlogin" class="btn btn-info btn-lg font-weight-bold" value="Entrar">
                                    </div>
                                    <div id="register-link" class="text-center">
                                        <a href="<%=request.getContextPath()%>/jsp/remember.jsp" id="remember" class="text-info">¿Olvidaste tu Contraseña?</a>
                                        
                                    </div><br>
                                    <div class="text-center">
                                        <label>
                                            <span style="color:#3A4559;">¿Aun no tienes Cuenta?</span><br>
                                            <span ><a href="<%=request.getContextPath()%>/jsp/create.jsp" id="create" class="text-center font-azul">Crear Cuenta</a></span>
                                        </label><br>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
        <div class="col-lg-12">
            <div class="row pie">
                <div class="col-lg-1 col-sm-1"></div>
                
                <div class="col-lg-3 col-sm-10 py-3 px-2 pieitem">
                    <img src="/Mitotalco/img/Menus/package_mini.png"><span style="color:#3B4559;">&nbsp; &nbsp;Conoce que incluye tu paquete</span>
                </div>
               
                <div class="col-lg-3 col-sm-10 py-3 px-2 pieitem">
                    <img src="/Mitotalco/img/Menus/coin_mini.png"><span style="color:#3B4559;">&nbsp; &nbsp; Consulta y paga tu saldo</span>
                </div>
                
                <div class="col-lg-4 col-sm-10 py-3 px-2 pieitem">
                    <img src="/Mitotalco/img/Menus/file_mini.png;"><span style="color:#3B4559;">Verificar el detalle de tu estado de cuenta</span>
                </div>
                
                 <div class="col-lg-1 col-sm-1"></div>
                 
            </div>
        </div>   
      
        
        <jsp:include page="/jsp/footer.jsp"/>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/settings.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/aes.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/pbkdf2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/AesUtil.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/nuevoLogin.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/fancybox2/jquery.fancybox.js"></script>
        
        <script type="text/javascript">
            /******************************************* Inicio click entrar ********************************************************/
            $('#createlogin').on('click',function(){
                $(this).trigger('iniciaSesion');
            });
            
            $('#createlogin').bind('iniciaSesion',function(){
                var Cx = window.cX;
                Cx.callQueue.push(['sendEvent','iniciarSesionMTP']);
            });
        </script>
    </body>
</html>
