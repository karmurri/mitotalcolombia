<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="com.totalplay.vo.UsuarioVO"%>

<html>
<head>
         <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-7618852-11');
        </script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Mi Cuenta Totalplay</title>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/icn/favicon.ico">
        <link rel="apple-touch-icon" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-114x114.png">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/alert.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/Login.css" media="screen" />
         
         <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui-1.9.0.custom.min.js" ></script>
      
        <script type="text/javascript" src="<%=request.getContextPath()%>/fancybox2/jquery.fancybox.js"></script>
        
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
        <script type="text/javascript"  src="<%=request.getContextPath()%>/js/settings.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/core2.js"></script>

        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
        <!-- Cxense script begin >
        <script type="text/javascript">
            var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
            cX.callQueue.push(['cint', '200']);
            cX.callQueue.push(['sendPageViewEvent']);
            // one time initialization for all sendEvent calls
            cX.callQueue.push(['setEventAttributes', 
            { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
        </script>
        <script type="text/javascript">
            (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
            e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
            t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
        </script>        
        <!-- Cxense script end >
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/eventos.js"></script-->
       
</head>
<body>
        <div id="content_pop">
            <h3 >Recupera tu contrase&ntilde;a</h3><br>
            <div >
                Para recuperar tu contrase&ntilde;a, deber&aacute;s de proporcionar uno de los siguientes datos:
                <ul class="ulmodal">
                        <li> tu n&uacute;mero de cuenta</li>
                        <li> tu n&uacute;mero telef&oacute;nico de Totalplay (a 10 d&iacute;gitos)</li>
                </ul>
            
                Con esta informaci&oacute;n te enviaremos al correo electr&oacute;nico que registraste tu contrase&ntilde;a temporal con la que podr&aacute;s acceder a tu cuenta.<br /><br />
                <span style="color:#fd0606;">Te recomendamos cambiar tu contrase&ntilde;a temporal desde la opci&oacute;n &#34;Administrar mi contrase&ntilde;a&#34;.</span><br /><br />
                
                <form method="post"  autocomplete="off" id="formRemember" style="height:112px !important;">
                    <label for="username">N&uacute;mero de cuenta o tel&eacute;fono a 10 d&iacute;gitos: </label>
                    <input type="text" class="form-control"  id="username" name="username" value="" 
                    placeholder="No. de cuenta o tel&eacute;fono." />
                    <!--br /><br/-->
                    <div class="row py-4">
                        <div class="col-lg-4 col-sm-1"></div>
                        <div class="col-lg-4 col-sm-10">
                            <input type="button" id="rememberBtn" class="btn text-center" value="Recuperar Contraseņa"></input>		
                        </div>
                        <div class="col-lg-4 col-sm-1"></div>
                    </div>
                </form>
            </div>
        </div>	
        
         <div class="modal fade" id="myModalLoading">
            <div class="modal-dialog">
                 <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Espere un momento por favor...</h4>
                    </div>
                    <div class="modal-body" style="text-align:center;">
                       <img src="<%=request.getContextPath()%>/assets/loading.gif" alt=""/>
                    </div>
                    <div class="modal-footer">
                    </div>
                 </div>
            </div>
        </div>
        
         <div class="modal fade" id="myModalMsj">
           <div class="modal-dialog">
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="msjTitle"></h4>
               </div>
                <div class="modal-body" id="msjBody"></div>
                <div class="modal-footer" id="msjFooter"></div>
             </div>
           </div>
         </div>
       
</body>
</html>