<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="com.totalplay.vo.HistoricoFacturaVO"%>
<%@ page import="com.totalplay.vo.FacturaVO"%>
<%@ page import="java.util.*"%>

<%@ page contentType="text/html;charset=windows-1252"%>
<html>

    <%
        HttpSession login = request.getSession(false);
        int sizeListFact = (login.getAttribute("sizeListFact")!=null)?(Integer)login.getAttribute("sizeListFact"):0;
        HistoricoFacturaVO facturasVO = (login.getAttribute("facturas")!=null)?(HistoricoFacturaVO)login.getAttribute("facturas"):new HistoricoFacturaVO();
    %>

  <head>
    <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-7618852-11');
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
    <title>facturasVista</title>
    <!-- Cxense script begin -->
    <script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
        cX.callQueue.push(['cint', '200']);
        cX.callQueue.push(['sendPageViewEvent']);
        // one time initialization for all sendEvent calls
        cX.callQueue.push(['setEventAttributes', 
        { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
        </script>
        <script type="text/javascript">
        (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
        e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
        t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
    </script>        
    <!-- Cxense script end -->    
  </head>
  <body>
     <div>
                            <table style="width:100%; margin-left: auto; margin-right: auto;">
                                <% if( sizeListFact > 0 ){ %>
                                <!--<logic:greaterThan name="sizeListFact" value="0">-->
                                    <tr>
                                            <td style="text-align:left;" colspan="3">Aqu&iacute; podr&aacute;s consultar el detalle de tus consumos seleccionando el estado de cuenta y consultar el archivo en PDF</td>
                                    </tr>
                                    <tr class="non">
                                            <td style="padding-left:40px; text-align:left;"><strong>Mes</strong></td>
                                            <td style="padding-left:40px;"><strong>Monto</strong></td>
                                            <td style="padding-left:40px;"><strong>Link de consulta</strong></td>
                                    </tr>
                                    <%
                                        /*Iterator it = facturasVO.getHistoricoFac().iterator(); 
                                        while( it.hasNext() ){
                                        FacturaVO objFac = (FacturaVO)it.next();
                                        */
                                        List<FacturaVO> objFac = facturasVO.getHistoricoFac();
                                        ArrayList<FacturaVO> lista= new ArrayList<FacturaVO>(objFac);
                                        
                                        for(int iFac=0;iFac<=sizeListFact;iFac++){
                                    %>
                                    <!--<logic:iterate name="facturas" property="historicoFac" id="facturaVista">-->
                                            <tr>
                                                <td style="padding-left:40px; text-align:left; font-size:12px;">
                                                <!--<bean:write name="facturaVista" property="fechaPago"/>-->
                                                <%= lista.get(iFac).getFechaPago().toString() %></td>
                                                <td style="padding-left:40px; text-align:left; font-size:12px;"> 
                                                <!--<bean:write name="facturaVista" property="monto"/>-->
                                                <%= lista.get(iFac).getMonto().toString() %></td>
                                                <td style="padding-left:40px; text-align:left; font-size:12px; color:#F00 !important;">
                                                <!--<a href="<bean:write name="facturaVista" property="liga"/>" target="_blank">Consultar  estado de cuenta-->
                                                <a href="<%=lista.get(iFac).getLiga().toString()%>" target="_blank">Consultar  estado de cuenta
                                                <img src="<%=request.getContextPath()%>/images/pdf.png" alt="" title="" /></a></td>
                                            </tr>				
                                    <!--</logic:iterate>-->
                                    <% 
                                      } %>
                                <!--</logic:greaterThan>-->
                                <% } %>
                           
                                
                                <% if( sizeListFact == 0 ){ %>
                                <!--<logic:equal name="sizeListFact" value="0">-->
                                    <tr class="non"><td>No tiene  estados de cuenta registrados</td></tr> 
                                <!--</logic:equal>-->
                                <% } %>
                                
                                
                            </table>
                        </div>
  </body>
</html>