<div class="container-fluid">
    <div class="row">
        <div id="footer" class="footer p-3"><br>
            <p>Para una mejor experiencia y visualizaci&oacute;n de la p&aacute;gina te recomendamos utilizar los siguientes navegadores: 
            <br>Internet Explorer 11, Microsoft edge 25, Google Chrome 48, Firefox 44, Opera 35.
            <br>Todos los derechos reservados TOTALPLAY Telecomunicaciones S.A. de C.V. 2014
            </p>
            
            <p>
                <u><a href="https://www.totalplay.com/">www.totalplay.com.mx</a> </u>&nbsp; &nbsp;
                <a href="https://www.totalplay.com.mx/pdf/aviso-privacidad.pdf"> Aviso de Privacidad</a>
            </p>
        </div>
    </div>
</div>