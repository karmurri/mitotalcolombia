<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>

<%@ page import="com.totalplay.vo.UsuarioVO"%>
<%@ page import="com.totalplay.vo.TarjetaVO"%>

<%@ page import="com.totalplay.ws.vo.ReferenceBankVO"%>
<%@ page import="java.util.List"%> 
<%@ page import="java.util.ArrayList"%> 
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.totalplay.utils.VerificaUsuario"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="com.totalplay.vo.DireccionVO"%>
<%@ page import="com.totalplay.db.dbo.PromotionDBO"%>
<%@page import="org.owasp.encoder.Encode"%>

  <%

            HttpSession login = request.getSession(false);
            String error="";
            UsuarioVO usuarioVo =(login.getAttribute("usuario") != null)?(UsuarioVO)login.getAttribute("usuario"):null;
            List<TarjetaVO> listTarjetas= login.getAttribute("tarjetas")!=null?(List<TarjetaVO>)login.getAttribute("tarjetas"):new ArrayList<TarjetaVO>();
            List<ReferenceBankVO> referenciasBank =login.getAttribute("referencias")!=null?(List<ReferenceBankVO>)login.getAttribute("referencias"): new ArrayList<ReferenceBankVO>();
            DireccionVO direccion= (DireccionVO)login.getAttribute("direccion");
            String bandera=(login.getAttribute("bandera") != null)?(String)login.getAttribute("bandera"):null;
            //BinVO binesVO = login.getAttribute("binesVO") != null?(BinVO)login.getAttribute("binesVO"):null;
            List<PromotionDBO> promociones= request.getAttribute("promociones")!=null?(List<PromotionDBO>)request.getAttribute("promociones"):new ArrayList<PromotionDBO>();
            
             if(usuarioVo == null){
                request.getRequestDispatcher("home.jsp").forward(request, response);
                
            }
                String AuthBancaria=  (String)login.getAttribute("AuthBancaria"); 
                String errorType=null;
                if(login.getAttribute("errorType")!=null){
                    errorType=(String)login.getAttribute("errorType"); 
                }
                String sendEmail=null;
                if(login.getAttribute("sendEmail")!=null){
                    sendEmail=(String)login.getAttribute("sendEmail"); 
                }
                
            String sessionOtt= (String)login.getAttribute("ottID");
            String sessid= login.getId(); 
            String sessionJava=null;
            if(sessionOtt!=null && !sessionOtt.isEmpty())
                sessionJava=sessionOtt;
            else{
                sessionJava= sessid.substring(0, 25);
           }
           String tipoMoneda= (String)login.getAttribute("tipoMoneda")!=null ?(String)login.getAttribute("tipoMoneda"):"484";
           
%>
<html>
  <head>
    <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-7618852-11');
    </script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>pagosVista</title>

    <script type="text/javascript">
        function getContextPath() {
            return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
            //return "";
    }
         var interval;
             function EnmascaraV2(CampoMask,CampoHidd,bolDes){
                var iniAnt, mskCar;
                var tempBull = "1999";
                var objCMask = $('#'+CampoMask);
                var objCHidd = $('#'+CampoHidd);
                var tempValIni = "";
                var tempValFin = "";
                var LenghtMsk = objCMask.val().length;
                var CaulBol = bolDes ? LenghtMsk : (LenghtMsk - 1);
                var tamMask = objCMask.attr("maxlength")>4?((objCMask.attr("maxlength"))-4):2000000;
                for (x=0;x<LenghtMsk;x++){
                    mskCar = objCMask.val().charAt(x);
                    iniAnt = objCHidd.val().charAt(x);
                    if (mskCar!=unescape('%u0FBF')){
                        tempValIni+=mskCar;
                        if (x<tamMask && x != CaulBol)
                            tempValFin += unescape('%u0FBF');
                        else
                            tempValFin += mskCar;
                        } else {
                            tempValIni+=iniAnt;
                            if (iniAnt!="") {
                                if (x<tamMask)
                                tempValFin += unescape('%u0FBF');
                                else
                                tempValFin += iniAnt;
                            }
                        }
                    }
                objCHidd.val(tempValIni);
                objCMask.val("");
                objCMask.val(tempValFin);
                eval('det_' + CampoMask + ' = setTimeout(\'EnmascaraV2("' + CampoMask +'","' + CampoHidd + '",true);\',' + tempBull + ');');
               	eval('clearTimeout(det_' + CampoMask + ');');
                //console.log('Enmascara!');
            }
            function enmascara(){
                setTimeout(EnmascaraV2('cCardNumberhdn','cCardNumber',true),10);
            }
            
           /* function enmascara2(){
                EnmascaraV2('cCardNumberhdn1','cCardNumber2',true);
            }*/
            function enmascara5(){
                setTimeout(EnmascaraV2('cCardNumberhdn2','cCardNumber2',true),10);
            }
            function enmascara3(){
                setTimeout(EnmascaraV2('cCardNumberhdn3','cCardNumber3',true),10);
            }
            
            function enmascara4(){
                setTimeout(EnmascaraV2('cCardNumberhdn4','cCardNumber4',true),10);
            }
            function enmascaraCVV(){
                setTimeout(EnmascaraV2('cCardSec2','cCardSecReg',true),10);
            }
            function enmascaraCVV2(){
                setTimeout(EnmascaraV2('cCardSec','cCardSecPay',true),10);
            }
            function noTeclasEspec(e){
                status = false;
                t = (document.all)?e.keyCode:e.which;
                if((t==86&&e.ctrlKey)||(t==67&&e.ctrlKey)||(t==88&&e.ctrlKey)){
                        status = false;
                }else{
                    status = numberOnly(e.which);
                }
                //console.log(status);
                return status;
            }
            
            $(document).ready(function () {   
            
             muestraLogos();
                var tipMoneda= '<%=tipoMoneda%>';
                
                 if(tipMoneda!='484'){
                    console.log('Oculta formas de pago: '+tipMoneda);
                    document.getElementById("pagoUnico").style.display = "none";
                    document.getElementById("regTDC").style.display = "none";
                    document.getElementById("pagoTarjeta").style.display = "none";
                    document.getElementById("regTarjeta").style.display = "none";
                    document.getElementById("tarjReg").style.display = "none";
                    document.getElementById("hrPagos").style.display = "none";
                    document.getElementById("hrTarjetas").style.display = "none";    
                    document.getElementById("hrInicial").style.display = "none"; 
                 }
                
                ocultaCampos();
                
                $('#cCardSec').keyup(function (e) { 
                    var status=false;
                    if(!e.ctrlKey) {
                        status = numberOnly(e.which);
                    } else { 
                        status=  false; 
                    }
                    return status; 
                }); 
                  $('#cCardCP').keyup(function (e) { 
                    var status=false;
                    if(!e.ctrlKey) {
                        status = numberOnly(e.which);
                    } else { 
                        status=  false; 
                    }
                    return status; 
                }); 
                $('#cCardNumberhdn').keyup(function (e) { 
                    var status=true;
                    if(!e.ctrlKey && !e.shiftKey && !e.altKey) {
                        status = numberOnly2(e.which);
                        if(status)
                            enmascara();
                    } else { 
                            status=  false; 
                    }
                    return status; 
                });
                
                /*$('#cCardNumberhdn1').keydown(function (e) { 
                    var status=false;
                    if(!e.ctrlKey && !e.shiftKey && !e.altKey) {
                        status = numberOnly2(e.which);
                        if(status)
                            interval = setInterval(enmascara2, 10);
                    } else { 
                            status=  false; 
                    }
                    return status; 
                });*/ 
                $('#cCardNumberhdn2').keyup(function (e) { 
                    var status=false;
                    if(!e.ctrlKey && !e.shiftKey && !e.altKey) {
                        status = numberOnly2(e.which);
                        if(status)
                            enmascara5();
                    } else { 
                            status=  false; 
                    }
                    return status; 
                });
                $('#cCardNumberhdn3').keyup(function (e) { 
                    var status=false;
                    if(!e.ctrlKey && !e.shiftKey && !e.altKey) {
                        status = numberOnly2(e.which);
                        if(status)
                            enmascara3();
                    } else { 
                            status=  false; 
                    }
                    return status; 
                }); 
                $('#cCardNumberhdn4').keyup(function (e) { 
                    var status=false;
                    if(!e.ctrlKey && !e.shiftKey && !e.altKey) {
                        status = numberOnly2(e.which);
                        if(status)
                            enmascara4();
                    } else { 
                            status=  false; 
                    }
                    return status; 
                }); 
                
                
                $('#cCardSec2').keyup(function (e) { 
                    var status=false;
                    if(!e.ctrlKey && !e.shiftKey && !e.altKey) {
                        status = numberOnly2(e.which);
                        if(status)
                            enmascaraCVV();
                    } else { 
                            status=  false; 
                    }
                    return status; 
                }); 
                $('#cCardSec').keyup(function (e) { 
                    var status=false;
                    if(!e.ctrlKey && !e.shiftKey && !e.altKey) {
                        status = numberOnly2(e.which);
                        if(status)
                           enmascaraCVV2();
                    } else { 
                            status=  false; 
                    }
                    return status; 
                }); 
                
                $('#SaveCard').on('click', function() {
                    if( $(this).is(':checked') ){
                        muestraCampos();
                    }else{
                        $('#tel').val(" ");
                        $('#cel').val(" ");
                        $('#correo').val(" ");
                        $('#promocion').val(" ");
                        ocultaCampos();
                    }
                });
                
                function ocultaCampos(){
                    document.getElementById("oculto1").style.display = "none";
                    document.getElementById("oculto2").style.display = "none";
                    document.getElementById("oculto3").style.display = "none";
                    document.getElementById("oculto4").style.display = "none";                    
                }
                function muestraCampos(){
                    document.getElementById("oculto1").style.display = "table-row";
                    document.getElementById("oculto2").style.display = "table-row";
                    document.getElementById("oculto3").style.display = "table-row";
                    document.getElementById("oculto4").style.display = "table-row";                    
                }
                
                mensaje('<%=AuthBancaria%>', '<%=errorType%>', '<%=sendEmail%>', '<%=usuarioVo.getEmail()%>', '<%=usuarioVo.getName()%>');
                $("#regTarjeta").click( function(){
                //console.log("Muestra registro de tdc");
                //$('#regTDC').show();
                document.getElementById("regTDC").style.display = "inline";
                document.getElementById("pagoUnico").style.display = "none";
                });

                $("#pagoTarjeta").click( function(){
                    //console.log("Muestra pago tdc");
                    //$('#regTDC').hide();
                    document.getElementById("pagoUnico").style.display = "inline";
                    document.getElementById("regTDC").style.display = "none";
                });
                $.validaTarjeta2=function(field1,field2){
                    if($('#'+field1).val()==$('#'+field2).val()){
                        $("#MensajeAlert").html('');
                        $("#paymentModal").modal("hide");
                    } else {
                        $("#MensajeAlert").html('El número de la tarjeta ingresado en la confirmación no es igual.');
                        $("#paymentModal").modal("show");
                    }
                };
            });
       $.validaTarjetaR=function() {
              var binAux = $('#cCardNumber3').val();
              var bin = binAux.substring(0, 6);
            $.ajax({
               cache:false,
               async:true,
               type: "POST",
               dataType: "json",
               contentType: "application/x-www-form-urlencoded",
               url:'/binser.html',
               data: {
                    binParam : bin                       
               },
               success:llegadaDatos2,
               error: problemas2,
               timeout:500000
             });
             function llegadaDatos2(datos){
                 $("#tipopago2").html(datos.nombreMarca);
                 $("#cTipoPago2").val(datos.nombreMarca);
                 $("#tipotarjeta2").html(datos.tipoTarjeta);
                 $("#acepta2").html(datos.cargosAutomaticos);
                $("#cCardBankReg").val(datos.nombreBanco);
                $("#Ctype02").val(datos.nombreMarca);
                $("#cTipoTarjeta2").val(datos.tipoTarjeta);
                $("#cCargoRecurrent2").val(datos.cargosAutomaticos);
                $("#cCardBankId2").val(datos.idBanco);
                if(datos.cargosAutomaticos!="SI"){
                alert('Inserta otra tarjeta que acepte cargos recurrentes');
                    /*$("#MensajeAlert").html('Inserta otra tarjeta.');
                    $("#paymentModal").modal("show");*/
                }
            }
            function problemas2(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log("Error: "+thrownError);
                alert('Ingresa otra tarjeta');
/*                $("#MensajeAlert").html('Inserta otra tarjeta.');
                $("#paymentModal").modal("show");*/

            }     
        }
             
            
            function numberOnly(valor){
                val=false;
                
                switch(valor){
                
                    case 8:	//backspace
                    case 45:	// insert
                    case 46:	// delete
                    case 0:
                        val=true;
                        break;
                    default:
                        if (valor >= 48 && valor <= 57) {		// numbers
                            val=true;
                        }  else if (valor >= 97 && valor <= 122) {
                           // val=true;
                        }
                }
                return val;
            }
            function numberOnly2(valor){
                val=false;
//                console.log(valor);
                switch(valor){
                    case 186:
                    case 180:	// acute
                    case 225:	// a acute
                    case 233:	// e acute
                    case 237:	// i acute
                    case 243:	// o acute
                    case 250:	// u acute
                    case 241:	// n tilde
                    case 193:	// A acute
                    case 201:	// E acute
                    case 205:	// I acute
                    case 211:	// O acute
                    case 218:	// U acute
                    case 209:	// N tilde
                        val=false;
                        break;
                    case 8:	//backspace
                    case 9:	//tab
                    case 45:	// insert
                    case 46:	// delete
                    case 0:
                        val=true;
                        break;
                    default:
                        if (valor >= 48 && valor <= 57) {		// numbers
                            val=true;
                        }  else if (valor >= 96 && valor <= 122) {
                            val=true;
                        }break;
                }
               
                return val;
            }
            
            $("#descargaPDF").click( function(){
                 $.descargaFile();            
             });
             
            
             
    </script>  
    <!-- Cxense script begin -->
    <script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
        cX.callQueue.push(['cint', '200']);
        cX.callQueue.push(['sendPageViewEvent']);
        // one time initialization for all sendEvent calls
        cX.callQueue.push(['setEventAttributes', 
        { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
        </script>
        <script type="text/javascript">
        (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
        e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
        t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
    </script>        
    <!-- Cxense script end -->     
  </head>
  <body>
  
     <a name="pagosa" id="pagos"></a>
        <div id="ss">
            Estimado cliente: 
            <br>
            En fechas recientes detectamos que usuarios en redes sociales se ofrecen a pagar tu factura Totalplay a cambio de un descuento. 
            <br>
            Te informamos que Totalplay no ofrece este tipo de promociones. Si te llegan a contactar puede tratarse de fraude. 
            <br>
            <a href="http://www.totalplay.com.mx/web/manuales/donde-pagar-totalplay.pdf" target="_blank"><u><i>Da clic y conoce las formas de pago autorizadas.</i></u></a>
        </div>
        <br/>
        <p style="padding:8px; ">Podr&aacute;s realizar el pago de tu  estado de cuenta con cargo por &uacute;nica ocasi&oacute;n a tarjeta de cr&eacute;dito o d&eacute;bito, es decir, sin domiciliar tu tarjeta.
        <br /><br />
        Si es la primera vez que vas a pagar con tu tarjeta, debes seleccionar la opci&oacute;n &#8220;<a href="#titulouno" title="Registrar nueva tarjeta" style="color:#bd2026 !important;">Pagar con nueva tarjeta</a>&#8221;.<br /><br />
        Si anteriormente ya hab&iacute;as pagado en la l&iacute;nea <strong>TOTALPLAY</strong> te recomendamos ir a la secci&oacute;n de &#8220;<a href="#titulodos" title="Mis tarjetas registradas" style="color:#bd2026 !important;">Mis tarjetas registradas</a>&#8221; para revisar si tu tarjeta ya se encuentra registrada.
        <br /><br />  
        Si deseas realizar tu pago en alguna Instituci&oacute;n Bancaria puedes consultar 
        &#8220;<a href="#titulotres" title="Pagar con referencia bancaria" style="color:#bd2026 !important;">Pagar con referencia bancaria</a>&#8221;.
        <br /><br />    
         <%-- Si deseas registrar tu tarjeta para cargo recurrente debes seleccionar la opci&oacute;n <strong>&#8220;Registrar Tarjeta para Cargo recurrente&#8221;</strong> o comunicarte a la l&iacute;nea Totalplay al <span style="color:#bd2026 !important;"><b>1579&nbsp;8000</b> y <b>01 800 510 0510</b>.</span> --%>
        Si deseas registrar tu tarjeta para cargo recurrente debes comunicarte a la l&iacute;nea Totalplay al <span style="color:#bd2026 !important;"><b>1579&nbsp;8000</b> y <b>01 800 510 0510</b>.</span>
        <br/><br/>
        <span style="color:#bd2026 !important;">Horario de atenci&oacute;n: 7:00 am a 11:00 pm de lunes a domingo y un asesor te atender&aacute;.</span></p>

<br/><br/>
<div style="text-align:center">
    <table align="center" style="width:90%;">
      <tr>
        <td><input type="button" value="PAGAR CON NUEVA TARJETA" id="pagoTarjeta" /></td>
        <td width="35px">&nbsp;</td>
        <!--<td><input type="button" value="REGISTRAR TARJETA PARA CARGO RECURRENTE" id="regTarjeta" /></td>-->
      </tr>
    </table>    </div>
    <br/>
<br><hr id="hrInicial"><br> 
<div id="pagoUnico" style="display:inline; width:90%">
<h2 style="padding-left:5px;">Pagar con nueva tarjeta</h2><a name="titulouno"></a>
    <div>
    	<br /><br/>
		 <form id="paymentForm" method="post" action="<%=request.getContextPath()%>/paynewcardser.html" autocomplete="off" accept-charset="UTF-8" >
                        <input type="hidden" id="cCardNumberEnc" name="cCardNumberEnc" value="" />
			<input type="hidden" id="cCardNumber" name="cCardNumber" value=""/>
			<input type="hidden" id="cCardNumber2" name="cCardNumber2" value=""/>
                        <input type="hidden" name="cCardBankId" id="cCardBankId"/>
                        <input type="hidden" id="cCardBank" name="cCardBank" value=""/>
                        <input type="hidden" name="Ctype" id="Ctype0" value="" />
                        <input type="hidden" id="cTipoTarjeta" name="cTipoTarjeta" value=""  />
                        <input type="hidden" name="cCargoRecurrent" id="cCargoRecurrent" value="" />
                         <input type="hidden" name="cCardSec" id="cCardSecPay" value="" />

 			<table>
				<tr><td style="width:38%"><label for="cCardname0">Nombre(s) del tarjetahabiente: </label> </td><td style="width:30%"><input type="text" name="cCardname" id="cCardname0" value="" maxlength="25" /></td>
					<td rowspan="5" style="padding:0px !important;">
                                            <input type="hidden" id="Mastercard" name="Ctype" value="Mastercard" />
                                        </td>
				</tr>
				<tr><td style="width:35%"><label for="cCardLastname0">Apellido paterno del tarjetahabiente: </label> </td><td style="width:30%"><input type="text" name="cCardLastname" id="cCardLastname0" value="" maxlength="25" /></td></tr>
                                <tr><td style="width:35%"><label for="cCardLastname1">Apellido materno del tarjetahabiente: </label> </td><td style="width:30%"><input type="text" name="cCardLastname1" id="cCardLastname1" value="" maxlength="25" /></td></tr>
				<tr><td><label for="cCardNumberhdn">N&uacute;mero de la tarjeta de cr&eacute;dito/d&eacute;bito: </label> </td><td><input type="text" name="cCardNumberhdn" id="cCardNumberhdn" value="" maxlength="16"/></td></tr>
                                <tr><td><label for="cCardNumberhdn2">Confirmaci&oacute;n de n&uacute;mero de la tarjeta de cr&eacute;dito/d&eacute;bito: </label> </td><td><input type="text" name="cCardNumberhdn2" id="cCardNumberhdn2" value="" maxlength="16" onblur="$.validaTarjeta()" /></td></tr>
                                
				<tr><td><label for="cCardSec">N&uacute;mero de seguridad: </label> </td><td><input type="text" name="cCardSecTemp" id="cCardSec" value="" size="5" maxlength="4" style="width:20%;" /></td></tr>
				<tr><td><label for="expMonth">Fecha de vencimiento: </label> </td><td>

					<select name="expMonth" id="expMonth">
						<option value="">-</option>
							<% for(int i=1; i<=12; i++){
							%><option value="<%=String.format("%02d",i)%>"><%=String.format("%02d",i)%></option>
							<% }%>
						</select>
                                                <% SimpleDateFormat formato = new SimpleDateFormat("yy");
    
                                                 String year= formato.format(new Date());
                                                 int yearMax=Integer.parseInt(year)+9;
                                                 %>
						<select name="expYear">
							<option value="">-</option>
							<% for(int i=Integer.parseInt(year); i<=yearMax;i++ ){ %>
							<option value="<%=i%>"><%=i%></option>
							<%}%>
						</select>
				<br /></td></tr>
				<tr><td><label for="expMonth2">Confirmaci&oacute;n de fecha de vencimiento: </label> </td><td>
					<select name="expMonth2" id="expMonth2">
						<option value="">-</option>
							<% for(int i=1; i<=12; i++){
							%><option value="<%=String.format("%02d",i)%>"><%=String.format("%02d",i)%></option>
							<% }%>
						</select>
                                                <% SimpleDateFormat formato2 = new SimpleDateFormat("yy");
    
                                                 String year2= formato2.format(new Date());
                                                 int yearMax2=Integer.parseInt(year2)+9;
                                                 %>
						<select name="expYear2">

							<option value="">-</option>
                                                                                                         
							<% for(int i=Integer.parseInt(year2); i<=yearMax2;i++ ){ %>
							<option value="<%=i%>"><%=i%></option>
							<%}%>
						</select>
				<br /></td></tr>
				<%-- <tr><td><label for="cCardBank">Banco: </label> </td><td><select name="cCardBank" id="cCardBank">
						<option value="">-</option>
						
									<option>AMERICAN EXPRESS</option>
									<option>BANAMEX</option>
									<option>BANCA MIFEL</option>
									<option>BANCEN</option>
									<option>BANCO ABC CAPITAL</option>
									<option>BANCO AFIRME</option>
									<option>BANCO AZTECA</option>
									<option>BANCO DEL BAJIO</option>
									<option>BANCO INBURSA</option>
									<option>BANCO INDUSTRIAL</option>
									<option>BANCO INTERACCIONES</option>
									<option>BANCO MULTIVA</option>
									<option>BANCOMEXT</option>
									<option>BANCOPPEL</option>
									<option>BANCRECER</option>
									<option>BANJERCITO</option>
									<option>BANOBRAS</option>
									<option>BANORTE</option>
									<option>BANPAIS</option>
									<option>BANREGIO</option>
									<option>BANRURAL</option>
									<option>BANSI</option>
									<option>BANXICO</option>
									<option>BBVA BANCOMER</option>
									<option>BOSTON</option>
									<option>CITIBANK</option>
									<option>CONFIA</option>
									<option>DRESDNER</option>
									<option>EDENRED</option>
									<option>ELEKTRA</option>
									<option>FMM CONSUMO S.A</option>
									<option>FONACOT</option>
									<option>HSBC</option>
									<option>IXE</option>
									<option>LIVERPOOL</option>
									<option>NAFINSA</option>
									<option>PALACIO DE HIERRO</option>
									<option>SANTANDER</option>
									<option>SCOTIA BANK INVERLAT</option>
									<option>SPIRA</option>
									<option>SURESTE</option>
				
				</select></td><td>&nbsp;</td></tr> --%>
                                
                                <tr>
                                    <td colspan="6">
                                        <span style="display:inline-block !important; width:15%;">Tipo de pago: </span> 
                                        <span id="tipopago" style="display:inline-block !important; width:10%;">&nbsp;</span> 
                                        <span style="display:inline-block !important; width:20%;">Tipo de tarjeta: </span> 
                                        <span id="tipotarjeta" style="display:inline-block !important; width:10%;">&nbsp;</span> 
                                        <span style="display:inline-block !important; width:25%;">Acepta cargos autom&aacute;ticos: </span> 
                                        <span id="acepta" style="display:inline-block !important; width:10%;">&nbsp;</span> 
                                    </td>
                                </tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="2"><label><i>Domicilio donde recibes tus estados de cuenta</i></label></td><td rowspan="5">&nbsp;</td></tr>
				
				<tr><td><label for="cCardAdd">Calle y n&uacute;mero: </label> </td><td><input type="text" name="cCardAdd" id="cCardAdd" value="" size="4" /></td></tr>
				<tr><td><label for="cCardCol">Colonia: </label> </td><td><input type="text" name="cCardCol" id="cCardCol" value="" size="4"  /></td></tr>
				<tr><td><label for="cCardCP">C&oacute;digo Postal: </label> </td><td><input type="text" name="cCardCP" id="cCardCP" value="" size="4" maxlength="5" /></td></tr>
				<tr><td><label for="cCardDel">Delegaci&oacute;n o Municipio: </label> </td><td><input type="text" name="cCardDel" id="cCardDel" value="" size="4" /></td></tr>
				<tr><td><label for="cCardEdo">Estado: </label> </td><td colspan="2">
				<select name="cCardEdo" id="cCardEdo">
					<option value="AG">Aguascalientes</option>
                                        <option value="BC">Baja California</option>
                                        <option value="BS">Baja California Sur</option>
                                        <option value="CM">Campeche</option>
                                        <option value="CS">Chiapas</option>
                                        <option value="CH">Chihuahua</option>
                                        <option value="CO">Coahuila de Zaragoza</option>
                                        <option value="CL">Colima</option>
                                        <option value="DF"  selected="selected">Distrito Federal</option>
                                        <option value="DG">Durango</option>
                                        <option value="GT">Guanajuato</option>
                                        <option value="GR">Guerrero</option>
                                        <option value="HG">Hidalgo</option>
                                        <option value="JC">Jalisco</option>
                                        <option value="ME">Mexico</option>
                                        <option value="MN">Michoac&aacute;n de Ocampo</option>
                                        <option value="MS">Morelos</option>
                                        <option value="NT">Nayarit</option>
                                        <option value="NL">Nuevo Le&oacute;n</option>
                                        <option value="OC">Oaxaca</option>
                                        <option value="PL">Puebla</option>
                                        <option value="QO">Quer&eacute;taro de Arteaga</option>
                                        <option value="QR">Quintana Roo</option>
                                        <option value="SP">San Luis Potos&iacute;</option>
                                        <option value="SL">Sinaloa</option>
                                        <option value="SR">Sonora</option>
                                        <option value="TC">Tabasco</option>
                                        <option value="TS">Tamaulipas</option>
                                        <option value="TL">Tlaxcala</option>
                                        <option value="VZ">Veracruz</option>
                                        <option value="YN">Yucat&aacute;n</option>
                                        <option value="ZS">Zacatecas</option>
				</select></td></tr>
				<tr><td><label for="cCardCtr">Pa&iacute;s: </label> </td><td  colspan="2">
				<select name="cCardCtr" id="cCardCtr" >
					<option value="AF">Afganist&aacute;n</option>
					<option value="AL">Albania</option>
					<option value="DE">Alemania</option>
					<option value="AD">Andorra</option>
					<option value="AO">Angola</option>
					<option value="AI">Anguilla</option>
					<option value="AQ">Ant&aacute;rtida</option>
					<option value="AG">Antigua y Barbuda</option>
					<option value="AN">Antillas Holandesas</option>
					<option value="SA">Arabia Saud&iacute;</option>
					<option value="DZ">Argelia</option>
					<option value="AR">Argentina</option>
					<option value="AM">Armenia</option>
					<option value="AW">Aruba</option>
					<option value="AU">Australia</option>
					<option value="AT">Austria</option>
					<option value="AZ">Azerbaiy&aacute;n</option>
					<option value="BS">Bahamas</option>
					<option value="BH">Bahrein</option>
					<option value="BD">Bangladesh</option>
					<option value="BB">Barbados</option>
					<option value="BE">B&eacute;lgica</option>
					<option value="BZ">Belice</option>
					<option value="BJ">Benin</option>
					<option value="BM">Bermudas</option>
					<option value="BY">Bielorrusia</option>
					<option value="MM">Birmania</option>
					<option value="BO">Bolivia</option>
					<option value="BA">Bosnia y Herzegovina</option>
					<option value="BW">Botswana</option>
					<option value="BR">Brasil</option>
					<option value="BN">Brunei</option>
					<option value="BG">Bulgaria</option>
					<option value="BF">Burkina Faso</option>
					<option value="BI">Burundi</option>
					<option value="BT">But&aacute;n</option>
					<option value="CV">Cabo Verde</option>
					<option value="KH">Camboya</option>
					<option value="CM">Camer&uacute;n</option>
					<option value="CA">Canad&aacute;</option>
					<option value="TD">Chad</option>
					<option value="CL">Chile</option>
					<option value="CN">China</option>
					<option value="CY">Chipre</option>
					<option value="VA">Ciudad del Vaticano (Santa Sede)</option>
					<option value="CO">Colombia</option>
					<option value="KM">Comores</option>
					<option value="CG">Congo</option>
					<option value="CD">Congo, Rep&uacute;blica Democr&aacute;tica del</option>
					<option value="KR">Corea</option>
					<option value="KP">Corea del Norte</option>
					<option value="CI">Costa de Marf&iacute;l</option>
					<option value="CR">Costa Rica</option>
					<option value="HR">Croacia (Hrvatska)</option>
					<option value="CU">Cuba</option>
					<option value="DK">Dinamarca</option>
					<option value="DJ">Djibouti</option>
					<option value="DM">Dominica</option>
					<option value="EC">Ecuador</option>
					<option value="EG">Egipto</option>
					<option value="SV">El Salvador</option>
					<option value="AE">Emiratos &aacute;rabes Unidos</option>
					<option value="ER">Eritrea</option>
					<option value="SI">Eslovenia</option>
					<option value="ES">Espa&ntilde;a</option>
					<option value="US">Estados Unidos</option>
					<option value="EE">Estonia</option>
					<option value="ET">Etiop&iacute;a</option>
					<option value="FJ">Fiji</option>
					<option value="PH">Filipinas</option>
					<option value="FI">Finlandia</option>
					<option value="FR">Francia</option>
					<option value="GA">Gab&oacute;n</option>
					<option value="GM">Gambia</option>
					<option value="GE">Georgia</option>
					<option value="GH">Ghana</option>
					<option value="GI">Gibraltar</option>
					<option value="GD">Granada</option>
					<option value="GR">Grecia</option>
					<option value="GL">Groenlandia</option>
					<option value="GP">Guadalupe</option>
					<option value="GU">Guam</option>
					<option value="GT">Guatemala</option>
					<option value="GY">Guayana</option>
					<option value="GF">Guayana Francesa</option>
					<option value="GN">Guinea</option>
					<option value="GQ">Guinea Ecuatorial</option>
					<option value="GW">Guinea-Bissau</option>
					<option value="HT">Hait&iacute;</option>
					<option value="HN">Honduras</option>
					<option value="HU">Hungr&iacute;a</option>
					<option value="IN">India</option>
					<option value="ID">Indonesia</option>
					<option value="IQ">Irak</option>
					<option value="IR">Ir&aacute;n</option>
					<option value="IE">Irlanda</option>
					<option value="BV">Isla Bouvet</option>
					<option value="CX">Isla de Christmas</option>
					<option value="IS">Islandia</option>
					<option value="KY">Islas Caim&aacute;n</option>
					<option value="CK">Islas Cook</option>
					<option value="CC">Islas de Cocos o Keeling</option>
					<option value="FO">Islas Faroe</option>
					<option value="HM">Islas Heard y McDonald</option>
					<option value="FK">Islas Malvinas</option>
					<option value="MP">Islas Marianas del Norte</option>
					<option value="MH">Islas Marshall</option>
					<option value="UM">Islas menores de Estados Unidos</option>
					<option value="PW">Islas Palau</option>
					<option value="SB">Islas Salom&oacute;n</option>
					<option value="SJ">Islas Svalbard y Jan Mayen</option>
					<option value="TK">Islas Tokelau</option>
					<option value="TC">Islas Turks y Caicos</option>
					<option value="VI">Islas V&iacute;rgenes (EE.UU.)</option>
					<option value="VG">Islas V&iacute;rgenes (Reino Unido)</option>
					<option value="WF">Islas Wallis y Futuna</option>
					<option value="IL">Israel</option>
					<option value="IT">Italia</option>
					<option value="JM">Jamaica</option>
					<option value="JP">Jap&oacute;n</option>
					<option value="JO">Jordania</option>
					<option value="KZ">Kazajist&aacute;n</option>
					<option value="KE">Kenia</option>
					<option value="KG">Kirguizist&aacute;n</option>
					<option value="KI">Kiribati</option>
					<option value="KW">Kuwait</option>
					<option value="LA">Laos</option>
					<option value="LS">Lesotho</option>
					<option value="LV">Letonia</option>
					<option value="LB">L&iacute;bano</option>
					<option value="LR">Liberia</option>
					<option value="LY">Libia</option>
					<option value="LI">Liechtenstein</option>
					<option value="LT">Lituania</option>
					<option value="LU">Luxemburgo</option>
					<option value="MK">Macedonia</option>
					<option value="MG">Madagascar</option>
					<option value="MY">Malasia</option>
					<option value="MW">Malawi</option>
					<option value="MV">Maldivas</option>
					<option value="ML">Mal&iacute;</option>
					<option value="MT">Malta</option>
					<option value="MA">Marruecos</option>
					<option value="MQ">Martinica</option>
					<option value="MU">Mauricio</option>
					<option value="MR">Mauritania</option>
					<option value="YT">Mayotte</option>
					<option value="MX" selected="selected">M&eacute;xico</option>
					<option value="FM">Micronesia</option>
					<option value="MD">Moldavia</option>
					<option value="MC">M&oacute;naco</option>
					<option value="MN">Mongolia</option>
					<option value="MS">Montserrat</option>
					<option value="MZ">Mozambique</option>
					<option value="NA">Namibia</option>
					<option value="NR">Nauru</option>
					<option value="NP">Nepal</option>
					<option value="NI">Nicaragua</option>
					<option value="NE">N&iacute;ger</option>
					<option value="NG">Nigeria</option>
					<option value="NU">Niue</option>
					<option value="NF">Norfolk</option>
					<option value="NO">Noruega</option>
					<option value="NC">Nueva Caledonia</option>
					<option value="NZ">Nueva Zelanda</option>
					<option value="OM">Om&aacute;n</option>
					<option value="NL">Pa&iacute;ses Bajos</option>
					<option value="PA">Panam&aacute;</option>
					<option value="PG">Pap&uacute;a Nueva Guinea</option>
					<option value="PK">Paquist&aacute;n</option>
					<option value="PY">Paraguay</option>
					<option value="PE">Per&uacute;</option>
					<option value="PN">Pitcairn</option>
					<option value="PF">Polinesia Francesa</option>
					<option value="PL">Polonia</option>
					<option value="PT">Portugal</option>
					<option value="PR">Puerto Rico</option>
					<option value="QA">Qatar</option>
					<option value="UK">Reino Unido</option>
					<option value="CF">Rep&uacute;blica Centroafricana</option>
					<option value="CZ">Rep&uacute;blica Checa</option>
					<option value="ZA">Rep&uacute;blica de Sud&aacute;frica</option>
					<option value="DO">Rep&uacute;blica Dominicana</option>
					<option value="SK">Rep&uacute;blica Eslovaca</option>
					<option value="RE">Reuni&oacute;n</option>
					<option value="RW">Ruanda</option>
					<option value="RO">Rumania</option>
					<option value="RU">Rusia</option>
					<option value="EH">Sahara Occidental</option>
					<option value="KN">Saint Kitts y Nevis</option>
					<option value="WS">Samoa</option>
					<option value="AS">Samoa Americana</option>
					<option value="SM">San Marino</option>
					<option value="VC">San Vicente y Granadinas</option>
					<option value="SH">Santa Helena</option>
					<option value="LC">Santa Luc&iacute;a</option>
					<option value="ST">Santo Tom&eacute; y Pr&iacute;ncipe</option>
					<option value="SN">Senegal</option>
					<option value="SC">Seychelles</option>
					<option value="SL">Sierra Leona</option>
					<option value="SG">Singapur</option>
					<option value="SY">Siria</option>
					<option value="SO">Somalia</option>
					<option value="LK">Sri Lanka</option>
					<option value="PM">St. Pierre y Miquelon</option>
					<option value="SZ">Suazilandia</option>
					<option value="SD">Sud&aacute;n</option>
					<option value="SE">Suecia</option>
					<option value="CH">Suiza</option>
					<option value="SR">Surinam</option>
					<option value="TH">Tailandia</option>
					<option value="TW">Taiw&aacute;n</option>
					<option value="TZ">Tanzania</option>
					<option value="TJ">Tayikist&aacute;n</option>
					<option value="TF">Territorios franceses del Sur</option>
					<option value="TP">Timor Oriental</option>
					<option value="TG">Togo</option>
					<option value="TO">Tonga</option>
					<option value="TT">Trinidad y Tobago</option>
					<option value="TN">T&uacute;nez</option>
					<option value="TM">Turkmenist&aacute;n</option>
					<option value="TR">Turqu&iacute;a</option>
					<option value="TV">Tuvalu</option>
					<option value="UA">Ucrania</option>
					<option value="UG">Uganda</option>
					<option value="UY">Uruguay</option>
					<option value="UZ">Uzbekist&aacute;n</option>
					<option value="VU">Vanuatu</option>
					<option value="VE">Venezuela</option>
					<option value="VN">Vietnam</option>
					<option value="YE">Yemen</option>
					<option value="YU">Yugoslavia</option>
					<option value="ZM">Zambia</option>
					<option value="ZW">Zimbabue</option>
				</select>
				</td></tr>
				<tr><td><label for="amm">Monto a pagar: </label> </td><td colspan="2"><input type="text" name="amm" id="amm" value="" /></td></tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
       				<tr><td colspan="6" style="text-align:center;">Todos los datos son obligatorios</td></tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
            			<!--<tr><td colspan="3"><input type="checkbox" name="SaveCard" id="SaveCard" onclick="$(this).attr('value', this.checked ? 1 : 0)" value="0" style="float:left;"><label for="SaveCard" class="mini" style="margin:5px;">Quiero registrar mi tarjeta para cargo recurrente <br/></label></input></td></tr>-->

                                    <tr id="oculto1"><td ><label for="tel">Tel&eacute;fono fijo:</label></td><td><input type="text" name="tel" id="tel"/> </td><td><label for="extension">Extensi&oacute;n: </label><input type="text" name="extension"/></td></tr>
                                    <tr id="oculto2"><td ><label for="cel">Tel&eacute;fono m&oacute;vil:</label></td><td><input type="text" name="cel" id="cel"/> </td><td rowspan="2">&nbsp;</td></tr>
                                    <tr id="oculto3"><td ><label for="correo">Correo electr&oacute;nico:</label></td><td><input type="text" name="correo" id="correo"/> </td><td rowspan="2">&nbsp;</td></tr>
                                    <tr id="oculto4"><td ><label for="promocion">Promoci&oacute;n:</label></td>                                    
                                    <td>
                                        <select name="promocion" id="promocion">  
                                                <option value="">Selecciona una opci&oacute;n</option>
                                            <%for(PromotionDBO promo: promociones){%>
                                                    <option value="<%= Encode.forHtmlAttribute(promo.getId_promocion()) %>"><%= Encode.forHtml(promo.getPromocion())%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                    <td rowspan="2">&nbsp;</td></tr>
                                    
                                <tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="6" style="text-align:center;"><input type="button" value="ACEPTAR" 
                                onclick="<% 
                                boolean pagoHoy= VerificaUsuario.verificaPagoHoy(usuarioVo.getEmployeenumber());
                                    if(pagoHoy){
                                        out.print("if(confirm('El d&iacute;a de hoy ya realizaste un pago.\\n Deseas continuar?'))");    
                                    }                                
                                %>
                                $.SubmitForm('paymentForm')" id="PagoNuevaTDC" /></td></tr>
			</table>
		</form>
            </div>
</div>

<div id="regTDC" style="display:none; width:90%">
<h2 style="padding-left:5px;">Registrar Tarjeta cr&eacute;dito o d&eacute;bito para Cargo Recurrente</h2><a name="titulounoa"></a>
    <br/><br/>
    <div>
    <p style="font-weight: bold;">
     <span style="display:inline-block !important; width:25%;">N&uacute;mero de cuenta:</span> <%=usuarioVo.getEmployeenumber()%><br/>
     <span style="display:inline-block !important; width:25%;">DN Totalplay:</span> <%=usuarioVo.getTelephone()%>
    	<br /><br/>
     <span style="display:inline-block !important; width:45%;"><strong>Datos del cliente</strong></span><br/>
     <span style="display:inline-block !important; width:25%;">Nombre:</span>    <%=usuarioVo.getName()+" "+usuarioVo.getLastname()%><br/>
     <%if(direccion.getBilling_street_address()!=null && !direccion.getBilling_street_address().equals(" ")){%><span style="display:inline-block !important; width:25%;">Direcci&oacute;n:</span> <%=direccion.getBilling_street_address()+" #"+direccion.getBilling_xtnum()+" Col."+direccion.getBilling_suburb()+", "+direccion.getBilling_city()+" C.P. "+direccion.getBilling_postcode()%><%}%>
     </p><br />
		 <form id="regTdcForm" method="post" autocomplete="off">
                        <input type="hidden" id="cCardNumberEnc3" name="cCardNumberEnc" value="" />
                        <input type="hidden" id="cCardNumberEnc4" name="cCardNumberEnc2" value="" />
			<input type="hidden" id="cCardNumber3" name="cCardNumber" value=""/>
			<input type="hidden" id="cCardNumber4" name="cCardNumber2" value=""/>
                        <input type="hidden"  name="Ctype" id="Ctype02" value="" />
                        <input type="hidden" name="cCardLastname" id="cCardLastname0" />
                        <input type="hidden" name="cCardCtr" id="cCardCtr" value="MX"/>
                        <input type="hidden" name="cCardBankId" id="cCardBankId2" value=""/>
                        <input type="hidden" name="cTipoPago" id="cTipoPago2" value="" />
                        <input type="hidden" name="cTipoTarjeta" id="cTipoTarjeta2" value="" />
                        <input type="hidden" name="cCargoRecurrent" id="cCargoRecurrent2" value="" />
                        <input type="hidden" name="cCardSec" id="cCardSecReg" value="" />
                        <input type="hidden" id="cCardBankReg" name="cCardBank" value=""/>
                        
                        <span style="display:inline-block !important; width:45%;"><strong>Datos del tarjetahabiente:</strong></span><br/>
			<table>
				<tr><td style="width:38%"><label for="cCardname0">Nombre(s) del tarjetahabiente: </label> </td><td style="width:30%"><input type="text" name="cCardname" id="cCardname0" value="" maxlength="25" /></td></tr>
				<tr><td style="width:35%"><label for="cApellidoPat0">Apellido paterno del tarjetahabiente: </label> </td><td style="width:30%"><input type="text" name="cApellidoPat" id="cApellidoPatId" value="" maxlength="25" /></td></tr>
                                <tr><td style="width:35%"><label for="cApellidoMat0">Apellido materno del tarjetahabiente: </label> </td><td style="width:30%"><input type="text" name="cApellidoMat" id="cApellidoMatId" value="" maxlength="25" /></td></tr>
                                <tr><td><label for="telFijo">Tel&eacute;fono fijo: </label> </td><td><input type="text" name="telFijo" id="telFijo" value="" maxlength="12" /></td></tr>
                                <tr><td><label for="extension">Extensi&oacute;n: </label> </td><td><input type="text" name="extension" id="extension" value="" maxlength="8" /></td></tr>                                
                                <tr><td><label for="celular">Tel&eacute;fono m&oacute;vil: </label> </td><td><input type="text" name="celular" id="celular" value="" maxlength="10" /></td></tr>                                
                                <tr><td><label for="email">Correo electr&oacute;nico: </label> </td><td><input type="text" name="email" id="email" value="" maxlength="50" /></td></tr>                                
                                <tr><td><label for="cCardSec">N&uacute;mero de seguridad: </label> </td><td><input type="text" name="cCardSecMask" id="cCardSec2" value="" size="4" maxlength="4" /></td></tr>

				<tr><td><label for="cCardNumber3">N&uacute;mero de la tarjeta de cr&eacute;dito/d&eacute;bito: </label> </td><td><input type="text" name="cCardNumberhdn" id="cCardNumberhdn3" value="" maxlength="16" onblur="$.validaTarjetaR()" /></td></tr>
                                <tr><td><label for="cCardNumber4">Confirmaci&oacute;n de n&uacute;mero de la tarjeta de cr&eacute;dito/d&eacute;bito: </label> </td><td><input type="text" name="cCardNumberhdn1" id="cCardNumberhdn4" value="" maxlength="16" /></td></tr>
                                
				<tr><td><label for="expMonth">Fecha de vencimiento: </label> </td><td>
					<select name="expMonth" id="expMonth">
						<option value="">-</option>
							<% for(int i=1; i<=12; i++){
							%><option value="<%=String.format("%02d",i)%> "><%=String.format("%02d",i)%> </option>
							<% }%>
						</select>
                                                
                                                <% SimpleDateFormat formato3 = new SimpleDateFormat("yy");
    
                                                 String year3= formato3.format(new Date());
                                                 int yearMax3=Integer.parseInt(year3)+9;
                                                 %>
						<select name="expYear" id="expYear">
							<option value="">-</option>
							<% for(int i=Integer.parseInt(year3); i<=yearMax3;i++ ){ %>
							<option value="<%=i%>"><%=i%></option>
							<%}%>
						</select>
				<br /></td></tr>
                                <tr><td><label for="expMonth">Confirmación fecha de vencimiento: </label> </td><td>
					<select name="expMonth2" id="expMonth2">
						<option value="">-</option>
							<% for(int i=1; i<=12; i++){
							%><option value="<%=String.format("%02d",i)%> "><%=String.format("%02d",i)%> </option>
							<% }%>
						</select>
                                                
                                                <% SimpleDateFormat formato1 = new SimpleDateFormat("yy");
    
                                                 String year1= formato1.format(new Date());
                                                 int yearMax1=Integer.parseInt(year1)+9;
                                                 %>
						<select name="expYear2" id="expYear2">
							<option value="">-</option>
							<% for(int i=Integer.parseInt(year1); i<=yearMax1;i++ ){ %>
							<option value="<%=i%>"><%=i%></option>
							<%}%>
						</select>
				<br /></td></tr>

				
                                <tr>
                                    <td colspan="2" style="font-weight:bold">
                                        <span style="display:inline-block !important; width:15%;">Tipo de pago: </span> 
                                        <span id="tipopago2" style="display:inline-block !important; width:15%;">&nbsp;</span> 
                                        <span style="display:inline-block !important; width:20%;">Tipo de tarjeta: </span> 
                                        <span id="tipotarjeta2" style="display:inline-block !important; width:10%;">&nbsp;</span> 
                                        <span style="display:inline-block !important; width:25%;">Acepta cargos autom&aacute;ticos: </span> 
                                        <span id="acepta2" style="display:inline-block !important; width:10%;">&nbsp;</span> 
                                    </td>
                                </tr>
                                
				
				
				<tr><td><label for="cCardAdd">Direcci&oacute;n: </label> </td><td><input type="text" name="cCardAdd" id="cCardAdd" value="" /></td></tr>
				<tr><td><label for="cCardCol">Ciudad: </label> </td><td><input type="text" name="cCardCol" id="cCardCol" value="" /></td></tr>
				<tr><td><label for="cCardEdo">Estado: </label> </td><td colspan="2">
				<select name="cCardEdo" id="cCardEdo">
					<option value="AG">Aguascalientes</option>
                                        <option value="BC">Baja California</option>
                                        <option value="BS">Baja California Sur</option>
                                        <option value="CM">Campeche</option>
                                        <option value="CS">Chiapas</option>
                                        <option value="CH">Chihuahua</option>
                                        <option value="CO">Coahuila de Zaragoza</option>
                                        <option value="CL">Colima</option>
                                        <option value="DF"  selected="selected">Distrito Federal</option>
                                        <option value="DG">Durango</option>
                                        <option value="GT">Guanajuato</option>
                                        <option value="GR">Guerrero</option>
                                        <option value="HG">Hidalgo</option>
                                        <option value="JC">Jalisco</option>
                                        <option value="ME">Mexico</option>
                                        <option value="MN">Michoac&aacute;n de Ocampo</option>
                                        <option value="MS">Morelos</option>
                                        <option value="NT">Nayarit</option>
                                        <option value="NL">Nuevo Le&oacute;n</option>
                                        <option value="OC">Oaxaca</option>
                                        <option value="PL">Puebla</option>
                                        <option value="QO">Quer&eacute;taro de Arteaga</option>
                                        <option value="QR">Quintana Roo</option>
                                        <option value="SP">San Luis Potos&iacute;</option>
                                        <option value="SL">Sinaloa</option>
                                        <option value="SR">Sonora</option>
                                        <option value="TC">Tabasco</option>
                                        <option value="TS">Tamaulipas</option>
                                        <option value="TL">Tlaxcala</option>
                                        <option value="VZ">Veracruz</option>
                                        <option value="YN">Yucat&aacute;n</option>
                                        <option value="ZS">Zacatecas</option>
				</select></td></tr>
                                <tr><td><label for="cCardCP">C&oacute;digo Postal: </label> </td><td><input type="text" name="cCardCP" id="cCardCP" value="" size="6" maxlength="5" /></td></tr>
                                <tr><td><label for="promocion">Promoci&oacute;n:</label></td>
                                <td>
                                    <select name="promocion">  
                                        <option value="">Selecciona una opci&oacute;n</option>
                                         <%for(PromotionDBO promo: promociones){
                                           %>
                                                <option value="<%=Encode.forHtmlAttribute(promo.getId_promocion())%>"><%=Encode.forHtml(promo.getPromocion())%></option>
                                        <% 
                                        }%>
                                    </select>
                                </td>
                                <td rowspan="2">&nbsp;</td></tr>
                                
                                <tr><td><label for="amm">&nbsp;</label> </td><td colspan="2"><input type="hidden" name="amm" id="amm2" value="0.01" readonly="readonly" /></td></tr>
				<tr><td colspan="2" style="text-align:center;">Todos los datos son obligatorios</td></tr>
				<tr><td colspan="3" style="text-align:center;"><input type="button" value="REGISTRAR TARJETA" onclick="$.SubmitForm('regTdcForm','<%=usuarioVo.getName()+" "+usuarioVo.getLastname()%>')" id="GuardarTDC" /></td></tr>
			</table>
		</form>
                </div>
<br><hr id="hrPagos"><br>   
     
</div>

<div id="tarjReg">
<h2 style="padding-left:5px;">Mis tarjetas registradas</h2><a name="titulodos"></a>

    	<div>
		<form id="paymentForm0" method="post" autocomplete="off" action=<%=request.getContextPath()%>/payextcardser.html >
		
                        <input type="hidden" name="idCardList" id="idCardList" value="" />
			<input type="hidden" name="sec" id="sec" value="" />
              
		<table class="tarjetas">
                    <%
                        if(listTarjetas!=null && !listTarjetas.isEmpty() && listTarjetas.size()!=0){
                    %>
				<tr ><td colspan="6">Selecciona la tarjeta de credito con la que deseas realizar el pago</td></tr>
                                <tr><td colspan="6"></td></tr>
				<tr style="border-bottom:2px dotted #C8D31F;"><td colspan="2" style="width:45px;">&nbsp;</td><td>No. tarjeta de cr&eacute;dito/d&eacute;bito</td><td>Nombre del tarjetahabiente</td><td>Fecha de vencimiento</td><td>No. de seguridad</td></tr>
                            <% int ContTarjeta=1; 
                            for(TarjetaVO tarjeta: listTarjetas){ %>
				<tr style="font-size:12px;">
					<td class="clean"><input type="radio" style="required" name="CcardOption" id="CcardOption<%=ContTarjeta%>" value="<%=ContTarjeta%>" onclick="$.UpdateForm(<%=ContTarjeta%>,'<%=tarjeta.getFullName()%>','<%=tarjeta.getEndingCardNumber().substring(1)%>','<%=tarjeta.getCode()%>','<%=tarjeta.getBank()%>')" /></td>
					<td class="clean"><span title="<%=tarjeta.getBank()%>">&nbsp;</span></td>
                                        <td class="clean"><%="XXXX XXXX XXXX "+tarjeta.getEndingCardNumber().substring(1)%></td>
					<td class="clean"><%= tarjeta.getFullName().toUpperCase()%></td>
					<td class="clean">
						<select name="Month_<%=ContTarjeta%>">
                                                    <% for(int a=1; a<=12; a++) {
                                                        String b= String.format("%1$" + 2 + "s", a).replace(" ", "0");
                                                    %>
							<option value="<%=b%>"><%=b%></option>
                                                    <%}%>
						</select>
						<select name="Year_<%=ContTarjeta%>">
                                                    <% for(int a=0; a<8; a++) {
                                                            Calendar c1= Calendar.getInstance();
                                                            int FullYear=c1.get(Calendar.YEAR)+a;
                                                            int MinYear= (c1.get(Calendar.YEAR)-2000)+a;
                                                            String b= String.format("%1$" + 2 + "s", MinYear+"").replace(" ", "0");
                                                    %>
                      					<option value="<%=MinYear%>"><%=b%></option>
                                                    <%}%>
						</select>
              
					</td>
					<td class="clean"><input type="password" name="CCV_<%=ContTarjeta%>" size="4" maxlength="4" title=""/></td>
				</tr>
                                <%
                                ContTarjeta++;
                                }%>
          
				<tr><td colspan="6"><label for="amm0">Monto a pagar: </label>&nbsp;&nbsp;&nbsp;<input type="text" name="amm" id="amm0" value="" /></td></tr>
				<tr><td colspan="6" style="text-align:center;">
					<input type="hidden" name="ammdbt" id="ammdbt0" value="" />
					<input type="button" value="REALIZAR PAGO" id="btnRealizaPago" onclick="<%
                                            boolean pagoTCEHoy= false;
                                            pagoTCEHoy= VerificaUsuario.verificaPagoHoy(usuarioVo.getEmployeenumber());
                                            if(pagoTCEHoy){
                                                out.print("if(confirm('El d&iacute;a de hoy ya realizaste un pago.\\n Deseas continuar?'))");    
                                            }                                
                                        %>$.SubmitForm('paymentForm0')" />
				</td></tr>
                    <%}else { %>
                        <tr class="non"><td>No tienes tarjetas registradas</td></tr>
                    <%}%>              
                </table>
	</form>
    </div>
<br><hr id="hrTarjetas"><br> 
</div>
        <div class="modal fade" id="paymentModal">
            <div class="modal-dialog">
                 <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body" style="text-align:center;">
                       <div id="MensajeAlert">Espere un momento por favor...</div>
                    </div>
                    <div class="modal-footer">
                    </div>
                 </div>
            </div>
        </div>
    <div style="padding:10px; font-size:1.0em; clear:both;">   
        <div style="margin-left: auto; margin-right: auto; height:290px;">
     <%if(referenciasBank!=null && referenciasBank.size()>0 && referenciasBank.get(1).getReferencia()!=null &&!referenciasBank.get(1).getReferencia().isEmpty() && !referenciasBank.get(1).getBanco().isEmpty() && referenciasBank.get(0)!=null){ %>   
            <h2 style="padding-left:5px;">Pagar con Referencia Bancaria</h2><a name="titulotres"></a>
            <p>Podr&aacute;s pagar tambi&eacute;n en cualquiera de los siguientes bancos con la referencia bancaria correspondiente:</p>
                <div class="modulo" style="float:left; margin-top:15px; width:575px;">
                    <div class="referencia" style="border: none !important;width:450px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Referencia bancaria</div>
                                
                    <table id="ReferenciasBancarias" style="width:575px;">
                            <tr><th style="width:200px;">Banco</th><th colspan="2" style="text-align:center;">Referencia bancaria</th></tr>
                            <% for(ReferenceBankVO ref: referenciasBank){
                                    if(ref.getReferencia()!=null ){
                                        if(!ref.getBanco().equals(referenciasBank.get(0).getBanco())){
                            %>
                                            <tr><td><%=ref.getBanco()%></td><td class="monto" colspan="2"><%= ref.getReferencia()%></td></tr>
                            <%         } else{
                                            if(!ref.getReferencia().trim().equals("")){%>
                                                <tr><td colspan="2" style="width:200px;white-space:nowrap;"><%=ref.getBanco()%></td><td class="monto"><%=ref.getReferencia()%></td></tr>
                                <%          }
                                        }
                                    }   
                                }%>
                            
                    </table>
            <div class="separator" style="display:none"></div>
            </div>
         <%}else{%>
            <table id="ReferenciasBancarias" style="width:575px;float:left;">
            </table>
        <%}%>  
            <div class="modulo2" style="float:left; margin-top:15px; border:#ccc 1px solid; left:20px; position: relative;">
		<div class="opcionespago  ui-corner-all ui-corner-all" style="border: none !important;">Otras opciones de pago</div>
		<table id="opcionespago">
			<tr><td>
			<br />
                        <p>Si realizas el pago en Banco Azteca, Mi cuenta Totalplay, v&iacute;a telef&oacute;nica, tu pago se ver&aacute; reflejado <strong>inmediatamente</strong>.<br>
                        Si realizas el pago en alguna otra instituci&oacute;n bancaria, &eacute;ste se ver&aacute; reflejado en un lapso de 24 a 72 horas, por lo que te recomendamos realizarlo a m&aacute;s tardar en el s&eacute;ptimo d&iacute;a natural de tu fecha de corte para aplicar el precio de Pronto Pago
                        </p>
			
			<br />
			<div class="tarjetaspie" style="display:none;">
				<img src="<%=request.getContextPath()%>/images/mastercard2.png" style="display:block; width:55px; height:22px; float:left; " alt="Mastercard" title="Mastercard" /><img src="<%=request.getContextPath()%>/images/visa2.png" style="display:block; float:left;  width:55px; height:22px; " alt="Visa" title="Visa" /><img src="<%=request.getContextPath()%>/images/amex2.png" style="display:block; float:left;  width:55px; height:22px;" alt="American Express" title="American Express" /></div>
			<div class="tarjetaspieesquina">&nbsp;</div>
			</td></tr>
		</table>
            </div>
        </div>
    </div>
    <br/><br/><br/><br/>
	<div>
            <p style="font-size: 1.3em;">Y en los siguientes establecimientos:</p><br/>
            <table style="color:white;text-align:center; font-size: 1.2em; font-weight: bold;font-family: inherit;">
                <tr>
                    <td style="background-color:rgb(128,136,136);padding:5px;" colspan="2">BANCOS</td>
                </tr>
                <tr>
                    <td id="logosBancos" colspan="2" style="background-color:rgb(245, 246, 248);"></td>
                </tr>
                <tr>
                    <td style="background-color:rgb(136,137,140);padding:5px;" colspan="2">FARMACIAS y TIENDA DE CONVENIENCIA</td>
                </tr>
                <tr>
                     <td id="logosTiendas" colspan="2" style="background-color:rgb(245, 246, 248);"></td>
                </tr>
                <tr>
                    <td style="background-color:rgb(248, 177, 52); padding:5px;" colspan="2">OTROS</td>
                </tr>
                <tr>
                     <td id="logosOtros" colspan="2" style="background-color:rgb(245, 246, 248);"></td>
                </tr>
                <tr>
                    <td id="leyenda" style="background-color:rgb(245, 246, 248); text-align:center; font-size:8pt; color:black;" colspan="2">Realiza tu pago en cualquier tienda que cuente con estos logotipos.</td>
                </tr>
                <tr>
                    <td><img style="height:200px; padding:20px; margin-top:-40px;" src="images/Factura.png" </td>
                    <td style="text-align:left;">
                        <p style="font-size:13pt; color:rgb(20,165,177);">¡No olvides llevar tu último estado de cuenta!</p>
                        <p style="font-size:12pt; color:black;font-weight: normal; width:400px;">En &eacute;l encontrar&aacute;s la informaci&oacute;n necesaria para realizar tu dep&oacute;sito en alguno de los puntos de pago.</p>
                    </td>
                </tr>

            </table>
        </div>
        
        <div class="modal" id="myModalMsj">
           <div class="modal-dialog modal-sm">
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="msjTitle"></h4>
               </div>
                <div class="modal-body" id="msjBody">
               </div>
               <div class="modal-footer" id="msjFooter">
               </div>
             </div>
           </div>
         </div>
        
  </body>
</html>
