<%@ page import="com.totalplay.vo.UsuarioVO"%>
<%@ page import="com.totalplay.vo.EstadoCuentaVO"%>
<%@ page import="com.totalplay.utils.FileProperty"%>

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%
    HttpSession loginmenu = request.getSession(false);
    UsuarioVO usr =(loginmenu.getAttribute("usuario") != null)?(UsuarioVO)loginmenu.getAttribute("usuario"):null;
    EstadoCuentaVO balanceVO = (loginmenu.getAttribute("balance")!=null)?(EstadoCuentaVO)loginmenu.getAttribute("balance"):null;
     
    String statussaldo1="";
    String imgstatussaldo1="";
    if(balanceVO!=null){
        if( balanceVO.getStatusCuenta() != null && balanceVO.getStatusCuenta().length() != 0 ){ 
            if( balanceVO.getStatusCuenta().equalsIgnoreCase("sinsaldo") ){
                imgstatussaldo1="face_mini.png";
                statussaldo1="Sin saldo pendiente";
            }
            if( balanceVO.getStatusCuenta().equalsIgnoreCase("prontopago") ){
                imgstatussaldo1="carita2.png";
                statussaldo1="Saldo pendiente";
            }
            if( balanceVO.getStatusCuenta().equalsIgnoreCase("limitepago") ){
                imgstatussaldo1="carita3.png";
                statussaldo1="Saldo pendiente";
            }
            if( balanceVO.getStatusCuenta().equalsIgnoreCase("moroso") ){ 
                imgstatussaldo1="carita3.png";
                statussaldo1="Sin servicio";
            }
        }
    }
    FileProperty filePropertyCFG=new FileProperty("com.totalplay.prop.ApplicationResources");
    String MIWIFI=(filePropertyCFG.getProperty("MIWIFI")).trim();
    %>
    <style>
        a#menuMiEdoCta:hover>#edos{
            display: none;
        }
        a#menuMiEdoCta:hover>#edos2{
            display: inline !important;
        }
        
        a#change:hover>#planGris{
            display: none;
        }
        a#change:hover>#planBlanco{
            display: inline !important;
        }
        
        a#menuPorta:hover>#portabilidadG{
            display: none;
        }
        a#menuPorta:hover>#portabilidadB{
            display: inline !important;
        }
    </style>
    <div class="d-none d-lg-block p-3" style="font-family:'Montserrat-bold' !important;">
        <div class="row">
            <div class="col-lg-2 pl-3 pt-4">
                <img src="<%=request.getContextPath()%>/img/Menus/Logo_mini.png" style="height: 57px; width: 149px;">
            </div>
            <div class="col-lg-4 texto pt-4" style="margin-top: 4px">Bienvenid@ <%=usr.getName() + " " + usr.getLastname()%></div>
            <div class="col-lg-2 txtmedium pt-4">
                <img src="<%=request.getContextPath()%>/img/Menus/<%=imgstatussaldo1%>">&nbsp;<label><%=statussaldo1%></label>
            </div>
            <div class="col-lg-4 texto">
                <div class="row">
                    <div class="col-8">
                        <a class="aboton" href="<%=request.getContextPath()%>/jsp/myaccount.jsp" id="admonCta" >
                            <button type="button" class="btn boton aboton">
                            Administrar mi contrase&ntilde;a
                            </button>
                        </a>
                    </div>
                    <div class="col-2">
                        <a class="aboton" href="<%=request.getContextPath()%>/jsp/logout.jsp">
                            <button type="button" class="btn boton aboton">
                                Salir
                            </button>
                        </a>
                    </div>
                </div>
                <div class="row pt-4">
                    <div class="col-12" style="padding-right: 0px">
                        <button type="button" class="btn botonpago" id="pagarAhora">
                             <img align="left" src="<%=request.getContextPath()%>/img/Menus/pesos.png"></img>
                             <label style="margin-top: 0.5rem" >Pagar <%=balanceVO.getSaldoActual()%></label>
                        </button>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    
    <div  style="font-family:'Montserrat-medium' !important;"> 
    <nav class="main navbar navbar-expand-sm bg-coler navbar-light" id="tpeMenuHeader">
    
      <button class="navbar-toggler" type="button" data-toggle="collapse" 
        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
        aria-expanded="false" aria-label="Toggle navigation" onclick="openMenu()">
        <span class="navbar-toggler-icon"></span>
      </button>
    
        <div class="d-none d-lg-block collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav col-lg-12" >
            <div class="col-lg-2 col-sm-5 mx-auto" id="n2">
                    <li class="nav-item" id="dedos">
                            <a class="nav-link txto" href="<%=request.getContextPath()%>/facturasser.html" id="menuMiEdoCta">
                                <img src="<%=request.getContextPath()%>/img/Menus/edo_cuenta.png" id="edos">
                                <img src="<%=request.getContextPath()%>/img/Menus/edo_cuenta_b.png" id="edos2" style="display:none;">
                                <label id="txto">Estado <p>de Cuenta</p></label>
                            </a>
                        </li>
                    </div>
                    <div class="col-lg-2 col-sm-5 mx-auto" id="n3">
                        <li class="nav-item" >
                            <a class="nav-link txto" href="<%=request.getContextPath()%>/planclienteser.html" id="change">
                                <img src="<%=request.getContextPath()%>/img/Menus/plan_contratado.png" style="margin-top:-2em;" id="planGris">
                                <img src="<%=request.getContextPath()%>/img/Menus/plan_contratado_b.png" style="margin-top:-2em;display:none;" id="planBlanco">
                                <label id="txto">Mi plan <p>Contratado</p></label>
                            </a>
                        </li>
                    </div>
                    <div class="col-2 mx-auto" id="n4" style="padding-top: 6px;">
                        <li class="nav-item" >
                            <a class="nav-link txto" href="<%=request.getContextPath()%>/portabilidad.html" id="menuPorta">
                                <img src="<%=request.getContextPath()%>/img/Menus/portabilidad.png" id="portabilidadG">
                                <img src="<%=request.getContextPath()%>/img/Menus/portabilidad_b.png" style="display:none;" id="portabilidadB">
                                <label id="txto">Portabilidad</label>
                            </a>
                        </li>
                    </div>
                    <div class="col-2 mx-auto" id="n5" style="padding-top: 6px; display:none;">
                        <li class="nav-item" id="wifi">
                            <a class="nav-link txto" href="<%=request.getContextPath()%>/jsp/RedWifi.jsp" id="menuMiWifi">
                                <img src="<%=request.getContextPath()%>/img/Menus/mi_wifi.png"> 
                                <label id="txto">Mi WiFi</label>
                            </a>
                        </li>
                    </div>
                </ul>
        </div>
        </nav>
    </div>
    
     <div class="sidebar bar-block card animate-right bg-oscuro" id="izquierdamenu"> 
            <div class="col pagination justify-content-end sepRight"> 
                <button onclick="closeMenu()" class="btn-close bg-white" style="border: none;">
                <img src="/Mitotalco/img/Menus/close_icon.png" class="closeimg">
                </button> 
            </div> 
            <div class="tpe-opcion-lateral"> 
                <ul class="navbar-nav mr-auto ml-auto">
                    <li class="nav-item">
                        <a class="navbar-brand" href="https://www.totalplay.com.mx">
                            <img src="<%=request.getContextPath()%>/img/Menus/Logo_mini.png" width="60%"
                            class="d-inline-block align-top" alt="">
                        </a>
                    </li>
                    <li class="nav-item" >
                        <a class="txto" href="<%=request.getContextPath()%>/facturasser.html">
                         <img src="<%=request.getContextPath()%>/img/Menus/edo-cuenta_m.png">&nbsp;&nbsp;&nbsp;Estado de cuenta
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="txto" href="<%=request.getContextPath()%>/planclienteser.html" >
                         <img src="<%=request.getContextPath()%>/img/Menus/plan_contratado_m.png">&nbsp;&nbsp;&nbsp;Mi plan contratado
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="txto" href="<%=request.getContextPath()%>/portabilidad.html" >
                            <img width="5%" src="<%=request.getContextPath()%>/img/Menus/portabilidad_m.png">&nbsp;&nbsp;&nbsp;Portabilidad
                        </a>
                    </li>
                    <li class="nav-item" style="display:none;">
                        <a class="txto" href="<%=request.getContextPath()%>/jsp/RedWifi.jsp" >
                            <img width="5%" src="<%=request.getContextPath()%>/img/Menus/mi_wifi_m.png">&nbsp;&nbsp;&nbsp;Mi Wifi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="txto" href="<%=request.getContextPath()%>/jsp/myaccount.jsp" id="admonCta1">
                            <img width="5%" src="<%=request.getContextPath()%>/img/Menus/settings.png">&nbsp;&nbsp;&nbsp;Administrar mi contrase&ntilde;a
                        </a>
                    </li>
                   
                    <li class="nav-item">
                        <a class="txto" href="<%=request.getContextPath()%>/jsp/logout.jsp">
                            <img width="5%" src="<%=request.getContextPath()%>/img/Menus/logout.png">&nbsp;&nbsp;&nbsp;Cerrar Sesión</a>
                    </li>
                </ul>
            </div>
                  
                </ul>
            </div>
    </div>
    <script>
    function openMenu() {
        console.log(".......edededede");
        $('#izquierdamenu').removeClass('menuHide');
        $('.main').removeClass('bg-coler');
        $('#izquierdamenu').addClass('menuShow');
        //$('.main').addClass('tpe-moving-left');
    }
    function closeMenu() {
     console.log(".......cerrrando");
        //$('.main').removeClass('tpe-moving-left');
        $('#izquierdamenu').removeClass('menuShow');
        //$('.main').addClass('tpe-moving-rigth');
        $('#izquierdamenu').addClass('menuHide');
    
        setTimeout(function(){ $('.main').removeClass('tpe-moving-rigth'); }, 2000);
    }
    
    </script>
     
     
    
    