<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="es">

<jsp:include page="/head.jsp"/>
<%@ page import="com.totalplay.vo.UsuarioVO"%>

<%HttpSession sesion = request.getSession(false);
String numCuenta =(sesion.getAttribute("numCuenta") != null)?(String)sesion.getAttribute("numCuenta"):null;   
%>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/cupertino/jquery-ui-1.9.0.custom.min.css" />
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.js" ></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-3.0.0.min.js" ></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.min.js" ></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.file.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.maskedinput.js" ></script>
    <style type="">
    .ui-state-error{
        background: #8246AF !important; 
    }
    </style>
  </head>
<body>    
    <section id="miplancontratado">
    <jsp:include page="/jsp/menuLoged.jsp"/>
        <br>
        <div class="d-block d-sm-none text-center">
            <p style="font-size: 24px">Portabilidad</p>
        </div>
        <div class="container">
            <!--div class="row d-block d-sm-none" id="banner">
                <img src="<%=request.getContextPath()%>/img/portabilidad/banner1300px.png" style="width:100%;max-height:100%;">
            </div-->
            
            <div class="row d-block d-sm-none" id="banner" style="display: block !important;">
                <img src="<%=request.getContextPath()%>/img/portabilidad/banner1300px.png" style="width:100%;max-height:100%;" id="imgBanner">
            </div>
            
            <div class="row pt-4" >
                <div class="d-none d-lg-block col-lg-3"></div>
                <div class="col-sm-12 col-lg-6">
                    <ul class="tabs-menu">
                        <li class="portabilidad tabReq current" id="tabRequisitos"><a href="#tab-1">Requisitos</a></li>
                        <li class="portabilidad itramite" id="tabTramite"><a href="#tab-2"></a></li>
                        <li class="portabilidad stramite" id="tabConsulta"><a  href="#tab-3"></a></li>
                    </ul>
                </div>
            </div>
            
            <div class="tab">
                <div id="tab-1" class="tab-content">
                      <div class="row fontnormal15 pt-4">
                        <div class="d-none d-lg-block col-lg-2"></div>
                        <div class="col-sm-12 col-lg-8">
                            <p class="font-azul">Requisitos</p><br>
                            <p >
                                Para poder iniciar con tu trámite de portabilidad es necesario contar con 
                                los siguientes requisitos:
                                <ul class="listporta">
                                    <li><b><b style="color:red !important;">*</b>Número a portar (Teléfono que deseas seguir conservando)</b></li>
                                    <li><b><b style="color:red !important;">*</b> NIP</b> (Lo obtienes marcando al 051 forzosamente desde la línea con la otra compañia)</li>
                                </ul>
                            </p>
                            
                            <p>En caso de que tu línea con la otra compañia se encuentre cancelada,
                                  es necesario contar con los siguientes requisitos:
                                    <ul class="listporta">
                                        <li><b>Formato de portabilidad</b> (Descarga formato <a href="<%=request.getContextPath()%>/assets/portabilidad-formato.pdf" target="_blank" style="font-style:italic; color:blue;">aquí</a>)</li>
                                        <li><b>Copia de identificación oficial</b> (INE, CURP, Pasaporte, Licencia para conducir)</li>
                                        <li><b>Carta de Cancelación</b> (No debe ser mayor a 40 días)</li>
                                    </ul>
                            </p>
                            <p>Sólo para personas morales <b>Copia de tu Acta Constitutiva o Poder notarial</b> (no es necesario el NIP)</p>
                            <p>Si deseas realizar la portabilidad de más de un número de teléfono es necesario ingresar una solicitud por cada línea.</p>
                           
                        </div>
                        <div class="d-none d-lg-block col-lg-2"></div>
                    </div>
                  </div>
                <div id="tab-2" class="tab-content">
                    <div class="row pt-4 fontnormal15">
                        <div class="d-none d-lg-block col-lg-2"></div>
                        <div class="col-lg-8 col-sm-12">
                            <p class="font-azul">Iniciar trámite de portabilidad</p>
                            
                            <form method="POST" id="formPorta">
                            
                            <div class="form-group">
                                <p>Datos del cliente</p>
                            </div>
                                
                            <div class="row">
                                <div class="d-none d-lg-block col-lg-3"></div>
                                <div class="col-6 col-lg-3"><input type="radio" name="razonSocial" id="rbtnFisica"  value="fisica" checked="checked" onclick="$.ocultaArch()" >Persona física</input></div> 
                                <div class="col-6 col-lg-3"><input type="radio" name="razonSocial" id="rbtnMoral"  value="moral" onclick="$.ocultaArch()">Persona moral</input></div> 
                            </div> 
                         
                               
                             <div class="form-group pt-3" id="ctnLineaActiva">  
                                <p>¿Tu línea telefónica con el proveedor anterior continúa activa?</p>
                             </div>
                                
                            <div class="row" id="ctnLineaActiva1">
                                <div class="d-none d-lg-block col-lg-4"></div>
                                <div class="col-6 col-lg-2"><input type="radio" id="rbtnActiva" name="rbtnActiva" onclick="$.ocultaFechaCancel()" checked="checked">Sí</input></div>
                                <div class="col-6 col-lg-2"><input type="radio" name="rbtnActiva" onclick="$.ocultaFechaCancel()">No</input></div>
                            </div>  
                               
                             <div class="form-group pt-3" id="ctnFechaCancel">
                                <p>¿La fecha de cancelación de tu línea tiene más de 40 días?</p>
                            </div>
                                
                            <div class="row" id="ctnFechaCancel1">
                                <div class="d-none d-lg-block col-lg-4"></div>
                                <div class="col-6 col-lg-2"><input type="radio" id="rbtnFechaCancel" name="rbtnFechaCanc" onclick="$.ocultaCartaCancel()">Sí</input></div>
                                <div class="col-6 col-lg-2"><input type="radio" id="rbtnFechaCancelNO" name="rbtnFechaCanc"  onclick="$.ocultaCartaCancel()" checked="checked">No</input></div>
                            </div>
                            
                            <div class="form-group pt-3" id="numCuenta">
                                <label>Número de cuenta</label>
                                <label><b><%=numCuenta%></b></label>
                            </div>  
                               
                            <div class="row" id="numPortar">
                                <div class="col-lg-6">
                                    <label>Número a portar</label>
                                    <input type="text" id="DN_0" class="form-control" name="DN_0" maxlength="10"></input>
                                </div>
                                <div class="col-lg-6">
                                    <label>Teléfono de contacto</label>
                                    <input type="text" id="telCont" class="form-control" name="telCont" maxlength="10"></input>
                               </div>
                            </div>  
                               
                            <div class="form-group" id="ctnNIP">
                                <label>NIP</label>
                                <input type="text" class="form-control" id="nip_0" name="nip_0" maxlength="4"></input>
                             </div>  
                            
                            <div class="row pt-4" id="ctnIdenOf">
                                <div class="col-12 col-lg-4 "><label>Identificación Oficial</label></div>
                                <div class="col-6 col-lg-5 ">
                                    <input class="form-control" type="text" name="txtIdenPorta" id="txtIdenPorta"></input>
                                </div>
                                <div class="col-6 col-lg-3 ">
                                    <label class="btnCargaDoc">
                                        <input type="file" id="btnIdenPorta" class="btnCargaArchivos" name="btnIdenPorta" onchange="$.nombreArchivo(this.id)"/> Examinar
                                    </label>
                                </div>
                            </div>
                               
                            <div class="row pt-2" id="ctnFormSol">
                                <div class="col-12 col-lg-4"><label>Formato de solicitud</label></div>
                                <div class="col-6 col-lg-5"><input class="form-control" type="text" name="txtSolPorta" id="txtSolPorta"></input></div>
                                <div class="col-6 col-lg-3">
                                    <label class="btnCargaDoc">
                                    <input type="file" id="btnSolPorta" class="btnCargaArchivos" name="btnSolPorta"  onchange="$.nombreArchivo(this.id)"/> Examinar</label>
                                </div>
                           </div> 
                               
                           <div class="row pt-2" id="ctnCartaCancel">  
                               <div class="col-12 col-lg-4"><label>Carta de cancelación</label></div>
                               <div class="col-6 col-lg-5"><input class="form-control" type="text"  name="txtCartaCancel" id="txtCartaCancel"></input></div>
                               <div class="col-6 col-lg-3">
                                    <label class="btnCargaDoc">
                                    <input type="file" id="btnCartaCancel" class="btnCargaArchivos" name="btnCartaCancel"  onchange="$.nombreArchivo(this.id)"/> Examinar</label>
                               </div>
                             </div> 
                               
                           <div class="row pt-2" id="ctnActaCons">     
                                <div class="col-12 col-lg-4"><label>Acta constitutiva</label></div>
                                <div class="col-6 col-lg-5"><input class="form-control" type="text" name="txtActaCons" id="txtActaCons"></input></div>
                                <div class="col-6 col-lg-3">
                                    <label class="btnCargaDoc" >
                                    <input type="file" id="btnActaCons" class="btnCargaArchivos" name="btnActaCons"  onchange="$.nombreArchivo(this.id)"/> Examinar
                                    </label>
                                </div>
                             </div> 
                               
                           <div class="form-group pt-2" id="ctnMensaje">
                                <p style="font-size:10pt;">Estimado cliente, no es posible realizar tu trámite de portabilidad debido a que tu cancelación tiene más de 40 días.</p>
                           </div> 
                           
                           <div class="form-group text-center pt-2">
                                <button type="submit" class="btn btnAceptar" name="btnAceptar" id="btnAceptar" >Aceptar</button>
                                <button class="btn btnLimpiar" name="btnLimpia" id="btnLimpia">Limpiar</button>
                           </div>  
                           
                        </form>
            
                        </div>
                    </div>
                </div> 
                <div id="tab-3" class="tab-content">
                    <div class="row fontnormal15 pt-4">
                        <div class="d-none d-lg-block col-lg-2"></div>
                        <div class="col-sm-12 col-lg-8">
                            <p class="font-azul">Consulta de portabilidad</p>
                            <br/>
                             <p>Tienes registrados los siguientes folios de solicitud de portabilidad:</p>
                            <br/>
                            <div class="table-responsive">
                                <table id="consPorta" class="table table-striped pt-2 fontnormal15">
                                <thead>
                                    <tr class="fontbold15">
                                        <td width="10%">Folio</td>
                                        <td width="20%">Tel. a portar</td>
                                        <td width="20%">Fecha de solicitud</td>
                                        <td width="10%">Estatus</td>
                                        <td width="20%">Observaciones</td>
                                        <td width="20%">Fecha de portabilidad</td>
                                    </tr>
                                </thead>
                                <tbody id="resFolios">
                                    <tr >
                                        <td>1</td>
                                        <td>551111111</td>
                                        <td>10/12/2019</td>
                                        <td>Pendiente</td>
                                        <td>Observaciones</td>
                                        <td>10/12/2019</td>
                                    </tr>
                                    <tr >
                                        <td>2</td>
                                        <td>551111111</td>
                                        <td>10/12/2019</td>
                                        <td>Pendiente</td>
                                        <td>Observaciones</td>
                                        <td>10/12/2019</td>
                                    </tr>
                                </tbody>
                             </table>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
                 
            
          
            
           
            
    </div>
    </section>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/factura.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/additional-methods.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/validations.js"></script>
    
    <jsp:include page="/jsp/footerLoged.jsp"/>
    
    <script type="text/javascript">
        /*******************************************Portabilidad********************************************************/
        $('#btnAceptar').on('click',function(){
            $(this).trigger('iniciaPortabilidad');
        });
    
        $('#btnAceptar').bind('iniciaPortabilidad',function(){
                var Cx = window.cX;
                Cx.callQueue.push(['sendEvent','iniciaPortabilidad']);
        });
  </script>
</body>
</html>