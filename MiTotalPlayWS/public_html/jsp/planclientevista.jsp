<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.*"%>


<%@ page import="com.totalplay.vo.PlanVO"%>
<%@ page import="com.totalplay.vo.ServicioVO"%>
<%@ page import="com.totalplay.vo.ProductoVO"%>
<%@ page contentType="text/html;charset=windows-1252"%>

<html>

    <%
    HttpSession login = request.getSession(false);
    PlanVO objPlan = (login.getAttribute("infoPlan")!=null)?(PlanVO)login.getAttribute("infoPlan"):null;
    %>

  <head>
     <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-7618852-11');
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
    <title>PlanClienteVista</title>
    <!-- Cxense script begin -->
    <script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
        cX.callQueue.push(['cint', '200']);
        cX.callQueue.push(['sendPageViewEvent']);
        // one time initialization for all sendEvent calls
        cX.callQueue.push(['setEventAttributes', 
        { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
        </script>
        <script type="text/javascript">
        (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
        e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
        t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
    </script>        
    <!-- Cxense script end -->
  </head>
  <body>
  
        <div class="modal fade" id="myModalCancel">
            <div class="modal-dialog">
                 <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Cancelar mi servicio</h4>
                    </div>
                    <div class="modal-body">
                         Estimado cliente, 
                        <br /><br />
                        Para poder tramitar tu cancelaci&oacute;n es necesario que nos env&iacute;es un correo a <a href="mailto:cancelaciones@totalplay.com.mx?Subject=Solicitud%20de%20cancelacion&body=Para poder tramitar tu cancelaci&oacute;n es necesario que nos env&iacute;es la siguiente informaci&oacute;n:%0D%0A%0D%0AN�mero%20de%20Cuenta:%0D%0ATitular:%0D%0ARaz�n%20por%20la%20cual%20solicitas%20la%20cancelaci�n:%0D%0AN�mero%20telef�nico%20donde%20podamos%20localizarte:">cancelaciones&#64;totalplay.com.mx</a> con tu n&uacute;mero de cuenta, nombre del titular, raz&oacute;n por la cual solicitas la cancelaci&oacute;n y un n&uacute;mero telef&oacute;nico donde podamos localizarte.
                        <br /><br />
                        Nosotros nos comunicaremos contigo dentro de un periodo de 72 horas para dar seguimiento a tu solicitud.
                        <br /><br />
                        O si prefieres, puedes comunicarte con nosotros a la l&iacute;nea Totalplay al 1579 8000 o al 01800 510 0510 en un horario 9:00a.m. a 9:00p.m.
                        <br /><br />
                        Atentamente,<br /> 
                        Servicio a Clientes Totalplay
                    </div>
                    <div class="modal-footer">
                    </div>
                 </div>
            </div>
        </div>
        
        <% if( objPlan.getServicios() != null ){ %>
        <!--<logic:present name="infoPlan" >-->
                    <table style="width:100%; margin-left: auto; margin-right: auto;">
                        <%String paquete= "";%>
                        <%  
                            List<ServicioVO> lst = objPlan.getServicios();
                            for( ServicioVO srvc: lst ){
                        %>
                        
                        <%-- srvc.getDescripcion() + ", " + srvc.getTipoServicio() + ", " + paquete + " | " --%>
                        
                        <!--<logic:iterate name="infoPlan" property="servicios" id="servicioVista">-->
                            <% if( !srvc.getDescripcion().equalsIgnoreCase(paquete) ){ %>
                            <!--<logic:notEqual name="servicioVista" property="descripcion" value="<%=paquete%>">-->
                                <tr><td style="padding-left:40px; font-size:14px;"><b>
                                <!--<bean:write name="servicioVista" property="descripcion"/>-->
                                <%=srvc.getDescripcion()%>
                                </b></td></tr>
                            <!--</logic:notEqual>-->                                                
                            <% } if( srvc.getDescripcion().equalsIgnoreCase(paquete) ){ %>
                                <tr><td>  </td></tr>
                            <% } %>
                            <%
                                //paquete=((ServicioVO)servicioVista).getDescripcion();
                                paquete = srvc.getDescripcion();
                            %>
                                <tr><td style="padding-left:40px; font-size:12px;"><b>
                                <!--<bean:write name="servicioVista" property="tipoServicio"/>-->
                                <%=srvc.getTipoServicio()%>:</b>
                                    <% for( ProductoVO prod: srvc.getProductos() ){ %>
                                    <!--<logic:iterate name= "servicioVista" property= "productos" id="productosVista">-->
                                        <!--<bean:write name="productosVista" property="descripcionAdicional"/>-->
                                        <%=prod.getDescripcionAdicional()%>
                                    <!--</logic:iterate>-->
                                    <% } %>
                                </td>
                            </tr>
                        <!--</logic:iterate>-->
                        <% } %>
                         <tr><td style="padding-left:40px; font-size:12px;"><i><b>Renta precio de lista: </b>
                         <!--<bean:write name="infoPlan" property="precioLista"/>-->
                         <%=objPlan.getPrecioLista()%></i></td></tr>
                    </table>
        <!--</logic:present>-->
        <% } %>
        
        <% if( objPlan.getServicios() == null || objPlan.getServicios().size() == 0 ){ %>
        <!--<logic:notPresent name="infoPlan">-->
             <table style="width:100%; margin-left: auto; margin-right: auto;">
                <tr class="non"><td style="padding-left:40px; font-size:14px;">El detalle de tu paquete no est&aacute; disponible en este momento.</td></tr>
             </table>
        <!--</logic:notPresent>-->
        <% } %>
  </body>
</html>