<!DOCTYPE HTML>


<%@ page import="com.totalplay.vo.ServicioVO"%>
<%@ page import="com.totalplay.vo.UsuarioVO"%>
<%@ page import="com.totalplay.vo.TarjetaVO"%>
<%@ page import="com.totalplay.vo.EstadoCuentaVO"%>
<%@ page import="com.totalplay.vo.HistoricoFacturaVO"%>
<%@ page import="com.totalplay.ws.vo.ReferenceBankVO"%>
<%@ page import="java.util.List"%> 
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.totalplay.utils.VerificaUsuario"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="encrypt.Encrypt3Des"%>
<%@ page import="com.totalplay.utils.FileProperty"%>

<html>
<%

	    HttpSession login = request.getSession(false);
	    String error="";
	    UsuarioVO usuarioVo =(login.getAttribute("usuario") != null)?(UsuarioVO)login.getAttribute("usuario"):null;
  
	     if(usuarioVo == null){
		request.getRequestDispatcher("home.jsp").forward(request, response);
	    }else{
		EstadoCuentaVO balanceVO = (login.getAttribute("balance")!=null)?(EstadoCuentaVO)login.getAttribute("balance"):null;
		HistoricoFacturaVO facturasVO = (login.getAttribute("facturas")!=null)?(HistoricoFacturaVO)login.getAttribute("facturas"):new HistoricoFacturaVO();
		int sizeListFact = (login.getAttribute("sizeListFact")!=null)?(Integer)login.getAttribute("sizeListFact"):0;
                
		String AuthBancaria=  (String)login.getAttribute("AuthBancaria"); 
                
		String errorType=null;
		if(login.getAttribute("errorType")!=null){
		    errorType=(String)login.getAttribute("errorType"); 
		}
		String sendEmail=null;
		if(login.getAttribute("sendEmail")!=null){
		    sendEmail=(String)login.getAttribute("sendEmail"); 
		}
                
	    String sessionOtt= (String)login.getAttribute("ottID");
	    String sessid= login.getId(); 
	    String sessionJava=null;
	    if(sessionOtt!=null && !sessionOtt.isEmpty())
		sessionJava=sessionOtt;
	    else{
		sessionJava= sessid.substring(0, 25);
	   }  
	 FileProperty filePropertyCFG=new FileProperty("com.totalplay.prop.ApplicationResources");
	 String PORTABILIDAD=(filePropertyCFG.getProperty("PORTABILIDAD")).trim();
	 String MIWIFI=(filePropertyCFG.getProperty("MIWIFI")).trim();
	%>  
        
<head>
        <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-7618852-11');
        </script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
	<meta HTTP-EQUIV="Expires" CONTENT="-1"/>
	<title>Mi Cuenta Totalplay</title>
	<link rel="shortcut icon" href="<%=request.getContextPath()%>/images/icn/favicon.ico"/>
	<link rel="apple-touch-icon" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon.png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-72x72.png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-114x114.png"/>
       
	<meta name="description" content="" />
        
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.js" ></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.min.js" ></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.file.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.maskedinput.js" ></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/fancybox2/jquery.fancybox.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/aes-enc.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/sha1.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/base64.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/BigInt.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/Barrett.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/RSA.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/settings.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/logotiposPago.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.fileDownload.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/aes.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/pbkdf2.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/AesUtil.js"></script>
        
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/core2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/eventos.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.nivo.slider.js" type="text/javascript"></script>
        
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style2.css" media="screen" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/nivo-slider.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/themes/default/default.css" type="text/css" media="screen" />   
        
            
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/cupertino/jquery-ui-1.9.0.custom.min.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/jquery.fancybox-1.3.4.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/fancybox2/jquery.fancybox.css" media="screen" />
      
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
                
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/additional-methods.min.js"></script>
        
            
	 <script type="text/javascript">
	    $(document).ready(function () {
		mensaje('<%=AuthBancaria%>','<%=errorType%>','<%=sendEmail%>','<%=usuarioVo.getEmail()%>','<%=usuarioVo.getName()%>');
	    function getContextPath() {
		return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
		//return "";
	    }  
	    function llenaAcordeon(urlParam,divContent){
		    abierta=divContent;
		    function llegadaDatos(datos){   
			$(divContent).html(datos);
		    }
		    function problemas(xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(thrownError);
			console.log("Errores");
		    }
		   $.ajax({
		       cache:false,
		       async:true,
		       type: "POST",
		       dataType: "html",
		       //contentType: "application/x-www-form-urlencoded",
		       url: urlParam,
		       success:llegadaDatos,
		       error: problemas,
		       timeout:100000
		     }); 
		}
                
		 $("#pagarAhora").on( "click",function (e) {
			e.preventDefault();
			llenaAcordeon(getContextPath()+"/infopagoser.html","#pago_1");
			$("#accordion .ui-accordion-content").show();
			var new_position = $('#pago').offset();
			window.scrollTo(new_position.left,new_position.top);
		});
                
		$("#imgRecomClick").on( "click",function (e) {
			e.preventDefault();
			llenaAcordeon(getContextPath()+"/jsp/recomendados.jsp","#recomendados_1");
			$("#accordion .ui-accordion-content").show();
			var new_position = $('#recomendadosTotalplay').offset();
			window.scrollTo(new_position.left,new_position.top);
		}); 
	    });  
	</script>
	<script type="text/javascript">
	if (top.location!= self.location) {
		top.location = self.location.href
	}

	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','<%=request.getContextPath()%>/js/analytics.js','ga');

	  ga('create', 'UA-42318544-1', 'mitotalplay.com.mx');
	  ga('send', 'pageview');
	</script>
       <!-- Cxense script begin -->
	<script type="text/javascript">
	    var cX = cX || {}; cX.callQueue = cX.callQueue || [];
	    cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
            cX.callQueue.push(['cint', '200']);
	    cX.callQueue.push(['sendPageViewEvent']);
	    // one time initialization for all sendEvent calls
	    cX.callQueue.push(['setEventAttributes', 
	    { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
	    </script>
	    <script type="text/javascript">
	    (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
	    e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
	    t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
	</script>    
        <script>             
        entrar();
        function entrar(){
            var Cx = window.cX;
            Cx.callQueue.push(['sendEvent','loginSerEvent']);     
        }        
        </script>   
	<!-- Cxense script end -->       
 </head>       
 <body style=" background-image:url(<%=request.getContextPath()%>/assets/txture001.png); background-repeat:repeat;">
  <form method="post" action="https://totalgo.totalplay.com.mx:444/TPW/TotalPlay.htm" target="_blank" id="ott" autocomplete="off">
	<input type="hidden" value="<%=sessionJava%>" name="session" id="session" />
</form>
 
 <div id="backgrounds">
	<div id="container">
		<div id="header">
			<a href="http://www.totalplay.com.mx" target="_blank" id="headerlogo"></a>
			<div id="login"><br /><br />
                            
			    <span style="color:#FFF !important;">Bienvenid&#64;</span> <img src="<%=request.getContextPath()%>/images/icn_perfil.png" alt="" title="" /><span style="color:#AED137 !important;"> <%=usuarioVo.getName() + " " + usuarioVo.getLastname()%> </span><br>
			    <a href="<%=request.getContextPath()%>/jsp/myaccount.jsp" id="admonCta" style=" color:#B2B1B2;" ><img src="<%=request.getContextPath()%>/images/icn_config.png"/> Administrar  mi Contrase&ntilde;a</a> | <a href="<%=request.getContextPath()%>/jsp/logout.jsp" style="padding-left:15px; color:#B2B1B2;"> <img src="<%=request.getContextPath()%>/images/icn_close.png"/> Salir</a>
				<br /><br />
			</div>
		</div>
		<div id="div1"></div>
		<div id="navbar">
		</div>		
		<div id="content">	
                
		    <div id="searchbar" align="center"><img src="<%=request.getContextPath()%>/assets/banners/banner_top.jpg" alt="" title="" /></div>
                   	
		    <div id="top2" align="center">
                        <div style="width:818px;">
                            <div class="slider">
                               <div class="slider-wrapper theme-default">
                                    <div id="slider" class="nivoSlider">
                                        <img src="<%=request.getContextPath()%>/assets/banners/Banner_Micuenta_Crecelo (1).jpg" alt="" title="" />
                                        <a onclick=""  id ="imgRecomClick" target="_blank"><img src="<%=request.getContextPath()%>/assets/banners/Banner_Micuenta_Recomendados (1).jpg" alt="" title="" /></a>                                        
                                        <img src="<%=request.getContextPath()%>/assets/banners/Banner_Micuenta_cargo (1).jpg" alt="" title="" />
                                        <img id="bannerNetflix" src="<%=request.getContextPath()%>/assets/banners/MiCuenta-Netflix.jpg" alt="" title="#htmlcaption" />                                         
                                        <img src="<%=request.getContextPath()%>/assets/banners/Banner_Micuenta_LIVERPOOL.jpg" alt="" title="" />
                                    </div>
                                    <style type="text/css">
                                    .nivo-caption{
                                        background: transparent !important;
                                        left: 25% !important;
                                        bottom: 16% !important;
                                    }
                                    
                                    .nivo-caption>a {
                                        background: transparent !important;
                                        padding-left: 100px;
                                        padding-right: 50px;
                                        padding-top: 10px;
                                        padding-bottom: 10px;
                                        border-radius: 100px;
                                    }
                                    </style>
                                    <div id="htmlcaption" class="nivo-html-caption" >     
                                        <a href="" onclick="$.netflix()" class="" id="contratarNetflix"></a>
                                        <a href="" onclick="$.netflixOlvide()" class="" id="olvideCorreoNetflix"></a>
                                    </div>
                                 </div>
                            </div>
                        </div>
		    </div>
		<div id="cx_2f19bc55fa6eb3fd45ddb0e9c07ce4774cff28ce" style="display: none;"></div>
		<div style="clear: both; min-height: 420px; margin-left: auto; margin-right: auto; width: 850px;">
		    <% if( balanceVO != null ){ %>
		    <!--<logic:present name="balance">-->
		    <div class="modulo" style="float:left;">
                  
			<h2 class="saldo">Mi saldo actual es: <b>
			<%=balanceVO.getSaldoActual()%>
			<!-- <bean:write name="balance" property="saldoActual"/> -->
			</b></h2><br />
                        
			<% if( balanceVO.getTotalPago() == null || balanceVO.getTotalPago().length() == 0 ){ %>
			<!-- <logic:notPresent name="balance" property="totalPago" > -->
			    <div>El detalle de tu &uacute;ltimo estado de cuenta no est&aacute; disponible en este momento.</div>
			<!-- </logic:notPresent> -->
			<% } %>
                        
			<% if( balanceVO.getTotalPago() != null && balanceVO.getTotalPago().length() != 0 ){ %>
			<!-- <logic:present name="balance" property="totalPago"> -->
			<br/> 
			<div style="background-color:#CACACA; color:#4E4E4E; font-weight: bold;">Saldo de tu &uacute;ltimo  estado de cuenta</div>
                       
					<table>
						<tr class="non"><td style="width:200px;"><b>Total a pagar:</b></td>
						<td class="monto"><b> <!--<bean:write name="balance" property="totalPago"/> --> <%= balanceVO.getTotalPago() %> </b></td></tr>
						<tr class="par"><td>Fecha l&iacute;mite de pago:</td>
						<td class="monto"> <!--<bean:write name="balance" property="fechaLim"/> --> <%= balanceVO.getFechaLimDesc() %> </td></tr>
                                                
						<% if( balanceVO.getTotalPagoDesc() == "0" ){ %>
						<!-- <logic:notEqual name="balance" property="totalPagoDesc" value="0"> -->
						    <tr class="non"><td>Total a pagar con descuento de pronto pago:</td>
						    <td class="monto"> <!--<bean:write name="balance" property="totalPagoDesc"/>--> <%=balanceVO.getTotalPagoDesc()%> </td></tr>
						    <tr class="par"><td>Fecha l&iacute;mite de pago con descuento de pronto pago:</td>
						    <td class="monto"> <!--<bean:write name="balance" property="fechaLimDesc"/>--> <%=balanceVO.getFechaLimDesc()%> </td></tr>
						<!-- </logic:notEqual> -->
						<% } %>
                                                
						<tr class="non"><td>Fecha de corte:</td>
						<td class="monto"> <!--<bean:write name="balance" property="fechaCorte"/>--> <%=balanceVO.getFechaCorte()%> </td></tr>
                                                
					</table>    
			 <!-- </logic:present> -->
			 <% } %>
                         
                         
			 <% if( balanceVO.getEstadoCuentaAnt() != null && balanceVO.getEstadoCuentaAnt().length() != 0 ){ %>
			 <!--<logic:present name="balance" property="estadoCuentaAnt">-->
				<table id="accordion3">
					<tr><td style="padding: 0 !important; padding-left: 8px !important; width: 20px; background-color:#CACACA; "><span id="Arr" class="Flecha1">&nbsp;</span></td><td style="padding-left:0px !important; background-color:#CACACA; color:#4E4E4E; font-weight: bold;">Resumen de tu estado de cuenta</td></tr>
				</table>
				<div id="edoccTable">
				    <table>
					    <tr class="non"><td>Estado de cuenta anterior:</td>
					    <td class="monto"> <!--<bean:write name="balance" property="estadoCuentaAnt"/>--> <%=balanceVO.getEstadoCuentaAnt()%>  </td></tr>
					    <tr class="par"><td>Su pago:</td>
					    <td class="monto"> <!--<bean:write name="balance" property="pagoRealizado"/>-->  <%=balanceVO.getPagoRealizado()%> </td></tr>
					    <tr class="non"><td>Notas de cr&eacute;dito:</td>
					    <td class="monto"> <!--<bean:write name="balance" property="notasCred"/>-->  <%=balanceVO.getNotasCred()%> </td></tr>
					    <!--Inicia nuevo requerimiento[Servicios contratados desde IPTV]-->
					    <tr class="non"><td>Servicios terceros:</td>
					    <% if(balanceVO.getRentaServiciosTerceros() != null && balanceVO.getRentaServiciosTerceros().length() != 0){ %>
						<td class="monto"> <%=balanceVO.getRentaServiciosTerceros()%> </td>
					    <% } %>
					    </tr>
					    <!--Termina nuevo requerimiento[Servicios contratados desde IPTV]-->
					    <tr class="non"><td>Estado de cuenta del periodo actual:</td>
					    <td class="monto"> <!--<bean:write name="balance" property="totalEstadoActual"/>--> <%=balanceVO.getTotalEstadoActual()%> </td></tr>
					    <tr class="par"><td><b>Total a pagar:</b></td>
					    <td class="monto totalamt"> <!--<bean:write name="balance" property="totalEstadoActual"/>-->  <%=balanceVO.getTotalEstadoActual()%> </td></tr>
				    </table>
				</div>
			 <!--</logic:present>-->
			 <% } %>
                         
                         
			 <% if( balanceVO.getTotalPago() != null && balanceVO.getTotalPago().length() != 0 ){ %>
			 <!--<logic:present name="balance" property="totalPago">-->
				<table>
				    <tr class="separadorCl2"><td colspan="2" style="background-color:#CACACA; color:#4E4E4E; font-weight: bold;">Resumen de tu &uacute;ltimo estado de cuenta</td></tr>
				    <tr class="par"><td>Periodo:</td>
				    <td class="monto">del <!--<bean:write name="balance" property="periodo"/>--> <%=balanceVO.getPeriodo()%> </td></tr>
				    <tr class="non"><td>Rentas:</td>
				    <td class="monto"> <!--<bean:write name="balance" property="rentas"/>--> <%=balanceVO.getRentas()%> </td></tr>
				    <tr class="par"><td>Bonificaciones/Promociones:</td>
				    <td class="monto"> <!--<bean:write name="balance" property="bonifica"/>-->  <%=balanceVO.getBonifica()%> </td></tr>
				    <tr class="non"><td>Consumos del periodo:</td>
				    <td class="monto"> <!--<bean:write name="balance" property="consumoPeriodo"/>--> <%=balanceVO.getConsumoPeriodo()%> </td></tr>
				    <tr class="par"><td>Subtotal:</td>
				    <td class="monto totalamt"> <!--<bean:write name="balance" property="subtotal"/>--> <%=balanceVO.getSubtotal()%> </td></tr>
				    <tr class="non"><td class="total">Total de este estado de cuenta:</td>
				    <td class="monto totalamt">  <!--<bean:write name="balance" property="totalPago"/>--> <%=balanceVO.getTotalPago()%> </td></tr>
				</table>
			   <!--</logic:present>-->
			   <% } %>
		    </div>
            
                    
		<div class="modulo2" style="float:left; height:100%;">
		    <% if( balanceVO.getStatusCuenta() != null && balanceVO.getStatusCuenta().length() != 0 ){ %>
		    <!--<logic:present name="balance" property="statusCuenta">-->
			    <table style="width:394px; background-color:transparent;">
                                    
				    <% if( balanceVO.getStatusCuenta().equalsIgnoreCase("sinsaldo") ){ %>
				    <!--<logic:equal name="balance" property="statusCuenta" value="sinsaldo">-->
					<tr>
					    <td style="width:65px; padding-left:10px;"><img src="<%=request.getContextPath()%>/assets/icon_perfil_001.png" alt="" title="" /></td>
					    <td style="width:329px; padding-left:10px;">
							    <b>SIN SALDO PENDIENTE</b><br />
					    Tu saldo se encuentra al corriente <br />
					    <br />
					    <i>Agradecemos su preferencia.</i>
					    </td>
					</tr>
				    <!--</logic:equal>-->
				    <% } %>
                                    
				    <% if( balanceVO.getStatusCuenta().equalsIgnoreCase("prontopago") ){ %>
				    <!--<logic:equal name="balance" property="statusCuenta" value="prontopago">-->
					<tr>
					    <td style="width:65px; padding-left:10px;"><img src="<%=request.getContextPath()%>/assets/icon_perfil_002.png" alt="" title="" /></td>
					    <td style="width:329px; padding-left:10px;">
							    <b>SALDO PENDIENTE</b><br />
					    Tu saldo se encuentra dentro <br />
					    de la fecha de PRONTO PAGO.<br >
					    <i>Aproveche el descuento por PRONTO PAGO.</i>
					    </td>
					</tr>
					<tr><td colspan="2" style="padding-left:10px;"><a href="#pago" id="pagarAhora">Pagar ahora</a></td></tr>
				    <!--</logic:equal>-->
				    <% } %>
                                    
				    <% if( balanceVO.getStatusCuenta().equalsIgnoreCase("limitepago") ){ %>
				    <!--<logic:equal name="balance" property="statusCuenta" value= "limitepago">-->
					<tr>
					    <td style=" padding-left:10px;"><img src="<%=request.getContextPath()%>/assets/icon_perfil_003.png" alt="" title="" /></td>
					    <td style="width:329px; padding-left:10px;">
						    <b>SALDO PENDIENTE</b><br />
					    Tu saldo se encuentra dentro <br />
					    de la fecha de PRECIO DE LISTA.<br />
					    <i>Agradecemos su pago.</i>
					    </td>
					</tr>
					<tr><td colspan="2" style="padding-left:10px;"><a href="#pago" id="pagarAhora">Pagar ahora</a></td></tr>
				    <!--</logic:equal>-->
				    <% } %>
                                    
				    <% if( balanceVO.getStatusCuenta().equalsIgnoreCase("moroso") ){ %>
				    <!--<logic:equal name="balance" property="statusCuenta" value="moroso">-->
					<tr>
					    <td style=" padding-left:10px;"><img src="<%=request.getContextPath()%>/assets/icon_perfil_004.png" alt="" title="" /></td>
					    <td style="width:329px; padding-left:10px;">
					    <b>SIN SERVICIO</b><br />
					    Tu servicio ha sido suspendido<br />
					    por un adeudo pendiente.<br >
					    <i>Se reactivar&aacute; su servicio en 48 hrs.</i>
					    </td>
					</tr>
					<tr><td colspan="2" style="padding-left:10px;"><a href="#pago" id="pagarAhora">Pagar ahora</a></td></tr>
				    <!--</logic:equal>-->
				    <% } %>
                                    
                                    
			    </table>
			<!--</logic:present>-->
			<% } %>
                        
                        
		    <br />
              
		    <a target="_blank" id="refActualizaDatos" href="https://www.totalplay.online/totalplayatencion/chatbeacon/content/windows/chat.html?accountid=1&siteid=1&queueid=1&theme=slim&popout=true"><img src="<%=request.getContextPath()%>/assets/banners/Banner_Datos_Ajuste.jpg" alt="" title="" style="width:340px; height: 340px; padding-left: 11px;" /></a>
		    <br />
		</div>
		<!--</logic:present>-->
		<% } %>
	    </div>
            
            
	    <div style="clear:both; "></div>
              
	      <div id="accordion">
                    
		    <h3 id="recomendadosTotalplay">&#161;Gana 1 mes gratis!</h3>
			<div id="recomendados_1" style="overflow-y:hidden;">                     
			</div>
                    
                                       
		    <h3 id="plan">Mi plan</h3>
                    
		    <div id="plan_1">
                            
		    </div>
                    
			<% 
			if( facturasVO.getHistoricoFac() != null){
			    if (facturasVO.getHistoricoFac().size() != 0 ){
			%>
		     <!--<logic:present name="facturas">  -->
			<!--<logic:equal name="sizeListFact" value ="3">-->
			<% if( sizeListFact == 3 ){ %>
			    <h3 id="facturas">Mis &uacute;ltimos 3  estados de cuenta</h3>
			<% } %>
			<!--</logic:equal>-->
			<!--<logic:equal name="sizeListFact" value ="2">-->
			<% if( sizeListFact == 2 ){ %>
			    <h3 id="facturas">Mis &uacute;ltimos 2  estados de cuenta</h3>
			<% } %>
			<!--</logic:equal>-->
			<!--<logic:equal name="sizeListFact" value ="1">-->
			<% if( sizeListFact == 1 ){ %>
			    <h3 id="facturas">Mi estado de cuenta</h3>
			<% } %>
			<!--</logic:equal>-->
		    <!--</logic:present> -->
		    <%      }
			}else{ %>
		    <!--<logic:notPresent name="facturas">-->
			<h3 id="facturas">Mi estado de cuenta</h3>
		    <!--</logic:notPresent>-->
		    <%  } %>
                    
		    <div id="facturas_1">
			<table style="width:100%; margin-left: auto; margin-right: auto;">
			</table>
		    </div>
                        
                        
		    <h3 id="mis_compras">Mis compras</h3>
		    <div id="misCompras_1">
                    
		    </div>
                        
		    <h3 id="pago">Opciones de pago</h3>
		    <div id="pago_1" style="overflow-y:hidden;">
                     
		    </div>
		    <h3 id="contrato">Descarga tu contrato</h3>
		    <div style="display:none; width:1px; height:1px;" ></div>
		    <%if(PORTABILIDAD.equals("1")){%>
			<h3 id="portabilidad">Portabilidad</h3>
			<div id="portabilidad_1" style="overflow-y:hidden;">
			</div>
		    <%}%>
		    <%if(MIWIFI.equals("1")){%>
			<h3 id="wifi">Mi Red WiFi</h3>
		    <%}%>
	    </div>
	</div>
   </div>
</div>

<div id="footer" class="footer">
	 Para una mejor experiencia y visualizaci&oacute;n de la p&aacute;gina te recomendamos tener Internet explorer 7 o superior. <br>
	 Todos los derechos reservados TOTALPLAY Telecomunicaciones S.A. de C.V. 2014<br>
	 <a href="http://www.totalplay.com.mx/" target="_blank">www.totalplay.com.mx</a> | <a href="https://www.totalplay.com.mx/terminos-condiciones-servicio" target="_blank">T&eacute;rminos y condiciones</a> | <a href="https://www.totalplay.com.mx/pdf/aviso-privacidad.pdf" target="_blank">Aviso de privacidad</a> &nbsp;&nbsp;&nbsp; <a data-toggle="modal" href="#myModalCancel" >Cancelar</a>    </i>
</div>
	</div>
	<div style="display:none;"><img src="<%=request.getContextPath()%>/assets/loading.gif" alt=""/></div>
	<div class="modal fade" id="myModalLoading">
	    <div class="modal-dialog modal-sm">
		 <div class="modal-content">
		    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Espere un momento por favor...</h4>
		    </div>
		    <div class="modal-body" style="text-align:center;">
		       <img src="<%=request.getContextPath()%>/assets/loading.gif" alt=""/>
		    </div>
		    <div class="modal-footer">
		    </div>
		 </div>
	    </div>
	</div>
        
	 <div class="modal fade" id="myModalMsj">
	   <div class="modal-dialog ">
	     <div class="modal-content">
	       <div class="modal-header">
		 <button type="button" id="closeModal" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		 <h4 class="modal-title" id="msjTitle"></h4>
	       </div>
		<div class="modal-body" id="msjBody">
	       </div>
	       <div class="modal-footer" id="msjFooter">
	       </div>
	     </div>
	   </div>
	 </div>
         
	 <div class="modal fade" id="myRecomendados">
	   <div class="modal-dialog ">
	     <div class="modal-content">
	       <div class="modal-header">
		 <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color:#fff; opacity:100;">&times;</button>                  
	       </div>
		<div class="modal-body" id="msjBodyReco">
                                    
	       </div>
	     </div>
	   </div>
	 </div>
           
	 <div class="modal fade" id="myModalCancel">
	    <div class="modal-dialog">
		 <div class="modal-content">
		    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Cancelar mi servicio</h4>
		    </div>
		    <div class="modal-body">
			 Estimado cliente, 
			<br /><br />
			Para poder tramitar tu cancelaci&oacute;n es necesario que nos env&iacute;es un correo a <a href="mailto:cancelaciones@totalplay.com.mx?Subject=Solicitud%20de%20cancelacion&body=Para poder tramitar tu cancelaci&oacute;n es necesario que nos env&iacute;es la siguiente informaci&oacute;n:%0D%0A%0D%0ANúmero%20de%20Cuenta:%0D%0ATitular:%0D%0ARazón%20por%20la%20cual%20solicitas%20la%20cancelación:%0D%0ANúmero%20telefónico%20donde%20podamos%20localizarte:">cancelaciones&#64;totalplay.com.mx</a> con tu n&uacute;mero de cuenta, nombre del titular, raz&oacute;n por la cual solicitas la cancelaci&oacute;n y un n&uacute;mero telef&oacute;nico donde podamos localizarte.
			<br /><br />
			Nosotros nos comunicaremos contigo dentro de un periodo de 72 horas para dar seguimiento a tu solicitud.
			<br /><br />
			O si prefieres, puedes comunicarte con nosotros a la l&iacute;nea Totalplay al 1579 8000 o al 01800 510 0510 en un horario 9:00a.m. a 9:00p.m.
			<br /><br />
			Atentamente,<br /> 
			Servicio a Clientes Totalplay
		    </div>
		    <div class="modal-footer">
		    </div>
		 </div>
	    </div>
	</div>  
	<div style="display:none">
	    <div id="datanetflix">
		<center><h1>Lo sentimos</h1></center>
		<p align="justify">Estimado cliente su cuenta muestra un saldo pendiente,<br>
		favor de ponerse en contacto con nuestro centro de atenci&oacute;n <br>
		a clientes Totalplay en www.totalplay.com.mx o <br>
		al 1579 8000 / 01 800 510 0510.</p>
	    </div>
	</div>
        
	<div style="display:none">
	    <div id="datanetflix_error">
		<center><h1>Lo sentimos</h1></center>
		<p align="justify">Estimado cliente, por el momento su solicitud no puede ser atendida.</p>
	    </div>
	</div>
                  
	<%
		String respuestanetflix = request.getParameter("fn");
		if( respuestanetflix != null ){ %>
                    
		    <script>
		    $(document).ready(function(){
			$.fancybox("#datanetflix").showLoading();
		    });
		    </script>
                    
		<% } %>              
    
</body>
<%}

%>
</html>