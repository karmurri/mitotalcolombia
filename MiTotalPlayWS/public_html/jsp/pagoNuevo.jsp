<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.totalplay.vo.UsuarioVO"%>
<%@ page import="com.totalplay.vo.TarjetaVO"%>
<%@ page import="com.totalplay.vo.EstadoCuentaVO"%>
<%@ page import="java.util.*"%> 
<%@ page import="java.util.List"%> 
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.totalplay.utils.VerificaUsuario"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="encrypt.Encrypt3Des"%>
<%@ page import="com.totalplay.utils.FileProperty"%>
<%@ page import="org.apache.logging.log4j.LogManager"%>
<%@ page import="org.apache.logging.log4j.Logger"%>

<html lang="es">
<%
    Logger logger = LogManager.getLogger(this.getClass().getName());
    HttpSession login = request.getSession(false);
    String error="";
    UsuarioVO usuarioVo =(login.getAttribute("usuario") != null)?(UsuarioVO)login.getAttribute("usuario"):null;
    
     if(usuarioVo == null){
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }else{
        EstadoCuentaVO balanceVO = (login.getAttribute("balance")!=null)?(EstadoCuentaVO)login.getAttribute("balance"):null;        
        String statussaldo="";
        String imgstatussaldo="";
        if(balanceVO!=null){
            if( balanceVO.getStatusCuenta() != null && balanceVO.getStatusCuenta().length() != 0 ){ 
                if( balanceVO.getStatusCuenta().equalsIgnoreCase("sinsaldo") ){
                    imgstatussaldo="icon_perfil_001.png";
                    statussaldo="Sin saldo pendiente";
                }
                if( balanceVO.getStatusCuenta().equalsIgnoreCase("prontopago") ){
                    imgstatussaldo="icon_perfil_002.png";
                    statussaldo="Saldo pendiente";
                }
                if( balanceVO.getStatusCuenta().equalsIgnoreCase("limitepago") ){
                    imgstatussaldo="icon_perfil_003.png";
                    statussaldo="Saldo pendiente";
                }
                if( balanceVO.getStatusCuenta().equalsIgnoreCase("moroso") ){ 
                    imgstatussaldo="icon_perfil_004.png";
                    statussaldo="Sin servicio";
                }
            } 
        }
        
        String sessionOtt= (String)login.getAttribute("ottID");
        String sessid= login.getId(); 
        String sessionJava=null;
        if(sessionOtt!=null && !sessionOtt.isEmpty()){
            sessionJava=sessionOtt;
        }else{
            sessionJava= sessid.substring(0, 25);
        }
        
        String errorType=null;
        if(login.getAttribute("errorType")!=null){
            errorType=(String)login.getAttribute("errorType"); 
        }
        
        //TARJETAS
        List<TarjetaVO> listTarjetas= login.getAttribute("tarjetas")!=null?(List<TarjetaVO>)login.getAttribute("tarjetas"):new ArrayList<TarjetaVO>();
        
    %>

  <jsp:include page="/head.jsp"/>
  <body style="font-family: 'Montserrat-light'; !important">
    <section id="edoCta">
        <jsp:include page="/jsp/menuLoged.jsp"/>
        <br>
        <div class="container" id="pagoNuevo">
        
            <div class="row">                
                <!--Izquierda PAGO-->
                <div class="col-md-7 col-lg-7 col-sm-7" id="izquierdaPago">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="fontbold16gris">Mis tarjetas registradas</p>
                        </div>
                    </div>
                    <!--MIS TARJETAS REGISTRADAS-->
                    <form id="paymentForm0" method="post" autocomplete="off" action=<%=request.getContextPath()%>/payextcardser.html>
                        <input type="hidden" name="idCardList" id="idCardList" value="" />
			<input type="hidden" name="sec" id="sec" value="" />
                        <div class="row">
                            <div class="col-md-10" >
                                <!--div>El detalle de tu último estado de cuenta no está disponible en este momento.</div-->
                                <% if(listTarjetas != null && !listTarjetas.isEmpty() && listTarjetas.size() != 0){ %>
                                <table class="table table-striped" id="tarjetasRegistradas">
                                    <tbody class="text-justify">
                                        <tr class="text-center">
                                            <td class="text-center font-weight-bold" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;">Check</td>
                                            <td class="text-center font-weight-bold" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;">Logo</td>
                                            <td  class="text-center font-weight-bold" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;">Tarjeta</td>
                                            <td class="text-center font-weight-bold" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;">Expira</td>
                                            <td class="text-center font-weight-bold" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;">CVV</td>
                                        </tr>
                                        <% int ContTarjeta=1; 
                                        for(TarjetaVO tarjeta: listTarjetas){ %>
                                        <tr class="text-center">
                                            <td><input type="radio" name="CcardOption" id="CcardOption<%=ContTarjeta%>" value="<%=ContTarjeta%>" onclick="$.UpdateForm(<%=ContTarjeta%>,'<%=tarjeta.getFullName()%>','<%=tarjeta.getEndingCardNumber().substring(1)%>','<%=tarjeta.getCode()%>','<%=tarjeta.getBank()%>')" /></td>
                                            <td>
                                            <% if(tarjeta.getProvider().toString().equals("M")){ //Mastercard %>                                        
                                                <img src="<%=request.getContextPath()%>/img/EstadoCta/mastercard_mini.png">
                                            <% } else if(tarjeta.getProvider().toString().equals("V")){ %>
                                                <img src="<%=request.getContextPath()%>/img/EstadoCta/visa_mini.png">
                                            <% }else{ %>
                                                <img src="<%=request.getContextPath()%>/img/EstadoCta/americanexpress_mini.png">
                                            <% } %>
                                            </td>
                                            <td><%="**** **** **** "+tarjeta.getEndingCardNumber().substring(1)%></td>
                                            <td>
                                                <select name="Month_<%=ContTarjeta%>">
                                                    <% for(int a=1; a<=12; a++) {
                                                        String b= String.format("%1$" + 2 + "s", a).replace(" ", "0");
                                                    %>
                                                    <option value="<%=b%>"><%=b%></option>
                                                    <%}%>
                                                    </select>
                                                    <select name="Year_<%=ContTarjeta%>">
                                                        <% for(int a=0; a<8; a++) {
                                                            Calendar c1= Calendar.getInstance();
                                                            int FullYear=c1.get(Calendar.YEAR)+a;
                                                            int MinYear= (c1.get(Calendar.YEAR)-2000)+a;
                                                            String b= String.format("%1$" + 2 + "s", MinYear+"").replace(" ", "0");
                                                        %>
                                                        <option value="<%=MinYear%>"><%=b%></option>
                                                        <%}%>
                                                    </select>
                                            </td>
                                            <td>
                                                <input type="password" name="CCV_<%=ContTarjeta%>" size="4" style="border-width: 1px;border-color: black;border-style: solid;" maxlength="4" title=""/>
                                            </td>
                                        </tr>
                                        <% ContTarjeta++; %>
                                        <% } %>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>
                                            <%String montoPagar = "";
                                            String monedaTemp = balanceVO.getSaldoActual().replace("$","");
                                            String moneda = monedaTemp.replace(",","");
                                            montoPagar = moneda.substring(0, 4);%>
                                            <input type="text" name="amm" id="amm0" value="<%=montoPagar%>" class="text-center font-weight-bold" style="border: none;color: #1F9BC7;font-family: 'Montserrat-bold';font-size: 17px;" />
                                        </label>
                                   </div>
                                   <div class="col-md-6">
                                        <input type="hidden" name="ammdbt" id="ammdbt0" value="" />
                                        <input style="background-color: #8246AF;border: none;color: white;" class="btn btn-primary btn-block font-weight-bold" type="button" value="REALIZAR PAGO" id="btnRealizaPago" onclick="<%
                                            boolean pagoTCEHoy= false;
                                            pagoTCEHoy= VerificaUsuario.verificaPagoHoy(usuarioVo.getEmployeenumber());
                                            if(pagoTCEHoy){
                                                out.print("if(confirm('El d&iacute;a de hoy ya realizaste un pago.\\n Deseas continuar?'))");    
                                            }                                
                                        %>$.SubmitForm('paymentForm0')" />
                                   </div>
                                </div>
                                <% }else{ %>
                                    <div><h2><b>Usted no cuenta con tarjetas registradas</b></h2></div>
                                <% } %>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </form>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="fontbold16gris">Agregar un nuevo m&eacute;todo de pago</p>
                        </div>
                    </div>
                    <!--AGREGAR UN NUEVO METODO DE PAGO-->
                    <div class="row">
                        <div class="col-md-10">
                            <p style="font-family: 'Montserrat-semibold';color: #3A4559;">Tarjetas de cr&eacutedito o d&eacutebito</p>
                        </div>
                        <div class="col-md-10">
                            <p style="font-family: 'Montserrat-semibold';color: #3A4559;font-size: 12px;">Tarjetas de cr&eacutedito o d&eacutebito</p>
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<%=request.getContextPath()%>/img/EstadoCta/visa_mini.png" />
                                </div>
                                <div class="col-md-4">
                                    <img src="<%=request.getContextPath()%>/img/EstadoCta/mastercard_mini.png" />
                                </div>
                                <div class="col-md-4">
                                    <img src="<%=request.getContextPath()%>/img/EstadoCta/americanexpress_mini.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <form id="paymentForm" method="post" action="<%=request.getContextPath()%>/paynewcardser.html" autocomplete="off" accept-charset="UTF-8">
                        <input type="hidden" id="cCardNumberEnc" name="cCardNumberEnc" value="" />
			<input type="hidden" id="cCardNumber" name="cCardNumber" value=""/>
			<input type="hidden" id="cCardNumber2" name="cCardNumber2" value=""/>
                        <input type="hidden" name="cCardBankId" id="cCardBankId"/>
                        <input type="hidden" id="cCardBank" name="cCardBank" value=""/>
                        <input type="hidden" name="Ctype" id="Ctype0" value="" />
                        <input type="hidden" id="cTipoTarjeta" name="cTipoTarjeta" value=""  />
                        <input type="hidden" name="cCargoRecurrent" id="cCargoRecurrent" value="" />
                        <input type="hidden" name="cCardSec" id="cCardSecPay" value="" />
                         
                        <div class="row">
                            <div class="col-md-10 col-lg-10 col-sm-10">
                                <p style="font-family: 'Montserrat-regular';">Introduce informaci&oacuten de la tarjeta</p>
                                <table class="table table-responsive" id="tRegistrarTarjeta">
                                    <tbody class="text-justify">
                                            <tr class="text-center">
                                                <td class="text-center font-weight-bold" style="font-family: 'Montserrat-regular' !important;">Nombre en la tarjeta</td>
                                                <td class="text-center font-weight-bold" style="font-family: 'Montserrat-regular' !important;">Apellido Paterno en la tarjeta</td>
                                                <td class="text-center font-weight-bold" style="font-family: 'Montserrat-regular' !important;">Apellido Materno en la tarjeta</td>
                                                <td class="text-center font-weight-bold" style="font-family: 'Montserrat-regular' !important;">N&uacutemero de la tarjeta</td>
                                                <td class="text-center font-weight-bold" style="font-family: 'Montserrat-regular' !important;">Fecha de vencimiento</td>
                                                <td class="text-center font-weight-bold" style="font-family: 'Montserrat-regular' !important;">CVV</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td><input type="text" name="cCardname" id="cCardname0" value="" maxlength="25" /></td>
                                                <td><input type="text" name="cCardLastname" id="cCardLastname0" value="" maxlength="25" /></td>
                                                <td><input type="text" name="cCardLastname1" id="cCardLastname1" value="" maxlength="25" /></td>
                                                <td><input type="text" name="cCardNumberhdn2" id="cCardNumberhdn2" value="" maxlength="16" onblur="$.validaTarjeta()" /></td>
                                                <td>
                                                    <select name="expMonth" id="expMonth">
                                                        <option value="">-</option>
                                                            <% for(int i=1; i<=12; i++){ %>
                                                            <option value="<%=String.format("%02d",i)%>"><%=String.format("%02d",i)%></option>
                                                            <% } %>
                                                    </select>
                                                    <% SimpleDateFormat formato = new SimpleDateFormat("yy");
                                                     String year= formato.format(new Date());
                                                     int yearMax=Integer.parseInt(year)+9;
                                                     %>
                                                    <select name="expYear">
                                                        <option value="">-</option>
                                                            <% for(int i=Integer.parseInt(year); i<=yearMax;i++ ){ %>
                                                            <option value="<%=i%>"><%=i%></option>
                                                            <%}%>
                                                    </select>
                                                </td>
                                                <td><input type="text" name="cCardSecTemp" id="cCardSec" value="" size="5" maxlength="4" /></td>
                                            </tr>
                                        </tbody>
                                </table>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>
                                            <%String montoPagar2 = "";
                                            String monedaTemp2 = balanceVO.getSaldoActual().replace("$","");
                                            String moneda2 = monedaTemp2.replace(",","");
                                            montoPagar2 = moneda2.substring(0, 4);%>
                                            <input type="text" name="amm" id="amm" value="<%=montoPagar2%>" class="text-center font-weight-bold" style="border: none;color: #1F9BC7;font-family: 'Montserrat-bold';font-size: 17px;" />
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <input style="background-color: #8246AF;border: none;color: white;" class="btn btn-primary btn-block font-weight-bold" type="button" value="Agregar tarjeta" onclick="<% 
                                        boolean pagoHoy= VerificaUsuario.verificaPagoHoy(usuarioVo.getEmployeenumber());
                                            if(pagoHoy){
                                                out.print("if(confirm('El d&iacute;a de hoy ya realizaste un pago.\\n Deseas continuar?'))");    
                                            }                                
                                        %>
                                        $.SubmitForm('paymentForm')" id="PagoNuevaTDC" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                
                <!--Derecha-->
                <div class="col-md-5 col-lg-5 col-sm-5" id="izquierda">
                   <div class="row p-4 text-center"  >
                        <!--bienvenido -->
                        <div class="d-block d-sm-none texto col-12" >
                            <p class="fontbold16gris" style="font-size: 20px;">Bienvenid@</p>
                            <p class="fontbold16gris"><%=usuarioVo.getName() + " " + usuarioVo.getLastname()%></p>
                        </div>
                    
                        <!--banner-->
                        <div id="carouselExampleControls" class="carousel slide col-12 py-3" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="<%=request.getContextPath()%>/img/EstadoCta/banner.png" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="<%=request.getContextPath()%>/img/EstadoCta/banner.png" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="<%=request.getContextPath()%>/img/EstadoCta/banner.png" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    
                    
                        <div class="d-block d-sm-none col-12 py-3" style="padding-right: 0px">
                            <button type="button" class="btn botonpago">
                                 <img align="left" src="<%=request.getContextPath()%>/img/Menus/pesos.png"></img>
                                 <label style="margin-top: 0.5rem" >Pagar <%=balanceVO.getSaldoActual()%></label>
                            </button>
                        </div>
                        
                       <div class="d-block d-sm-none col-12">
                           <img src="<%=request.getContextPath()%>/img/Menus/face_mini.png">&nbsp;<label>
                           <%=statussaldo%></label>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    <div class="modal" id="myModalMsj" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="msjTitle"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="msjBody" style="color: black !important;">
                </div>
                <div class="modal-footer" id="msjFooter" style="border-top: none;">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/factura.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/pagos.js"></script>
    <jsp:include page="/jsp/footerLoged.jsp"/>
    
    <script>
    $('#cCardSec').keyup(function (e) { 
        var status=false;
        if(!e.ctrlKey) {
            status = numberOnly(e.which);
        } else { 
            status=  false; 
        }
        return status; 
    });
    
    $('#cCardSec').keyup(function (e) { 
        var status=false;
        if(!e.ctrlKey && !e.shiftKey && !e.altKey) {
            status = numberOnly2(e.which);
            if(status)
               enmascaraCVV2();
        } else { 
                status=  false; 
        }
        return status; 
    });
                
    $('#cCardNumberhdn2').keyup(function (e) { 
        var status=false;
        if(!e.ctrlKey && !e.shiftKey && !e.altKey) {
            status = numberOnly2(e.which);
            if(status)
                enmascara5();
        } else { 
                status=  false; 
        }
        return status; 
    });
    
    function numberOnly(valor){
        val=false;
        
        switch(valor){
        
            case 8:	//backspace
            case 45:	// insert
            case 46:	// delete
            case 0:
                val=true;
                break;
            default:
                if (valor >= 48 && valor <= 57) {		// numbers
                    val=true;
                }  else if (valor >= 97 && valor <= 122) {
                   // val=true;
                }
        }
        return val;
    }

    function numberOnly2(valor){
        val=false;
        switch(valor){
            case 186:
            case 180:	// acute
            case 225:	// a acute
            case 233:	// e acute
            case 237:	// i acute
            case 243:	// o acute
            case 250:	// u acute
            case 241:	// n tilde
            case 193:	// A acute
            case 201:	// E acute
            case 205:	// I acute
            case 211:	// O acute
            case 218:	// U acute
            case 209:	// N tilde
                val=false;
                break;
            case 8:	//backspace
            case 9:	//tab
            case 45:	// insert
            case 46:	// delete
            case 0:
                val=true;
                break;
            default:
                if (valor >= 48 && valor <= 57) {		// numbers
                    val=true;
                }  else if (valor >= 96 && valor <= 122) {
                    val=true;
                }break;
        }
       
        return val;
    }
    
    function enmascara5(){
        setTimeout(EnmascaraV2('cCardNumberhdn2','cCardNumber',true),10);
    }
    
    function EnmascaraV2(CampoMask,CampoHidd,bolDes){
        var iniAnt, mskCar;
        var tempBull = "1999";
        var objCMask = $('#'+CampoMask);
        var objCHidd = $('#'+CampoHidd);
        var tempValIni = "";
        var tempValFin = "";
        var LenghtMsk = objCMask.val().length;
        var CaulBol = bolDes ? LenghtMsk : (LenghtMsk - 1);
        var tamMask = objCMask.attr("maxlength")>4?((objCMask.attr("maxlength"))-4):2000000;
        for (x=0;x<LenghtMsk;x++){
            mskCar = objCMask.val().charAt(x);
            iniAnt = objCHidd.val().charAt(x);
            if (mskCar!=unescape('%u0FBF')){
                tempValIni+=mskCar;
                if (x<tamMask && x != CaulBol)
                    tempValFin += unescape('%u0FBF');
                else
                    tempValFin += mskCar;
                } else {
                    tempValIni+=iniAnt;
                    if (iniAnt!="") {
                        if (x<tamMask)
                        tempValFin += unescape('%u0FBF');
                        else
                        tempValFin += iniAnt;
                    }
                }
            }
        objCHidd.val(tempValIni);
        objCMask.val("");
        objCMask.val(tempValFin);
        eval('det_' + CampoMask + ' = setTimeout(\'EnmascaraV2("' + CampoMask +'","' + CampoHidd + '",true);\',' + tempBull + ');');
        eval('clearTimeout(det_' + CampoMask + ');');
        //console.log('Enmascara!');
    }
    
    function enmascaraCVV2(){
        setTimeout(EnmascaraV2('cCardSec','cCardSecPay',true),10);
    }
            
    entrar();
    function entrar(){
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','loginSerEvent']);     
    }        
    </script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/settings.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/aes.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/pbkdf2.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/AesUtil.js"></script>
  </body>
  <% } %>
</html>