<!DOCTYPE html>
 <%@ page import="com.totalplay.utils.FileProperty"%>

<html>

<head>
        <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-7618852-11');
        </script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
        <meta HTTP-EQUIV="Expires" CONTENT="-1"/>
	<title>Mi Cuenta Totalplay</title>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/icn/favicon.ico">
        <link rel="apple-touch-icon" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-114x114.png">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style2.css" media="screen" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/nivo-slider.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/themes/default/default.css" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/fancybox2/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/auth-buttons.css" media="screen" />
	<meta name="description" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui-1.9.0.custom.min.js" ></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/aes-enc.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/sha1.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/base64.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/BigInt.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/Barrett.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/RSA.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/aes.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/pbkdf2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/lib/AesUtil.js"></script>
        
	<script type="text/javascript" src="<%=request.getContextPath()%>/fancybox2/jquery.fancybox.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.nivo.slider.js" type="text/javascript"></script>
	<script type="text/javascript"  src="<%=request.getContextPath()%>/js/settings.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/core.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/eventos.js"></script>

	
        <% 
        HttpSession sesion= request.getSession(false);
        String error=  (String)sesion.getAttribute("tipoError");   
         if(sesion!=null && sesion.getAttribute("usuario")!=null ){
            request.getRequestDispatcher("loged.jsp").forward(request, response);
         } 
        %>

        <script>
        $(document).ready(function () {
             mensaje('<%=error%>');
	});        
        </script>            
        <!-- Cxense script begin -->
        <script type="text/javascript">
            var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
            cX.callQueue.push(['cint', '200']);
            cX.callQueue.push(['sendPageViewEvent']);
            // one time initialization for all sendEvent calls
            cX.callQueue.push(['setEventAttributes', 
            { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
            </script>
            <script type="text/javascript">
            (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
            e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
            t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
        </script>        
        <!-- Cxense script end -->            
</head>
<body style=" background-image:url(<%=request.getContextPath()%>/assets/txture001.png); background-repeat:repeat;">
<div id="backgrounds">

	<div id="container">
		<div id="header">
			<a href="http://www.totalplay.com.mx/" target="_blank" id="headerlogo"></a>
                            <div id="login_home">
				<form action="<%=request.getContextPath()%>/loginser.html" method="POST" id="loginform" autocomplete="off" >
                                   
					<input type="hidden" id="secdat" name="secdat" value="" />

						<table style="background-color:#000000; width:auto;">
							<tr>
								<td style="padding:0px 10px 0px 0px; width: 350px; font-weight:normal; white-space:nowrap;">
									<label for="iUser">Para ingresar a tu cuenta totalplay por favor ingresa tu e-mail,<br /> n&uacute;mero de cuenta o tel&eacute;fono de Totalplay a 10 d&iacute;gitos</label>
								</td><td style="padding:0px; width: 280px; font-weight:normal; white-space:nowrap;">
									<input type="text" id="iUser" name="iUser" class="text" value="" placeholder="No. de cuenta o e-mail o tel&eacute;fono." title="Para ingresar a tu cuenta totalplay por favor ingresa t&uacute; e-mail o n&uacute;mero de cuenta o tel&eacute;fono de Totalplay a 10 d&iacute;gitos" />
                                                                        
								</td>
                                                                <td> 
                                                                </td>
							</tr>
							<tr>
								<td style="padding:0px 10px 0px 0px; width: 350px; font-weight:normal; text-align:right">
									<label for="iPwd" style="float:right;">Ingresa tu contrase&ntilde;a:</label>
								</td><td style="padding:0px; width: 280px; font-weight:normal; white-space:nowrap;">
									<input type="password" id="iPwd" name="iPwd" class="text" value="" placeholder="Contrase&ntilde;a" title="Ingresa tu contrase&ntilde;a" autocomplete="off" /><input type="submit" id="createlogin" value="Entrar" style="background-color:#B2B1B2 !important; color: #000 !important; font-size:1.0em !important; height:26px !important; padding:4px 9px !important; margin:4px !important; cursor:pointer !important;" />
								</td>
							</tr>
							<tr>
                                                            <td colspan="2" style="font-size:1.0em !important; text-align:right;">
                                                                    <div style="text-align: right; width: 450px; line-height:24px !important; float: right; margin-right: 25px;">
                                                                            &iquest;No tienes cuenta? Reg&iacute;strate y obt&eacute;n tu contrase&ntilde;a <a href="<%=request.getContextPath()%>/jsp/create.jsp" id="create">aqu&iacute;</a>. Olvid&eacute; mi <a href="<%=request.getContextPath()%>/jsp/remember.jsp" id="remember">contrase&ntilde;a</a>.
                                                                    </div>
                                                            </td>
                                                        </tr>
						</table>
				</form>
			</div>
		</div>
		<div id="div1"></div>



    <div id="content">	
        <div id="contents">
            <div id="lat1"><div id="lat2">
                <div style="width: 830px; margin-left: 30px; background-image:url(<%=request.getContextPath()%>/assets/BANNER-HOME-MI-CUENTA.jpg); background-repeat:no-repeat;">
                <div id="contenidos_bg">
                    <div id="contenidos">
                        <div style="float:left; margin-right:10px; margin-left:400px; margin-top:80px; text-align:center; color:#002741;">
                            <div>
                                <p><span style="color:#4E4E4E; font-style:italic; font-size:1.2em;">Estimado Cliente,</span>
                                  </p>
                                <p><span style="font-family:arial,helvetica,sans-serif; padding:5px; font-size:0.8em;"><b>Mi cuenta Totalplay</b> es un portal web donde podr&aacute;s:
                                  </span><br>
                                </p>
                                    <p style="font-family:arial,helvetica,sans-serif; font-size:0.8em; text-align:left;">                                    
                                        <ol style="font-family:arial,helvetica,sans-serif; font-size:0.8em; text-align:left; padding: 10px; padding-left:50px;">
                                            <li> Conocer que incluye tu paquete</li>
                                            <li> Consultar y pagar tu saldo</li>
                                            <li> Verificar el detalle de tus estados de cuenta</li>
                                            
                                        </ol>
                                      </p>
				  <p>
                                  <span style="font-family:arial,helvetica,sans-serif; font-size:0.8em; font-weight:lighter;">
                                  Para disfrutar de este portal s&oacute;lo 

                                  <span style="color:#036; font-weight:bolder;">ingresa</span> o <span style="color:#036; font-weight:bolder;">crea tu cuenta</span> en la parte superior. 
                                                                      </span>
                                    </p>
                            </div>
    </div>
    
    
                    </div>
                </div>
                </div>
            </div></div>
        </div>
    </div>
    <div id="footer" class="footer">
             Para una mejor experiencia y visualizaci&oacute;n de la p&aacute;gina te recomendamos tener Internet explorer 7 o superior. <br>
             Todos los derechos reservados TOTALPLAY Telecomunicaciones S.A. de C.V. 2014<br>
             <a href="http://www.totalplay.com.mx/" target="_blank">www.totalplay.com.mx</a> | <a href="https://www.totalplay.com.mx/pdf/aviso-privacidad.pdf" target="_blank">Aviso de privacidad</a></i>
    </div>
        </div>
    </div>
    
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>
    
    <div style="display:none;"><img src="<%=request.getContextPath()%>/assets/loading.gif" alt=""/></div>
    <script type="text/javascript">
            /******************************************* Inicio click entrar********************************************************/
            $('#createlogin').on('click',function(){
                $(this).trigger('iniciaSesion');
            });
            
            $('#createlogin').bind('iniciaSesion',function(){
                    var Cx = window.cX;
                    Cx.callQueue.push(['sendEvent','iniciarSesionMTP']);
            });
        </script>
</body>
</html>