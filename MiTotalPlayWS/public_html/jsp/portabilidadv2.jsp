<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="com.totalplay.vo.UsuarioVO"%>
<%
    HttpSession sesion = request.getSession(false);
    String numCuenta =(sesion.getAttribute("numCuenta") != null)?(String)sesion.getAttribute("numCuenta"):null;   
    

%>
<html>
  <head>
   <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-7618852-11');
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
    <title>portabilidadv2</title>
    <script type="text/javascript">
            $(document).ready(function () {
                $(".tabs-menu a").click(function(event) {
                    event.preventDefault();
                    $(this).parent().addClass("current");
                    $(this).parent().siblings().removeClass("current");
                    var tab = $(this).attr("href");
                    $(".tab-content").not(tab).css("display", "none");
                    $(tab).fadeIn();
                });
                $("#tabTramite").click(function(){
                    $.ocultaArch();
                    $("#resCreaPorta").hide();
                    $("#contentTramite").show();
                });
                $("#tabConsulta").click(function(){
                    $.buscaPortado();
                });
            });
    </script>
   
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/validations.js" ></script>
    <!-- Cxense script begin -->
    <script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
        cX.callQueue.push(['cint', '200']);
        cX.callQueue.push(['sendPageViewEvent']);
        // one time initialization for all sendEvent calls
        cX.callQueue.push(['setEventAttributes', 
        { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
        </script>
        <script type="text/javascript">
        (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
        e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
        t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
    </script>        
    <!-- Cxense script end -->
  </head>
  <body>
    <!--<div id="tabs-container">-->
    <ul class="tabs-menu">
        <li class="current"><a href="#tab-1">Requisitos</a></li>
        <li id="tabTramite"><a href="#tab-2">Iniciar tr�mite</a></li>
        <li><a id="tabConsulta" href="#tab-3">Consultar estatus</a></li>
    </ul>
    <div class="tab">
        <div id="tab-1" class="tab-content">
            <h4><b>Solicitud de portabilidad</b></h4><br/>
            <p>
                Para poder iniciar con tu tr�mite de portabilidad es necesario contar con los siguientes requisitos:
                
                <ul style="padding-left:7%;">
                <li><b>N�mero a portar (Tel�fono que deseas seguir conservando)</b></li>
                <li><b>NIP</b> (Lo obtienes marcando al 051 forzosamente desde la l�nea con la otra compa�ia)</li>
                </ul>
                <p>En caso de que tu l�nea con la otra compa�ia se encuentre cancelada, es necesario contar con los siguientes requisitos:</p>
                <ul style="padding-left:7%;">
                    <li><b>Formato de portabilidad</b> (Descarga formato <a href="<%=request.getContextPath()%>/assets/portabilidad-formato.pdf" target="_blank" style="font-style:italic; color:blue;">aqu�</a>)</li>
                    <li><b>Copia de identificaci�n oficial</b> (INE, CURP, Pasaporte, Licencia para conducir)</li>
                    <li><b>Carta de Cancelaci�n</b> (No debe ser mayor a 40 d�as)</li>
                </ul>
            </p>
            
            <p>S�lo para personas morales <b>Copia de tu Acta Constitutiva o Poder notarial</b> (no es necesario el NIP)</p>
            <p>Si deseas realizar la portabilidad de m�s de un n�mero de tel�fono es necesario ingresar una solicitud por cada l�nea.</p>
            
            <!--<ul style="padding-left:7%;">
            <li><b>Formato de portabilidad</b> (Descarga formato <a href="<%=request.getContextPath()%>/assets/portabilidad-formato.pdf" target="_blank" style="font-style:italic; color:blue;">aqu�</a>)</li>
            <li><b>Copia de identificaci�n oficial</b> (INE, CURP, Pasaporte, Licencia para conducir)</li>                    
            <li><b>NIP</b> (Lo obtienes marcando al 051 forsozamente desde la l�nea con la otra compa��a)</li>
            <li>Si ya cancelaste tu servicio con el proveedor anterior, es necesario contar con la <b>Carta de cancelaci�n</b> (No debe ser mayor a 40 d�as)</li>
            <li>S�lo para personas morales <b>Copia de tu Acta Constitutiva o Poder notarial</b> (no es necesario el NIP)</li>
            </ul>
            </p>
            
            <p>Si deseas realizar la portabilidad de m�s de un n�mero de tel�fono es necesario ingresar una solicitud por cada l�nea.</p>
            -->
        </div>
        <div id="tab-2" class="tab-content">
            <div id="contentTramite">
            <h4><b>Iniciar tr�mite de portabilidad</b></h4><br/>
            <p><b>Datos del cliente</b></p>
            
            <form method="POST" id="formPorta">
                <table id="tblForm" style="width:85%;margin:0 auto 0 auto;">
                    <tbody>
                    <tr>
                        <td style="width:260px !important;"></td>
                        <td style="width:170px !important;"><input type="radio" name="razonSocial" id="rbtnFisica"  value="fisica" checked="checked" onclick="$.ocultaArch()" >Persona f�sica</input></td>
                        <td style="width:165px !important;"><input type="radio" name="razonSocial" id="rbtnMoral"  value="moral" onclick="$.ocultaArch()">Persona moral</input></td>
                        <td style="width:145px !important;"></td>
                    </tr>
                    <tr id="ctnLineaActiva">
                        <td colspan="2"><label>�Tu l�nea telef�nica con el proveedor anterior contin�a activa?</label></td>
                        <td style="text-align:center;"><input type="radio" id="rbtnActiva" name="rbtnActiva" onclick="$.ocultaFechaCancel()" checked="checked">S�</input></td>
                        <td><input type="radio" name="rbtnActiva" onclick="$.ocultaFechaCancel()">No</input></td>
                    </tr>
                     <tr id="ctnFechaCancel">
                        <td colspan="2"><label>�La fecha de cancelaci�n de tu l�nea tiene m�s de 40 d�as?</label></td>
                        <td style="text-align:center;"><input type="radio" id="rbtnFechaCancel" name="rbtnFechaCanc" onclick="$.ocultaCartaCancel()">S�</input></td>
                        <td><input type="radio" id="rbtnFechaCancelNO" name="rbtnFechaCanc"  onclick="$.ocultaCartaCancel()" checked="checked">No</input></td>
                    </tr>
                    <tr id="numCuenta">
                        <td><label>N�mero de cuenta</label></td>
                        <td><label><b><%=numCuenta%></b></label></td>
                                           
                    </tr>
                    <tr id="numPortar">
                        <td><label>N�mero a portar</label></td>
                        <td><input type="text" id="DN_0" name="DN_0" maxlength="10"></input></td>
                        <td><label>Tel�fono de contacto</label></td>
                        <td><input type="text" id="telCont" name="telCont" maxlength="10"></input></td>
                    </tr>
                     <tr id="ctnNIP">
                        <td><label>NIP</label></td>
                        <td><input type="text" id="nip_0" name="nip_0" maxlength="4"></input></td>
                    </tr>
                     <tr id="ctnIdenOf">
                        <td><label>Identificaci�n Oficial</label></td>
                        <td colspan="2"><input type="text" style="width:90%;" name="txtIdenPorta" id="txtIdenPorta"></input></td>
                        <td><label class="btnCargaDoc" style="width:22%;border-radius:2px;padding:5px !important;"><input type="file" id="btnIdenPorta" class="btnCargaArchivos" name="btnIdenPorta" onchange="$.nombreArchivo(this.id)"/> Examinar...</label></td>
                    </tr>
                     <tr id="ctnFormSol">
                        <td><label>Formato de solicitud</label></td>
                        <td colspan="2"><input type="text" style="width:90%;" name="txtSolPorta" id="txtSolPorta"></input></td>
                        <td><label class="btnCargaDoc" style="width:22%;border-radius:2px;padding:5px !important;"><input type="file" id="btnSolPorta" class="btnCargaArchivos" name="btnSolPorta"  onchange="$.nombreArchivo(this.id)"/> Examinar...</label></td>
                    </tr>
                    <tr id="ctnCartaCancel">
                        <td><label>Carta de cancelaci�n</label></td>
                        <td colspan="2"><input type="text" style="width:90%;" name="txtCartaCancel" id="txtCartaCancel"></input></td>
                        <td><label class="btnCargaDoc" style="width:22%;border-radius:2px;padding:5px !important;"><input type="file" id="btnCartaCancel" class="btnCargaArchivos" name="btnCartaCancel"  onchange="$.nombreArchivo(this.id)"/> Examinar...</label></td>
                    </tr>
                    <tr id="ctnActaCons">
                        <td><label>Acta constitutiva o Poder Notarial</label></td>
                        <td colspan="2"><input type="text" style="width:90%;" name="txtActaCons" id="txtActaCons"></input></td>
                        <td><label class="btnCargaDoc" style="width:22%;border-radius:2px;padding:5px !important;"><input type="file" id="btnActaCons" class="btnCargaArchivos" name="btnActaCons"  onchange="$.nombreArchivo(this.id)"/> Examinar...</label></td>
                    </tr>
                    <tr id="ctnMensaje">
                        <td align="center" colspan="4"><br/><br/><p style="font-size:10pt;">Estimado cliente, no es posible realizar tu tr�mite de portabilidad debido a que tu cancelaci�n tiene m�s de 40 d�as.</p></td>
                    </tr>
                    </tbody>
                    <tfoot>
                        <tr >
                            <td align="center" colspan="4"><input type="submit" value="Aceptar" name="btnAceptar" id="btnAceptar" style="height:20px !important;padding:10px;margin:15px !important;"/><input type="reset" value="Limpiar" id="btnLimpia" name="btnLimpia" style="height:20px !important;padding:10px;margin:15px !important;"/></td>
                        </tr>
                    </tfoot>
                </table>
            </form>
            </div>
            <div id="resCreaPorta">
                <div id="confirma" style="text-align: center; width:80%; margin:0 auto 0 auto;font-weight: bold;"><h5>Tu solicitud de portabilidad para la l&iacute;nea <b><span id="numPortadoRes"></span></b> fue enviada satisfactoriamente.</h5> 
                    <h5>Tu folio de seguimiento es:</h5>
                    <p style="color: rgb(65,130,171); font-size:28pt;" id="folioPorta"></p>
                    <h5>A partir de 24 horas h&aacute;biles, puedes ingresar a la opci&oacute;n "Consultar Estatus" para conocer la fecha en que ser&aacute; programada tu portabilidad.</h5><br/>
                    <input type="button" value="Consultar estatus" style="padding:10px;" onclick="generaClickTab();"/>
                </div>
            </div>
        </div>
        <div id="tab-3" class="tab-content">
             <h4><b>Consulta de portabilidad</b></h4><br/>
             <p><b>Tienes registrados los siguientes folios de solicitud de portabilidad:</b></p>
             <br/>
             <table id="consPorta">
                <thead>
                    <tr>
                        <td>Folio</td>
                        <td># Tel. a portar</td>
                        <td>Fecha de solicitud</td>
                        <td>Estatus</td>
                        <td>Observaciones</td>
                        <td>Fecha de portabilidad</td>
                    </tr>
                </thead>
                <tbody id="resFolios">
                </tbody>
             </table>
        </div>
    </div>
<!--</div>-->
  <script type="text/javascript">
    /*******************************************Portabilidad********************************************************/
    $('#btnAceptar').on('click',function(){
        $(this).trigger('iniciaPortabilidad');
    });
    
    $('#btnAceptar').bind('iniciaPortabilidad',function(){
            var Cx = window.cX;
            Cx.callQueue.push(['sendEvent','iniciaPortabilidad']);
    });
  </script>
  
  </body>
</html>