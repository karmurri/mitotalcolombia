<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
        <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-7618852-11');
        </script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Mi cuenta Totalplay</title>
    <link rel="shortcut icon" href="/images/icn/favicon.ico">
    <link rel="apple-touch-icon" href="/images/icn/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/icn/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/icn/apple-touch-icon-114x114.png">
    <link rel="stylesheet" type="text/css" href="/css/style2.css" media="screen" />
    <link rel="stylesheet" href="/css/alert.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <!-- Cxense script begin -->
        <script type="text/javascript">
            var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
            cX.callQueue.push(['cint', '200']);
            cX.callQueue.push(['sendPageViewEvent']);
            // one time initialization for all sendEvent calls
            cX.callQueue.push(['setEventAttributes', 
            { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
        </script>
        <script type="text/javascript">
            (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
            e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
            t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
        </script>        
        <!-- Cxense script end -->
        
</head>
<body>
	<div id="content_pop"><br />
            <%
            String title=request.getAttribute("title")!=null?(String)request.getAttribute("title"):" ";
            String msj=request.getAttribute("msj")!=null?(String)request.getAttribute("msj"):" ";
            if(!msj.isEmpty()){%>
                <h1><%=title%></h1><br/>
                <h2><%=msj%></h2><br />       
            <%}%>
        </div>
	
</body>
</html>
