<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.totalplay.vo.DetalleCompraVO"%>
<%@ page import="org.apache.logging.log4j.LogManager"%>
<%@ page import="org.apache.logging.log4j.Logger"%>
<html>
    <%
    Logger logger = LogManager.getLogger(this.getClass().getName());
    HttpSession login = request.getSession(false);
    DetalleCompraVO detalleCompra = (login.getAttribute("detalleCompra") != null)?(DetalleCompraVO)login.getAttribute("detalleCompra"):null;
    %>
    <head>
        <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-7618852-11');
        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
        <title>PlanClienteVista</title>
        <!-- Cxense script begin -->
        <script type="text/javascript">
            var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
            cX.callQueue.push(['cint', '200']);
            cX.callQueue.push(['sendPageViewEvent']);
            // one time initialization for all sendEvent calls
            cX.callQueue.push(['setEventAttributes', 
            { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
            </script>
            <script type="text/javascript">
            (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
            e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
            t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
        </script>        
    <!-- Cxense script end -->
    
    </head>
    <body >
        <% if(detalleCompra.getMesesFinanciados() != null && detalleCompra.getMontoMensual() != null && 
        detalleCompra.getNoContrato() != null && detalleCompra.getNombre() != null && 
        detalleCompra.getTotal() != null){%>
        
        
        <div style="width:95%;">
                      <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                          <% if(detalleCompra.getNombre() != null && detalleCompra.getNombre().length() != 0){%>
                              <h5 style="color:#9D0C18; font-weight: bold;"><%=detalleCompra.getNombre()%></h5>
                          <% }%>
                        </div>  
                      </div>
                        <div class="row">
                          <div class="col-sm-1 col-sm-offset-3">
                              <img src="<%=request.getContextPath()%>/img/miscompras/Logo_Elektra.png">
                          </div>
                          <div class="col-sm-2" style="">
                              <div class="row" style="">
                                  <div class="col-sm-3" style="font-size:12px;">
                                      <span>Monto Mensual:</span>
                                  </div>
                                  <div class="col-sm-3" style="">
                                  <% if(detalleCompra.getMontoMensual() != null && detalleCompra.getMontoMensual().length() != 0){%>
                                      <span><strong>$ <%=detalleCompra.getMontoMensual()%></strong></span>
                                  <% } %>
                                  </div>
                              </div>
                          </div>
                          <div class="col-sm-2" style="">
                              <div class="row" style="">
                                  <div class="col-sm-3" style="font-size:  12px;">
                                      <span>Meses:</span>
                                  </div>
                                  <div class="col-sm-3" style="font-size:  12px;">
                                  <% if(detalleCompra.getMesesFinanciados() != null && detalleCompra.getMesesFinanciados().length() != 0){%>
                                      <span><strong><%=detalleCompra.getMesesFinanciados()%></strong></span>
                                  <% } %>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row" style="margin-bottom:10px;">
                        <div class="col-sm-2 col-sm-offset-4" style="">
                            <div class="row" style="">
                                <div class="col-sm-3">
                                    <span>Total:</span>
                                </div>
                                <div class="col-sm-3" style="">
                                <% if(detalleCompra.getTotal() != null && detalleCompra.getTotal().length() != 0){ %>
                                    <span><strong>$ <%=detalleCompra.getTotal()%></strong></span>
                                <% } %>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="row" style="">
                                <div class="col-sm-3">
                                    <span>Contrato:</span>
                                </div>
                                <div class="col-sm-3">
                                <% if(detalleCompra.getNoContrato() != null && detalleCompra.getNoContrato().length() != 0){ %>
                                    <span><strong><%=detalleCompra.getNoContrato()%></strong></span>
                                <% } %>
                                </div>
                            </div>
                        </div>
                      </div>                      
                  </div>
              
        <% }else{ %>
        <div>
            <div style="color: #002841; font-weight:bold; margin:0 auto; float:none; display:table; padding-bottom:10px; font-size:16px;">
                <span>No hay información que mostrar</span>
            </div>
        </div>
        <% } %>
    </body>
</html>