<!doctype html>
<html>
<%@ page import="com.totalplay.vo.UsuarioVO"%>
<%@page import="org.owasp.encoder.Encode"%>
<%
    HttpSession login = request.getSession(false);
    String error="";
    UsuarioVO usuario =(login.getAttribute("usuario") != null)?(UsuarioVO)login.getAttribute("usuario"):null;
    
    if(usuario == null){
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }
    
%>

<head>
<!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-7618852-11');
</script>
<meta charset="UTF-8">
<title>Totalplay - Configuraci&oacute;n de WiFi</title>
<meta name="viewport" content="width=device-width, user-scalable=no">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/redwifi/reset.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/css/redwifi/main.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/redwifi/modal.css" />

<!--script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script--> 
<script src="<%=request.getContextPath()%>/js/jquery-3.3.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/factura.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/eventos.js"></script>
<!-- Cxense script begin -->
<script type="text/javascript">
    var cX = cX || {}; cX.callQueue = cX.callQueue || [];
    cX.callQueue.push(['setSiteId', '1131772398957904842']);// 1131772398957904842
    cX.callQueue.push(['cint', '200']);
    cX.callQueue.push(['sendPageViewEvent']);
    // one time initialization for all sendEvent calls
    cX.callQueue.push(['setEventAttributes', 
    { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
    </script>
    <script type="text/javascript">
    (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
    e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
    t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
</script>        
<!-- Cxense script end -->
</head>

<body>
<div class="allCont">
  <div class="bannerBox" style="background-image:url(<%=request.getContextPath()%>/img/redwifi/miCuentaBanner.jpg)"></div>
  <header>
    <div class="backMenu"></div>
  </header>
  <div class="pageCont">
    <div class="infoPage">
      <div class="totalplayBrand"><img src="<%=request.getContextPath()%>/img/redwifi/Totalplay-white.svg" width="100%" height="100%"></div>
      <div class="nameUser">Bienvenid@ <%=usuario.getName()+ " "  + usuario.getLastname()%><br><span><a href="<%=request.getContextPath()%>/jsp/loged.jsp" target="_self" style="color:inherit;">Volver a Mi Cuenta</a></span></div>
    </div>
    <div class="boxTitle">
      <div class="rowBrand">
        <div class="wifiTotalBrand"><img src="<%=request.getContextPath()%>/img/redwifi/wifiTotalBrand.svg" width="100%" height="100%"></div>
        <div class="textBox">
          <h3>Configura los ajustes de</h3>
          <h1>Mi Red WiFi</h1>
          <h2>en la nube</h2>
        </div>
    </div>
    </div>
    <div class="cardData"> 
      <div class="rowInfo">
        <div class="formColumn">
          <div class="contInputAddPop">
            <div class="titleInputBox">Nombre de red (SSID)</div>
            <div class="inputTextCont">
                <input id="iSSID" class="textLineInput" name="textLineInput" type="text" placeholder="Nombra tu red" value="<%=Encode.forHtmlAttribute( login.getAttribute("ssid").toString() )%>"></input>                           
            </div>
            <div  id="dErrorSSID" class="hintErrorTextInputHide">* Requerido</div>  <!--  hintErrorTextInput / hintTextInput /  hintErrorTextInputHide -->                  
          </div>
        </div>
        <div class="formColumn">
          <div class="contInputAddPop"><p>M&iacute;nimo 8, m&aacute;ximo 20 caracteres. No uses caracteres especiales. Podr&aacute;s cambiarlo cuantas veces quieras.</p></div>
        </div>
        </div>
        <div class="rowInfo">
        <div class="formColumn">
          <div class="contInputAddPop">
            <div class="titleInputBox">Tipo de seguridad</div>
            <div class="inputTextCont">
              <select id="sSeguridad" onchange="$.changeSecurity($('#sSeguridad').val())">
                    <option>Selecciona tu tipo de seguridad </option>
                    <option value="WPAand11i">WPA/TKIP + WPA2/AES</option>
                    <option value="WPA">WPA/TKIP</option>
                    <option value="11i">WPA2/AES</option>
              </select>
            </div>
            <div id="dErrorSeguridad" class="hintErrorTextInputHide">* Requerido</div>  <!--  hintErrorTextInput / hintTextInput /  hintErrorTextInputHide -->                  
          </div> 
        </div>
        <div class="formColumn">
        <div class="contInputAddPop" id="security"></div>
          
          
        </div>
        </div>
        <div class="rowInfo">
        <div class="formColumn">
          <div class="contInputAddPop">
            <div class="titleInputBox">Contrase&ntilde;a</div>
            
            <div class="inputTextCont hide" id="divPwdFake">
                <input id="iContrasenaFake" class="passLineInput" type="text" placeholder="Contrase&ntilde;a" value="<%=Encode.forHtmlAttribute( login.getAttribute("pwd").toString() )%>" ></input>                           
            </div>
            
            <div class="inputTextCont" id="divPwd">
                <input id="iContrasena" class="passLineInput" name="passLineInput" type="password" placeholder="Contrase&ntilde;a" value="<%=Encode.forHtmlAttribute( login.getAttribute("pwd").toString() )%>" ></input>                           
            </div>
            
            <div class="showOff" id="showPwd"></div>
            <div class="colorPassword" style="background-color: #bedc32"></div>  <!--   Color de criterios de seguridad de password  /  Ok = #bedc32 -->
            <div id="dErrorContrasena" class="hintErrorTextInputHide">* Requerido</div>  <!--  hintErrorTextInput / hintTextInput /  hintErrorTextInputHide -->                  
          </div> 
        </div>
        <div class="formColumn">
          <div class="contInputAddPop"><p>M&iacute;nimo 10 caracteres, m&aacute;ximo 15 caracteres. Recuerda incluir al menos una min&uacute;scula,una may&uacute;scula y un n&uacute;mero.</p></div>
        </div>
        </div>
        <div class="rowInfo">
        <div class="formColumn">
          <div class="contInputAddPop">
            <div id="sincronizarRed" class="buttonOff" onclick="$.sincronizarRed()">  <!--  buttonOff  /  buttonDisable -->
              Sincronizar
              <div class="iconWifi"><img src="<%=request.getContextPath()%>/img/redwifi/syncWiFi.svg" width="100%" height="100%"></div>
            </div>
          </div>             
        </div> 
        <div class="formColumn">
          <div class="contInputAddPop"><p id="rService">Al sincronizar unificar&aacute;s el usuario y contrase&ntilde;a de todos tus decodificadores y m&oacute;dem para acceder a tu red Wifi.</p></div>
        </div>               
      </div>  <!--  rowInfo  -->
    </div>
  </div>
</div>  <!--  allCont  -->

<div class="modal fade" id="myModalLoading">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Espere un momento por favor...</h4>
            </div>
            <div class="modal-body" style="text-align:center;">
                <img src="<%=request.getContextPath()%>/assets/loading.gif" alt=""/>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalMsj">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="msjTitle"></h4>
            </div>
            <div class="modal-body" id="msjBody">
            </div>
            <div class="modal-footer" id="msjFooter">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
    $("#iContrasena").on("input", function() {
        $("#iContrasenaFake").val(this.value);
    });
    
    $("#iContrasenaFake").on("input", function() {
        $("#iContrasena").val(this.value);
    });
    
       $("#showPwd").mousedown(function(){
                $('#divPwd').addClass("hide");
                $('#iContrasenaFake').val($('#iContrasena').val());
                $('#divPwdFake').removeClass("hide");
            }).mouseup(function(){
                $('#divPwdFake').addClass("hide");
                $('#divPwd').removeClass("hide");
        });
        
        var secIncoming = '<%=Encode.forHtml( login.getAttribute("sec").toString() )%>';
        if(secIncoming.length>0){
           $.changeSecurity(secIncoming);
           $('#sSeguridad').val(secIncoming)
        }else{
           $.changeSecurity("WPAand11i");
           $('#sSeguridad').val("WPAand11i")
        }
        
    });
    
    function getContextPath() {
        //return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
        return "";
    }
    
    $(document).ajaxStart(function(){
        $("#myModalLoading").modal("show"); 
    }).ajaxStop(function(){
        $("#myModalLoading").modal("hide");
    });
    
    $.changeSecurity=function(id){ 
        $("#security").html('');
        if(id!== null && id !== ''){
            if(id==="WPAand11i"){
                $("#security").html('Son dos tipos de seguridad que pueden ser utilizados por una red WiFi. Disponibles simult&aacute;neamente, en caso de que un dispositivo tenga incompatibilidad con WPA2 utiliza la versi&oacute;n WPA.');
            }
            
            if(id==="WPA"){
                $("#security").html('Tipo de  seguridad que puede ser utilizada por una red WiFi. Es poco segura pero compatible con dispositivos antiguos que no soportan WPA2.');
            }
            
            if(id==="11i"){
                $("#security").html('Tipo de seguridad que puede ser utilizada por una red WiFi. Es muy segura y compatible con la mayor&iacute;a de los dispositivos.');
            }
        }
    }
    
    $.sincronizarRed=function(){
         $("#dErrorContrasena").html('');
         $("#dErrorSSID").html('');
         $("#dErrorSeguridad").html('');
         $("#dErrorContrasena").removeClass("hintErrorTextInput" ).addClass("hintErrorTextInputHide");
         $("#dErrorSSID").removeClass("hintErrorTextInput" ).addClass("hintErrorTextInputHide");
         $("#dErrorSeguridad").removeClass("hintErrorTextInput" ).addClass("hintErrorTextInputHide");
    
    
        function llegadaDatos(datos){
            console.log(datos);
            $('#rService').html('');
            if(datos.trim() === 'ok' || datos.trim() === "ok"){ 
                $('#msjTitle').html("<strong style='font-weight:bold'>Mi Totalplay<\/strong>");
                $('#msjBody').html("<br />Se guard&oacute; exitosamente la configuraci&oacute;n elegida.<br/>");
                $('#msjFooter').html(" ");
                $("#myModalMsj").modal("show");
                
            } else if(datos.trim() === 'nsupport' || datos.trim() === "nsupport"){
                $('#rService').html('Ocurri&oacute; un error en el proceso de sincronizaci&oacute;n. Por favor, comun&iacute;cate a nuestro centro de atenci&oacute;n a clientes para atender tu solicitud <strong style=\'font-weight:bold\'>(55) 1579-8000<\/strong><br /><br /> <strong style=\'font-weight:bold\'>Codigo de error: -200<\/strong>');
                $('#rService').css('color', 'red');
            } else if(datos.trim() === 'nmigrate' || datos.trim() === "nmigrate"){
                $('#rService').html('Ocurri&oacute; un error en el proceso de sincronizaci&oacute;n. Por favor, comun&iacute;cate a nuestro centro de atenci&oacute;n a clientes para atender tu solicitud <strong style=\'font-weight:bold\'>(55) 1579-8000<\/strong><br /><br /> <strong style=\'font-weight:bold\'>Codigo de error: -100<\/strong>');
                $('#rService').css('color', 'red');
            } else {
                $('#rService').html('Ocurri&oacute; un error en el servicio intente m&aacute;s tarde .<br /><br /> <strong style=\'font-weight:bold\'>Codigo de error: -300<\/strong>');
                $('#rService').css('color', 'red');
            } 
             
        }
        
        function problemas(xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            console.log("Errores al sincronizar ONT");
        }
        
        var ssid = $("#iSSID").val();
        var seguridad = $("#sSeguridad").val();
        var contrasena = $("#iContrasena").val();
        var ssidError = "";
        var seguridadError = "";
        var contrasenaError = ""; 
        var flagSSID = false;
        var flagSeguridad = false;
        var flagPassword = false;
        
        var regSSID = new RegExp(/^[A-Za-z0-9]+$/);    
         
        if(ssid !== null && ssid !== '' && ssid.length >= 8){
            if(ssid.length <= 20){
                if(regSSID.test(ssid)){
                    flagSSID = true;
                } else {
                    //ssidError = "* No estan permitidos caracteres especiales, solo letras y numeros";
                    ssidError = "* M&iacute;nimo 8,m&aacute;ximo 20 caracteres. No uses caracteres especiales. Podr&aacute;s cambiarlo cuantas veces quieras.";
                    flagSSID = false;
                }
            } else{
                //ssidError = "* La longitud m&aacute;xima es de 20 caracteres";
                ssidError = "* M&iacute;nimo 8, m&aacute;ximo 20 caracteres. No uses caracteres especiales. Podr&aacute;s cambiarlo cuantas veces quieras.";
                flagSSID = false;
            }
        } else {
            //ssidError = "* Requerido";
            ssidError = "* M&iacute;nimo 8, m&aacute;ximo 20 caracteres. No uses caracteres especiales. Podr&aacute;s cambiarlo cuantas veces quieras.";
            flagSSID = false;
        }
        
        
        if(seguridad === "WPA" || seguridad === "11i" || seguridad === "WPAand11i"){ 
            flagSeguridad = true;
        } else {
            seguridadError = "* Requerido";
            flagSeguridad = false;
        }
        
        if(contrasena !== null && contrasena !== '' && contrasena.length >= 10){
            if(contrasena.length <= 15){
                if(regSSID.test(contrasena)){ 
                    if(contrasena.match(/[A-Z]/) !== null && contrasena.match(/[a-z]/) !== null && contrasena.match(/[0-9]/) !== null) {
                        flagPassword = true;
                    }else{
                        //contrasenaError = "* Debes incluir almenos una may&uacute;scula y un n&uacute;mero";
                        contrasenaError = "* M&iacute;nimo 10 caracteres, m&aacute;ximo 15 caracteres. Recuerda incluir al menos una min&uacute;scula,una may&uacute;scula y un n&uacute;mero.";
                        flagPassword = false;
                    }
                } else {
                    //contrasenaError = "* No estan permitidos caracteres especiales, solo letras y numeros";
                    contrasenaError = "* M&iacute;nimo 10 caracteres, m&aacute;ximo 15 caracteres. Recuerda incluir al menos una min&uacute;scula,una may&uacute;scula y un n&uacute;mero.";
                    flagPassword = false;
                }
            }else{
                //contrasenaError = "* La longitud m&iacute;nima es de 10 caracteres";
                contrasenaError = "* M&iacute;nimo 10 caracteres, m&aacute;ximo 15 caracteres. Recuerda incluir al menos una min&uacute;scula,una may&uacute;scula y un n&uacute;mero.";
                flagPassword = false;
            }
        } else {
            //contrasenaError = "* Requerido";
            contrasenaError = "* M&iacute;nimo 10 caracteres, m&aacute;ximo 15 caracteres. Recuerda incluir al menos una min&uacute;scula,una may&uacute;scula y un n&uacute;mero.";
            flagPassword = false;
        }
        
        if(flagSSID === true && flagSeguridad === true && flagPassword === true){
            //SE QUITA PARA COLOMBIA*/
            /*$.ajax({
                cache:false,
                async:true,
                type: "POST",
                dataType: "html", 
                url: getContextPath() + '/redwifiser.html',
                data:{
                    "ssid" : ssid,
                    "seguridad" : seguridad,
                    "contrasena" : contrasena
                },
                success:llegadaDatos,
                error: problemas,
                timeout:1000000
            });*/ 
        }else{
            if(contrasenaError !== ""){
                $("#dErrorContrasena").html('');
                $("#dErrorContrasena").removeClass("hintErrorTextInputHide" ).addClass("hintErrorTextInput");
                $("#dErrorContrasena").html(contrasenaError);
            }
            
            if(ssidError !== ""){
                $("#dErrorSSID").html('');
                $("#dErrorSSID").removeClass("hintErrorTextInputHide" ).addClass("hintErrorTextInput");
                $("#dErrorSSID").html(ssidError);
            }
            
            if(seguridadError !== ""){
                $("#dErrorSeguridad").html('');
                $("#dErrorSeguridad").removeClass("hintErrorTextInputHide" ).addClass("hintErrorTextInput");
                $("#dErrorSeguridad").html(seguridadError);
            }
        }
    }



</script>
</body>
</html>