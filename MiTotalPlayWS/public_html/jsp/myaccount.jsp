<!DOCTYPE html>


<%@ page import="com.totalplay.vo.UsuarioVO"%>
<%@ page import="com.totalplay.utils.Constantes"%>
<%
HttpSession login = request.getSession(false);
  String error="";
 UsuarioVO usuarioVo =(login.getAttribute("usuario") != null)?(UsuarioVO)login.getAttribute("usuario"):null;
 if(usuarioVo == null){
    //response.sendRedirect("jsp/home.jsp"); 
    request.getRequestDispatcher("jsp/home.jsp").forward(request, response);
}else{
 %>
<html>
<head>
         <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-7618852-11');
        </script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Mi cuenta Totalplay</title>
    <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/icn/favicon.ico">
    <link rel="apple-touch-icon" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-114x114.png">
    
   
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/alert.css" type="text/css" media="screen" />
     <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style2.css" media="screen" />
     <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui-1.9.0.custom.min.js" ></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.min.js" ></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.maskedinput.js" ></script>
      <script type="text/javascript" src="<%=request.getContextPath()%>/fancybox2/jquery.fancybox.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/aes-enc.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/sha1.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/base64.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/BigInt.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/Barrett.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/RSA.js"></script>
        <script type="text/javascript"  src="<%=request.getContextPath()%>/js/settings.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/core2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/eventos.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/updateaccount.js"></script>
        
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>

        
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
            <!-- Cxense script begin -->
        <script type="text/javascript">
            var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
            cX.callQueue.push(['cint', '200']);
            cX.callQueue.push(['sendPageViewEvent']);
            // one time initialization for all sendEvent calls
            cX.callQueue.push(['setEventAttributes', 
            { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
            </script>
            <script type="text/javascript">
            (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
            e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
            t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
        </script>        
        <!-- Cxense script end -->
</head>

<body style="">
        <div id="content_pop"><br />
            <h1 style="color: #002841 !important; padding:0px !important; margin:0px !important; font-size:28px !important; font-weight:600 !important;">Cambiar Mi Contrase&ntilde;a</h1><br />
                <div>
			Cambia tu contrase&ntilde;a de acceso a Mi Cuenta Totalplay. Tu nueva contrase&ntilde;a deber&aacute; de incluir letras en may&uacute;sculas y min&uacute;sculas, n&uacute;meros y por lo menos uno de los siguientes signos: <i>. - $</i> &oacute; <i>@</i>.<br /><br />	
                            <form method="post"  autocomplete="off" id="formmyaccount">
                                <div style="display:block; text-align:left; ">										
                                    <span>
                                        <label for="username" style="text-align:right; width:250px;">Nombre: </label><input type="text" id="username" name="username" value="<%=usuarioVo.getName()%> " style="border:#ccc 1px solid; color:#999 !important;" maxlength="25" readonly disabled />
                                    </span><br /><br />
                                    <span>
                                        <label for="lastname" style="text-align:right; width:250px;">Apellidos</label><input type="text" id="lastname" name="lastname" value="<%=usuarioVo.getLastname()%>" style="border:#ccc 1px solid; color:#999 !important;" maxlength="25" readonly disabled />
                                    </span><br /><br />
                                    <span>
                                        <label for="password" style="text-align:right; width:250px;">Nueva contrase&ntilde;a: </label><input type="password" id="password" name="password" value="" style="border:#ccc 1px solid; display:inline-block;" />
                                    </span><span id="ok1" class="" ></span>
                                    <br /><br />
                                    <span>
                                        <label for="" style="text-align:right; width:250px;">Nivel de seguridad de la contrase&ntilde;a: </label>
                                        <span id="robusto" class="baja"></span>
                                    </span>
                                    <br /><br />
                                    <span>
                                        <label for="passwordconf" style="text-align:right; width:250px;">Confirmar contrase&ntilde;a: </label>
                                        <input type="password" id="passwordconf" name="passwordconf" value="" style="border:#ccc 1px solid;"/> 
                                        <span id="ok2" class="" ></span>
                                        <br />
                                        <label for="" style="text-align:right; width:250px;">&nbsp;</label>
                                        <span id="passwordEqu" style="font-weight:bold; color:#FF0000; font-size:0.90em"></span>
                                    </span>
                                    <br /><br />
                                   
                                    <span>
                                            <label for="privacity" style="text-align:right; width:250px;">Acepto&nbsp;el&nbsp;<a class="privacyBlue" style="color:#0b6eaa !important; text-decoration:none !important;" href="http://www.totalplay.com.mx/web/avisos/aviso-privacidad-abril-2014.pdf" target="_blank">Aviso&nbsp;de&nbsp;privacidad</a></label> 
                                            <input type="checkbox" id="privacity" name="privacity" value=""  />
                                    </span>                  
                                    <br /><br />
                                    <span align="left">		
                                            <input type="button" class="unicBtnBlue" id="submit_btn" value="Actualizar Cuenta"></input>
                                    </span><br /><br />
                                 </div>
                                 
                            </form>
                </div>
        </div>
          <div class="modal fade" id="myModalMsj">
           <div class="modal-dialog">
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="msjTitle"></h4>
               </div>
                <div class="modal-body" id="msjBody">
               </div>
               <div class="modal-footer" id="msjFooter">
               </div>
             </div>
           </div>
         </div>
                <div class="modal fade" id="myModalLoading">
            <div class="modal-dialog">
                 <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Espere un momento por favor...</h4>
                    </div>
                    <div class="modal-body" style="text-align:center;">
                       <img src="<%=request.getContextPath()%>/assets/loading.gif" alt=""/>
                    </div>
                    <div class="modal-footer">
                    </div>
                 </div>
            </div>
        </div>
</body>
<%}%>
</html>
