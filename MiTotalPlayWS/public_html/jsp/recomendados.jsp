<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8 "%>
<%@ page import="com.totalplay.vo.UsuarioVO"%>
<%
    HttpSession sesion = request.getSession(false);
    String numCuenta =(sesion.getAttribute("numCuenta") != null)?(String)sesion.getAttribute("numCuenta"):null;   
    

%>
<html>
  <head>
    <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-7618852-11');
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8 "/>
    <title>portabilidadv2</title>
    <script type="text/javascript">
            $(document).ready(function () {
                $(".tabs-menu a").click(function(event) {
                    event.preventDefault();
                    $(this).parent().addClass("current");
                    $(this).parent().siblings().removeClass("current");
                    var tab = $(this).attr("href");
                    $(".tab-content").not(tab).css("display", "none");
                    $(tab).fadeIn();
                });
            });
    </script>
   
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/recomenda.js" ></script>
    
    <!-- Cxense script begin -->
    <script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
        cX.callQueue.push(['cint', '200']);
        cX.callQueue.push(['sendPageViewEvent']);
        // one time initialization for all sendEvent calls
        cX.callQueue.push(['setEventAttributes', 
        { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
        </script>
        <script type="text/javascript">
        (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
        e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
        t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
    </script>        
    <!-- Cxense script end -->    
  </head>
  <body>
    <ul class="tabs-menu">
        <li class="current reco" style="background-color: rgb(202,202,202);">
            <a href="#tab-1" style="color: rgb(76,76,76); font-weight:600;">Capturar Recomendado</a>
        </li>
    </ul>
    <div class="tab">
        <div id="tab-1" class="tab-content">
            <h4 style="font-size: 1.3em;">GANA 1 MES GRATIS, invitando a tus amigos a contratar Totalplay, tu recomendado recibir� 10% 
            de descuento en su plan durante los primeros 3 meses.</h4>
            <form method="POST" id="formRecomen">
                <table id="tblRecomendadosTP" style="width:90%; margin:0 auto 0 auto;">
                    <tbody>
                    <tr>
                        <td style="width:35% !important;"></td>
                        <td style="width:65% !important;"></td>
                    </tr>
                    <tr >
                        <td><label class="recoLabel">Nombre*:</label></td>
                        <td ><input type="text" id="txtNombre" name="txtNombre"></input>
                            <p class="msjReco" id="errorNom"></p></td>                        
                    </tr>
                     <tr>
                        <td ><label class="recoLabel">Apellido paterno*:</label></td>
                        <td><input type="text" id="txtApellidoP" name="txtApellidoP"></input>
                            <p class="msjReco" id="errorAP"></p></td>
                    </tr>
                    <tr>
                        <td><label class="recoLabel">Apellido materno*:</label></td>
                        <td><input type="text" id="txtApellidoM" name="txtApellidoM"></input>
                             <p class="msjReco" id="errorAM"></p></td>
                                           
                    </tr>
                    <tr>
                        <td><label class="recoLabel ">Tel�fono*:</label></td>
                        <td><input class="solo-numeros" type="text" id="txtTel" name="txtTel" maxlength="10" ></input>
                            <p class="msjReco" id="errorTel"></p></td>
                    </tr>
                     <tr>
                        <td><label class="recoLabel">Correo electr�nico:</label></td>
                        <td><input type="text" id="txtEmail" name="txtEmail" ></input>
                            <p class="msjReco" id="errorMail"></p></td>
                    </tr>                                      
                    <tr>
                        <td style="text-align: right;"><input type="checkbox" id="chkAviso" name="chkAviso" ></input></td>
                        <td><label class="recoLabel" style="color:#362b36;float: left !important;">Acepto 
                        <a target="_blank" href="https://www.totalplay.com.mx/pdf/aviso-privacidad.pdf" style="text-decoration: underline; color:#362b36;">t&eacute;rminos y condiciones*</a></label></td>
                    </tr>      
                    <tr><td colspan="2" style="text-align: center;"><p class="msjReco" id="errorAviso"></p></td></tr>
                     <tr>
                        <td><label class="recoLabel"  style="font-weight: 100; ">* Campos obligatorios</label></td>
                    </tr> 
                    </tbody>
                    <tfoot>
                        <tr >
                            <td align="center" colspan="2">
                                <input type="button" value="ENVIAR" name="btnAceptarReco" id="btnAceptarReco" style="height:20px !important;padding:10px 10px 30px 10px;margin:15px !important;"/>
                                <input type="button" id="btnMisRecomen" name="btnMisRecomen" value="MIS RECOMENDADOS" style="height:20px !important;padding:10px 10px 30px 10px;margin:15px !important;"/>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </form>            
        </div>
    </div>
  
  <script type="text/javascript">
    /*******************************************Recomendados********************************************************/
    /*Inicio enviar nuevo recomendado*/
    $('#btnAceptarReco').on('click',function(){
        $(this).trigger('guardarRecomendado');
    });
    
    $('#btnAceptarReco').bind('guardarRecomendado',function(){
            var Cx = window.cX;
            Cx.callQueue.push(['sendEvent','guardarRecomendado']);
    });
    /*fin enviar nuevo recomendado*/
    
    /*Inicio click mis recomendados*/
    $('#btnMisRecomen').on('click',function(){
        $(this).trigger('misRecomendados');
    });
    
    $('#btnMisRecomen').bind('misRecomendados',function(){
            var Cx = window.cX;
            Cx.callQueue.push(['sendEvent','misRecomendados']);
    });
    /*fin click mis recomendados*/
    
    $('#btnContactar').on('click',function(){
        $(this).trigger('consultaContactar');
    });
    
    $('#btnContactar').bind('consultaContactar',function(){
            var Cx = window.cX;
            Cx.callQueue.push(['sendEvent','consultaContactar']);
    });
    
    $('#btnInstalar').on('click',function(){
        $(this).trigger('consultaInstalar');
    });
    
    $('#btnInstalar').bind('consultaInstalar',function(){
            var Cx = window.cX;
            Cx.callQueue.push(['sendEvent','consultaInstalar']);
    });
    
    $('#btnNoExitoso').on('click',function(){
        $(this).trigger('consultaNoexito');
    });
    
    $('#btnNoExitoso').bind('consultaNoexito',function(){
            var Cx = window.cX;
            Cx.callQueue.push(['sendEvent','consultaNoexito']);
    });
    
    $('#btnInstalados').on('click',function(){
        $(this).trigger('consultaInstalados');
    });
    
    $('#btnInstalados').bind('consultaInstalados',function(){
            var Cx = window.cX;
            Cx.callQueue.push(['sendEvent','consultaInstalados']);
    });

  </script>
  </body>
</html>