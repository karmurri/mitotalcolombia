<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="es">
<%@ page import="java.util.*"%>
<%@ page import="com.totalplay.vo.PlanVO"%>
<%@ page import="com.totalplay.vo.ServicioVO"%>
<%@ page import="com.totalplay.vo.ProductoVO"%>
<%@ page import="com.totalplay.vo.UsuarioVO"%>

    <%
    HttpSession login = request.getSession(false);
    UsuarioVO usr =(login.getAttribute("usuario") != null)?(UsuarioVO)login.getAttribute("usuario"):null;
    PlanVO objPlan = (login.getAttribute("infoPlan")!=null)?(PlanVO)login.getAttribute("infoPlan"):null;
    
    %>
<jsp:include page="/head.jsp"/>
<body style="font-family: 'Montserrat-light' !important;font-size: 15px !important;">    
    <section id="miplancontratado">
    <jsp:include page="/jsp/menuLoged.jsp"/>
        <br>
        <div class="container">
            <div class="d-block d-sm-none text-center">
                <p class="fontbold15" style="font-size: 24px">Mi Plan Contratado</p>
            </div>
        
            <div class="row">
                <!--Izquierda-->
                <div class="col-md-4 col-lg-3 col-sm-4 p-4">
                    <div class="row mx-auto imgplan">
                        <div>
                            <img src="<%=request.getContextPath()%>/img/MiPlan/plan100.png" style="width:100%;max-height:100%;">
                        </div>
                    </div>
                </div>
                <!--En medio-->
                
                    <div class="col-md-4 col-lg-5 col-sm-4 p-4">
                        <table class="table table-striped">
                            <tbody>
                                <%String img="";
                                if( objPlan.getServicios() != null ){ 
                                    List<ServicioVO> lst = objPlan.getServicios();
                                        for( ServicioVO srvc: lst ){
                                            if(srvc.getTipoServicio().equals("Teléfono")){img="telefonomini.png";}
                                            if(srvc.getTipoServicio().equals("Internet")){img="wifimini.png";}
                                            if(srvc.getTipoServicio().equals("Televisión")){img="tvmini.png";}%>
                                            <tr>
                                                <td style="background-color: white !important;">
                                                    <img src="<%=request.getContextPath()%>/img/MiPlan/<%=img%>" class="mx-auto d-block">
                                                </td>
                                                <td class="text-justify rounded">
                                                    <%for(ProductoVO prod: srvc.getProductos() ){%>
                                                        <%=prod.getDescripcionAdicional()%>
                                                       
                                                    <%}%>
                                                </td>
                                            </tr>
                                            <%}
                                }%>
                                 
                                <tr>
                                    <td style="background-color: white !important;"><img src="<%=request.getContextPath()%>/img/MiPlan/pesosmini.png" class="mx-auto d-block"></td>
                                    <td class="text-justify rounded font-weight-bold font-azul">Renta precio de lista: <%=objPlan.getPrecioLista()%></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
               
               <!--Derecha-->
                <div class="col-md-4 col-lg-4 col-sm-4 p-4">
                    <div class="row col-md-12 col-lg-12 col-sm-12  pl-5">
                        <!--banner-->
                        <div id="carouselExampleControls" class="carousel slide mx-auto" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100 mx-auto d-block" src="<%=request.getContextPath()%>/img/MiPlan/paquete200_mini.png" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 mx-auto d-block" src="<%=request.getContextPath()%>/img/MiPlan/paquete30_mini.png" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 mx-auto d-block" src="<%=request.getContextPath()%>/img/MiPlan/paquete10_mini.png" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 mx-auto d-block" src="<%=request.getContextPath()%>/img/MiPlan/paquete500_mini.png" alt="Second slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="row col-md-12 col-lg-12 col-sm-12 pl-5" style="">
                        <button type="button" class="btn btn-primary btn-lg font-weight-bold mx-auto" style="border: none;" id="btnCrecePlan">Crece tu plan</button>
                    </div>
                    <div class="row mx-auto pt-4">
                        <div>
                            <img src="<%=request.getContextPath()%>/img/MiPlan/publi.png" style="max-width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalPop" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: none !important;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <b><%=usr.getName()%>,</b>
                                    <p>para crecer tu plan llama al</p>
                                    <b id="phoneWeb">551798000</b>
                                    <a href="tel:551798000" style="color: black;font-size: 20px !important;display: none;" class="font-weight-bold" id="phoneMobile">
                                        <b>551798000</b>
                                    </a>
                                    <p>o si lo prefieres uno de nuestros asesores se pondrá en contacto contigo</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="border-top: none !important;">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-primary font-weight-bold mx-auto" id="btnContactame" type="button" style="border: none;background-color: #8246AF;">Contactarme</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/factura.js"></script>
    <jsp:include page="/jsp/footerLoged.jsp"/>
  </body>
</html>