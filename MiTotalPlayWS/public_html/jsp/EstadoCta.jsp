<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.totalplay.vo.ServicioVO"%>
<%@ page import="com.totalplay.vo.UsuarioVO"%>
<%@ page import="com.totalplay.vo.TarjetaVO"%>
<%@ page import="com.totalplay.vo.EstadoCuentaVO"%>
<%@ page import="com.totalplay.vo.HistoricoFacturaVO"%>
<%@ page import="com.totalplay.vo.FacturaVO"%>
<%@ page import="com.totalplay.ws.vo.ReferenceBankVO"%>
<%@ page import="java.util.*"%> 
<%@ page import="java.util.List"%> 
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.totalplay.utils.VerificaUsuario"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="encrypt.Encrypt3Des"%>
<%@ page import="com.totalplay.utils.FileProperty"%>
<%@ page import="org.apache.logging.log4j.LogManager"%>
<%@ page import="org.apache.logging.log4j.Logger"%>

<html lang="es">
<%
    Logger logger = LogManager.getLogger(this.getClass().getName());
    logger.error("estoy en estado de cuenta");
    HttpSession login = request.getSession(false);
    String error="";
    UsuarioVO usuarioVo =(login.getAttribute("usuario") != null)?(UsuarioVO)login.getAttribute("usuario"):null;
    
     if(usuarioVo == null){
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }else{
        EstadoCuentaVO balanceVO = (login.getAttribute("balance")!=null)?(EstadoCuentaVO)login.getAttribute("balance"):null;
        HistoricoFacturaVO facturasVO = (login.getAttribute("facturas")!=null)?(HistoricoFacturaVO)login.getAttribute("facturas"):new HistoricoFacturaVO();
        int sizeListFact = (login.getAttribute("sizeListFact")!=null)?(Integer)login.getAttribute("sizeListFact"):0;
                
        String AuthBancaria=  (String)login.getAttribute("AuthBancaria"); 
                
        String errorType=null;
        if(login.getAttribute("errorType")!=null){
            errorType=(String)login.getAttribute("errorType"); 
        }
        String sendEmail=null;
        if(login.getAttribute("sendEmail")!=null){
            sendEmail=(String)login.getAttribute("sendEmail"); 
        }
        
        String statussaldo="";
        String imgstatussaldo="";
        if(balanceVO!=null){
        if( balanceVO.getStatusCuenta() != null && balanceVO.getStatusCuenta().length() != 0 ){ 
            if( balanceVO.getStatusCuenta().equalsIgnoreCase("sinsaldo") ){
                imgstatussaldo="icon_perfil_001.png";
                statussaldo="Sin saldo pendiente";
            }
            if( balanceVO.getStatusCuenta().equalsIgnoreCase("prontopago") ){
                imgstatussaldo="icon_perfil_002.png";
                statussaldo="Saldo pendiente";
            }
            if( balanceVO.getStatusCuenta().equalsIgnoreCase("limitepago") ){
                imgstatussaldo="icon_perfil_003.png";
                statussaldo="Saldo pendiente";
            }
            if( balanceVO.getStatusCuenta().equalsIgnoreCase("moroso") ){ 
                imgstatussaldo="icon_perfil_004.png";
                statussaldo="Sin servicio";
            }
        } 
    }
                
        String sessionOtt= (String)login.getAttribute("ottID");
        String sessid= login.getId(); 
        String sessionJava=null;
        if(sessionOtt!=null && !sessionOtt.isEmpty()){
            sessionJava=sessionOtt;
        }else{
            sessionJava= sessid.substring(0, 25);
        }
        
        //TARJETAS
        List<TarjetaVO> listTarjetas= login.getAttribute("tarjetas")!=null?(List<TarjetaVO>)login.getAttribute("tarjetas"):new ArrayList<TarjetaVO>();
        if(listTarjetas!=null && !listTarjetas.isEmpty() && listTarjetas.size()!=0){
            for(TarjetaVO tarjeta: listTarjetas){
                logger.error("edo cta, bank: "+ tarjeta.getBank());
                logger.error("edo cta, provider: "+tarjeta.getProvider());
                logger.error("edo cta, num tarj: "+tarjeta.getCardNumber());
                logger.error("edo cta, ult digitos: "+tarjeta.getEndingCardNumber());
            }
        }
        
    %>

  <jsp:include page="/head.jsp"/>
  <body style="font-family: 'Montserrat-light'; !important">
    <section id="edoCta">
        <jsp:include page="/jsp/menuLoged.jsp"/>
        <br>
        <div class="container">
        
            <div class="row">
                <!--Izquierda-->
                <div class="col-md-6 col-lg-6 col-sm-6" id="derecha">
                <% if( balanceVO != null ){ %>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="fontbold16gris">Saldo de tu último estado de cuenta</p>
                        </div>
                    </div>
                    <br>
                    <!--Saldo ultimo estado cuenta style="margin-left: inherit;"-->
                    <div class="row">
                        <div class="col-md-10" >
                        <% if(balanceVO.getTotalPago() == null || balanceVO.getTotalPago().length() == 0){ %>
                            <div>El detalle de tu último estado de cuenta no está disponible en este momento.</div>
                        <% } %>
                        <% if(balanceVO.getTotalPago() != null && balanceVO.getTotalPago().length() != 0){ %>
                            <table class="table table-striped">
                                <tbody class="text-justify">
                                    <tr>
                                        <td  width="50%" class="text-left font-weight-bold" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;">Total a pagar:</td>
                                        <td width="50%" class="text-right font-weight-bold" id="totalPagar" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;"><%=balanceVO.getTotalPago()%></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Fecha límite de pago:</td>
                                        <td class="text-right" id="limitePago"><%=balanceVO.getFechaLim()%></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;">Total pronto pago:</td>
                                        <td class="text-right font-weight-bold" id="totalProntoPago" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;"><%=balanceVO.getTotalPagoDesc()%></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Fecha límite de pronto pago:</td>
                                        <td class="text-right" id="limiteProntoPago"><%=balanceVO.getFechaLimDesc()%></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Fecha de corte:</td>
                                        <td class="text-right" id="corte"><%=balanceVO.getFechaCorte()%></td>
                                    </tr>
                                </tbody>
                            </table>
                        <% } %>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="fontbold16gris">Resumen de tu estado de cuenta</p>
                        </div>
                    </div>
                    <br>
                    <!--Resumen estado cuenta-->
                    <div class="row">
                        <div class="col-md-10">
                        <% if(balanceVO.getTotalPago() != null && balanceVO.getTotalPago().length() != 0){ %>
                            <table class="table table-striped">
                                <tbody class="text-justify">
                                    <tr>
                                        <td width="60%" class="text-left">Periodo:</td>
                                        <td width="40%" class="text-right" id="periodo"><%=balanceVO.getPeriodo()%></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Rentas:</td>
                                        <td class="text-right" id="rentas"><%=balanceVO.getRentas()%></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Bonificaciones/Promociones:</td>
                                        <td class="text-right" id="bonifprom"><%=balanceVO.getBonifica()%></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Consumos del periodo:</td>
                                        <td class="text-right" id="consumos"><%=balanceVO.getConsumoPeriodo()%></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Subtotal del periodo:</td>
                                        <td class="text-right" id="subtotal"><%=balanceVO.getSubtotal()%></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left font-weight-bold" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;">Total:</td>
                                        <td class="text-right font-weight-bold" id="total" style="color: #1F9BC7;font-family: 'Montserrat-bold' !important;"><%=balanceVO.getTotalPago()%></td>
                                    </tr>
                                </tbody>
                            </table>
                        <% } %>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <!--div class="col-md-2"></div-->
                        <div class="col-md-10">
                            <button id="pagar" type="button" class="btn btn-primary btn-lg btn-block font-weight-bold" style="font-size:22px;">Pagar ahora</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                <% }%>
                </div>
                
                <!--Derecha-->
                <div class="col-md-6 col-lg-6 col-sm-6" id="izquierda">
                   <div class="row p-4 text-center"  >
                        <!--bienvenido -->
                        <div class="d-block d-sm-none texto col-12" >
                            <p class="fontbold16gris" style="font-size: 20px;">Bienvenid@</p>
                            <p class="fontbold16gris"><%=usuarioVo.getName() + " " + usuarioVo.getLastname()%></p>
                        </div>
                    
                        <!--banner-->
                        <div id="carouselExampleControls" class="carousel slide col-12 py-3" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="<%=request.getContextPath()%>/img/EstadoCta/banner.png" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="<%=request.getContextPath()%>/img/EstadoCta/banner.png" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="<%=request.getContextPath()%>/img/EstadoCta/banner.png" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    
                    
                        <div class="d-block d-sm-none col-12 py-3" style="padding-right: 0px">
                            <button type="button" class="btn botonpago">
                                 <img align="left" src="<%=request.getContextPath()%>/img/Menus/pesos.png"></img>
                                 <label style="margin-top: 0.5rem" >Pagar <%=balanceVO.getSaldoActual()%></label>
                            </button>
                        </div>
                        
                       <div class="d-block d-sm-none col-12">
                           <img src="<%=request.getContextPath()%>/img/Menus/face_mini.png">&nbsp;<label>
                           <%=statussaldo%></label>
                       </div>
                    </div>
                       
                    <br>
                    <div class="d-none d-lg-block row text-center">
                        <!--estados cta pdf-->
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            <p class="text-center font-weight-bold" style="font-family: 'Montserrat-bold' !important;font-size: 17px !important;color: #3A4559;">Descarga tu estado de cuenta en PDF</p>
                            <div class="row">
                                <div class="col-2"></div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-8">
                                            <p clas="text-center font-weight-bold" style="color:#1F9BC7;font-family:'Montserrat-bold';font-size:14px;">Selecciona el mes</p>
                                        </div>
                                        <div class="col-4">
                                            <button id="pdfButton" type="button" class="btn btn-light" style="background-image:url(<%=request.getContextPath()%>/img/EstadoCta/file-pdf-mini.png);"></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                </div>
                            </div>
                            <!--Select estados de cuenta-->
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-lg-8">  
                                            <select class="custom-select" id="selectEdoCta">
                                            <% if(sizeListFact > 0){
                                            List<FacturaVO> objFac = facturasVO.getHistoricoFac();
                                            ArrayList<FacturaVO> lista= new ArrayList<FacturaVO>(objFac);%>
                                                <!--option value="0">Facturas</option-->
                                            <% for(int iFac=0;iFac<sizeListFact;iFac++){%>
                                                <option value="<%=lista.get(iFac).getLiga()%>"><%=lista.get(iFac).getFechaPago()%></option>
                                            <% } %>
                                            <% } %>
                                            <% if(sizeListFact == 0){%>
                                                <option value="0">No hay info...</option>
                                            <% } %>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-4">
                                            <button id="downButton" type="button" class="btn btn-light" style="background-image:url(<%=request.getContextPath()%>/img/EstadoCta/ArrowDownMini.png);"></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            
            <!--movil derecha-->
            <div class="row d-block d-sm-none p-3">
                <div class="col-12 pt-4">
                    <p class="fontbold16gris">Descarga tu estado de cuenta en PDF</p>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-8">
                            <p clas="text-center font-weight-bold" style="color:#1F9BC7;font-family:'Montserrat-bold';font-size:14px;">Selecciona el mes</p>
                        </div>
                        <div class="col-4">
                            <button id="pdfButton" type="button" class="btn btn-light" style="background-image:url(<%=request.getContextPath()%>/img/EstadoCta/file-pdf-mini.png);"></button>
                        </div>
                        <div class="col-8">
                            <select class="custom-select" id="selectEdoCta">
                                    <% if(sizeListFact > 0){
                                    List<FacturaVO> objFac = facturasVO.getHistoricoFac();
                                    ArrayList<FacturaVO> lista= new ArrayList<FacturaVO>(objFac);%>
                                        <!--option value="0">Facturas</option-->
                                    <% for(int iFac=0;iFac<sizeListFact;iFac++){%>
                                        <option value="<%=lista.get(iFac).getLiga()%>"><%=lista.get(iFac).getFechaPago()%></option>
                                    <% } %>
                                    <% } %>
                                    <% if(sizeListFact == 0){%>
                                        <option value="0">No hay info...</option>
                                    <% } %>
                                    </select>
                        </div>
                        <div class="col-4">
                            <button id="downButton" type="button" class="btn btn-light" style="background-image:url(<%=request.getContextPath()%>/img/EstadoCta/ArrowDownMini.png);"></button>
                        </div>
                    </div>
                </div>
            </div>      
            
            
        </div>
    </section>
    <br>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/factura.js"></script>
    <jsp:include page="/jsp/footerLoged.jsp"/>
    
    <script>             
    entrar();
    function entrar(){
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','loginSerEvent']);     
    }        
    </script>
  </body>
  <% } %>
</html>