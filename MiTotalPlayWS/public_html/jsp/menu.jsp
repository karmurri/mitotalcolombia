
<div class="d-none d-lg-block"> 
    <nav class="navbar navbar-expand-lg  bg-color"> 
        <div class="collapse navbar-collapse" id="navbarNav"> 
            <ul class="navbar-nav" style="margin-left:70px !important;"> 
                <div class="col-5"> 
                <li> 
                    <a class="navbar-brand" href="https://www.totalplay.com.mx"> 
                        <img id="logoTP" src="<%=request.getContextPath()%>/img/Menus/total_play_logo.png" class="d-inline-block align-top" alt=""> 
                    </a> 
                </li> 
                </div> 
                <div class="col-2"> 
                <li class="nav-item active tpr-font"> 
                    <a class="nav-link" href="#">Internet <br>Total</a> 
                </li> 
                </div> 
                <div class="col-2"> 
                <li class="nav-item"> 
                    <a class="nav-link" href="#">TV <br>Total</a> 
                </li> 
                </div> 
                <div class="col-2"> 
                    <li class="nav-item"> 
                    <a class="nav-link" href="#">Paquetes</a> 
                </li> 
                </div> 
                <div class="col-2"> 
                <li class="nav-item"> 
                    <a class="nav-link" href="#">Promociones</a> 
                </li> 
                </div> 
                <div class="col-2"> 
                <li class="nav-item" id="color-blue"> 
                    <a class="nav-link" href="#">&iexcl;Contrata!</a> 
                </li> 
                </div> 
            </ul> 
        </div> 
    </nav> 
</div> 