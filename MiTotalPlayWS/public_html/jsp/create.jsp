<!doctype html>
<html>
<head>
    <!-- Etiqueta global de sitio (gtag.js) de Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7618852-11"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-7618852-11');
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title>Mi Cuenta Totalplay</title>
    <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/icn/favicon.ico">
    <link rel="apple-touch-icon" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<%=request.getContextPath()%>/images/icn/apple-touch-icon-114x114.png">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/Login.css" media="screen" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/alert.css" type="text/css" media="screen" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/alert.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/bootstrap.min.css" />
        
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/prettify/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/prettify/css/prettify.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/prettify/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/prettify/css/tooltipster.css" />
        
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.jgfeed.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/prettify.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/lang-css.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/scripts.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.tooltipster.js"></script>
        <script type="text/javascript">
	
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','<%=request.getContextPath()%>/js/analytics.js','ga');

	  ga('create', 'UA-42318544-1', 'mitotalplay.com.mx');
	  ga('send', 'pageview');
	</script>
        <!-- Cxense script begin >
        <script type="text/javascript">
            var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['setSiteId', '1131772398957904842']);//1132899846499742441
            cX.callQueue.push(['cint', '200']);
            cX.callQueue.push(['sendPageViewEvent']);
            // one time initialization for all sendEvent calls
            cX.callQueue.push(['setEventAttributes', 
            { origin: 'tlp-queryPer', persistedQueryId: '9096aa9d1320f6e6a91421e0c557230ef546787b' }]);
            </script>
            <script type="text/javascript">
            (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
            e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
            t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
        </script>        
        <!-- Cxense script end >
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/eventos.js"></script-->        
</head>
<body>
        <div id="content_pop"><br>
            <h3 >Reg&iacute;strate en mi cuenta Totalplay</h1><br/>
            <div>
                Consulta de manera segura y sencilla el saldo de tu cuenta, realiza pago en l&iacute;nea y mucho m&aacute;s.
                <br />
                Para iniciar tu registro, deber&aacute;s de proporcionar uno de los siguientes datos:
		<ul class="ulmodal">
                        <li> tu n&uacute;mero de cuenta</span></li>
                        <li> tu n&uacute;mero telef&oacute;nico de Totalplay (a 10 d&iacute;gitos)</li>
                </ul>
                Con esta informaci&oacute;n te enviaremos al correo electr&oacute;nico que registraste tu contrase&ntilde;a temporal con la que podr&aacute;s acceder a tu cuenta.
                <br /><br />
                <form method="post" id="formCreate" autocomplete="off">
                    <label for="username">N&uacute;mero de cuenta o tel&eacute;fono:</label>
                    <input type="text" class="form-control" id="username" name="username" value=""  placeholder="No. de cuenta o tel&eacute;fono."  />
                      <div class="row py-4">
                        <div class="col-lg-4 col-sm-1"></div>
                        <div class="col-lg-4 col-sm-10">
                            <input type="button" id="create_btn" class="btn text-center" value="Crear Cuenta"></input>		
                        </div>
                        <div class="col-lg-4 col-sm-1"></div>
                    </div>
                    <!--input type="button" class="unicBtnBlue" value="Crear Cuenta" id="create_btn" /-->
                </form>
            </div>
        </div>
	   <div class="modal fade" id="myModalLoading">
            <div class="modal-dialog">
                 <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Espere un momento por favor...</h4>
                    </div>
                    <div class="modal-body" style="text-align:center;">
                       <img src="<%=request.getContextPath()%>/assets/loading.gif" alt=""/>
                    </div>
                    <div class="modal-footer">
                    </div>
                 </div>
            </div>
        </div>
        
         <div class="modal fade" id="myModalMsj">
           <div class="modal-dialog">
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="msjTitle"></h4>
               </div>
                <div class="modal-body" id="msjBody" style="text-align:center;">
               </div>
               <div class="modal-footer" id="msjFooter">
               </div>
             </div>
           </div>
         </div>
       
</body>
</html>