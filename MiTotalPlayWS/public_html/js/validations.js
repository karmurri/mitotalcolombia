jQuery.validator.setDefaults({
  debug: true,
  submitHandler: function(form) {
            
            function getContextPath() {
                return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
                //return "";
            }
            
            idForm=$(form).prop('id');
            switch(idForm){
                case 'formPorta':
                    
                    console.log('Se valid� correctamente el formulario formPorta');
                    creaPortabilidad(getContextPath()+"/creaportabilidad");
                break;
               default:console.log('No se envi� formulario'+idForm);
            }
	},
         showErrors: function(map, list) {
		// there's probably a way to simplify this
		var focussed = document.activeElement;
             
		if (focussed && $(focussed).is("input, textarea")) {
			$(this.currentForm).tooltip("close", {
				currentTarget: focussed
			}, true)
		}
		this.currentElements.removeAttr("title").removeClass("ui-state-error");
		$.each(list, function(index, error) {
			$(error.element).attr("title", error.message).addClass("ui-state-error");
		});
		if (focussed && $(focussed).is("input, textarea")) {
			$(this.currentForm).tooltip("open", {
				currentTarget: focussed
			});
		}
	}
});

jQuery(function() {
    $("#formPorta").tooltip({
            show: false,
            hide: false
    });
    
    
    var rulesSET = {
            DN_0:{
                required:true,
                digits:true,
                maxlength:10,
                minlength:10
            },
            telCont:{
                required:true,
                digits:true,
                maxlength:10,
                minlength:10
            },
            nip_0:{
                required:function (element) {
                    return($('#rbtnActiva').is(':checked') && $('#rbtnFisica').is(':checked') && $('#nip_0').is(':visible'));
                }, 
                digits:true,
                maxlength:4,
                minlength:4
            },
            nip_1:{
                required:function (element) {
                    return($('#rbtnActiva').is(':checked') && $('#rbtnFisica').is(':checked') && $('#nip_0').is(':visible'));
                }                
                //equalTo:'#nip_0'
            },
            emailPorta:{
                required:true,
                email:true
            },
            fechaPort:{
              required:true  
            },
            btnIdenPorta:{
                required:function(element){
                    return( ($('#rbtnMoral').is(':checked') || ($('#rbtnFisica').is(':checked') && $('#rbtnFechaCancelNO').is(':checked'))) && $('#btnIdenPorta').is(':visible') );
                },
                //equalTo:'#btnIdenPorta',
                extension: "pdf|jpg",
                maxFileSize: {
                    "unit": "MB",
                    "size": 1
                }
            },
            btnSolPorta:{
                required:function(element){
                    return( ($('#rbtnMoral').is(':checked') || ($('#rbtnFisica').is(':checked') && $('#rbtnFechaCancelNO').is(':checked'))) &&  $('#btnSolPorta').is(':visible') );
                },
                //equalTo:'#btnSolPorta',
                extension: "pdf|jpg",
                maxFileSize: {
                    "unit": "MB",
                    "size": 1
                }
            },
            btnActaCons:{
                required:function(element){
                    return( $('#rbtnMoral').is(':checked') && $('#btnActaCons').is(':visible'));
                },
                //equalTo:'#btnActaCons',
                extension: "pdf|jpg",
                maxFileSize: {
                    "unit": "MB",
                    "size": 1
                }
            },
            btnCartaCancel:{
                required:function(element){
                    return( ($('#rbtnFisica').is(':checked') && $('#rbtnFechaCancelNO').is(':checked')) && $('#btnCartaCancel').is(':visible'));
                },
                //equalTo:'#btnCartaCancel',
                extension: "pdf|jpg",
                maxFileSize: {
                    "unit": "MB",
                    "size": 1
                }
            }
            
            
            
            
    }
    
    
    var messagesO = {
            DN_0:{
                required:"El n\u00FAmero de tel\u00E9fono a portar es obligatorio",
                digits:"Ingresa s\u00F3lo n\u00FAmeros",
                maxlength:"El n\u00FAmero a portar no debe tener m\u00E1s de 10 d\u00EDgitos",
                minlength:"El n\u00FAmero a portar debe tener 10 d\u00EDgitos"
            },
            telCont:{
                required:"El tel\u00E9fono de contacto es obligatorio",
                digits:"Ingresa s\u00F3lo n\u00FAmeros",
                maxlength:"El tel\u00E9fono de contacto no debe tener m\u00E1s de 10 d\u00EDgitos",
                minlength:"El tel\u00E9fono de contacto debe tener 10 d\u00EDgitos"
            },
            nip_0:{
                required:"El NIP es obligatorio",
                digits:"Ingresa s\u00F3lo n\u00FAmeros",
                maxlength:"El NIP no debe tener m\u00E1s de 4 d\u00EDgitos",
                minlength:"El NIP debe tener 4 d\u00EDgitos"
            },
            nip_1:{
                required:"Confirma tu NIP"
                //equalTo:'Los valores de NIP no coinciden'
            },
            emailPorta:{
                required:"El correo electr\u00F3nico es obligatorio",
                email:"Ingresa un correo electr\u00F3nico con formato correcto"
            },
            fechaPort:{
              required:"La fecha de portabilidad es obligatoria"  
            },
            btnIdenPorta:{
                required:"Selecciona un archivo",
                extension:"El formato del archivo no es v\u00E1lido, favor de usar s\u00F3lo formatos .PDF y .JPG", 
                maxFileSize: "El tama\u00F1o del archivo debe ser menor a 1MB"
            },
            btnSolPorta:{
                required:"Selecciona un archivo",
                extension:"El formato del archivo no es v\u00E1lido, favor de usar s\u00F3lo formatos .PDF y .JPG", 
                maxFileSize: "El tama\u00F1o del archivo debe ser menor a 1MB"
            },
            btnCartaCancel:{
                required:"Selecciona un archivo",
                extension:"El formato del archivo no es v\u00E1lido, favor de usar s\u00F3lo formatos .PDF y .JPG", 
                maxFileSize: "El tama\u00F1o del archivo debe ser menor a 1MB"
            },
            btnActaCons:{
                required:"Selecciona un archivo",
                extension:"El formato del archivo no es v\u00E1lido, favor de usar s\u00F3lo formatos .PDF y .JPG", 
                maxFileSize: "El tama\u00F1o del archivo debe ser menor a 1MB"
            }
        }//Cierre de mensajes
    $("#formPorta").validate({
        ignore: [],
        rules: rulesSET,//rules
        messages: messagesO
        //Inicio msg de validacion
    });
    
});

function generaClickTab(){
    $("#tabConsulta" ).trigger( "click" );
}


function creaPortabilidad(servletName){
 
        var formdata = new FormData(); 
        formdata.append("DN_0" , document.getElementById("DN_0") != null ? document.getElementById("DN_0").value : ""); 
        formdata.append("telCont", document.getElementById("telCont") != null ?  document.getElementById("telCont").value : ""); 
        formdata.append("nip_0" , document.getElementById("nip_0") != null ? document.getElementById("nip_0").value : ""); 
        formdata.append("emailPorta" ,document.getElementById("emailPorta") != null ? document.getElementById("emailPorta").value : ""); 
        formdata.append("fechaPort" , document.getElementById("fechaPort") != null ?  document.getElementById("fechaPort").value : ""); 
        formdata.append("razonSocial" , document.getElementById("rbtnFisica").checked ? "fisica" : "moral"); 
        
        if($('#rbtnMoral').is(':checked') || ($('#rbtnFisica').is(':checked') && $('#rbtnFechaCancelNO').is(':checked'))  ){
            formdata.append("btnIdenPorta" , document.getElementById("btnIdenPorta") != null ? document.getElementById("btnIdenPorta").files[0] : " "); 
            console.log("Agregue la IFE");
            formdata.append("btnSolPorta" , document.getElementById("btnSolPorta") != null ? document.getElementById("btnSolPorta").files[0] : " "); 
            console.log("Agregue la Solicitud de porta");
        }
        
        if($('#rbtnFisica').is(':checked') && $('#rbtnFechaCancelNO').is(':checked')){
            formdata.append("btnCartaCancel" , document.getElementById("btnCartaCancel") != null ? document.getElementById("btnCartaCancel").files[0] : " "); 
            console.log("Agregue la carta de cancelaion persona fisica");
        }
        
        if($('#rbtnMoral').is(':checked')){
            formdata.append("btnActaCons" , document.getElementById("btnActaCons") != null ? document.getElementById("btnActaCons").files[0] : " ");
            console.log("Agregue la actaconstitutiva");
        }
        
        
        
        var xhr = new XMLHttpRequest();
        $("#myModalLoading").modal("show");
        xhr.open("POST",servletName, true); 
        xhr.send(formdata); 
        xhr.onload = function(e) { 
            if (this.status == 200) {  
                console.log(this.responseText);
                $("#myModalLoading").modal("hide");
                $("#formPorta")[0].reset();
                var jsonResp=JSON.parse(this.response);
                var code= jsonResp.code.trim();
                var numPortado= jsonResp.numeroPortado.trim();    
                var folio= jsonResp.folio.trim();
                switch(code){
                    case '0': 
                        $("#folioPorta").html(folio);
                        $("#numPortadoRes").text(numPortado);
                        $("#resCreaPorta").show();
                        $("#contentTramite").hide();
                    break;
                    case '1': 
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">&iexcl;Error al cargar archivo!<\/h5>');
                        $('#msjBody').html('El archivo de Identificaci&oacute;n Oficial tiene un formato distinto a los permitidos (.pdf y .jpg).');
                        $('#msjFooter').html(" ");
                        $("#myModalMsj").modal("show");
                    break;
                    case '2': 
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">&iexcl;Error al cargar archivo!<\/h5>');
                        $('#msjBody').html('El archivo de Formato de solicitud tiene un formato distinto a los permitidos (.pdf y .jpg).');
                        $('#msjFooter').html(" ");
                        $("#myModalMsj").modal("show");
                    break;
                    case '3': 
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">&iexcl;Error al cargar archivo!<\/h5>');
                        $('#msjBody').html('El archivo de Carta de cancelaci&oacute;n tiene un formato distinto a los permitidos (.pdf y .jpg).');
                        $('#msjFooter').html(" ");
                        $("#myModalMsj").modal("show");
                    break;
                    case '4': 
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">&iexcl;Error al cargar archivo!<\/h5>');
                        $('#msjBody').html('El archivo de Acta constitutiva o Poder Notarial tiene un formato distinto a los permitidos (.pdf y .jpg).');
                        $('#msjFooter').html(" ");
                        $("#myModalMsj").modal("show");
                    break;
                    case '-1': 
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">&iexcl;Error!<\/h5>');
                        $('#msjBody').html('Ocurri&oacute; un error al enviar los datos de solicitud de portabilidad, por favor reintenta m&aacute;s tarde.');
                        $('#msjFooter').html(" ");
                        $("#myModalMsj").modal("show");
                    break;
                    default: break;                                        
                }
              
            }

        };          
}
