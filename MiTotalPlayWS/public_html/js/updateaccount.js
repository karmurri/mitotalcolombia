		 var RecaptchaOptions = {
				    theme : 'clean',
				    lang : 'es'
				 };
			$.minlength = 8;
			 
		jQuery(document).ready(function() {
                        
                         function getContextPath() {
                               return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
                               //return "";
                            }
                         
			$.passwordStrength = function(password)
			{
				var desc = new Array("Muy d&eacute;bil", "D&eacute;bil", "Bajo", "Medio","Alto", "Robusto");
				var score   = 0;
				if (password.length > 6) score++;
				if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) score++;
				if (password.match(/\d+/)) score++;
				if (password.match(/.-,.,$,\@]/) )	score++;
				if (password.length > 8) score++;
				if (password.length < 4) score=0;
				//$("#passwordDescription").html(desc[score]);
				robustoClass = 'baja';
				if(score<2) {
					robustoClass = 'baja';
				} else {
					if(score<5)
						robustoClass = 'media';
					if(score>4)
						robustoClass = 'alta';
				}
				$("#robusto").removeClass().addClass(robustoClass);
			}
			$.validPassword = function(){
				if($("#password").val().length >3){
					if($("#password").val()==$("#passwordconf").val()){
						$("#passwordEqu").html("");
						$("#ok1").addClass("ok");
						$("#ok2").addClass("ok");
					} else {
						$("#passwordEqu").html("Las contrase&ntilde;as no coinciden.");
						$("#ok1").removeClass();
						$("#ok2").removeClass();
					}

				}
			}

			$("#passwordconf").keyup(function () {
				$.validPassword(); 
				});
			
			$("#password").keypress(function () {
				$.passwordStrength($("#password").val()); 
			//	$.validPassword(); 
				});
			$("#submit_btn").click(function(){
				$.err = false;
				$.lab = "";

				password = $("#password").val();
				/*if(password.match(/[a-zA-Z0-9,-,.,$,\@]/))
					A += "Tiene Alfanumerico ";*/

			
				if(!$.err && $("#username").val() == ""){
					$.lab += "\n Tu nombre es obligatorio";	
				}
				if(!$.err && $("#lastname").val() == ""){
					$.lab += "\n Tu apellido es obligatorio";	
				}
				if(!$.err && !$("#privacity").is(':checked')){
					$.lab += "\n Debes aceptar el Aviso de privacidad";	
				}
				if(!$.err && $("#password").val().length < 6){
					$.lab += "\n La contrase�a debe de ser mayor a 5 caracteres";	
				}
				if(!$.err && $("#password").val() == ""){
					$.lab += "\n La contrase�a es obligatoria";	
				}

				if (!$.err && password.match(/[\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\?,\',\",\�,\!,\�,\#,\%,\&,\�,\(,\),\*,\+,\,,\/,\:,\;,\<,\=,\>,\?,\[,\\,\],\],\_,\`,\{,\|,\},\~,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�,\�]/) )	{
					$.lab += "\n La contrase�a tiene caracteres no autorizados";	
				}

				if(!$.err && $("#passwordconf").val() == ""){
					$.lab += "\n La confirmaci�n de la contrase�a es obligatoria";	
				}
				
				if(!$.err && $("#password").val() != $("#passwordconf").val()){
					$.lab += "\n La contrase�a no coincide";	
				}
				//if(!$.err && $("#recaptcha_response_field").val() == ""){
					//$.lab += "\n Ingresa las palabras";	
				//}
				if($.lab!="")
					$.err = true;
				if($.err) {
					alert("Para poder continuar es necesario capturar todos los campos: " + $.lab);
					Recaptcha.reload();
					return false;
				} else {
                                
                                //var recaptcha_response_field= $("#formmyaccount :input[name=recaptcha_response_field]").val();
                                //var recaptcha_challenge_field= $("#formmyaccount :input[name=recaptcha_challenge_field]").val();
                                
                               // $.fancybox.close();
                                $.ejecutaSubmit();
                                function llegadaDatos (response) {
                                        if(response=='success'){
                                            console.log("Recaptcha validado correctamente");
                                            $.ejecutaSubmit();
                                        }
                                        else{
                                            Recaptcha.reload();
                                            $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">�Error!<\/h5>');
                                            $('#msjBody').html('Los caracteres de la imagen no coinciden, por favor int�ntalo nuevamente.');
                                            $('#msjFooter').html(" ");
                                            $("#myModalMsj").modal("show");
                                            console.log("Recaptcha incorrecto");
                                        }
                                }
                                function problemas(xhr, ajaxOptions, thrownError) {
                                    Recaptcha.reload();
                                    console.log(xhr.status);
                                    console.log("Error: "+thrownError);
                                    console.log("Errores al cambiar contrase�a");
                                }  
                               
				}
			});
		});
                

                        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','../js/analytics.js','ga');

			  ga('create', 'UA-42318544-1', 'mitotalplay.com.mx');
			  ga('send', 'pageview');