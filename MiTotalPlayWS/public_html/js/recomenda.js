$(document).ready(function () {
    function getContextPath(){
        return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
        //return "";
    }
    
    //validacion correo
    function ValidateEmail(mail) {
     if(mail.lenth == 0)
        {return (true)}
        else{
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)){
            return (true)
          }
            return (false)
        }
    }
    function validaName(nombre) {
     if (/^([a-zA-Z\�\�]+)((\s)*([a-zA-Z\�\�]+))*$/.test(nombre)){
        return (true)
      }
        return (false)
    }
    
    $("#btnAceptarReco").on("click", function(){
            var nombre = $.trim($("#txtNombre").val());
            var apellidoP = $.trim($("#txtApellidoP").val());
            var apellidoM = $.trim($("#txtApellidoM").val());
            var tel = $.trim($("#txtTel").val());
            var correo = $.trim($("#txtEmail").val());
            var termino = $("#chkAviso").is(":checked");
            bandera = validacion(nombre, apellidoP, apellidoM, tel, correo, termino);
            //console.log("bandera " + bandera);
            if(bandera){
                creaRecomenda(nombre.trim(), apellidoP.trim(), apellidoM.trim(), tel.trim(), correo.trim());
            }       
            
    }); 
    
    function validacion(nombre, ap, am, tel, correo, termino){      
      bandera2 = true;
      ocultar();
      if(nombre.length == 0){
        $("#errorNom").html("Es un campo requerido");
        bandera2 = false;
      } 
      if(validaName(nombre) == false){
        $("#errorNom").html("Ingrese un nombre v\u00e1lido ");
        bandera2 = false;
      }
      if(ap.length == 0){
        $("#errorAP").html("Es un campo requerido");
        bandera2 = false;
      } 
      if(validaName(ap) == false){
        $("#errorAP").html("Ingrese el apellido v\u00e1lido");
        bandera2 = false;
      }
      if(am.length == 0){
        $("#errorAM").html("Es un campo requerido");
        bandera2 = false;
      } 
      if(validaName(am) == false){
        $("#errorAM").html("Ingrese el apellido v\u00e1lido");
        bandera2 = false;
      }
      if(tel.length == 0 || tel.length != 10){
        $("#errorTel").html("Es un campo requerido");
        bandera2 = false;
      } 
      if(ValidateEmail(correo) == false && correo.length != 0){
        $("#errorMail").html("Ingrese un correo v\u00e1lido");
        bandera2 = false;
      }
      if(termino == false){
        $("#errorAviso").html("Debe aceptar los t\u00E9rminos y condiciones.");
        bandera2 = false;
      }
      return bandera2;
    }
    
    $('.solo-numeros').keydown(function (e){
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }); 
    
    function creaRecomenda(nombre, ap, am, tel, correo){        
        var nombreCom = nombre + " " + ap + " " + am;                
        var payload = { "nombre":nombreCom, "tel":tel, "correo":correo }
        $_encripted = $.otpyrc2(JSON.stringify(payload));
        $.ajax({
            url: getContextPath() + '/recomendados',
            type: 'POST', data : {
                "data":$_encripted
            },
            beforeSend: function(){
            },
            success:function(data){ 
                if(data.status == 0){                    
                    $("#myModalMsj").modal("show");
                    $('#msjBody').html("<br> <p style='font-size: 20px;font-weight: 600;text-align: center;'> " +
                    " &iexcl;Gracias&excl; Tu recomendado se ha registrado exitosamente.<\/p><br>" +
                    "<p style='font-size: 18px;text-align: center;'>En breve tu recomendado recibir&aacute; " +
                    "un correo electr&oacute;nico con la confirmaci&oacute;n.<\/p>") ;
                     document.getElementById("msjFooter").style.display = "none";    
                     $('input[type="text"]').val('');$("#chkAviso").prop('checked', false) ;
                }else if(data.status == 3){
                    $("#myModalMsj").modal("show");
                    $('#msjBody').html("<br> <p style='font-size: 20px;font-weight: 600;text-align: center;'> " +
                    "Referencia ya existente, ingrese una nueva.<\/p><br>");
                     document.getElementById("msjFooter").style.display = "none";    
                } else{
                     $("#myModalMsj").modal("show");
                     $('#msjBody').html("<br> <p style='font-size: 20px;font-weight: 600;text-align: center;'> " +
                        " Ocurri&oacute; un error al registrar su recomendado.<\/p><br>");
                     document.getElementById("msjFooter").style.display = "none";
                }
                setTimeout(function(){
                    $("#myModalMsj").modal("hide");
                },7000);                
            }
        });  
    }
    
    $.otpyrc2 = function(str){
        var strCo = "";
        try{
            var aeut = new AesUtil(ks,ic);
            strCo = aeut.encrypt(tlas,vi,esarhpssap,str);
        }catch(e){
            strCo = "";
        }
        return strCo;
    }
    
    $("#btnMisRecomen").on("click", function(){        
        var html = "";
        $.ajax({
            url: getContextPath() + '/consultaReco',
            type: 'POST',
            beforeSend: function(){
            },
            success:function(data){ 
                if(data.status == 0){
                    html += "<br><h4 class='modal-title center' style='font-weight: 600;'>MIS RECOMENDADOS<\/h4><br>";
                    html += "<div id='cont-Recomendados' style='width:90%; margin: auto; text-align:center;'>";
                    html += "<input onclick='$.getDetalle(1)' class='buttonReco' type='button' id='btnContactar' name='btnContactar' value='Por contactar "+ data.numContactar +"' />";
                    html += "<input onclick='$.getDetalle(2)' class='buttonReco' type='button' id='btnInstalar' name='btnInstalar' value='Por instalar "+ data.numInstalar +"' />";
                    html += "<input onclick='$.getDetalle(3)' class='buttonReco' type='button' id='btnNoExitoso' name='btnNoExitoso' value='No exitoso "+ data.numNoExito +"' />";
                    html += "<input onclick='$.getDetalle(4)' class='buttonReco' type='button' id='btnInstalados' name='btnInstalados' value='Instalados "+ data.numinstalados +"'/>  <\/div><br><br>";
                    document.getElementById("closeModal").style.color = "#fff";
                    document.getElementById("closeModal").style.opacity = 100;
                    document.getElementById("msjFooter").style.display = "none";
                    $("#msjBody").empty().append(html);
                    $("#myModalMsj").modal("show");
                }else{
                    error();
                }
            }
        });     
   });
   
   $.getDetalle = function(valor){
       $("#myModalMsj").modal("hide");
       var titulo = [];
       titulo[0] = "POR CONTACTAR";
       titulo[1] = "POR INSTALAR";
       titulo[2] = "NO EXITOSO";
       titulo[3] = "INSTALADOS";
       
       /*PARA CXENSE*/
       var Cx = window.cX;
       if(valor == 1){
           //Por contactar           
           Cx.callQueue.push(['sendEvent','consultaContactar']);
       }else if(valor == 2){
           //Por instalar
           Cx.callQueue.push(['sendEvent','consultaInstalar']);
       }else if(valor == 3){
           //No exitoso
           Cx.callQueue.push(['sendEvent','consultaNoexito']);
       }else if(valor == 4){
           //Instalados
           Cx.callQueue.push(['sendEvent','consultaInstalados']);
       }
       /*PARA CXENSE*/
       
       
       $.ajax({
            url: getContextPath() + '/consultaDetalleReco',
            type: 'POST', data : {
                "tipo":valor
            },
            beforeSend: function(){
            },
            success: function(data){
                if(data.status == 0){
                    var html2 = "";
                    html2 += "<br><h4 class='modal-title center' style='font-weight: 600;'>" + titulo[valor-1] + "<\/h4><br>";
                    html2 += "<div id='cont-DetalleUsuarios' style='width:90%; margin: auto;height: 370px;overflow-y: auto;'>";
                    html2 += "<table id='tblDetalle'><tbody>";
                    for(x in data.contactos){
                            html2 += "<tr><td colspan='2'><p> " + data.contactos[x].nombre +"<\/p><\/td><\/tr>";
                            html2 += " <tr class='linea'><td><p>" +  data.contactos[x].fecha +"<\/p><\/td>";
                            html2 += "<td><p>Folio "+  data.contactos[x].folio +"<\/p><\/td><\/tr>";
                    }
                    html2 += "<\/tbody><\/table><\/div><\/div>";
                    $('#msjBodyReco').html(html2);
                    
                    $("#myRecomendados").modal("show");
                }                
                if(data.status == 4){
                    $("#myModalMsj").modal("show");
                    $('#msjBody').html("<br> <p style='font-size: 20px;font-weight: 600;text-align: center;'> " +
                    " No cuentas con recomendados en este estatus.<\/p><br>");
                    document.getElementById("msjFooter").style.display = "none";
                }
                if(data.status != 0 && data.status != 4){
                    error();
                }
                
            }
        });       
   }
   
    function error(){
        $("#myModalMsj").modal("show");
        $('#msjBody').html("<br> <p style='font-size: 20px;font-weight: 600;text-align: center;'> " +
        " Ocurri&oacute; un error al mostrar la informaci&oacute;n.<\/p><br>");
        document.getElementById("msjFooter").style.display = "none";
    }
   
   function ocultar(){
        $("#errorNom").html("");
        $("#errorAM").html("");
        $("#errorAP").html("");
        $("#errorTel").html("");
        $("#errorMail").html("");
        $("#errorAviso").html("");
   } 

});