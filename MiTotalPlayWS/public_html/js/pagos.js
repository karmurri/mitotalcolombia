$(document).ready(function () {
    $.UpdateForm = function (ccI, th, cend, sec, banco) {
        $("#paymentForm0 :input[name=idCardList]").val(ccI);
        $("#paymentForm0 :input[name=cCardname]").val(th);
        $("#paymentForm0 :input[name=cend]").val(cend);
        $("#paymentForm0 :input[name=sec]").val(sec);
        $("#paymentForm0 :input[name=bank]").val(banco);
        // alert('Datos: '+ccI+' '+th+' '+ cend+' '+sec+' '+banco);
    };
    
    $.SubmitForm = function (FormName, operacion, extra){
        //var soloLetras=  /^[a-zA-Z����������������������\s]+$/;
        var soloLetras=/^[a-zA-Z\u00F1\u00D1\u00E1\u00E9\u00ED\u00F3\u00FA\u00C1\u00C9\u00CD\u00D3\u00DA\u00E4\u00EB\u00EF\u00F6\u00FC\u00C4\u00CB\u00CF\u00D6\u00DC\s]+$/;
        //var soloLetrasNums= /^[a-zA-Z����������������������\s\d#\.&()\/-]+$/;
        var soloLetrasNums=/^[a-zA-Z\u00F1\u00D1\u00E1\u00E9\u00ED\u00F3\u00FA\u00C1\u00C9\u00CD\u00D3\u00DA\u00E4\u00EB\u00EF\u00F6\u00FC\u00C4\u00CB\u00CF\u00D6\u00DC\s\d#\.&()\/-]+$/;
        var soloNumeros=/^[0-9]+$/;
            $.Error = false;
        $.Err = false;
        $.ErrText=" ";
            $.ErrorText = "Para poder continuar:<p>";
            $.Item = 0;
        var decimales=/^[0-9]+([\.][0-9]+)?$/;
            switch(FormName){
            case 'paymentForm0':
                $.Item = $("#"+FormName +" input:checked").val();
                if($.Item == undefined){
                    $.Error = true;
                                    $.ErrorText += "</br> Selecciona la tarjeta con la que vas a pagar";
                            } else {
                    $.SecNumber = $("#"+FormName +" :input[name=CCV_"+ $.Item +"]").val();
                                    if($.SecNumber==="") {
                        $.Error = true;
                                            $.ErrorText += "</br> Ingresa el n\u00FAmero de seguridad";
                                    } 
                                    
                    if(isNaN($.SecNumber)) {
                        $.Error = true;
                                            $.ErrorText += "</br> Ingresa un n\u00FAmero de seguridad v\u00E1lido";
                                    }
                                    // $("#"+FormName +" :input[name=Month_"+ $.Item +"]").val()
                                    // $("#"+FormName +" :input[name=Year_"+ $.Item +"]").val()
                    if($("#"+FormName +" :input[name=amm]").val() === ""){
                        $.Error = true; 
                        $.ErrorText += "</br> Ingresa el monto que deseas pagar";
                    }else if(decimales.test($("#"+FormName +" :input[name=amm]").val())==false){
                        $.Error = true;
                        $.ErrorText += "</br> El monto a pagar tiene formato incorrecto";
                    }
                    mandaCxense("realizarPago");
                            }
                            break;
                    case 'paymentForm':
                if($("#"+FormName +" :input[name=cCardNumber]").val() === ""){
                    $.Error = true; $.ErrorText += "<\/br> El n\u00FAmero de tarjeta es obligatorio";
                }
                
                //if($("#"+FormName +" :input[name=cCardNumber2]").val() === ""){
                    //$.Error = true; $.ErrorText += "<\/br> La confirmaci\u00F3n del n\u00FAmero de tarjeta es obligatorio";
                //}
                //Registro de tarjeta CR
                /*if($("#"+FormName +" :input[name=cCardNumber]").val() !== $("#"+FormName +" :input[name=cCardNumber2]").val() ){
                    $.Error = true; $.ErrorText += "<\/br>Los n\u00FAmeros de tarjeta no coinciden.";
                }*/
    
                if($("#"+FormName +" :input[name=cCardname]").val() === ""){
                    $.Error = true; $.ErrorText += "</br> Ingresa el nombre del tarjetahabiente";
                }else if(soloLetras.test($("#"+FormName +" :input[name=cCardname]").val())==false){
                    $.Error = true; $.ErrorText += "</br> El nombre del tarjetahabiente s\u00F3lo debe contener letras";
                }
                                    
                            if($("#"+FormName +" :input[name=cCardLastname]").val() === ""){
                    $.Error = true; $.ErrorText += "</br> Ingresa el apellido del tarjetahabiente";
                }else if(soloLetras.test($("#"+FormName +" :input[name=cCardLastname]").val())==false){
                    $.Error = true; $.ErrorText += "</br> El apellido del tarjetahabiente s\u00F3lo debe contener letras";
                }
                                    
                var tipoTarj=$("#"+FormName +" :input[name=Ctype]").val();
    
                            if($("#"+FormName +" :input:checked").val()==="Amex"){
                    if($("#"+FormName +" :input[name=cCardNumber]").val().length != 15 || soloNumeros.test($("#"+FormName +" :input[name=cCardNumber]").val())==false || !isValidCreditCard(tipoTarj,$("#"+FormName +" :input[name=cCardNumber]").val())){ 
                        $.Error = true; $.ErrorText += "</br> Ingresa un n\u00FAmero de tarjeta v\u00E1lido";
                    }
    
                                    if($("#"+FormName +" :input[name=cCardSec]").val().length != 4 && soloNumeros.test($("#"+FormName +" :input[name=cCardSec]").val())==false ){
                        $.Error = true; $.ErrorText += "</br> Ingresa un n\u00FAmero de seguridad v\u00E1lido";
                    }
                            } else {
                    
                    if($("#"+FormName +" :input[name=cCardNumber]").val().length != 16 || soloNumeros.test($("#"+FormName +" :input[name=cCardNumber]").val())==false || !isValidCreditCard(tipoTarj,$("#"+FormName +" :input[name=cCardNumber]").val())){
                        $.Error = true; $.ErrorText += "</br> Ingresa un n\u00FAmero de tarjeta v\u00E1lido";
                    }
    
                                    if($("#"+FormName +" :input[name=cCardSec]").val().length != 3 && soloNumeros.test($("#"+FormName +" :input[name=cCardSec]").val())==false ){
                        $.Error = true; $.ErrorText += "</br> Ingresa un n\u00FAmero de seguridad v\u00E1lido";
                    }
                            }
    
                            if($("#"+FormName +" :input[name=expMonth] :selected").val() === "" || $("#"+FormName +" :input[name=expMonth] :selected").val() === undefined){
                    $.Error = true; $.ErrorText += "</br> Selecciona el mes de vencimiento";
                }/*else {
                    if($("#"+FormName +" :input[name=expMonth2] :selected").val() === "" || $("#"+FormName +" :input[name=expMonth2] :selected").val() === undefined){
                        $.Error = true; $.ErrorText += "</br> Confirma el mes de vencimiento";
                    }else{
                        if($("#"+FormName +" :input[name=expMonth] :selected").val() !== $("#"+FormName +" :input[name=expMonth2] :selected").val()){
                            $.Error = true; $.ErrorText += "</br> La confirmaci\u00F3n del mes de vencimiento debe ser igual";
                        }
                    }
                }*/
                                    
                            if($("#"+FormName +" :input[name=expYear] :selected").val() === "" || $("#"+FormName +" :input[name=expYear] :selected").val() === undefined){
                    $.Error = true; $.ErrorText += "</br> Selecciona el a\u00F1o de vencimiento";
                }/*else {
                    if($("#"+FormName +" :input[name=expYear2] :selected").val() === "" || $("#"+FormName +" :input[name=expYear2] :selected").val() === undefined){
                        $.Error = true; $.ErrorText += "</br> Confirma el a\u00F1o de vencimiento";
                    }else{
                        if($("#"+FormName +" :input[name=expYear] :selected").val() !== $("#"+FormName +" :input[name=expYear2] :selected").val()){
                            $.Error = true; $.ErrorText += "</br> La confirmaci\u00F3n del a\u00F1o de vencimiento debe ser igual";
                        }
                    }
                }*/
            
                            /*if($("#"+FormName +" :input[name=cCardAdd]").val() === ""){
                    $.Error = true; $.ErrorText += "</br> Ingresa tu domicilio";
                }else{
                    if(soloLetrasNums.test($("#"+FormName +" :input[name=cCardAdd]").val())==false){
                        $.Error = true; $.ErrorText += "</br> El domicilio no debe contener caracteres especiales";
                    }
                }*/
    
                /*if($("#"+FormName +" :input[name=cCardCol]").val() === ""){
                    $.Error = true; $.ErrorText += "</br> Ingresa la colonia";
                }else{
                    if(soloLetrasNums.test($("#"+FormName +" :input[name=cCardCol]").val())==false){
                        $.Error = true; $.ErrorText += "</br> La colonia no debe contener caracteres especiales";
                    }
                }*/
    
                            /*if($("#"+FormName +" :input[name=cCardDel]").val() === ""){ 
                    $.Error = true; $.ErrorText += "</br> Ingresa la delegaci\u00F3n o municipio";
                }else{
                    if(soloLetrasNums.test($("#"+FormName +" :input[name=cCardDel]").val())==false){
                        $.Error = true; $.ErrorText += "</br> La delegaci\u00F3n o municipio no debe contener caracteres especiales";
                    }
                }*/                                
                                   
                            /*if($("#"+FormName +" :input[name=cCardCP]").val() === ""){
                    $.Error = true; $.ErrorText += "</br> Ingresa el C\u00F3digo Postal";
                }else if(isNaN($("#"+FormName +" :input[name=cCardCP]").val()) || $("#"+FormName +" :input[name=cCardCP]").val().length<5){
                    $.Error = true; $.ErrorText += "</br> Ingresa un C\u00F3digo Postal v\u00E1lido";
                }*/
                                    
                /*if($("#"+FormName +" :input[name=cCardEdo] :selected").val() === "" || $("#"+FormName +" :input[name=cCardEdo] :selected").val() === undefined){
                    $.Error = true; $.ErrorText += "</br> Selecciona el estado";
                }*/
                                    
                /*if($("#"+FormName +" :input[name=cCardCtr] :selected").val() === "" || $("#"+FormName +" :input[name=cCardCtr] :selected").val() === undefined){
                    $.Error = true; $.ErrorText += "</br> Selecciona el pa\u00EDs";
                }*/
                                    
                if($("#"+FormName +" :input[name=amm]").val() === ""){
                    $.Error = true; $.ErrorText += "<\/br> Ingresa el monto que deseas pagar";
                }else if(decimales.test($("#"+FormName +" :input[name=amm]").val())==false){
                    $.Error = true; $.ErrorText += "<\/br> El monto a pagar tiene formato incorrecto";
                }
                                    
                            if(!$.Error){
                    str = $("#"+FormName +" :input[name=cCardNumber]").val();
                    $_encripted = $.otpyrc(str);
                                    $("#"+FormName +" :input[name=cCardNumberEnc]").val($_encripted);
                    //Registro de tarjeta CR
                                    $("#"+FormName +" :input[name=cCardNumber]").val("");
                                    $("#"+FormName +" :input[name=cCardNumber2]").val("");    
                            }
                    mandaCxense("agregatarjeta");			
                break;
            case 'regTdcForm':
                if($("#"+FormName +" :input[name=cCardNumberhdn]").val() === ""){
                    $.Error = true; $.ErrorText += "<\/br> El n\u00FAmero de tarjeta es obligatorio";
                }
                
                if($("#"+FormName +" :input[name=cCardNumberhdn2]").val() === ""){
                    $.Error = true; $.ErrorText += "<\/br> La confirmaci\u00F3n del n\u00FAmero de tarjeta es obligatorio";
                }
                
                if($("#"+FormName +" :input[name=cCardNumber]").val() !== $("#"+FormName +" :input[name=cCardNumber2]").val() ){
                    $.Error = true; $.ErrorText += "<\/br>Los n\u00FAmeros de tarjeta no coinciden.";
                }
    
                if($("#"+FormName +" :input[name=cCardname]").val() === ""){
                    $.Error = true; $.ErrorText += "</br> Ingresa el nombre del tarjetahabiente";
                }else if(soloLetras.test($("#"+FormName +" :input[name=cCardname]").val())==false){ 
                    $.Error = true; $.ErrorText += "</br> El nombre del tarjetahabiente s\u00F3lo debe contener letras";
                }
    
                            if($("#"+FormName +" :input[name=cApellidoPat]").val() === ""){
                    $.Error = true; $.ErrorText += "</br> Ingresa el apellido paterno del tarjetahabiente";
                }else if(soloLetras.test($("#"+FormName +" :input[name=cApellidoPat]").val())==false){
                    $.Error = true; $.ErrorText += "</br> El apellido paterno del tarjetahabiente s\u00F3lo debe contener letras";
                }
    
                            if($("#"+FormName +" :input[name=cApellidoMat]").val() === ""){
                    $.Error = true; $.ErrorText += "<\/br> Ingresa el apellido materno del tarjetahabiente";
                }else if(soloLetras.test($("#"+FormName +" :input[name=cApellidoMat]").val())==false){
                    $.Error = true; $.ErrorText += "<\/br> El apellido materno del tarjetahabiente s\u00F3lo debe contener letras";
                }            
                                    
                if($("#"+FormName +" :input[name=telFijo]").val() === ""){
                    $.Error = true; $.ErrorText += "<\/br> Ingresa tu tel\u00E9fono fijo";
                }
    
                if($("#"+FormName +" :input[name=celular]").val() === ""){
                    $.Error = true; $.ErrorText += "<\/br> Ingresa tu tel\u00E9fono m\u00F3vil";
                }
    
                if($("#"+FormName +" :input[name=email]").val() === ""){
                    $.Error = true; $.ErrorText += "<\/br> Ingresa tu correo electr\u00F3nico";
                }                                       
    
                            if($("#"+FormName +" :input[name=expMonth] :selected").val() === "" || $("#"+FormName +" :input[name=expMonth] :selected").val() === undefined){
                    $.Error = true; $.ErrorText += "<\/br> Selecciona el mes de vencimiento";
                }
                            
                if($("#"+FormName +" :input[name=expYear] :selected").val() === "" || $("#"+FormName +" :input[name=expYear] :selected").val() === undefined){
                    $.Error = true; $.ErrorText += "<\/br> Selecciona el a\u00F1o de vencimiento";
                }
                                
                            if($("#"+FormName +" :input[name=expMonth2] :selected").val() === "" || $("#"+FormName +" :input[name=expMonth2] :selected").val() === undefined){
                    $.Error = true; $.ErrorText += "<\/br> Confirma el mes de vencimiento";
                }
                            
                if($("#"+FormName +" :input[name=expYear2] :selected").val() === "" || $("#"+FormName +" :input[name=expYear2] :selected").val() === undefined){
                    $.Error = true; $.ErrorText += "<\/br> Confirma el a\u00F1o de vencimiento";
                }
                                
                            if($("#"+FormName +" :input[name=expMonth] :selected").val() != $("#"+FormName +" :input[name=expMonth2] :selected").val()){
                    $.Error = true; $.ErrorText += "<\/br> Verifica que el mes de vencimiento sea igual a la confirmaci\u00F3n";
                }
                            
                if($("#"+FormName +" :input[name=expYear] :selected").val()!=$("#"+FormName +" :input[name=expYear2] :selected").val() ){
                    $.Error = true; $.ErrorText += "<\/br> Verifica que el a\u00F1o  de vencimiento sea igual a la confirmaci\u00F3n";
                }
                
                if($("#"+FormName +" :input[name=cCardSec]").val().length != 3 && soloNumeros.test($("#"+FormName +" :input[name=cCardSec]").val())==false ){
                    $.Error = true; $.ErrorText += "<\/br> Ingresa un n\u00FAmero de seguridad v\u00E1lido";
                }
                            
                if($("#"+FormName +" :input[name=cCardAdd]").val() === ""){ 
                    $.Error = true; $.ErrorText += "<\/br> Ingresa tu domicilio";
                }else{
                    if(soloLetrasNums.test($("#"+FormName +" :input[name=cCardAdd]").val())==false){
                        $.Error = true; $.ErrorText += "<\/br> El domicilio no debe contener caracteres especiales";
                    }
                }
                            
                    if($("#"+FormName +" :input[name=cCardCol]").val() === ""){
                    $.Error = true; $.ErrorText += "</br> Ingresa la ciudad";
                }else{
                    if(soloLetrasNums.test($("#"+FormName +" :input[name=cCardCol]").val())==false){
                        $.Error = true; $.ErrorText += "</br> La ciudad  no debe contener caracteres especiales";
                    }
                }
                            
                    if($("#"+FormName +" :input[name=cCardDel]").val() === ""){
                    $.Error = true; $.ErrorText += "</br> Ingresa la delegaci\u00F3n o municipio";
                }else{
                    if(soloLetrasNums.test($("#"+FormName +" :input[name=cCardDel]").val())==false){
                        $.Error = true; $.ErrorText += "</br> La delegaci\u00F3n o municipio no debe contener caracteres especiales";
                    }
                }
                                    
                            if($("#"+FormName +" :input[name=cCardCP]").val() === ""){ 
                    $.Error = true; $.ErrorText += "</br> Ingresa el C\u00F3digo Postal";
                }else if(isNaN($("#"+FormName +" :input[name=cCardCP]").val()) || $("#"+FormName +" :input[name=cCardCP]").val().length<5){ 
                    $.Error = true; $.ErrorText += "</br> Ingresa un C\u00F3digo Postal v\u00E1lido";
                }
                            
                    if($("#"+FormName +" :input[name=cCardEdo] :selected").val() === "" || $("#"+FormName +" :input[name=cCardEdo] :selected").val() === undefined){
                    $.Error = true; $.ErrorText += "</br> Selecciona el estado";
                }
                                    
                            if(!$.Error){
                    str = $("#"+FormName +" :input[name=cCardNumber]").val();
                                    $_encripted = $.otpyrc(str);
                                    $("#"+FormName +" :input[name=cCardNumberEnc]").val($_encripted);
                                    $("#"+FormName +" :input[name=cCardNumber]").val("");
                                    $("#"+FormName +" :input[name=cCardNumber2]").val("");                                        
                            }
                break;
            case 'recomendar':
                var validaTel=/^\d{7,10}$/;
                var validaLada=/^\d{2,3}$/;
                var validaEmail=/^(.+\@.+\..+)$/;
                if(($("#"+FormName +" :input[name=nombreRec]").val() === "")){
                    $.Err=true;$.ErrText+="El nombre del recomendado es obligatorio</br>";
                }
                
                if($("#"+FormName +" :input[name=apellRec]").val() === ""){
                    $.Err=true;$.ErrText+="Apellidos del recomendado obligatorios </br>";
                }
                                
                if($("#"+FormName +" :input[name=ladat]").val() === ""){
                    $.Err=true;$.ErrText+="Por favor ingrese la lada</br>";
                }else if(validaLada.test($("#"+FormName +" :input[name=ladat]").val()) ==false){
                    $.Err=true;$.ErrText+="Ingrese un n\u00FAmero de lada v\u00E1lido</br>";
                }
                
                if($("#"+FormName +" :input[name=telRec]").val() === ""){
                    $.Err=true;$.ErrText+="El tel\u00E9fono del recomendado es obligatorio</br>";
                }else if(validaTel.test($("#"+FormName +" :input[name=telRec]").val()) ==false){
                    $.Err=true;$.ErrText+="Ingrese un n\u00FAmero telef\u00F3nico v\u00E1lido</br>";
                }
                
                if($("#"+FormName +" :input[name=ladac]").val() !="" || $("#"+FormName +" :input[name=telRec2]").val()!=""){
                    if(validaLada.test($("#"+FormName +" :input[name=ladac]").val()) ==false){
                        $.Err=true;$.ErrText+="Ingrese un n\u00FAmero de lada celular v\u00E1lido</br>";
                    }
                    if(validaTel.test($("#"+FormName +" :input[name=telRec2]").val()) ==false){
                        $.Err=true;$.ErrText+="Ingrese un n\u00FAmero celular v\u00E1lido</br>";
                    }
                }
    
                if(($("#"+FormName +" :input[name=emailRec]").val() != "")){
                    if(validaEmail.test($("#"+FormName +" :input[name=emailRec]").val()) ==false){
                        $.Err=true;$.ErrText+="Ingrese una cuenta de correo v\u00E1lida</br>";
                    }
                }
    
                if($("#"+FormName +" :input[name=ciudadRec]").val() === "Selecciona una opcion" || $("#"+FormName +" :input[name=ciudadRec]").val() === ""){
                    $.Err=true;$.ErrText+="El campo ciudad es obligatorio</br>";
                }
                break;
            case 'recomendar2':
                var validaTel=/^\d{7,10}$/;
                var validaLada=/^\d{2,3}$/;
                var validaEmail=/^(.+\@.+\..+)$/;
    
                if(($("#"+FormName +" :input[name=nombreRec2]").val() === "")){
                    $.Err=true;$.ErrText+="El nombre del recomendado es obligatorio</br>";
                }
    
                if($("#"+FormName +" :input[name=apellRec2]").val() === ""){
                    $.Err=true;$.ErrText+="Apellidos del recomendado obligatorios </br>";
                }
    
                if($("#"+FormName +" :input[name=telRec]").val() === ""){
                    $.Err=true;$.ErrText+="El tel\u00E9fono del recomendado es obligatorio</br>";
                }else if(validaTel.test($("#"+FormName +" :input[name=telRec]").val()) ==false){
                    $.Err=true;$.ErrText+="Ingrese un n\u00FAmero telef\u00F3nico v\u00E1lido</br>";
                }
                
                if($("#"+FormName +" :input[name=ladat]").val() === ""){
                    $.Err=true;$.ErrText+="Por favor ingrese la lada local</br>";
                }else if(validaLada.test($("#"+FormName +" :input[name=ladat]").val()) ==false){
                    $.Err=true;$.ErrText+="Ingrese un n\u00FAmero de lada v\u00E1lido</br>";
                }
                
                if($("#"+FormName +" :input[name=ladac]").val() !="" || $("#"+FormName +" :input[name=telRec2]").val()!=""){
                    if(validaLada.test($("#"+FormName +" :input[name=ladac]").val()) ==false){
                        $.Err=true;$.ErrText+="Ingrese un n\u00FAmero de lada celular v\u00E1lido</br>";
                    }
                    if(validaTel.test($("#"+FormName +" :input[name=telRec2]").val()) ==false){
                        $.Err=true;$.ErrText+="Ingrese un n\u00FAmero celular v\u00E1lido</br>";
                    }
                }
                                
                if(($("#"+FormName +" :input[name=emailRec2]").val() != "")){
                    if(validaEmail.test($("#"+FormName +" :input[name=emailRec2]").val()) ==false){
                        $.Err=true;$.ErrText+="Ingrese una cuenta de correo v\u00E1lida</br>";
                    }
                }
                if($("#"+FormName +" :input[name=ciudadRec2]").val() === "Selecciona una opcion" || $("#"+FormName +" :input[name=ciudadRec]").val() === ""){
                    $.Err=true;$.ErrText+="El campo ciudad es obligatorio</br>";
                }
                break;
            default:
                return false;
            }
            
        $.ErrorText += "</p>";
        
        if($.Err){
            $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">Datos inv\u00E1lidos o vac\u00EDos<\/h5>');
            $('#msjBody').html("Por favor revise los datos que ingres\u00F3 y vuelva a intentar guardar su recomendaci\u00F3n.<\/br><\/br>"+$.ErrText);
            $('#msjFooter').html(" ");
            $("#myModalMsj").modal("show");
        }else{
            if(operacion=='actualizar'){
                function llegadaDatos2(datos){
                    if(datos==='success'){
                        $("#myModalMsj").modal("hide");
                        $.fancybox(
                            'Se actualiz\u00F3 correctamente la informaci\u00F3n del recomendado.',
                            $.Style2
                        );
                        llenaAcordeon("/listarecomser.do","#misRecom_1");
                    }else if(datos=='fail'){
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">Error al actualizar recomendado<\/h5>');
                        $('#msjBody').html("\n Lo sentimos, ocurri\u00F3 un problema al actualizar la informaci\u00F3n del recomendado; intente m\u00E1s tarde.");
                        $('#msjFooter').html(" ");
                        $("#myModalMsj").modal("show");
                    }else if(datos=='nocoberturasuccess'){
                        console.log("Ejecuta nocoberturasuccess");
                        llenaAcordeon("/listarecomser.do","#misRecom_1");
                        $("#myModalLoading").modal("hide");
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">Recomendado fuera de cobertura<\/h5>');
                        $('#msjBody').html("\n Lo sentimos, su recomendaci\u00F3n se encuentra fuera de cobertura por lo que no aplica para promoci\u00F3n; por favor revise \u00E1rea de cobertura.");
                        $('#msjFooter').html(" ");
                        $("#myModalMsj").modal("show");
                                       
                    }                        
                }
                
                function problemas2(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    console.log("Errores al actualizar recomendado");
                }
                
                $.ajax({
                    cache:false,
                    async:true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    url: getContextPath()+'/editarecomser.html',
                    data:{
                        operacion:operacion,
                        idRecom:extra,
                        nombreRec:$("#"+FormName +" :input[name=nombreRec2]").val(),
                        apellRec:$("#"+FormName +" :input[name=apellRec2]").val(),
                        telRec:$("#"+FormName +" :input[name=telRec]").val(),
                        telRec2:$("#"+FormName +" :input[name=telRec2]").val(),
                        ladac:$("#"+FormName +" :input[name=ladac]").val(),
                        ladat:$("#"+FormName +" :input[name=ladat]").val(),
                        emailRec:$("#"+FormName +" :input[name=emailRec2]").val(),
                        ciudadRec:$("#"+FormName +" :input[name=ciudadRec2]").val()
                    },
                    success:llegadaDatos2,
                    error: problemas2,
                    timeout:100000
                });
                          
                    
            }
            
            if(operacion!='actualizar' && FormName!='paymentForm' && FormName!='paymentForm0' && FormName!='regTdcForm'){
                function llegadaDatos(datos){
                    console.log(datos);
                    if(datos=='success'){
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">Datos guardados exitosamente<\/h5>');
                        $('#msjBody').html("\n En un periodo de 48 horas m\u00E1ximo un asesor contactar\u00E1 a tu amigo o familiar. Entra a la secci\u00F3n de \"Comparte Totalplay\" para conocer a detalle el status de tus recomendaciones.");
                        $('#msjFooter').html(" ");
                        $("#myModalMsj").modal("show");
                        llenaAcordeon("/listarecomser.do","#misRecom_1");
                    }else if(datos=='fail'){
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">Error al guardar datos<\/h5>');
                        $('#msjBody').html("\n Ocurri\u00F3 un error. Verifica los datos y sigue aumentando amigos a tu lista.");
                        $('#msjFooter').html(" ");                                
                        $("#myModalMsj").modal("show");
                    }else if(datos=='nocoberturasuccess'){
                        $("#myModalLoading").modal("hide");
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">Fuera de cobertura<\/h5>');
                        $('#msjBody').html("\n Lo sentimos, las direcci\u00F3n de tu amigo o familiar se encuentra fuera de cobertura; comp\u00E1rtelo con otra persona.");
                        $('#msjFooter').html(" ");                                
                        $("#myModalMsj").modal("show");
                    }
                                
                }
                
                function problemas(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    console.log("Errores al insertar recomendado");
                }
                
                $.ajax({
                    cache:false,
                    async:true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    url:getContextPath()+'/createrecomser.html',
                    data:{
                        nombreRec:$("#"+FormName +" :input[name=nombreRec]").val(),
                        apellRec:$("#"+FormName +" :input[name=apellRec]").val(),
                        telRec:$("#"+FormName +" :input[name=telRec]").val(),
                        telRec2:$("#"+FormName +" :input[name=telRec2]").val(),
                        ladac:$("#"+FormName +" :input[name=ladac]").val(),
                        ladat:$("#"+FormName +" :input[name=ladat]").val(),
                        emailRec:$("#"+FormName +" :input[name=emailRec]").val(),
                        ciudadRec:$("#"+FormName +" :input[name=ciudadRec]").val()
                    },
                    success:llegadaDatos,
                    error: problemas,
                    timeout:100000
                }); 
                            
            }
                        
        }
    
            if($.Error){
            $.fancybox(
                $.ErrorText,
                            $.Style4
                 );
                                    
            } else {
            if(FormName=='regTdcForm'){
                Cantidad = $("#"+FormName +" :input[name=amm]").val();
                if(!isNaN(Cantidad) && Cantidad>0){
                    var Cantidad=parseFloat(Cantidad).toFixed(2);
                    terminacion= ($("#"+FormName +" :input[name=cCardNumber2]").val()).substring(12,16);
                    $("#"+FormName +" :input[name=amm]").val(Cantidad);
                    Confirma="Para finalizar el registro de su tarjeta con terminaci\u00f3n "+terminacion+" como m\u00e9todo de pago recurrente, se realizar\u00e1 un cargo por $ " + Cantidad + " pesos ("+nn(Cantidad)+"), el cual ser\u00e1 abonado directamente a su cuenta Totalplay";
                    if(confirm(Confirma)){
                        $_Disable = true;
                        $("#myModalLoading").modal("show");
                        $("#GuardarTDC").attr('disabled','disabled');
                        $(document).triggerHandler( "focus" );
                        $(document).focus();
                        $.guardaTarjeta();
                    } else {
                        $("#"+FormName)[0].reset();
                    }
                }else{
                    $.fancybox(
                        "El monto a pagar es incorrecto",
                                            $.Style2
                    );
                }
            }
                    
            if(FormName!='recomendar' && FormName!='recomendar2' && FormName!='regTdcForm'){
                            Cantidad = $("#"+FormName +" :input[name=amm]").val();
                if(!isNaN(Cantidad) && Cantidad>0){
                    var Cantidad=parseFloat(Cantidad).toFixed(2);
                    $("#"+FormName +" :input[name=amm]").val(Cantidad);
                    Confirma="Se realizar\u00e1 un cargo a su tarjeta por la cantidad de $ " + Cantidad + " pesos ("+nn(Cantidad)+"). \n \u00BFDesea continuar?";
                    if(confirm(Confirma)){
                        $_Disable = true;
                        $("#myModalLoading").modal("show");
                        $("#PagoNuevaTDC").attr('disabled','disabled');
                        $(document).triggerHandler( "focus" );
                        $(document).focus();
                        $.procesaPago(FormName);
                    } else {
                        $("#"+FormName)[0].reset();
                    }	
                }else{
                    $.fancybox(
                                            "El monto a pagar es incorrecto",
                                            $.Style2
                    );
                }
            }
                                            
            }
    };
    
    function mandaCxense(evento){
        var nameEvento;
        if(evento == "realizarPago"){
            nameEvento = "pago";
        }else{
            nameEvento = "agregatarjeta";
        }
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent',nameEvento]);
    }
    
    $.procesaPago=function(FormName){
        var postData= $("#"+FormName).serialize();
         var urlSvlt='';
            switch(FormName){
                case 'paymentForm':
                    urlSvlt='/paynewcardser.html';
                    break;
                case 'paymentForm0':
                    urlSvlt='/payextcardser.html';                
                    break;
                default:
                    console.log('No se envi\u00F3 formulario');
                    break;
            }
            function llegadaDatos(datos){
	 	if(datos!=null){
                    var error=datos.error;
                    var client= datos.cliente;
                    var auth=datos.auth;
                    var email=datos.email;   
                    var msjSendEmail='Se ha enviado un correo electr&oacute;nico de confirmaci&oacute;n a tu cuenta registrada: <b>'+email+'<\/b>';
                    console.log(datos);
                     switch(error){
                        case '0': 
                            $('#msjTitle').html('<h5 style="color:#1F9BC7;" class="text-center"><b>CARGO EXITOSO</b><\/h5>');
                            $('#msjBody').html("\n Hola <b>"+client+"<\/b> <br /><br />&#161;Tu pago ha sido realizado con &eacute;xito&#33;<br /><br />Te recomendamos realizar el cargo del servicio a tu tarjeta de cr&eacute;dito de manera autom&aacute;tica, as&iacute; siempre contar&aacute;s con el descuento de Pronto Pago y nunca te perder&aacute;s de la mejor programaci&oacute;n.<br /><br />Muchas gracias. <br /><br /><br /> Autorizaci&oacute;n bancaria: <b>"+auth+"<\/b><br /><br\/>"+msjSendEmail);
                            $('#msjFooter').html(" ");
                            $("#myModalMsj").modal("show");
                        break;
                        case '1':
                            $('#msjTitle').html('<h5 style="color:#1F9BC7;" class="text-center"><b>Error</b><\/h5>');
                            $('#msjBody').html("\n Hola <b>"+client+"<\/b> <br /><br />&#161;Tu pago no ha sido autorizado&#33;<br /><br />Te recomendamos intentar el cargo con otra tarjeta de cr&eacute;dito o d&eacute;bito o realizar tu pago en cualquier instituci&oacute;n bancaria, Farmacias del Ahorro o Farmacias Guadalajara.<br /><br />Muchas gracias.");
                            $('#msjFooter').html(" ");
                            $("#myModalMsj").modal("show");
                        break;
                        case '2': 
                            $('#msjTitle').html('<h5 style="color:#1F9BC7;" class="text-center"><b>Error</b><\/h5>');
                            $('#msjBody').html("\n Hola <b>"+client+"<\/b> <br /><br />&#161;A ocurrido un problema al procesar tu pago&#33;<br /><br />Por favor comun&iacute;cate a la L&iacute;nea Totalplay al 1579 8000 o al 01 800 510 0510 del interior de la republica.<br /><br />Muchas gracias.");
                            $('#msjFooter').html(" ");
                            $("#myModalMsj").modal("show");
                        break;
                        case '4': 
                            $('#msjTitle').html('<h5 style="color:#1F9BC7;" class="text-center"><b>Exito</b><\/h5>');
                            $('#msjBody').html("\n Hola <b>"+client+"<\/b> <br /><br />&#161;Tu pago ha sido realizado con &eacute;xito&#33;<br /><br />Te recomendamos realizar el cargo del servicio a tu tarjeta de cr&eacute;dito de manera autom&aacute;tica, as&iacute; siempre contar&aacute;s con el descuento de Pronto Pago y nunca te perder&aacute;s de la mejor programaci&oacute;n.<br /><br />Muchas gracias. <br /><br /><br /> Autorizaci&oacute;n bancaria: <b>"+auth+"<\/b><br /><br\/>");
                            $('#msjFooter').html(" ");
                            $("#myModalMsj").modal("show");
                        break;
                        case '5': 
                            $('#msjTitle').html('<h5 style="color:#1F9BC7;" class="text-center"><b>Exito</b><\/h5>');
                            $('#msjBody').html("\n Hola <b>"+client+"<\/b> <br /><br />&#161;Tu pago ha sido realizado con &eacute;xito&#33;<br /><br />Te recomendamos realizar el cargo del servicio a tu tarjeta de cr&eacute;dito de manera autom&aacute;tica, as&iacute; siempre contar&aacute;s con el descuento de Pronto Pago y nunca te perder&aacute;s de la mejor programaci&oacute;n.<br /><br />Muchas gracias. <br /><br /><br />");
                            $('#msjFooter').html(" ");
                            $("#myModalMsj").modal("show");
                        break;                        
                        case '6': 
                            $('#msjTitle').html('<h5 style="color:#1F9BC7;" class="text-center"><b>Error</b><\/h5>');
                            $('#msjBody').html("\n Los datos de la tarjeta capturada son incorrectos por favor verif&iacute;quelos.\n");
                            $('#msjFooter').html(" ");
                            $("#myModalMsj").modal("show");
                        break;
                        case '7':
                        $('#msjTitleEsp').html('');
                        $('#msjBodyEsp').html("\n Hola <b>"+client+"<\/b> <br /><br />&#161;Tu pago ha sido realizado con &eacute;xito&#33;<br /><br />&#161;Tu tarjeta ha sido correctamente registrada para cargo recurrente&#33;<br /><br />Un ejecutivo se pondr&aacute; en contacto para terminar el registro y otorgarle el beneficio. <br\/><br\/> Muchas gracias. <br /><br /><br /> Autorizaci&oacute;n bancaria: <b>"+auth+"<\/b><br /><br\/>"+msjSendEmail);
                        $('#msjFooterEsp').html("<input type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\" onclick=\"$.descargaFile();\" value=\"Aceptar\" />");
                        $("#myModalMsjEsp").modal("show");
                        break;
                        case '8':
                            $('#msjTitle').html('');
                            $('#msjBody').html("\n Hola <b>"+client+"<\/b> <br /><br />&#161;Tu pago ha sido realizado con &eacute;xito&#33;<br /><br />&#161;Tu tarjeta no ha sido correctamente registrada para cargo recurrente&#33;<br /><br />Muchas gracias. <br /><br /><br /> Autorizaci&oacute;n bancaria: <b>"+auth+"<\/b><br /><br\/>"+msjSendEmail);
                            $('#msjFooter').html(" ");
                            $("#myModalMsj").modal("show");
                        break;
                        default: break;
                    }   
                }else{
                        $('#msjTitle').html('<h5 style="color:rgb(255,255,255);">Error<\/h5>');
                        $('#msjBody').html("\n Ha ocurrido un problema al procesar tu pago, por favor reintenta m&aacute;s tarde.\n");
                        $('#msjFooter').html(" ");
                        $("#myModalMsj").modal("show");
                }

                document.getElementById(FormName).reset();
                console.log("Formulario: "+FormName+" procesado.");
               
            }
            function problemas(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                console.log("Errores al procesar pago con tarjeta");
            }
            $.ajax({
               cache:false,
               async:true,
               type: "POST",
               dataType: "json",
               contentType: "application/x-www-form-urlencoded",
               url: getContextPath()+urlSvlt,
               data:postData,
               success:llegadaDatos,
               error: problemas,
               timeout:1000000
             }); 
    }

    $.validaTarjeta=function() {
              var binAux = document.getElementById('cCardNumber').value;
              var bin = binAux.substring(0, 6);
        
            $.ajax({
               cache:false,
               async:true,
               type: "POST",
               dataType: "json",
               contentType: "application/x-www-form-urlencoded",
               url: getContextPath()+'/binser.html',
               data: {
                    binParam : bin                       
               },
               success:llegadaDatos2,
               error: problemas2,
               timeout:500000
             });
         
            function llegadaDatos2(datos){
                $("#cCardBank").val(datos.nombreBanco);
                $("#Ctype0").val(datos.nombreMarca);
                $("#cTipoTarjeta").val(datos.tipoTarjeta);
                $("#cCargoRecurrent").val(datos.cargosAutomaticos);      
                $("#cCardBankId").val(datos.idBanco);
                $("#tipopago").html(datos.nombreMarca);
                $("#tipotarjeta").html(datos.tipoTarjeta);
                $("#acepta").html(datos.cargosAutomaticos);
                
                var marca = datos.nombreMarca;
                if(marca == "MASTERCARD"){
                    $('#visacard').css('display','none');
                    $('#americancard').css('display','none');
                }else if(marca == "VISA"){
                    $('#mastercard').css('display','none');
                    $('#americancard').css('display','none');
                }else{
                    $('#mastercard').css('display','none');
                    $('#visacard').css('display','none');
                }
            }
            function problemas2(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log("Error: "+thrownError);
                console.log("Errores al buscar bin");
            }     
        }
        
    function isValidCreditCard(type, ccnum) {
        var re = /^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/;
        var type2=type.toUpperCase();
        switch(type2){
            case "VISA":
                // Visa: length 16, prefix 4, dashes optional.
                re = /^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/;
                break;
            case "MASTERCARD":
                // Mastercard: length 16, prefix 51-55, dashes optional.
                re = /^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/;
                break;
            case "DISC":
                // Discover: length 16, prefix 6011, dashes optional.
                re = /^6011-?\d{4}-?\d{4}-?\d{4}$/;
                break;
            case "AMEX":
                // American Express: length 15, prefix 34 or 37.
                re = /^3[4,7]\d{13}$/;
                break;
            case "DINN":
                // Diners: length 14, prefix 30, 36, or 38.
                re = /^3[0,6,8]\d{12}$/;
                break;
        }
        
        if (!re.test(ccnum)) return false;
       // Remove all dashes for the checksum checks to eliminate negative numbers
       ccnum = ccnum.split("-").join("");
       // Checksum ("Mod 10")
       // Add even digits in even length strings or odd digits in odd length strings.
       var checksum = 0;
       for (var i=(2-(ccnum.length % 2)); i<=ccnum.length; i+=2) {
          checksum += parseInt(ccnum.charAt(i-1));
       }
       // Analyze odd digits in even length strings or even digits in odd length strings.
       for (var i=(ccnum.length % 2) + 1; i<ccnum.length; i+=2) {
          var digit = parseInt(ccnum.charAt(i-1)) * 2;
          if (digit < 10) { checksum += digit; } else { checksum += (digit-9); }
       }
       if ((checksum % 10) == 0){

            return true; 
       }
       else return false;
    }
    
    $.otpyrc = function(str){
        var strCo = "";
        try{
            var aeut = new AesUtil(ks,ic);
            strCo = aeut.encrypt(tlas,vi,esarhpssap,str);
        }catch(e){
            strCo = "";
        }
        return strCo;
    }
    
    $('#myModalMsj').on('hidden.bs.modal', function (e) {
        window.location.href = getContextPath()+'/jsp/EstadoCta.jsp';
    });

});//fin document ready

var o=new Array("diez", "once", "doce", "trece", "catorce", "quince", "diecis\u00E9is", "diecisiete", "dieciocho", "diecinueve", "veinte", "veintiuno", "veintid\u00F3s", "veintitr\u00E9s", "veinticuatro", "veinticinco", "veintis\u00E9is", "veintisiete", "veintiocho", "veintinueve");
var u=new Array("cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve");
var d=new Array("", "", "", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa");
var c=new Array("", "ciento", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos");

function nn(n){
    var n=parseFloat(n).toFixed(2);
    var p=n.toString().substring(n.toString().indexOf(".")+1);
    var m=n.toString().substring(0,n.toString().indexOf("."));
    var m=parseFloat(m).toString().split("").reverse(); 
    var t="";
 
    /*Se analiza cada 3 digitos*/
   for (var i=0; i<m.length; i+=3){
    var x=t;
    /*formamos un numero de 2 digitos*/
    var b=m[i+1]!=undefined?parseFloat(m[i+1].toString()+m[i].toString()):parseFloat(m[i].toString());
    /*analizamos el 3 digito*/
    t=m[i+2]!=undefined?(c[m[i+2]]+" "):"";
    t+=b<10?u[b]:(b<30?o[b-10]:(d[m[i+1]]+(m[i]=='0'?"":(" y "+u[m[i]]))));
    t=t=="ciento cero"?"cien":t;
    if (2<i&&i<6)
      t=t=="uno"?"mil ":(t.replace("uno","un")+" mil ");
    if (5<i&&i<9)
      t=t=="uno"?"un mill&oacute;n ":(t.replace("uno","un")+" millones ");
    t+=x;
    //t=i<3?t:(i<6?((t=="uno"?"mil ":(t+" mil "))+x):((t=="uno"?"un mill�n ":(t+" millones "))+x));
   }
 
   t+=" pesos con "+p+"/100 M.N.";
   /*correcciones*/
   t=t.replace("  "," ");
   t=t.replace(" cero","");
   
   return t;
}