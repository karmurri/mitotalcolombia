$(document).ready(function () {
/*******************************************CXENSE**********************************************************/
/*******************************************ACORDEON********************************************************/
var bandera;

function cachaClick(iden){
    bandera = iden;
}

/*inicio mis compras*/
$('#mis_compras').on('click',function(){
    $(this).trigger('comprasTotal');
});

$('#mis_compras').bind('comprasTotal',function(){
    if(bandera != "#mis_compras"){
        cachaClick("#mis_compras");
        //console.log("evento para acordeon compras");
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','mis_compras']);
    }
});
/*fin mis compras*/

/*inicio plan*/
$('#plan').on('click',function(){
    $(this).trigger('planTotal');
});

$('#plan').bind('planTotal',function(){
    if(bandera != "#plan"){
        cachaClick("#plan");
        //console.log("evento para acordeon plan");
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','planAc']);
    }
});
/*fin plan*/

/*incio facturas*/
$('#facturas').on('click',function(){
    $(this).trigger('facturaTotal');
});

$('#facturas').bind('facturaTotal',function(){
    if(bandera != "#facturas"){
        cachaClick("#facturas");
        //console.log("evento para acordeon facturas");
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','facturasAc']);
    }
});
/*fin facturas*/

/*incio pago*/
$('#pago').on('click',function(){
    $(this).trigger('pagoTotal');
});

$('#pago').bind('pagoTotal',function(){
    if(bandera != "#pago"){
        cachaClick("#pago");
        //console.log("evento para acordeon pago");
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','pagoAc']);
    }
});
/*fin pago*/

/*inicio portabilidad*/
$('#portabilidad').on('click',function(){
    $(this).trigger('portabilidadTotal');
});

$('#portabilidad').bind('portabilidadTotal',function(){
    if(bandera != "#portabilidad"){
        cachaClick("#portabilidad");
        //console.log("evento para acordeon portabilidad");
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','portabilidadAc']);
    }
});
/*fin portabilidad*/

/*inicio recomendados*/
$('#recomendadosTotalplay').on('click',function(){
    $(this).trigger('recomenTotal');
});

$('#recomendadosTotalplay').bind('recomenTotal',function(){
    if(bandera != "#recomendadosTotalplay"){
        cachaClick("#recomendadosTotalplay");
        //console.log("evento para acordeon recomendados");
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','recomendadosAc']);
    }
});
/*fin recomendados*/
/*******************************************ACORDEON********************************************************/

/*******************************************CLICKS********************************************************/
/*inicio Home crear nueva cuenta*/
$('#create').on('click',function(){
   $(this).trigger('creacuentaTotal'); 
});

$('#create').bind('creacuentaTotal',function(){
   var Cx = window.cX;
   Cx.callQueue.push(['sendEvent','creacuenta']);
});
/*fin Home crear nueva cuenta*/

/*incio acepta crear cuenta*/
$('#create_btn').on('click',function(){
   $(this).trigger('crearTotal'); 
});

$('#create_btn').bind('crearTotal',function(){
   var Cx = window.cX;
   Cx.callQueue.push(['sendEvent','crearcuenta']);
});
/*fin acepta crear cuenta*/

/*incio home olvide mi contrasenia*/
$('#remember').on('click',function(){
   $(this).trigger('olvidecontrasenia'); 
});

$('#remember').bind('olvidecontrasenia',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','olvidecontrasenia']);
});
/*fin home olvide mi contrase�a*/

/*inicio acepta recuperar contrasenia*/
$('#rememberBtn').on('click',function(){
    $(this).trigger('recuperacontrasenia');
});

$('#rememberBtn').bind('recuperacontrasenia',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','recuperarcontrasenia']);
});
/*fin acepta recuperar contrasenia*/

/*incio agrega nueva tarjeta de pago*/
$('#PagoNuevaTDC').on('click',function(){
    $(this).trigger('nuevatarjeta');
});

$('#PagoNuevaTDC').bind('nuevatarjeta',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','nuevatarjetaPago']);
});
/*fin agrega nueva tarjeta de pago*/

/*inicio referencia pagar ahora cuando tiene saldo pendiente, abajo del banner,lado derecho*/
$('#pagarAhora').on('click',function(){
    $(this).trigger('pagarahora');
});

$('#pagarAhora').bind('pagarahora',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','pagarsaldopendiente']);
});
/*fin referencia pagar ahora cuando tiene saldo pendiente, abajo del banner,lado derecho*/

/*inicio imagen actualiza tus datos*/
$('#refActualizaDatos').on('click',function(){
   $(this).trigger('actualizadatos'); 
});

$('#refActualizaDatos').bind('actualizadatos',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','actualizadatos']);
});
/*fin imagen actualiza tus datos*/
/*******************************************CLICKS********************************************************/
/*******************************************admonCta********************************************************/
$('#admonCta').on('click',function(){
    $(this).trigger('admonCuenta');
});

$('#admonCta').bind('admonCuenta',function(){
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','admonCuenta']);
});

$('#submit_btn').on('click',function(){
    $(this).trigger('actualizarCuenta');
});

$('#submit_btn').bind('actualizarCuenta',function(){
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','actualizarCuenta']);
});
/*******************************************RedWifi********************************************************/
$('#sincronizarRed').on('click',function(){
    $(this).trigger('sincronizarRed');
});

$('#sincronizarRed').bind('sincronizarRed',function(){
        var Cx = window.cX;
        Cx.callQueue.push(['sendEvent','sincronizarRed']);
});

/*******************************************FIN CLICKS********************************************************/


/*******************************************BANNERS********************************************************/
/*inicio click banner netflix*/
$('#bannerNetflix').on('click',function(){
   $(this).trigger('bannernetflix'); 
});

$('#bannerNetflix').bind('bannernetflix',function(){
    var Cx= window.cX;
    Cx.callQueue.push(['sendEvent','bannerNetflix']);
});
/*fin click banner netflix*/



/*inicio click banner recomendados*/
$('#imgRecomClick').on('click',function(){
   $(this).trigger('bannerrecomen'); 
});

$('#imgRecomClick').bind('bannerrecomen',function(){
    var Cx= window.cX;
    Cx.callQueue.push(['sendEvent','bannerRecomendados']);
});
/*fin click banner recomendados*/
/*******************************************FIN BANNERS********************************************************/

/*******************************************NUEVOS CLICKS 25/03/19 ********************************************/
//del nuevo redisenio EstadoCta.jsp bot�n pagar, redirige a landing para pagar
$('#pagar').on('click',function(){
    $(this).trigger('inicioPagar');
});

$('#pagar').bind('inicioPagar',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','inicioPagar']);
});

//del nuevo redisenio miplancontratado.jsp
$('#btnCrecePlan').on('click',function(){
    $(this).trigger('creceTuPlan');
});

$('#btnCrecePlan').bind('creceTuPlan',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','creceTuPlan']);
});
/*******************************************FIN NUEVOS CLICKS 25/03/19 ****************************************/

/*******************************************MENU LOGUEADO******************************************************/
//Estado de cuenta
$('#menuMiEdoCta').on('click',function(){
    $(this).trigger('edoCtaM');
});

$('#menuMiEdoCta').bind('edoCtaM',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','edoCtaM']);
});

//Mi plan contratado
$('#change').on('click',function(){
    $(this).trigger('miPlanM');
});

$('#change').bind('miPlanM',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','miPlanM']);
});

//Portabilidad
$('#menuPorta').on('click',function(){
   $(this).trigger('portabilidadM'); 
});

$('#menuPorta').bind('portabilidadM',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','portabilidadM']);
});

//Mis compras
$('#menuMisCompras').on('click',function(){
    $(this).trigger('misComprasM');
});

$('#menuMisCompras').bind('misComprasM',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','misComprasM']);
});

//Mi wifi
$('#menuMiWifi').on('click',function(){
    $(this).trigger('miWifiM');
});

$('#menuMiWifi').bind('miWifiM',function(){
    var Cx = window.cX;
    Cx.callQueue.push(['sendEvent','miWifiM']);
});
/*******************************************FIN MENU LOGUEADO************************************************/

/*******************************************CXENSE**********************************************************/
});