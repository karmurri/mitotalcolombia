
package com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="noCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Pagos" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="pagoAjuste" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Ajustes" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="pagoAjuste" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Facturas" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="montoFacturado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="pagoAjuste" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "noCuenta",
    "pagos",
    "ajustes",
    "facturas"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(required = true)
    protected String noCuenta;
    @XmlElement(name = "Pagos", required = true)
    protected List<ProcessResponse.Pagos> pagos;
    @XmlElement(name = "Ajustes", required = true)
    protected List<ProcessResponse.Ajustes> ajustes;
    @XmlElement(name = "Facturas", required = true)
    protected List<ProcessResponse.Facturas> facturas;

    /**
     * Gets the value of the noCuenta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoCuenta() {
        return noCuenta;
    }

    /**
     * Sets the value of the noCuenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoCuenta(String value) {
        this.noCuenta = value;
    }

    /**
     * Gets the value of the pagos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pagos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPagos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessResponse.Pagos }
     * 
     * 
     */
    public List<ProcessResponse.Pagos> getPagos() {
        if (pagos == null) {
            pagos = new ArrayList<ProcessResponse.Pagos>();
        }
        return this.pagos;
    }

    /**
     * Gets the value of the ajustes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ajustes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAjustes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessResponse.Ajustes }
     * 
     * 
     */
    public List<ProcessResponse.Ajustes> getAjustes() {
        if (ajustes == null) {
            ajustes = new ArrayList<ProcessResponse.Ajustes>();
        }
        return this.ajustes;
    }

    /**
     * Gets the value of the facturas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the facturas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFacturas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessResponse.Facturas }
     * 
     * 
     */
    public List<ProcessResponse.Facturas> getFacturas() {
        if (facturas == null) {
            facturas = new ArrayList<ProcessResponse.Facturas>();
        }
        return this.facturas;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="pagoAjuste" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "documento",
        "fechaAplicacion",
        "fechaOperacion",
        "referencia",
        "pagoAjuste",
        "saldo"
    })
    public static class Ajustes {

        @XmlElement(required = true)
        protected String documento;
        @XmlElement(required = true)
        protected String fechaAplicacion;
        @XmlElement(required = true)
        protected String fechaOperacion;
        @XmlElement(required = true)
        protected String referencia;
        @XmlElement(required = true)
        protected String pagoAjuste;
        @XmlElement(required = true)
        protected String saldo;
        @XmlAttribute
        protected Integer elem;

        /**
         * Gets the value of the documento property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDocumento() {
            return documento;
        }

        /**
         * Sets the value of the documento property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDocumento(String value) {
            this.documento = value;
        }

        /**
         * Gets the value of the fechaAplicacion property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaAplicacion() {
            return fechaAplicacion;
        }

        /**
         * Sets the value of the fechaAplicacion property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaAplicacion(String value) {
            this.fechaAplicacion = value;
        }

        /**
         * Gets the value of the fechaOperacion property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaOperacion() {
            return fechaOperacion;
        }

        /**
         * Sets the value of the fechaOperacion property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaOperacion(String value) {
            this.fechaOperacion = value;
        }

        /**
         * Gets the value of the referencia property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferencia() {
            return referencia;
        }

        /**
         * Sets the value of the referencia property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferencia(String value) {
            this.referencia = value;
        }

        /**
         * Gets the value of the pagoAjuste property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPagoAjuste() {
            return pagoAjuste;
        }

        /**
         * Sets the value of the pagoAjuste property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPagoAjuste(String value) {
            this.pagoAjuste = value;
        }

        /**
         * Gets the value of the saldo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSaldo() {
            return saldo;
        }

        /**
         * Sets the value of the saldo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSaldo(String value) {
            this.saldo = value;
        }

        /**
         * Gets the value of the elem property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setElem(Integer value) {
            this.elem = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="montoFacturado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="pagoAjuste" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "documento",
        "fechaAplicacion",
        "fechaOperacion",
        "referencia",
        "montoFacturado",
        "pagoAjuste",
        "saldo"
    })
    public static class Facturas {

        @XmlElement(required = true)
        protected String documento;
        @XmlElement(required = true)
        protected String fechaAplicacion;
        @XmlElement(required = true)
        protected String fechaOperacion;
        @XmlElement(required = true)
        protected String referencia;
        @XmlElement(required = true)
        protected String montoFacturado;
        @XmlElement(required = true)
        protected String pagoAjuste;
        @XmlElement(required = true)
        protected String saldo;
        @XmlAttribute
        protected Integer elem;

        /**
         * Gets the value of the documento property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDocumento() {
            return documento;
        }

        /**
         * Sets the value of the documento property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDocumento(String value) {
            this.documento = value;
        }

        /**
         * Gets the value of the fechaAplicacion property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaAplicacion() {
            return fechaAplicacion;
        }

        /**
         * Sets the value of the fechaAplicacion property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaAplicacion(String value) {
            this.fechaAplicacion = value;
        }

        /**
         * Gets the value of the fechaOperacion property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaOperacion() {
            return fechaOperacion;
        }

        /**
         * Sets the value of the fechaOperacion property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaOperacion(String value) {
            this.fechaOperacion = value;
        }

        /**
         * Gets the value of the referencia property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferencia() {
            return referencia;
        }

        /**
         * Sets the value of the referencia property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferencia(String value) {
            this.referencia = value;
        }

        /**
         * Gets the value of the montoFacturado property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMontoFacturado() {
            return montoFacturado;
        }

        /**
         * Sets the value of the montoFacturado property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMontoFacturado(String value) {
            this.montoFacturado = value;
        }

        /**
         * Gets the value of the pagoAjuste property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPagoAjuste() {
            return pagoAjuste;
        }

        /**
         * Sets the value of the pagoAjuste property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPagoAjuste(String value) {
            this.pagoAjuste = value;
        }

        /**
         * Gets the value of the saldo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSaldo() {
            return saldo;
        }

        /**
         * Sets the value of the saldo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSaldo(String value) {
            this.saldo = value;
        }

        /**
         * Gets the value of the elem property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setElem(Integer value) {
            this.elem = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="pagoAjuste" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "documento",
        "fechaAplicacion",
        "fechaOperacion",
        "referencia",
        "pagoAjuste",
        "saldo"
    })
    public static class Pagos {

        @XmlElement(required = true)
        protected String documento;
        @XmlElement(required = true)
        protected String fechaAplicacion;
        @XmlElement(required = true)
        protected String fechaOperacion;
        @XmlElement(required = true)
        protected String referencia;
        @XmlElement(required = true)
        protected String pagoAjuste;
        @XmlElement(required = true)
        protected String saldo;
        @XmlAttribute
        protected Integer elem;

        /**
         * Gets the value of the documento property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDocumento() {
            return documento;
        }

        /**
         * Sets the value of the documento property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDocumento(String value) {
            this.documento = value;
        }

        /**
         * Gets the value of the fechaAplicacion property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaAplicacion() {
            return fechaAplicacion;
        }

        /**
         * Sets the value of the fechaAplicacion property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaAplicacion(String value) {
            this.fechaAplicacion = value;
        }

        /**
         * Gets the value of the fechaOperacion property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaOperacion() {
            return fechaOperacion;
        }

        /**
         * Sets the value of the fechaOperacion property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaOperacion(String value) {
            this.fechaOperacion = value;
        }

        /**
         * Gets the value of the referencia property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferencia() {
            return referencia;
        }

        /**
         * Sets the value of the referencia property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferencia(String value) {
            this.referencia = value;
        }

        /**
         * Gets the value of the pagoAjuste property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPagoAjuste() {
            return pagoAjuste;
        }

        /**
         * Sets the value of the pagoAjuste property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPagoAjuste(String value) {
            this.pagoAjuste = value;
        }

        /**
         * Gets the value of the saldo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSaldo() {
            return saldo;
        }

        /**
         * Sets the value of the saldo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSaldo(String value) {
            this.saldo = value;
        }

        /**
         * Gets the value of the elem property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setElem(Integer value) {
            this.elem = value;
        }

    }

}
