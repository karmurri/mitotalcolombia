
package com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "acountNo"
})
@XmlRootElement(name = "process")
public class Process {

    @XmlElement(required = true)
    protected String acountNo;

    /**
     * Gets the value of the acountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcountNo() {
        return acountNo;
    }

    /**
     * Sets the value of the acountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcountNo(String value) {
        this.acountNo = value;
    }

}
