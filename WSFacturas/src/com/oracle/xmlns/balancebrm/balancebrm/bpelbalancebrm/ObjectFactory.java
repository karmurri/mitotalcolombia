
package com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Ajustes }
     * 
     */
    public com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Ajustes createAjustes() {
        return new com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Ajustes();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Ajustes.Pagos }
     * 
     */
    public com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Ajustes.Pagos createAjustesPagos() {
        return new com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Ajustes.Pagos();
    }

    /**
     * Create an instance of {@link ProcessResponse.Pagos }
     * 
     */
    public ProcessResponse.Pagos createProcessResponsePagos() {
        return new ProcessResponse.Pagos();
    }

    /**
     * Create an instance of {@link ProcessResponse.Ajustes }
     * 
     */
    public ProcessResponse.Ajustes createProcessResponseAjustes() {
        return new ProcessResponse.Ajustes();
    }

    /**
     * Create an instance of {@link ProcessResponse.Facturas }
     * 
     */
    public ProcessResponse.Facturas createProcessResponseFacturas() {
        return new ProcessResponse.Facturas();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Pagos }
     * 
     */
    public com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Pagos createPagos() {
        return new com.oracle.xmlns.balancebrm.balancebrm.bpelbalancebrm.Pagos();
    }

}
