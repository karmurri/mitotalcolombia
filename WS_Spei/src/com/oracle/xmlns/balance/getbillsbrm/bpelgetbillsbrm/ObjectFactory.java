
package com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Process }
     * 
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Factura }
     * 
     */
    public com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Factura createFactura() {
        return new com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Factura();
    }

    /**
     * Create an instance of {@link ProcessResponse.BillInfo }
     * 
     */
    public ProcessResponse.BillInfo createProcessResponseBillInfo() {
        return new ProcessResponse.BillInfo();
    }

    /**
     * Create an instance of {@link ProcessResponse.BillInfo.HistoricalBilling }
     * 
     */
    public ProcessResponse.BillInfo.HistoricalBilling createProcessResponseBillInfoHistoricalBilling() {
        return new ProcessResponse.BillInfo.HistoricalBilling();
    }

    /**
     * Create an instance of {@link ProcessResponse.Factura }
     * 
     */
    public ProcessResponse.Factura createProcessResponseFactura() {
        return new ProcessResponse.Factura();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Datos }
     * 
     */
    public com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Datos createDatos() {
        return new com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Datos();
    }

    /**
     * Create an instance of {@link Process.UserPassIp }
     * 
     */
    public Process.UserPassIp createProcessUserPassIp() {
        return new Process.UserPassIp();
    }

    /**
     * Create an instance of {@link ProcessResponse.Response }
     * 
     */
    public ProcessResponse.Response createProcessResponseResponse() {
        return new ProcessResponse.Response();
    }

    /**
     * Create an instance of {@link ProcessResponse.ReferenciasBancarias }
     * 
     */
    public ProcessResponse.ReferenciasBancarias createProcessResponseReferenciasBancarias() {
        return new ProcessResponse.ReferenciasBancarias();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Factura.Datos }
     * 
     */
    public com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Factura.Datos createFacturaDatos() {
        return new com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm.Factura.Datos();
    }

    /**
     * Create an instance of {@link ProcessResponse.BillInfo.LastBillBalance }
     * 
     */
    public ProcessResponse.BillInfo.LastBillBalance createProcessResponseBillInfoLastBillBalance() {
        return new ProcessResponse.BillInfo.LastBillBalance();
    }

    /**
     * Create an instance of {@link ProcessResponse.BillInfo.SummaryStatement }
     * 
     */
    public ProcessResponse.BillInfo.SummaryStatement createProcessResponseBillInfoSummaryStatement() {
        return new ProcessResponse.BillInfo.SummaryStatement();
    }

    /**
     * Create an instance of {@link ProcessResponse.BillInfo.LastInvoiceSummary }
     * 
     */
    public ProcessResponse.BillInfo.LastInvoiceSummary createProcessResponseBillInfoLastInvoiceSummary() {
        return new ProcessResponse.BillInfo.LastInvoiceSummary();
    }

    /**
     * Create an instance of {@link ProcessResponse.BillInfo.HistoricalBilling.Billis }
     * 
     */
    public ProcessResponse.BillInfo.HistoricalBilling.Billis createProcessResponseBillInfoHistoricalBillingBillis() {
        return new ProcessResponse.BillInfo.HistoricalBilling.Billis();
    }

    /**
     * Create an instance of {@link ProcessResponse.Factura.Datos }
     * 
     */
    public ProcessResponse.Factura.Datos createProcessResponseFacturaDatos() {
        return new ProcessResponse.Factura.Datos();
    }

}
