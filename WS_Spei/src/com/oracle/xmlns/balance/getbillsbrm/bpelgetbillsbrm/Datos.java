
package com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Dato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dato"
})
@XmlRootElement(name = "Datos")
public class Datos {

    @XmlElement(name = "Dato", required = true)
    protected String dato;

    /**
     * Gets the value of the dato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDato() {
        return dato;
    }

    /**
     * Sets the value of the dato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDato(String value) {
        this.dato = value;
    }

}
