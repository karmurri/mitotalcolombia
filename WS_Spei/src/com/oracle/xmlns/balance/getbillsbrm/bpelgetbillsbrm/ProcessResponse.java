
package com.oracle.xmlns.balance.getbillsbrm.bpelgetbillsbrm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Factura">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Datos" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="previousDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ocLastBill" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ocBill" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ajusteRedondeo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ajustes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Response">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="eMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LastBillBalance">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="endT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="totalPaydiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="endTDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="courtT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="SummaryStatement">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="previiousBill" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="payment" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="creditNote" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="LastInvoiceSummary">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="bonosesPromotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="periodConsum" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="totalBills" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="HistoricalBilling">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Billis" maxOccurs="3" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ReferenciasBancarias">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="BANCO_AZTECA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BANCOMER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="HSBC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BANAMEX" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BANORTE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SCOTIABANK" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SANTANDER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SPEI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "factura",
    "response",
    "accountNo",
    "eMail",
    "name",
    "billInfo",
    "referenciasBancarias"
})
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "Factura", required = true)
    protected ProcessResponse.Factura factura;
    @XmlElement(name = "Response", required = true)
    protected ProcessResponse.Response response;
    @XmlElement(required = true)
    protected String accountNo;
    @XmlElement(required = true)
    protected String eMail;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(name = "BillInfo", required = true)
    protected ProcessResponse.BillInfo billInfo;
    @XmlElement(name = "ReferenciasBancarias", required = true)
    protected ProcessResponse.ReferenciasBancarias referenciasBancarias;

    /**
     * Gets the value of the factura property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.Factura }
     *     
     */
    public ProcessResponse.Factura getFactura() {
        return factura;
    }

    /**
     * Sets the value of the factura property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Factura }
     *     
     */
    public void setFactura(ProcessResponse.Factura value) {
        this.factura = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.Response }
     *     
     */
    public ProcessResponse.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Response }
     *     
     */
    public void setResponse(ProcessResponse.Response value) {
        this.response = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNo(String value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the eMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMail() {
        return eMail;
    }

    /**
     * Sets the value of the eMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMail(String value) {
        this.eMail = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the billInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.BillInfo }
     *     
     */
    public ProcessResponse.BillInfo getBillInfo() {
        return billInfo;
    }

    /**
     * Sets the value of the billInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.BillInfo }
     *     
     */
    public void setBillInfo(ProcessResponse.BillInfo value) {
        this.billInfo = value;
    }

    /**
     * Gets the value of the referenciasBancarias property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessResponse.ReferenciasBancarias }
     *     
     */
    public ProcessResponse.ReferenciasBancarias getReferenciasBancarias() {
        return referenciasBancarias;
    }

    /**
     * Sets the value of the referenciasBancarias property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.ReferenciasBancarias }
     *     
     */
    public void setReferenciasBancarias(ProcessResponse.ReferenciasBancarias value) {
        this.referenciasBancarias = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LastBillBalance">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="endT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="totalPaydiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="endTDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="courtT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="SummaryStatement">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="previiousBill" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="payment" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="creditNote" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="LastInvoiceSummary">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="bonosesPromotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="periodConsum" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="totalBills" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="HistoricalBilling">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Billis" maxOccurs="3" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "lastBillBalance",
        "summaryStatement",
        "lastInvoiceSummary",
        "historicalBilling"
    })
    public static class BillInfo {

        @XmlElement(name = "LastBillBalance", required = true)
        protected ProcessResponse.BillInfo.LastBillBalance lastBillBalance;
        @XmlElement(name = "SummaryStatement", required = true)
        protected ProcessResponse.BillInfo.SummaryStatement summaryStatement;
        @XmlElement(name = "LastInvoiceSummary", required = true)
        protected ProcessResponse.BillInfo.LastInvoiceSummary lastInvoiceSummary;
        @XmlElement(name = "HistoricalBilling", required = true)
        protected ProcessResponse.BillInfo.HistoricalBilling historicalBilling;

        /**
         * Gets the value of the lastBillBalance property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.BillInfo.LastBillBalance }
         *     
         */
        public ProcessResponse.BillInfo.LastBillBalance getLastBillBalance() {
            return lastBillBalance;
        }

        /**
         * Sets the value of the lastBillBalance property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.BillInfo.LastBillBalance }
         *     
         */
        public void setLastBillBalance(ProcessResponse.BillInfo.LastBillBalance value) {
            this.lastBillBalance = value;
        }

        /**
         * Gets the value of the summaryStatement property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.BillInfo.SummaryStatement }
         *     
         */
        public ProcessResponse.BillInfo.SummaryStatement getSummaryStatement() {
            return summaryStatement;
        }

        /**
         * Sets the value of the summaryStatement property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.BillInfo.SummaryStatement }
         *     
         */
        public void setSummaryStatement(ProcessResponse.BillInfo.SummaryStatement value) {
            this.summaryStatement = value;
        }

        /**
         * Gets the value of the lastInvoiceSummary property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.BillInfo.LastInvoiceSummary }
         *     
         */
        public ProcessResponse.BillInfo.LastInvoiceSummary getLastInvoiceSummary() {
            return lastInvoiceSummary;
        }

        /**
         * Sets the value of the lastInvoiceSummary property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.BillInfo.LastInvoiceSummary }
         *     
         */
        public void setLastInvoiceSummary(ProcessResponse.BillInfo.LastInvoiceSummary value) {
            this.lastInvoiceSummary = value;
        }

        /**
         * Gets the value of the historicalBilling property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessResponse.BillInfo.HistoricalBilling }
         *     
         */
        public ProcessResponse.BillInfo.HistoricalBilling getHistoricalBilling() {
            return historicalBilling;
        }

        /**
         * Sets the value of the historicalBilling property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.BillInfo.HistoricalBilling }
         *     
         */
        public void setHistoricalBilling(ProcessResponse.BillInfo.HistoricalBilling value) {
            this.historicalBilling = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Billis" maxOccurs="3" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "billis"
        })
        public static class HistoricalBilling {

            @XmlElement(name = "Billis")
            protected List<ProcessResponse.BillInfo.HistoricalBilling.Billis> billis;

            /**
             * Gets the value of the billis property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the billis property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getBillis().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ProcessResponse.BillInfo.HistoricalBilling.Billis }
             * 
             * 
             */
            public List<ProcessResponse.BillInfo.HistoricalBilling.Billis> getBillis() {
                if (billis == null) {
                    billis = new ArrayList<ProcessResponse.BillInfo.HistoricalBilling.Billis>();
                }
                return this.billis;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "bilingDate",
                "amount"
            })
            public static class Billis {

                @XmlElement(required = true)
                protected String bilingDate;
                @XmlElement(required = true)
                protected String amount;

                /**
                 * Gets the value of the bilingDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBilingDate() {
                    return bilingDate;
                }

                /**
                 * Sets the value of the bilingDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBilingDate(String value) {
                    this.bilingDate = value;
                }

                /**
                 * Gets the value of the amount property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount() {
                    return amount;
                }

                /**
                 * Sets the value of the amount property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount(String value) {
                    this.amount = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="endT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="totalPaydiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="endTDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="courtT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "totalPay",
            "endT",
            "totalPaydiscount",
            "endTDiscount",
            "courtT"
        })
        public static class LastBillBalance {

            @XmlElement(required = true)
            protected BigDecimal totalPay;
            @XmlElement(required = true)
            protected String endT;
            @XmlElement(required = true)
            protected BigDecimal totalPaydiscount;
            @XmlElement(required = true)
            protected String endTDiscount;
            @XmlElement(required = true)
            protected String courtT;

            /**
             * Gets the value of the totalPay property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTotalPay() {
                return totalPay;
            }

            /**
             * Sets the value of the totalPay property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTotalPay(BigDecimal value) {
                this.totalPay = value;
            }

            /**
             * Gets the value of the endT property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEndT() {
                return endT;
            }

            /**
             * Sets the value of the endT property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEndT(String value) {
                this.endT = value;
            }

            /**
             * Gets the value of the totalPaydiscount property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTotalPaydiscount() {
                return totalPaydiscount;
            }

            /**
             * Sets the value of the totalPaydiscount property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTotalPaydiscount(BigDecimal value) {
                this.totalPaydiscount = value;
            }

            /**
             * Gets the value of the endTDiscount property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEndTDiscount() {
                return endTDiscount;
            }

            /**
             * Sets the value of the endTDiscount property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEndTDiscount(String value) {
                this.endTDiscount = value;
            }

            /**
             * Gets the value of the courtT property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCourtT() {
                return courtT;
            }

            /**
             * Sets the value of the courtT property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCourtT(String value) {
                this.courtT = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="bonosesPromotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="periodConsum" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="totalBills" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "period",
            "rent",
            "bonosesPromotion",
            "periodConsum",
            "subtotal",
            "totalBills"
        })
        public static class LastInvoiceSummary {

            @XmlElement(required = true)
            protected String period;
            @XmlElement(required = true)
            protected BigDecimal rent;
            @XmlElement(required = true)
            protected BigDecimal bonosesPromotion;
            @XmlElement(required = true)
            protected BigDecimal periodConsum;
            @XmlElement(required = true)
            protected BigDecimal subtotal;
            @XmlElement(required = true)
            protected BigDecimal totalBills;

            /**
             * Gets the value of the period property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPeriod() {
                return period;
            }

            /**
             * Sets the value of the period property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPeriod(String value) {
                this.period = value;
            }

            /**
             * Gets the value of the rent property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getRent() {
                return rent;
            }

            /**
             * Sets the value of the rent property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setRent(BigDecimal value) {
                this.rent = value;
            }

            /**
             * Gets the value of the bonosesPromotion property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getBonosesPromotion() {
                return bonosesPromotion;
            }

            /**
             * Sets the value of the bonosesPromotion property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setBonosesPromotion(BigDecimal value) {
                this.bonosesPromotion = value;
            }

            /**
             * Gets the value of the periodConsum property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPeriodConsum() {
                return periodConsum;
            }

            /**
             * Sets the value of the periodConsum property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPeriodConsum(BigDecimal value) {
                this.periodConsum = value;
            }

            /**
             * Gets the value of the subtotal property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getSubtotal() {
                return subtotal;
            }

            /**
             * Sets the value of the subtotal property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setSubtotal(BigDecimal value) {
                this.subtotal = value;
            }

            /**
             * Gets the value of the totalBills property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTotalBills() {
                return totalBills;
            }

            /**
             * Sets the value of the totalBills property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTotalBills(BigDecimal value) {
                this.totalBills = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="previiousBill" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="payment" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="creditNote" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "previiousBill",
            "payment",
            "creditNote",
            "currentBilling",
            "totalPay"
        })
        public static class SummaryStatement {

            @XmlElement(required = true)
            protected BigDecimal previiousBill;
            @XmlElement(required = true)
            protected BigDecimal payment;
            @XmlElement(required = true)
            protected BigDecimal creditNote;
            @XmlElement(required = true)
            protected BigDecimal currentBilling;
            @XmlElement(required = true)
            protected BigDecimal totalPay;

            /**
             * Gets the value of the previiousBill property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPreviiousBill() {
                return previiousBill;
            }

            /**
             * Sets the value of the previiousBill property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPreviiousBill(BigDecimal value) {
                this.previiousBill = value;
            }

            /**
             * Gets the value of the payment property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPayment() {
                return payment;
            }

            /**
             * Sets the value of the payment property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPayment(BigDecimal value) {
                this.payment = value;
            }

            /**
             * Gets the value of the creditNote property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getCreditNote() {
                return creditNote;
            }

            /**
             * Sets the value of the creditNote property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setCreditNote(BigDecimal value) {
                this.creditNote = value;
            }

            /**
             * Gets the value of the currentBilling property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getCurrentBilling() {
                return currentBilling;
            }

            /**
             * Sets the value of the currentBilling property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setCurrentBilling(BigDecimal value) {
                this.currentBilling = value;
            }

            /**
             * Gets the value of the totalPay property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTotalPay() {
                return totalPay;
            }

            /**
             * Sets the value of the totalPay property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTotalPay(BigDecimal value) {
                this.totalPay = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Datos" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="previousDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ocLastBill" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ocBill" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ajusteRedondeo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ajustes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "datos"
    })
    public static class Factura {

        @XmlElement(name = "Datos")
        protected List<ProcessResponse.Factura.Datos> datos;

        /**
         * Gets the value of the datos property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the datos property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDatos().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProcessResponse.Factura.Datos }
         * 
         * 
         */
        public List<ProcessResponse.Factura.Datos> getDatos() {
            if (datos == null) {
                datos = new ArrayList<ProcessResponse.Factura.Datos>();
            }
            return this.datos;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="previousDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ocLastBill" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ocBill" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ajusteRedondeo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ajustes" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "previousDiscount",
            "ocLastBill",
            "ocBill",
            "ajusteRedondeo",
            "ajustes"
        })
        public static class Datos {

            @XmlElement(required = true)
            protected String previousDiscount;
            @XmlElement(required = true)
            protected String ocLastBill;
            @XmlElement(required = true)
            protected String ocBill;
            @XmlElement(required = true)
            protected String ajusteRedondeo;
            @XmlElement(required = true)
            protected String ajustes;

            /**
             * Gets the value of the previousDiscount property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPreviousDiscount() {
                return previousDiscount;
            }

            /**
             * Sets the value of the previousDiscount property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPreviousDiscount(String value) {
                this.previousDiscount = value;
            }

            /**
             * Gets the value of the ocLastBill property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOcLastBill() {
                return ocLastBill;
            }

            /**
             * Sets the value of the ocLastBill property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOcLastBill(String value) {
                this.ocLastBill = value;
            }

            /**
             * Gets the value of the ocBill property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOcBill() {
                return ocBill;
            }

            /**
             * Sets the value of the ocBill property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOcBill(String value) {
                this.ocBill = value;
            }

            /**
             * Gets the value of the ajusteRedondeo property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAjusteRedondeo() {
                return ajusteRedondeo;
            }

            /**
             * Sets the value of the ajusteRedondeo property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAjusteRedondeo(String value) {
                this.ajusteRedondeo = value;
            }

            /**
             * Gets the value of the ajustes property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAjustes() {
                return ajustes;
            }

            /**
             * Sets the value of the ajustes property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAjustes(String value) {
                this.ajustes = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="BANCO_AZTECA" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BANCOMER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="HSBC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BANAMEX" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BANORTE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SCOTIABANK" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SANTANDER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SPEI" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bancoazteca",
        "bancomer",
        "hsbc",
        "banamex",
        "banorte",
        "scotiabank",
        "santander",
        "spei"
    })
    public static class ReferenciasBancarias {

        @XmlElement(name = "BANCO_AZTECA", required = true)
        protected String bancoazteca;
        @XmlElement(name = "BANCOMER", required = true)
        protected String bancomer;
        @XmlElement(name = "HSBC", required = true)
        protected String hsbc;
        @XmlElement(name = "BANAMEX", required = true)
        protected String banamex;
        @XmlElement(name = "BANORTE", required = true)
        protected String banorte;
        @XmlElement(name = "SCOTIABANK", required = true)
        protected String scotiabank;
        @XmlElement(name = "SANTANDER", required = true)
        protected String santander;
        @XmlElement(name = "SPEI", required = true)
        protected String spei;

        /**
         * Gets the value of the bancoazteca property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBANCOAZTECA() {
            return bancoazteca;
        }

        /**
         * Sets the value of the bancoazteca property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBANCOAZTECA(String value) {
            this.bancoazteca = value;
        }

        /**
         * Gets the value of the bancomer property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBANCOMER() {
            return bancomer;
        }

        /**
         * Sets the value of the bancomer property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBANCOMER(String value) {
            this.bancomer = value;
        }

        /**
         * Gets the value of the hsbc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHSBC() {
            return hsbc;
        }

        /**
         * Sets the value of the hsbc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHSBC(String value) {
            this.hsbc = value;
        }

        /**
         * Gets the value of the banamex property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBANAMEX() {
            return banamex;
        }

        /**
         * Sets the value of the banamex property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBANAMEX(String value) {
            this.banamex = value;
        }

        /**
         * Gets the value of the banorte property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBANORTE() {
            return banorte;
        }

        /**
         * Sets the value of the banorte property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBANORTE(String value) {
            this.banorte = value;
        }

        /**
         * Gets the value of the scotiabank property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSCOTIABANK() {
            return scotiabank;
        }

        /**
         * Sets the value of the scotiabank property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSCOTIABANK(String value) {
            this.scotiabank = value;
        }

        /**
         * Gets the value of the santander property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSANTANDER() {
            return santander;
        }

        /**
         * Sets the value of the santander property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSANTANDER(String value) {
            this.santander = value;
        }

        /**
         * Gets the value of the spei property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSPEI() {
            return spei;
        }

        /**
         * Sets the value of the spei property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSPEI(String value) {
            this.spei = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "description"
    })
    public static class Response {

        @XmlElement(required = true)
        protected String code;
        @XmlElement(required = true)
        protected String description;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }

}
