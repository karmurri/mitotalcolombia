<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="java.util.List"%>
<%@ page import="project1.DatosRecomendadoVO"%> 
<%@ page import=" project1.CiudadesVO"%>
 <%
        List<DatosRecomendadoVO> listaRecom= (List<DatosRecomendadoVO> )request.getAttribute("listaRecomendados");
        
        List<CiudadesVO> listaCiudades=(List<CiudadesVO>)request.getAttribute("listaCiudades");
    %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <title>listaRecom</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/shop/font-awesome.css"/>

   
  </head>
  <body>
  <br/>
    <div align="center" style="background-color:#fff !important;">
        <h2>&iexcl;Bienvenido!</h2>
            <p style="font-size:1.2em; color:#333 !important; line-height:1.6em;">
            	Comparte tu experiencia con Totalplay y recibe hasta el 100% de descuento*.<br>
                Si quieres ahorrar, s&oacute;lo cu&eacute;ntales a tus amigos, familiares y conocidos lo incre&iacute;ble que es tu vida con Totalplay. Recomi&eacute;ndales que contraten y los dos recibir&aacute;n grandes descuentos. <br>
                Acum&uacute;lalos y olv&iacute;date de pagar tu siguiente estado de cuenta.
            </p>
            
    <br/>
    
      <p align="left"> Consulta T�rminos y condiciones del plan recomendados <a data-toggle="modal" href="#myModalTerms">AQU�</a></p>
         
 
       	<div align="center" style="background-color:#fff !important;">    
   
           <form class="form-horizontal" id="recomendar" method="post" accept-charset="UTF-8">
            <table  class="table table-datatable table-bordered" >
              <thead>
                <tr>
                  <th style="background-color:#002841; color:#fff !important; padding:8px; text-align:left; font-weight:bold;">Crear un recomendado</th>
                </tr>
              </thead>
              <tfoot align="center">
                <tr>
                	<td>
                    	<a data-toggle="modal" onclick="$.SubmitForm('recomendar')" class="btn btn-danger" style="color:#fff !important;"><i class="fa fa-floppy-o"></i> GUARDAR RECOMENDADO</a>
                        <button type="reset" class="btn btn-danger"><i class="fa fa-ban"></i> CANCELAR</button>
                    </td>
              	</tr>
              </tfoot>
              <tbody>
                <tr class="even gradeA">
                    <td>
                    	<div class="form-group">
                            <label for="fullname" class="col-sm-3 control-label">Nombre(s) del referido</label>
                            <div class="col-sm-8">
                              <input type="text" name="nombreRec" id="nombreRec" class="form-control"/>
                            </div>
                      	</div>
                    </td>
                </tr>	
                <tr class="even gradeA">
                    <td>
                    	<div class="form-group">
                            <label for="fullname" class="col-sm-3 control-label">Apellidos del referido</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" name="apellRec" id="apellRec" parsley-trigger="change" parsley-required="true" parsley-minlength="4" parsley-validation-minlength="1" placeholder=""/>
                            </div>
                      	</div>
                    </td>
                </tr>	
                <tr class="even gradeA">
                    <td>
                    	<div class="form-group">
                            <label for="telRec" class="col-sm-3 control-label">Tel�fono local</label>
                             <div class="col-sm-2">
                              <input type="text" maxlength="3" class="form-control" id="ladat" name="ladat" placeholder="Lada"/>
                            </div>
                            <div class="col-md-5">
                              <input type="text" maxlength="8" class="form-control" id="telRec" name="telRec" placeholder="Tel�fono"/>
                            </div>
                      	</div>
                    </td>
                </tr>	
                 <tr class="even gradeA">
                    <td>
                    	<div class="form-group">
                            <label for="telRec" class="col-sm-3 control-label">Tel�fono celular</label>
                            <div class="col-sm-2">
                              <input type="text" maxlength="3" class="form-control" id="ladac" name="ladac" placeholder="Lada"/>
                            </div>
                            <div class="col-sm-5">
                              <input type="text" maxlength="8" class="form-control" id="telRec2" name="telRec2" placeholder="Tel�fono"/>
                            </div>
                      	</div>
                    </td>
                </tr>	
                <tr class="even gradeA">
                    <td>
                    	<div class="form-group">
                            <label for="emailRec"  name="emailRec" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="emailRec" name="emailRec" parsley-trigger="change" parsley-required="true" parsley-minlength="4" parsley-validation-minlength="1" placeholder="ejemplo@email.com.mx"/>
                            </div>
                      	</div>
                    </td>
                </tr>	
                <tr class="even gradeA">
                    <td>
                     <div class="form-group">
                        <label for="input07" class="col-sm-3 control-label">Ciudad donde reside</label>
                        <%if(listaCiudades!=null && !listaCiudades.isEmpty()){%>
                        <div class="col-sm-8" id="selectbox">
                          <select class="chosen-select form-control" name="ciudadRec" id="ciudadRec" parsley-trigger="change" parsley-required="true" parsley-error-container="#selectbox">
                             <option>Selecciona una opcion</option>
                            <% for(CiudadesVO ciudad: listaCiudades){%>
                                <option><%=ciudad.getDCiudad()%> </option>
                            <%}%>
                          </select>
                        <%}else{%>
                        <div class="form-group">
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="ciudadRec" name="ciudadRec" parsley-trigger="change" parsley-required="true" parsley-minlength="4" parsley-validation-minlength="1" placeholder=""/>
                            </div>
                      	</div>
                        <%}%>
                        </div>
                      </div> 
                    </td>
                </tr>	
              </tbody>
            </table>   
        </form>  
        </div>      

        
       	<div align="center" style="background-color:#fff !important;">
            <table  class="table table-datatable table-bordered" id="basicDataTable">
              <thead>
              <tr>
                  <th style=" text-align: center;background-color:#002841; color:#fff !important; padding:8px; text-align:left; font-weight:bold;" colspan="4">
                        Mis recomendados
                  </th>
              </tr>
                <tr>
                  <th style="background-color:#002841; color:#fff !important; padding:8px; text-align:left; font-weight:bold;">Nombre del recomendado</tH>
                  <th style="background-color:#002841; color:#fff !important; padding:8px; text-align:left; font-weight:bold;">Canal</th>
                  <th style="background-color:#002841; color:#fff !important; padding:8px; text-align:left; font-weight:bold;">Estatus</th>
                  <th style="background-color:#002841; color:#fff !important; padding:8px; text-align:center; font-weight:bold;">Acciones</th>
                </tr>
              </thead>
              <tbody>
              <%if (listaRecom!=null){%>
              
                <%for(DatosRecomendadoVO recom: listaRecom){ %>
                <tr class="even gradeA">
                    <td style="color:#666666 !important; border-bottom:#ccc 1px dashed !important;" ><%=recom.getNombres()+" "+recom.getApellidos()%></td>
                    <td style="color:#666666 !important; border-bottom:#ccc 1px dashed !important; border-left:#ccc 1px dashed !important;" >
                        <%=recom.getCanalCap()%>
                    </td>
                    <td style="color:#666666 !important; border-bottom:#ccc 1px dashed !important; border-left:#ccc 1px dashed !important;" >
                      <button type="button" class="btn btn-default btn-xs"><%=recom.getNombreStatusFront()%></button>
                    </td>
                  
                     <%if(recom.getNombreStatusFront().equalsIgnoreCase("NUEVO")){%>
            
                        <td style="color:#666666 !important; border-bottom:#ccc 1px dashed !important; border-left:#ccc 1px dashed !important;" >
                            <button class="btn btn-default btn-xs" onclick="$.editarRecom(<%=recom.getIdRecomendado()%>,'actualizar')"><i class="fa fa-pencil"></i> EDITAR</button>
                            <button class="btn btn-default btn-xs" onclick="$.editarRecom(<%=recom.getIdRecomendado()%>,'eliminar')"><i class="fa fa-trash-o"></i> ELIMINAR</button>
                        </td>
                        <%}else if(recom.getNombreStatusFront().equalsIgnoreCase("ACEPTADO")){%>
                        <td style="color:#666666 !important; border-bottom:#ccc 1px dashed !important; border-left:#ccc 1px dashed !important;" >
                            <button class="btn btn-default btn-xs" onclick="$.muestraPromo('<%=recom.getIdPromocion()%>')"><i class="fa fa-gift"></i> VER PROMOCI�N</button>
                        </td>
                        <%}else if(recom.getNombreStatusFront().equalsIgnoreCase("FUERA DE COBERTURA")){%>
                        <td style="color:#666666 !important; border-bottom:#ccc 1px dashed !important; border-left:#ccc 1px dashed !important;" >
                            <button class="btn btn-default btn-xs" onclick="$.editarRecom(<%=recom.getIdRecomendado()%>,'actualizar')"><i class="fa fa-pencil"></i> EDITAR</button>
                            <button class="btn btn-default btn-xs" onclick="$.editarRecom(<%=recom.getIdRecomendado()%>,'eliminar')"><i class="fa fa-trash-o"></i> ELIMINAR</button>
                        </td>
                        <%}else if(recom.getNombreStatusFront().equalsIgnoreCase("RECHAZADO")){%>
                        <td style="color:#666666 !important; border-bottom:#ccc 1px dashed !important; border-left:#ccc 1px dashed !important;" >
                            <button class="btn btn-default btn-xs" onclick="$.muestraMotivo('<%=recom.getDescripcion()%>')"><i class="fa fa-pencil"></i>VER MOTIVO</button>
                            <button class="btn btn-default btn-xs" onclick="$.editarRecom(<%=recom.getIdRecomendado()%>,'eliminar')"><i class="fa fa-trash-o"></i> ELIMINAR</button>
                        </td>
                        <%}else{%>
                        <td style="color:#666666 !important; border-bottom:#ccc 1px dashed !important; border-left:#ccc 1px dashed !important;" >
                        </td>
                    
                        </tr>	
               <%      }
                    }
                %>
              
              <%} else {%>
              <tr class="even gradeA">
              <td colspan="4" >No tienes ning�n recomendado</td>
              </tr>
              <%}%>
              </tbody>
            </table>     
        </div>   
       </div> 
          
                        
        <div class="modal fade" id="myModalTerms">
           <div class="modal-dialog">
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="msjTitle2">T�rminos y condiciones</h4>
               </div>
                <div class="modal-body" id="msjBody2">
                    <h6 style="font-size:0.9em !important; color:#696969 !important; line-height: 1.2em !important;">
                            TOTAL PLAY TELECOMUNICACIONES S.A. DE C.V. TODOS LOS DERECHOS RESERVADOS. NO SE HAR&Aacute; NING&Uacute;N TIPO DE COMERCIALIZACI&Oacute;N DE DATOS, LA INFORMACI&Oacute;N PROPORCIONADA Y AUTORIZADA PREVIAMENTE POR EL CLIENTE TOTALPLAY SER&Aacute; PARA USO EXCLUSIVO DE LA MARCA Y SE UTILIZAR&Aacute; PARA CONTACTAR Y OFRECER LOS DIFERENTES PLANES Y SERVICIOS DE LA MISMA. * PARA PODER RECIBIR EL BENEFICIO, ES NECESARIO QUE TU RECOMENDADO CONTRATE EL SERVICIO Y SEA INSTALADO. CADA AMIGO Y/O FAMILIAR OTORGA UN 10% DE DESCUENTO EN LA PR&Oacute;XIMA RENTA, ACUMULABLE POR MES CON LA OPCI&Oacute;N DE SUMAR HASTA EL 100%. SI NO EXISTE DISPONIBILIDAD DE SERVICIO POR: UBICACI&Oacute;N GEOGR&Aacute;FICA, SITUACI&Oacute;N ECON&Oacute;MICA O CUALQUIERA QUE IMPOSIBILITE LA INSTALACI&Oacute;N DEL SERVICIO, EL CLIENTE NO PODR&Aacute; SER ACREEDOR DEL DESCUENTO. T&Eacute;RMINOS, CONDICIONES Y M&Aacute;S INFORMACI&Oacute;N EN: <a href="http://www.totalplay.com.mx/Comparte_Totalplay" target="_blank">www.totalplay.com.mx/Comparte_Totalplay</a>
                    </h6>
               </div>
               <div class="modal-footer" id="msjFooter2">
               </div>
             </div>
           </div>
         </div>                
        
  </body>
</html>