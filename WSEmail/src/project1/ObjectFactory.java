
package project1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the project1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendEmailWSResponse_QNAME = new QName("http://project1/", "sendEmailWSResponse");
    private final static QName _SendEmailWS_QNAME = new QName("http://project1/", "sendEmailWS");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: project1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendEmailWS }
     * 
     */
    public SendEmailWS createSendEmailWS() {
        return new SendEmailWS();
    }

    /**
     * Create an instance of {@link SendEmailWSResponse }
     * 
     */
    public SendEmailWSResponse createSendEmailWSResponse() {
        return new SendEmailWSResponse();
    }

    /**
     * Create an instance of {@link ResponseVO }
     * 
     */
    public ResponseVO createResponseVO() {
        return new ResponseVO();
    }

    /**
     * Create an instance of {@link EmailVO }
     * 
     */
    public EmailVO createEmailVO() {
        return new EmailVO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailWSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://project1/", name = "sendEmailWSResponse")
    public JAXBElement<SendEmailWSResponse> createSendEmailWSResponse(SendEmailWSResponse value) {
        return new JAXBElement<SendEmailWSResponse>(_SendEmailWSResponse_QNAME, SendEmailWSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailWS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://project1/", name = "sendEmailWS")
    public JAXBElement<SendEmailWS> createSendEmailWS(SendEmailWS value) {
        return new JAXBElement<SendEmailWS>(_SendEmailWS_QNAME, SendEmailWS.class, null, value);
    }

}
